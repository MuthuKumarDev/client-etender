module.exports = {
  "stories": [
    '../src/**/__stories__/*.stories.js',
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/preset-create-react-app",
    '@storybook/addon-actions',
		'@storybook/addon-knobs',
  ]
}