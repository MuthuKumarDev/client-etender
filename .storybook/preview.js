// StoryBook Preview
import React from "react";
import { addParameters } from "@storybook/react";

import "../src/styles/index.scss";

const viewPorts = {
  xs: {
    name: "Small Mobile (XS)",
    styles: {
      width: "320px",
      height: "480px"
    },
    type: "mobile"
  },
  sm: {
    name: "Mobile (SM)",
    styles: {
      width: "576px",
      height: "768px"
    },
    type: "mobile"
  },
  md: {
    name: "Tablet (MD)",
    styles: {
      width: "768px",
      height: "1024px"
    },
    type: "mobile"
  },
  lg: {
    name: "Desktop (LG)",
    styles: {
      width: "992px",
      height: "1024px"
    },
    type: "desktop"
  },
  xl: {
    name: "Large Screen (XL)",
    styles: {
      width: "1200px",
      height: "1200px"
    },
    type: "desktop"
  }
};

addParameters({
  viewport: {
    viewports: viewPorts,
    defaultViewport: "lg"
  }
});
