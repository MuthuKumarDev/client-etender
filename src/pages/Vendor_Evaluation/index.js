// VendorEvaluation Page Component
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import isEqual from "react-fast-compare";
import { Button } from "../../components/Button";
import { Table, Rate } from "antd";
import { CheckBox } from "../../components/Checkbox";
import { Sidebar } from "../../components/Sidebar";
import { Grid } from "../../components/Grid";

// Main Component
export default class VendorEvaluation extends PureComponent {
  render() {
    const dataSource = [
      {
        key: "1",
        criteria: "Capacity",
        description: "Does the supplier have the bandwidth to deliver?"
      },

      {
        key: "2",
        criteria: "Competency",
        description:
          "Is the supplier diligent and can complete the task in a given period of time? "
      },
      {
        key: "3",
        criteria: "Consistency",
        description: "Is there a consistent output from the supplier?"
      },
      {
        key: "4",
        criteria: "Control of Process",
        description:
          " Does the supplier offer flexibility and have systematic control over his/her process?"
      },
      {
        key: "5",
        criteria: "Cash",
        description:
          " Is the supplier financially independent or is there a third party involvement?"
      },
      {
        key: "6",
        criteria: "Cost",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Rhoncus egestas enim"
      },
      {
        key: "7",
        criteria: "Culture",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Rhoncus egestas enim"
      },
      {
        key: "8",
        criteria: "Clean",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Rhoncus egestas enim"
      },
      {
        key: "9",
        criteria: "Communication Efficiency",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Rhoncus egestas enim"
      }
    ];

    const columns = [
      {
        title: "Criteria",
        dataIndex: "criteria",
        name: "description",
        key: "1",
        width: 350,
        render: (_text, record) => (
          <div className="crt-cnt">
            <div className="crt-cnt-tit">{_text}</div>
            <div className="crt-cnt-des">{record.description}</div>
          </div>
        )
      },
      {
        title: "Poor",
        dataIndex: "poor",
        key: "2",
        width: 120,
        render: (_text) => (
          <div>
            <CheckBox variant="checkstyle" />
          </div>
        )
      },
      {
        title: "Fair",
        dataIndex: "fair",
        key: "3",
        width: 120,
        render: (_text) => (
          <div>
            <CheckBox variant="checkstyle" />
          </div>
        )
      },
      {
        title: "Satisfied",
        dataIndex: "satisfied",
        key: "4",
        width: 120,
        render: (_text) => (
          <div>
            <CheckBox variant="checkstyle" />
          </div>
        )
      },
      {
        title: "Good",
        dataIndex: "good",
        key: "5",
        width: 120,
        render: (_text) => (
          <div>
            <CheckBox variant="checkstyle" />
          </div>
        )
      },
      {
        title: "Excellent",
        dataIndex: "excellent",
        key: "6",
        width: 120,
        render: (_text) => (
          <div>
            <CheckBox variant="checkstyle" />
          </div>
        )
      }
    ];

    return (
      <div className="ven-evl">
        <Grid>
          <Grid.Cell span={20}>
            <Sidebar />
          </Grid.Cell>
          <Grid.Cell span={80}>
            <div className="ven-evl-top">
              <div className="ven-evl-top-hdr">
                <div className="hdr-tit">
                  <div className="smr-hdr-nme">Vendor Evaluation</div>
                </div>
              </div>
              <div className="ven-evl-cnt">
                <div className="ven-evl-cnt-pge">
                  <div className="ven-evl-cnt-tit">
                    Carter 10C model for vendor evaluation
                  </div>
                  <div className="ven-evl-cnt-des1">
                    This model looks into the aspects of evaluating the supplier
                    before engaging them. We encourage you to give utmost
                    attendtion before giving a rating to any vendor. This will
                    be used across organization for any vendor related
                    information.
                    <br></br>
                  </div>
                  <div className="ven-evl-cnt-des2">
                    In order to make the best value procurement and undertake
                    effective supplier risk management, organizations across the
                    Globe require insightful supplier assessment and vendor
                    evaluation.
                  </div>
                </div>
              </div>
              <div className="ven-pge-brk"></div>
              <div className="ven-crt">
                <div className="ven-crt-hdr">
                  <div className="ven-crt-hdr-tit">
                    Criteria list for vendor evaluation
                  </div>
                  <div className="ven-crt-hdr-cht">
                    {/* Pie chat diagram */}
                    The Pie chat representation
                  </div>
                </div>
                <div className="ven-crt-tbl">
                  <Table
                    dataSource={dataSource}
                    columns={columns}
                    size="small"
                    pagination={false}
                  />
                </div>
                <div className="ven-crt-btn">
                  <Button variant="secondary" label="Add Factor" size="big" />
                </div>
              </div>
              <div className="add-fed">
                <div className="add-txt">Additional Feedback</div>
                <div className="fed-cnt">
                  <div className="fed-cnt-tit">
                    Give your additonal feedback on this vendor
                  </div>
                </div>
              </div>
              <div className="ven-btn">
                <div className="ven-btn-fst">
                  <Button
                    variant="secondary"
                    label="Request for more details"
                    size="big"
                  />
                </div>
                &emsp;
                <div className="ven-btn-snd">
                  <Button
                    variant="secondary"
                    label="Save and Go Back"
                    size="big"
                  />
                </div>
                &emsp;
                <div className="ven-btn-thd">
                  <Button variant="primary" label="Aprove" size="big" />
                </div>
              </div>
            </div>
          </Grid.Cell>
        </Grid>
      </div>
    );
  }
}

VendorEvaluation.propTypes = {};

VendorEvaluation.defaultProps = {};
