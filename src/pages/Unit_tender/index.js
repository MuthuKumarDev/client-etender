// Unit Tender Component
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal } from "../../components/Modal";
import { Button } from "../../components/Button";
import { Grid } from "../../components/Grid";
import { Input } from "../../components/Input";
import { Button as ButtonIcon } from "reactstrap";
import { IconComponent } from "../../components/Icons";
import Select from "react-select";
import API from "../../api/api";

// Main Component
class UnitTender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      items: [],
      value: "",
      category: false,
      tender: ""
    };
  }

  handleValueChange = (value) => {
    this.setState({ value });
  };

  handleClick = () => {
    this.setState({ open: true, category: false  });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleAddItem = () => {
    let { value, items } = this.state;
    let list = [...items];
    this.setState({ value: "" });
    if (this.state.value !== "") {
      this.setState({ open: false, category: true });
      this.setState({ items: list });
      list.push(value);
    }
  };

  customStyles = {
    control: (base) => ({
      ...base,
      height: 30,
      minHeight: 30
    })
  };
  
  componentDidMount() {
    API.fetchData(API.Query.GET_RESOURCE_PRIMARY_CATEGRY).then((data) => {
      console.log("primary category ", data);
    });
  };

  render() {
    let { isOpen, hide } = this.props;
    let { items } = this.state;
    console.log("open", items);
    return (
      <Modal size="big" isOpen={isOpen} hide={hide}>
        <Modal.Content>
          {isOpen && (
            <div className="wrp-mdl">
              <div className="unt-hdr">
                <div className="hdr-tit heading">Worklist</div>
                <div className="hdr-cls" onClick={hide}>
                  <IconComponent name="close" />
                </div>
              </div>
              <div className="unt-cnt">
                <Grid>
                  <Grid.Cell span={30}>
                    <div>
                      <div>
                        {items.map((item) => {
                          return <div className="cat-lst">{item}</div>;
                        })}
                      </div>
                      <div className="wrk-lst-btn">
                        <ButtonIcon
                          outline
                          color="secondary"
                          style={{ height: 30, marginTop: 15, fontSize: 12 }}
                          onClick={this.handleClick}
                        >
                          <div className="btn-add-icn">
                            <IconComponent
                              name="circleplus"
                              fill="#fff"
                              width="15"
                              height="15"
                            />
                          </div>
                          Add a category
                        </ButtonIcon>
                      </div>
                    </div>
                  </Grid.Cell>
                  <Grid.Cell span={70}>
                    {this.state.open === true && (
                      <div className="wrp-ind-cnt">
                        <div className="wrp-add-cat">
                          <div className="add-cat-hdr">
                            <div className="add-cat-tit heading">Add a Category</div>
                            <div
                              className="add-cat-cls"
                              onClick={this.handleClose}
                            >
                              <IconComponent name="close" />
                            </div>
                          </div>
                          <div className="add-cat-cnt">
                            <div className="cnt-int">
                              <Input
                                size="small"
                                value={this.state.value}
                                onChange={this.handleValueChange}
                              />
                            </div>
                            <div className="int-lbl">
                              e.g., ‘Aluminium Glazing’ for items like curtain
                              walls, sliding door & window, etc.,
                            </div>
                          </div>
                          <div className="wrp-add-ftr">
                            <div className="tst1">
                              <Button
                                variant="link"
                                size="big"
                                label="cancel"
                                onClick={this.handleClose}
                              />
                            </div>
                            <div className="tst1">
                              <Button
                                variant="primary"
                                size="big"
                                label="Create"
                                onClick={this.handleAddItem}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                    {this.state.category === true && (
                      <div className="srh-res">
                        <div className="res-slt">
                          <div className="res-hed">Resource Master</div>
                          <div className="slt">
                            <Select styles={this.customStyles} />
                          </div>
                        </div>
                        <div className="int-ftr">
                          <Input size="small" />
                        </div>
                      </div>
                    )}
                  </Grid.Cell>
                </Grid>
              </div>
              <div className="unt-ftr">
                <div className="frt-btn">
                  <Button size="medium" variant="primary" label="Next" />
                </div>
              </div>
            </div>
          )}
        </Modal.Content>
      </Modal>
    );
  }
}

UnitTender.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  canClose: PropTypes.bool,
  hide: PropTypes.func
};

UnitTender.defaultProps = {
  name: "Unit Tender",
  isOpen: false,
  canClose: false,
  hide: null
};

export default UnitTender;
