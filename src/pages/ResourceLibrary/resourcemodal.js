// Resource Modal Component
import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "../../components/Modal";
import { IconComponent } from "../../components/Icons";
import { Button } from "../../components/Button";
import { Input } from "../../components/Input";
import { Input as Textarea, Toast, ToastBody, ToastHeader } from "reactstrap";
import Select from "react-select";
import { CustomInput } from "reactstrap";
import { red } from "@material-ui/core/colors";
import { withStyles } from "@material-ui/core/styles";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import { Grid } from "../../components/Grid";
import { Checkbox } from "antd";

// Main Component
const ResourceModal = (props) => {
  const {
    isOpen,
    hide,
    resId,
    resList,
    resourceName,
    onChange,
    onAddCategory,
    levelId,
    level2Id,
    level3Id,
    catlist,
    level2list,
    level3list,
  } = props;
  const [value, setValue] = useState("");
  const [value1, setValue1] = useState("");
  const [showkey, setshowKey] = useState(true);
  const [showspec, setShowSpec] = useState(false);
  const [billquality, setBillQuality] = useState(false);
  const [textInput1, setTextinput] = useState("");
  const [textInput2, setTextinput2] = useState("");
  const [textInput3, setTextinput3] = useState("");
  const [textInput4, setTextinput4] = useState("");
  const [checked, setChecked] = useState([]);
  const [state, setState] = React.useState({
    checkedB: false,
    checkedA: false,
    checkedC: false,
    checkedD: false
  });

  const options = [
    { value: "all", label: "All" },
    { value: "test1", label: "Test1" },
    { value: "test2", label: "Test2" }
  ];

  function handleRender() {
    setAddInput(true);
  }

  const handleValueChange = (value) => {
    if (onChange) {
      onChange(value);
    }
  };

  const handleValueChange1 = (value) => {
    setValue1(value);
  };

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const onInputChange = (value) => {
    if (onChange) {
      onChange(value);
    }
  };

  function handlekeyInfo() {
    setshowKey(true);
    setShowSpec(false);
    setBillQuality(false);
  }

  function handlespecs() {
    setshowKey(false);
    setShowSpec(true);
    setBillQuality(false);
  }

  const handlebill = () => {
    setshowKey(false);
    setShowSpec(false);
    setBillQuality(true);
  };

  const handletextarea = (event) => {
    setTextinput(event.target.value);
  };

  const handletextarea2 = (event) => {
    setTextinput2(event.target.value);
  };

  const handletextarea3 = (event) => {
    setTextinput3(event.target.value);
  };

  const handletextarea4 = (event) => {
    setTextinput4(event.target.value);
  };

  const handleCheckOnchange = (value) => {
    setChecked(value);
  };

  const CheckboxGroup = Checkbox.Group;

  const findresult = _.find(resList, (opt) => opt.id === resId) || null;
  const level1result =_.find(catlist, (opt) => opt.id === levelId) || null;
  const level2result = _.find(level2list, (opt) => opt.id === level2Id) || null;
  const level3result = _.find(level3list, (opt) => opt.id === level3Id) || null;
  
  const IOSSwitch = withStyles((theme) => ({
    root: {
      width: 31.5,
      height: 16,
      padding: 0,
      margin: theme.spacing(1)
    },
    switchBase: {
      padding: 3,
      "&$checked": {
        transform: "translateX(16px)",
        color: theme.palette.common.white,
        "& + $track": {
          backgroundColor: "#95BF47",
          opacity: 1,
          border: "none"
        }
      },
      "&$focusVisible $thumb": {
        color: "#52d869",
        border: "6px solid #fff"
      }
    },
    thumb: {
      width: 14,
      height: 14
    },
    track: {
      borderRadius: 24 / 1,
      border: `1px solid ${theme.palette.grey[300]}`,
      backgroundColor: "#d3d3d3",
      opacity: 1,
      transition: theme.transitions.create(["background-color", "border"])
    },
    checked: {},
    focusVisible: {}
  }))(({ classes, ...props }) => {
    return (
      <Switch
        focusVisibleClassName={classes.focusVisible}
        disableRipple
        classes={{
          root: classes.root,
          switchBase: classes.switchBase,
          thumb: classes.thumb,
          track: classes.track,
          checked: classes.checked
        }}
        {...props}
      />
    );
  });

  return (
    <Modal size="big" isOpen={isOpen} hide={hide}>
      <Modal.Content>
        <div className="res-mdl">
          <div className="res-mdl-hdr">
            <div className="res-mdl-hdr-tit heading">Add Resource</div>
            <div className="res-mdl-hdr-cls" onClick={hide}>
              <IconComponent name="close" />
            </div>
          </div>
          <div className="res-cnt-top">
            <div className="res-cnt-tit">You are now adding resources to</div>
            <div className="res-cnt-but">
            <div className="res-cnt-but-flw">
              {findresult && findresult.name}&ensp;
            </div>
            {level1result !== null && (
            <div className="res-cnt-but-flw1">
                {'>'}&ensp;{level1result &&
                  level1result.name
                }
                &nbsp;
              </div>
              )}
              {level2result !== null && (
              <div className="res-cnt-but-flw1">
                {'>'}&ensp;{level2result &&
                  level2result.name
                }
                &nbsp;
              </div>
              )}
              {level3result !== null && (
              <div className="res-cnt-but-flw1">
                {'>'}&ensp;{level3result &&
                  level3result.name
                }
                &nbsp;
              </div>
              )}
              </div>
          </div>
          <div className="res-tab">
            <div className="res-tab-key" onClick={handlekeyInfo}>
              Key Information
            </div>
            <div className="res-tab-key" onClick={handlespecs}>
              Specification
            </div>
            <div className="res-tab-key" onClick={handlebill}>
              Bill of Quantity
            </div>
            <div className="res-tab-key">Images</div>
          </div>
          <div className="res-cnt">
            {showkey === true && (
              <div className="res-cnt-fld">
                <div className="res-cnt-tit1">*&ensp;Resource Name</div>
                <div className="res-cnt-inp1">
                  <Input
                    size="small"
                    placeholder="Enter Resource Name"
                    value={resourceName}
                    onChange={handleValueChange}
                  />
                </div>
                <div className="res-cnt-tit1">Other Key Words</div>
                <div className="res-cnt-inp1">
                  <Input
                    size="small"
                    placeholder="Text Value"
                    name="text"
                    value={value1}
                    onChange={handleValueChange1}
                  />
                </div>
              </div>
            )}
            {showspec === true && (
              <div className="res-spc">
                <div className="res-spc-cnt">
                  <div className="res-spc-tit">
                    Specification &emsp;
                    <IconComponent name="infoicon" />
                  </div>
                  <Grid>
                    <Grid.Cell span={60}>
                      <div className="res-spc-flw">
                        <div className="res-spc-flw-iti">
                          Profile&emsp;&emsp;
                          <div>
                            <IconComponent name="drawpencil" />
                          </div>
                        </div>
                        <div className="res-spc-flw-swt">
                          Keyspec&ensp;
                          <FormGroup>
                            <FormControlLabel
                              control={
                                <IOSSwitch
                                  checked={state.checkedA}
                                  onChange={handleChange}
                                  name="checkedA"
                                />
                              }
                              label=""
                            />
                          </FormGroup>
                        </div>
                      </div>
                      <div>
                        <Textarea
                          type="textarea"
                          value={textInput1}
                          onChange={handletextarea}
                        />
                      </div>
                      <div className="res-spc-flw">
                        <div className="res-spc-flw-iti">
                          System&emsp;&emsp;
                          <div>
                            <IconComponent name="drawpencil" />
                          </div>
                        </div>
                        <div className="res-spc-flw-swt">
                          Keyspec&ensp;
                          <FormGroup>
                            <FormControlLabel
                              control={
                                <IOSSwitch
                                  checked={state.checkedB}
                                  onChange={handleChange}
                                  name="checkedB"
                                />
                              }
                              label=""
                            />
                          </FormGroup>
                        </div>
                      </div>
                      <div>
                        <Textarea
                          type="textarea"
                          value={textInput2}
                          onChange={handletextarea2}
                        />
                      </div>
                      <div className="res-spc-flw">
                        <div className="res-spc-flw-iti">
                          Surface Finish&emsp;
                          <div>
                            <IconComponent name="drawpencil" />
                          </div>
                        </div>
                        <div className="res-spc-flw-swt">
                          Keyspec&ensp;
                          <FormGroup>
                            <FormControlLabel
                              control={
                                <IOSSwitch
                                  checked={state.checkedC}
                                  onChange={handleChange}
                                  name="checkedC"
                                />
                              }
                              label=""
                            />
                          </FormGroup>
                        </div>
                      </div>
                      <div>
                        <Textarea
                          type="textarea"
                          value={textInput3}
                          onChange={handletextarea3}
                        />
                      </div>
                      <div className="res-spc-flw">
                        <div className="res-spc-flw-iti">
                          Assesories&emsp;
                          <div>
                            <IconComponent name="drawpencil" />
                          </div>
                        </div>
                        <div className="res-spc-flw-swt">
                          Keyspec&ensp;
                          <FormGroup>
                            <FormControlLabel
                              control={
                                <IOSSwitch
                                  checked={state.checkedD}
                                  onChange={handleChange}
                                  name="checkedD"
                                />
                              }
                              label=""
                            />
                          </FormGroup>
                        </div>
                      </div>
                      <div>
                        <Textarea
                          type="textarea"
                          value={textInput4}
                          onChange={handletextarea4}
                        />
                      </div>
                    </Grid.Cell>
                    <Grid.Cell span={40}>
                      <div>
                        <Toast
                          style={{
                            width: 220,
                            marginTop: -15,
                            marginLeft: 130
                          }}
                        >
                          <ToastHeader
                            style={{
                              backgroundColor: "#E0F5F5"
                            }}
                          >
                            <IconComponent name="tips" />
                          </ToastHeader>
                          <ToastBody style={{ backgroundColor: "#E0F5F5" }}>
                            Only specification marked as ‘Key Spec’ will be
                            considered while comparing the bids for preparing
                            pre-bid Summary.
                          </ToastBody>
                        </Toast>
                      </div>
                    </Grid.Cell>
                  </Grid>
                </div>
              </div>
            )}
            {billquality === true && (
              <div className="chk-lst">
                <div className="res-spc-tit">
                  Bill of Quantity &emsp;
                  <IconComponent name="infoicon" />
                </div>
                <Grid>
                  <Grid.Cell span={70}>
                    <div className="chk-lst-flw">
                      <CheckboxGroup
                        onChange={handleCheckOnchange}
                        className="chk-lst-flw"
                      >
                        <Checkbox value="article">Article name</Checkbox>
                        <div className="chk-lst-row">
                          <Checkbox value="length">Length</Checkbox>
                          <Checkbox value="breadth">Breadth</Checkbox>
                          <Checkbox value="height">Height</Checkbox>
                        </div>
                        <Checkbox value="quantity">Quantity</Checkbox>
                        <Checkbox value="measure">Unit of Measurement</Checkbox>
                        <Checkbox value="area">Area</Checkbox>
                        <Checkbox value="brand">Brand</Checkbox>
                        <Checkbox value="location">Location of Use</Checkbox>
                        <Checkbox value="quantity1">Quantity</Checkbox>
                        <Checkbox value="reference">BOQ Reference</Checkbox>
                      </CheckboxGroup>
                    </div>
                    <div className="add-boq-btn">
                      <div>
                        <IconComponent name="circleplus" />
                      </div>
                      <div>
                        <Button label="Add another BoQ" variant="link" />
                      </div>
                    </div>
                  </Grid.Cell>
                  <Grid.Cell span={30}>
                    <div>
                      <Toast
                        style={{
                          width: 220,
                          marginTop: -15,
                          marginLeft: 80
                        }}
                      >
                        <ToastHeader
                          style={{
                            backgroundColor: "#E0F5F5"
                          }}
                        >
                          <IconComponent name="tips" />
                        </ToastHeader>
                        <ToastBody style={{ backgroundColor: "#E0F5F5" }}>
                          Select all BOQ details applicable or suitable for this
                          resource
                        </ToastBody>
                      </Toast>
                    </div>
                  </Grid.Cell>
                </Grid>
              </div>
            )}
          </div>
          <div className="sub-btn">
            <div className="sub-btn-lvl">
              <Button
                label="Create"
                variant="primary"
                onClick={onAddCategory}
              />
            </div>
          </div>
        </div>
      </Modal.Content>
    </Modal>
  );
};

ResourceModal.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  canClose: PropTypes.bool,
  hide: PropTypes.func,
  resourceName: PropTypes.string,
  subValue: PropTypes.string,
  onChange: PropTypes.func,
  onAddCategory: PropTypes.func
};

ResourceModal.defaultProps = {
  name: "Unit Tender",
  isOpen: false,
  canClose: false,
  hide: null,
  resourceName: "",
  subValue: "",
  onchange: null,
  onAddCategory: null
};

export default ResourceModal;
