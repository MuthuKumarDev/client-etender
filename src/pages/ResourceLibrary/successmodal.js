// Succes Modal Component
import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "../../components/Modal";
import { IconComponent } from "../../components/Icons";
import { Button } from "../../components/Button";
import ResourceLibrary from "./index";

// Main Component
const SuccessModal = (props) => {
  const { isOpen, hide, value } = props;

  return (
    <Modal size="small" isOpen={isOpen} hide={hide}>
      <Modal.Content>
        <div className="suc-mdl">
          <div className="suc-mdl-hdr">
            <div className="suc-tit heading">New Primary Category Added</div>
            <div className="suc-cls" onClick={hide}>
              <IconComponent name="close" />
            </div>
          </div>
          <div className="suc-cnt">
            <div className="suc-cnt-shw">
              <div className="suc-cnt-icn">
                <IconComponent name="modalsuc" />
              </div>
              <div className="suc-cnt-des">
                <strong>
                  '{value}'&nbsp;
                   has been added as a Primary Category.
                </strong>
                <br /> <br /> Upon approval from eTender Admin, it will be made
                available in the resource library within 48 hours. Learn more
                about the approval process here.
              </div>
            </div>
          </div>
        </div>
      </Modal.Content>
    </Modal>
  );
};

SuccessModal.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  canClose: PropTypes.bool,
  hide: PropTypes.func
};

SuccessModal.defaultProps = {
  name: "Unit Tender",
  isOpen: false,
  canClose: false,
  hide: null
};

export default SuccessModal;
