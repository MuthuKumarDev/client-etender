import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Grid } from "../../components/Grid";
import { Input } from "../../components/Input";
import { withRouter } from "react-router";
import { Button as ButtonComp } from "reactstrap";
//import { Button as Button } from "../../components/Button";
import IconComponent from "../../components/Icons/icon";
import PrimaryModal from "./primarymodal";
import SuccessModal from "./successmodal";
import SubCategoryModal from "./subcategorymodal";
import ResourceModal from "./resourcemodal";
import _ from "lodash";
import { isEqual } from "lodash";

import API from "../../api/api";

import {
  Button,
  Toast,
  ToastBody,
  ToastHeader,
  ListGroup,
  ListGroupItem,
  Popover,
  PopoverBody
} from "reactstrap";
import './style.scss';

// Main Component
class ResourceLibrary extends PureComponent {
  constructor(props) {
    super(props);
    this.firstInput = null;
    this.state = {
      show: false,
      isOpen: false,
      ishide: false,
      catValue: "",
      resourceName: "",
      resourceNameLevel1: "",
      resourceNameLevel2: "",
      resourceNameLevel3: "",
      findValue: "",
      primaryid: 0,
      level1Id: 0,
      level2Id: 0,
      level3Id: 0,
      level4Id: 0,
      primarymodal: false,
      successmodal: false,
      resoucemodal: false,
      resoucemodalLevel1: false,
      resoucemodalLevel2: false,
      resoucemodalLevel3: false,
      submodal: false,
      submodalLevel1: false,
      submodalLevel2: false,
      submodalLevel3: false,
      showopop: false,
      level1: false,
      level2: false,
      level3: false,
      level4: false,
      findResults: [],
      Categorylist1: [],
      categorylist2: [],
      categorylist3: [],
      categorylist4: [],
      addinputs: [],
      fields: [{ label: null }, { label: null }, { label: null }],
      listitems: [],
      getprimarysearch: 0,
      getsecondarysearch: 0,
      getresourcesearch: 0
    };
  }

  // addsublist() {
  //   API.fetchData(API.Query.CREATE_RESOURCE_CATEGORY, {
  //     data: {
  //       secondary_category: "SubDataCheck",
  //       resource_category_id: "6",
  //       type: "secondarycategory"
  //     }
  //   }).then((data) => {
  //   });
  // }

  //   getfindlist() {
  //     API.fetchData(API.Query.GET_RESOURCES_CATEGORY, {
  //       primarycategory:"%cement%"
  //   }).then((data) => {
  //     console.log("Resource Find List", data)
  //     console.log(data.resource_category[0].primary_category);
  //     console.log(data.resource_category[0].secondarycategory.length);
  //     console.log(data.resource_category[0].secondarycategory[0].secondary_category);
  //   })
  //   .catch((err) => {
  //     console.log("Resource ERR", err.message)
  //   })
  // }

  componentDidMount() {
    this.getlist();
  }

  getlist() {
    API.fetchData(API.Query.GET_CATEGORY_AND_SUBCATEGORY).then((data) => {
      // console.log("Resource test", data.data);

      this.setState({
        listitems: data.data
      });
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState, this.state)) {
      this.getlist();
    }
  }

  toggle = () => {
    const { show } = this.state;
    this.setState({ show: !show });
  };

  modalhide = () => {
    this.setState({
      isOpen: false
    });
  };

  modalsucceshide = () => {
    this.setState({
      isOpen: false,
      catValue: "",
      primarymodal: false
    });
  };

  handlePrimaryCategory = () => {
    this.setState({
      isOpen: true,
      primarymodal: true,
    });
  };

  onInputChange = (i, event) => {
    const values = [...this.state.fields];
    values[i].label = event.target.value;
    this.setState({
      fields: values
    });
  };

  handleAddvalue = () => {
    const values = [...this.state.fields];
    values.push({ label: null });
    this.setState({
      fields: values
    });
  };

  handlePrimaryValue = (value) => {
    this.setState({ catValue: value });
  };

  handleResourceValue = (value) => {
    this.setState({
      resourceName: value
    });
  };

  handleResourceValueLevel1 = (value) => {
    this.setState({
      resourceNameLevel1: value,
      resourceNameLevel2: value,
      resourceNameLevel3: value,
    });
  };

  onAddSubCategorylist1 = (id) => {
    this.state.fields.forEach((field) => {
      if (
        field.label !== null &&
        field.label !== "" &&
        field.label !== " " &&
        field.label !== "  " &&
        field.label !== "   "
      ) {
        this.state.addinputs.push({
          name: field.label,
          resource_category_id: this.state.primaryid,
          type: "secondary_category",
          primary_category_id: this.state.primaryid
        });
      }
    });
    // console.log("fielstest33", this.state.addinputs);
    API.fetchData(API.Query.CREATE_RESOURCE_SUBCATEGORY, {
      data: this.state.addinputs
    }).then((res) => {
      this.getSecondarylistItem();
      this.setState({
        submodal: false,
        fields: [{ label: null }, { label: null }, { label: null }],
        addinputs: []
      });
    });

    // console.log("idsub", id);
    this.modalhide();
    this.setState({
      level1: true
    });
  };

  onAddSubCategorylist2 = () => {
    console.log(this.state.level1Id);
    this.state.fields.forEach((field) => {
      if (
        field.label !== null &&
        field.label !== "" &&
        field.label !== " " &&
        field.label !== "  " &&
        field.label !== "   "
      ) {
        this.state.addinputs.push({
          name: field.label,
          resource_category_id: this.state.level1Id,
          type: "secondary_category",
          primary_category_id: this.state.primaryid,
          secondary_category_id: this.state.level1Id
        });
      }
    });
    API.fetchData(API.Query.CREATE_RESOURCE_SUBCATEGORY, {
      data: this.state.addinputs
    }).then((res) => {
      this.getResourceListItem();
    });
    this.modalhide();
    this.setState({
      submodalLevel1: true,
      fields: [{ label: null }, { label: null }, { label: null }],
      addinputs: []
    });
  };

  onAddSubCategorylist3 = () => {
    console.log(this.state.level2Id);
    this.state.fields.forEach((field) => {
      if (
        field.label !== null &&
        field.label !== "" &&
        field.label !== " " &&
        field.label !== "  " &&
        field.label !== "   "
      ) {
        this.state.addinputs.push({
          name: field.label,
          resource_category_id: this.state.level2Id,
          type: "secondary_category",
          primary_category_id: this.state.primaryid,
          secondary_category_id: this.state.level2Id
        });
      }
    });
    API.fetchData(API.Query.CREATE_RESOURCE_SUBCATEGORY, {
      data: this.state.addinputs
    }).then((res) => {
      console.log("level3listitme", res);
      this.getLevel3Item();
    });
    this.modalhide();
    this.setState({
      fields: [{ label: null }, { label: null }, { label: null }],
      addinputs: []
    });
  };

  onAddSubCategorylist4 = () => {
    console.log(this.state.level3Id);
    this.state.fields.forEach((field) => {
      if (
        field.label !== null &&
        field.label !== "" &&
        field.label !== " " &&
        field.label !== "  " &&
        field.label !== "   "
      ) {
        this.state.addinputs.push({
          name: field.label,
          resource_category_id: this.state.level3Id,
          type: "secondary_category",
          primary_category_id: this.state.primaryid,
          secondary_category_id: this.state.level3Id
        });
      }
    });
    API.fetchData(API.Query.CREATE_RESOURCE_SUBCATEGORY, {
      data: this.state.addinputs
    }).then((res) => {
      console.log("level4subcate", res);
      this.getLevel4Item();
    });
    this.modalhide();
    this.setState({
      fields: [{ label: null }, { label: null }, { label: null }],
      addinputs: []
    });
  }

  onAddCategory = () => {
    if (
      this.state.catValue !== "" &&
      this.state.catValue !== " " &&
      this.state.catValue !== "  " &&
      this.state.catValue !== "   " &&
      this.state.catValue !== "    "
    ) {
      API.fetchData(API.Query.CREATE_RESOURCE_CATEGORY, {
        data: {
          name: this.state.catValue,
          type: "primary_category"
        }
      }).then((data) => {
        this.modalhide();
        this.handlePrimaryCategory();
        this.setState({
          successmodal: true,
          primarymodal: false
        });
      });
    }
  };

  handleClick = () => {
    this.setState({
      submodal: false,
      resoucemodal: false,
      submodalLevel1: false,
      submodalLevel2: false,
      submodalLevel3: false,
      resoucemodalLevel1: false,
      resoucemodalLevel2: false,
      resoucemodalLevel3: false,
      primarymodal: false,
      successmodal: false
    });
  };

  handleresetClick = () => {
    this.setState({
      primarymodal: false
    })
    console.log("value", this.state.primarymodal)
  }

  handleCreateSubCatLev1 = () => {
    this.setState({
      submodal: true,
      resoucemodal: false,
      isOpen: true,
      successmodal: false
    });
  };

  handleCreateSubCatLevel2 = () => {
    console.log("Success");
    this.setState({
      submodalLevel1: true,
      // submodal: true,
      isOpen: true
    });
  };

  handleCreateSubCatLevel3 = () => {
    this.setState({
      submodalLevel2: true,
      isOpen: true,
    });
  };

  handleCreateSubCatLevel4 = () => {
    console.log("sub sub sub");
    this.setState({
      submodalLevel3: true,
      isOpen: true,
    })
  }

  handlefindValue = (event) => {
    this.setState({
      findValue: event.target.value,
      showopop:!!event.target.value.trim()
    });
  };

  handlePopover = () => {
    API.fetchData(API.Query.SEARCH_RESOURCE_CATEGORY, {
      category: "%" + this.state.findValue + "%"
    })
      .then((data) => {
        this.setState({ findResults: data.resource_category });
        console.log("checkarray", this.state.findResults);
      })
      .catch((err) => {
        console.log("Resource ERR", err.message);
      });
  };

  getSecondarylistItem(level1) {
    let { primaryid } = this.state;
    console.log("passid", level1);
    if (this.state.primaryid === 0) {
      API.fetchData(API.Query.GET_SECONDARYCATEGORY, {
        categoryid: level1
      }).then((categoryid) => {
        this.setState({
          Categorylist1: categoryid.resource_category,
          level1: true,
          // primaryid: id,
          level2: false,
          level3: false,
          level4: false,
          // submodal: true,
          // resoucemodal: false,
          // primarymodal: false
        });
        console.log("searchlist", level1);
        console.log("Arraylist", this.state.Categorylist1);
      });
    } else {
      API.fetchData(API.Query.GET_SECONDARYCATEGORY, {
        categoryid: primaryid
      }).then((categoryid) => {
        this.setState({
          Categorylist1: categoryid.resource_category,
          level1: true,
          // primaryid: id,
          level2: false,
          level3: false,
          level4: false,
          // submodal: true,
          // resoucemodal: false,
          // primarymodal: false
        });
        console.log("secondaryid", this.state.primaryid);
        console.log("Array", this.state.Categorylist1);
      });
    }
  }

  getResourceListItem(level1) {
    let { level1Id } = this.state;
    console.log("testvalueid" ,this.state.level1Id)
    if (this.state.level1Id === 0) {
      console.log("value value", level1)
      API.fetchData(API.Query.GET_SECONDARYCATEGORY, {
        categoryid: level1
      }).then((categoryid) => {
        this.setState({
          categorylist2: categoryid.resource_category,
          // level1Id: id,
          level2: true,
          level3: false,
          level4: false,
        });
        console.log("searchlist", this.state.categorylist2);
        console.log("searchid", this.state.level1Id);
      });
    } else {
      API.fetchData(API.Query.GET_SECONDARYCATEGORY, {
        categoryid: level1Id
      }).then((categoryid) => {
        this.setState({
          categorylist2: categoryid.resource_category,
          // level1Id: id,
          level2: true,
          level3: false,
          level4: false,
        });
        console.log("sublistcterid1", this.state.categorylist2);
        console.log("Sublistlidvalue1", this.state.level1Id);
      });
    }
  }

  getLevel3Item() {
    let { level2Id } = this.state;
    API.fetchData(API.Query.GET_SECONDARYCATEGORY, {
      categoryid: level2Id
    }).then((categoryid) => {
      this.setState({
        categorylist3: categoryid.resource_category,
        level3: true,
        level4: false
      });
      console.log("idvalue", this.state.categorylist3);
      console.log("id", this.state.level2Id);
    });
  }

  getLevel4Item() {
    let { level3Id } = this.state;
    API.fetchData(API.Query.GET_SECONDARYCATEGORY, {
      categoryid: level3Id
    }).then((categoryid) => {
      this.setState({
        categorylist4: categoryid.resource_category,
        level4: true,
      });
      console.log("lastvalue", this.state.categorylist4)
    })
  }

  handlePrimaryDelete = () => {
    let { primaryid } = this.state;
    console.log("statedelete", this.state.primaryid);
    API.fetchData(API.Query.DELETE_RESOURCE_CATEGORY, {
      resource_categoryid: primaryid
    }).then((res) => {
      console.log(res);
    });
    this.setState({});
  };

  handleLevel1Del = () => {
    let { level1Id } = this.state;
    console.log("Subdelet", this.state.level1Id);
    API.fetchData(API.Query.DELETE_RESOURCE_CATEGORY, {
      resource_categoryid: level1Id
    }).then((res) => {
      console.log(res);
      this.getSecondarylistItem();
    });
    this.setState({});
  };

  handleLevel2Del = (id) => {
    let { level2Id } = this.state;
    console.log("Subdelet1", this.state.level2Id);
    API.fetchData(API.Query.DELETE_RESOURCE_CATEGORY, {
      resource_categoryid: level2Id
    }).then((res) => {
      console.log(res);
      this.getResourceListItem();
    });
    this.setState({});
  };

  handleLevel3Del = () => {
    let { level3Id } = this.state;
    console.log("level3del", this.state.level3Id);
    API.fetchData(API.Query.DELETE_RESOURCE_CATEGORY, {
      resource_categoryid: level3Id
    }).then((res) => {
      console.log(res);
      this.getLevel3Item();
    });
  }

  handleLevel4Del = () => {
    let { level4Id } = this.state;
    console.log("level4id", this.state.level4Id)
    API.fetchData(API.Query.DELETE_RESOURCE_CATEGORY, {
      resource_categoryid: level4Id
    }).then((res) => {
      console.log(res)
      this.getLevel4Item();
    })
  }

  onAddResource = () => {
    API.fetchData("create_resource_subcategory", {
      data: {
        name: this.state.resourceName,
        resource_category_id: this.state.primaryid,
        type: "resource",
        primary_category_id: this.state.primaryid
      }
    }).then((res) => {
      console.log("resourcename", res);
      this.modalhide();
      this.getSecondarylistItem();
      this.setState({
        resourceName: "",
        resourceNameLevel1: "",
        resourceNameLevel2: "",
        resourceNameLevel3: "",
      })
    });
  };

  onAddResourceLevel1 = () => {
    API.fetchData("create_resource_subcategory", {
      data: {
        name: this.state.resourceNameLevel1,
        resource_category_id: this.state.level1Id,
        type: "resource",
        primary_category_id: this.state.primaryid,
        secondary_category_id: this.state.level1Id
      }
    }).then((res) => {
      console.log("resourcename", res);
      this.modalhide();
      this.getResourceListItem();
      this.setState({
        resourceName: "",
        resourceNameLevel1: "",
        resourceNameLevel2: "",
        resourceNameLevel3: "",
      })
    });
  };

  onAddResourceLevel2 = () => {
    API.fetchData("create_resource_subcategory", {
      data: {
        name: this.state.resourceNameLevel2,
        resource_category_id: this.state.level2Id,
        type: "resource",
        primary_category_id: this.state.primaryid,
        secondary_category_id: this.state.level2Id
      }
    }).then((res) => {
      console.log("resoureljdls", res);
      this.modalhide();
      this.getLevel3Item();
      this.setState({
        resourceName: "",
        resourceNameLevel1: "",
        resourceNameLevel2: "",
        resourceNameLevel3: "",
      })
    });
  };

  onAddResourceLevel3 = () => {
    console.log("lsdkfjlks", this.state.level3Id)
    API.fetchData("create_resource_subcategory", {
      data: {
        name: this.state.resourceNameLevel3,
        resource_category_id: this.state.level3Id,
        type: "resource",
        primary_category_id: this.state.primaryid,
        secondary_category_id: this.state.level3Id
      }
    }).then((res) => {
      console.log("level3resource", res);
      this.modalhide();
      this.getLevel4Item();
      this.setState({
        resourceName: "",
        resourceNameLevel1: "",
        resourceNameLevel2: "",
        resourceNameLevel3: "",
      })
    })
  }

  handlePrimaryResource = () => {
    this.setState({
      submodal: false,
      resoucemodal: true,
      isOpen: true
    });
  };

  handleLev1Res = () => {
    this.setState({
      resoucemodalLevel1: true,
      // primarymodal: false,
      isOpen: true
    });
  };

  handleLev2Res = () => {
    console.log("level1res")
    this.setState({
      resoucemodalLevel2: true,
      isOpen: true
    });
  };

  handleLev3Res = () => {
    console.log("level2res")
    this.setState({
      resoucemodalLevel3: true,
      isOpen: true,
    })
  }

  handleLev4Res = () => {

  }
  handleCheck = (id1, id2, id3) => {
    let level1 = id1.props.children;
    let level2 = id2.props.children;
    let level3 = id3.props.children;

    if (level2 === null && level3 === null) {
      this.setState({
        getprimarysearch: level1
      });
      this.getlist();
    }

    if (level1 === null && level3 === null) {
      this.setState({
        getprimarysearch: level2.props.children[0],
        getsecondarysearch:
          level2.props.children[1] == null
            ? level2.props.children[2]
            : level2.props.children[1],
        getresourcesearch: level2.props.children[2]
      });
      this.getSecondarylistItem(
        level2.props.children[0]
        // level2.props.children[1],
      );
      if (level2.props.children[1] !== null) {
        this.getResourceListItem(
          level2.props.children[1],
          level2.props.children[2]
        );
      }
    }

    if (level1 === null && level2 === null) {
      this.setState({
        getprimarysearch: level3.props.children[0],
        getsecondarysearch:
          level3.props.children[1] == null
            ? level3.props.children[2]
            : level3.props.children[1],
        getresourcesearch: level3.props.children[2]
      });
      this.getSecondarylistItem(
        level3.props.children[0]
        // level3.props.children[2],
        // level3.props.children[1]
      );
      if (level3.props.children[1] !== null) {
        this.getResourceListItem(
          level3.props.children[1],
          level3.props.children[2]
        );
      }
    }

    console.log("level1", level1);
    console.log("level2", level2);
    console.log("level3", level3);
    // console.log("lvel", level2.props.children[0])
  };

  handlestatereset = () => {
    this.setState({
      getprimarysearch: 0,
      getsecondarysearch: 0,
      getresourcesearch: 0,
      primaryid: 0
    });
  };

  handlerefresh() {
    window.location.reload(false);
  }

  render() {
    return (
      <div className="wrp-res-lib">
        <Grid>
          <Grid.Cell span={60}>
            <div className="res-lib-cnt">
              <div className="res-lib-tit heading">Resource library</div>
              <div className="res-lib-fnd">
                <div className="fnd-tit heading">
                  Find a Category / Resource
                </div>
              </div>
              <div className="res-int">
                <div className="res-int-inp">
                  <input
                    className="res-int-inp"
                    onChange={this.handlefindValue}
                    ref={(el) => (this.firstInput = el)}
                    placeholder="Enter name of a Category or Resource / Product"
                    onKeyPress={this.handlePopover}
                    id="searchBox"
                  />
                  {/* <input
                    className="res-int-inp"
                    placeholder="Enter name of a Category or Resource / Product"
                    ref={(el) => (this.firstInput = el)}
                    onChange={this.searchData.bind(this)}
                    // onKeyPress={this.handlePopover}
                  />
                  {(this.state.list) ? <SearchResult data={this.state.list} /> : null  } */}
                </div>
                <div className="res-int-but" id="mypopover">
                  <Button
                    color="primary"
                    onClick={this.handlePopover}
                  >Find</Button>
                </div>
              </div>
              { (
                <Popover hideArrow isOpen={this.state.showopop === true && this.state.findResults.length>0} trigger="legacy" placement="bottom-start" target="searchBox">
                  <PopoverBody>
                    <ListGroup style={{width:'700px'}}>
                      {this.state.findResults.map((findresult) => (
                        <div>
                          {this.state.findValue === "" ? (
                            this.setState({
                              getprimarysearch: 0,
                              getsecondarysearch: 0,
                              getresourcesearch: 0
                            })
                          ) : (
                            // this.getlist()

                            <ListGroupItem
                              tag="button"
                              action
                              className="rs-list-item"
                              onMouseOver={this.handlestatereset}
                              // onClick={() =>
                              // this.setState(
                              //   { primaryid: listitem.id },
                              //   () => {
                              //     this.getSecondarylistItem();
                              //   }
                              // )
                              // }
                              onClick={() =>{
                                this.setState({showopop:false});
                                this.handleCheck(
                                  <div>
                                    {findresult &&
                                    findresult.type === "primary_category"
                                      ? findresult.id
                                      : null}
                                  </div>,
                                  <div>
                                    {findresult &&
                                    findresult.type === "secondary_category" ? (
                                      <div>
                                        {findresult &&
                                          findresult.primary_category &&
                                          findresult.primary_category.id}
                                        {findresult &&
                                        findresult.secondary_category === null
                                          ? null
                                          : findresult &&
                                            findresult.secondary_category &&
                                            findresult.secondary_category.id}
                                        {findresult.id}
                                      </div>
                                    ) : null}
                                  </div>,
                                  <div>
                                    {findresult &&
                                    findresult.type === "resource" ? (
                                      <div>
                                        {findresult &&
                                          findresult.primary_category &&
                                          findresult.primary_category.id}
                                        {findresult &&
                                        findresult.secondary_category === null
                                          ? null
                                          : findresult &&
                                            findresult.secondary_category &&
                                            findresult.secondary_category.id}
                                        {findresult && findresult.id}
                                      </div>
                                    ) : null}
                                  </div>
                                ) 
                              }
                              }
                            >
                              <div className="fin-res-pri">
                                {findresult &&
                                findresult.type === "primary_category"
                                  ? // findresult && findresult.primary_category && findresult.primary_category.name,
                                    findresult && findresult.name
                                  : null}
                                {/* {findresult && findresult.name}&ensp;{">"}&ensp; */}
                                {findresult &&
                                findresult.type === "secondary_category" ? (
                                  <div className="fin-res-pri">
                                    {findresult &&
                                      findresult.primary_category &&
                                      findresult.primary_category.name}
                                    &emsp;{">"}&emsp;
                                    {findresult &&
                                    findresult.secondary_category === null ? (
                                      ""
                                    ) : (
                                      <div>
                                        {findresult &&
                                          findresult.secondary_category &&
                                          findresult.secondary_category.name}
                                        &emsp;{">"}&emsp;
                                      </div>
                                    )}
                                    {findresult && findresult.name} &emsp;{">"}
                                    &emsp;
                                  </div>
                                ) : (
                                  ""
                                )}
                                {findresult && findresult.type === "resource" ? (
                                  <div className="fin-res-pri">
                                    {findresult &&
                                      findresult.primary_category &&
                                      findresult.primary_category.name}
                                    &emsp;{">"}&emsp;
                                    {findresult &&
                                    findresult.secondary_category === null ? (
                                      ""
                                    ) : (
                                      <div>
                                        {findresult &&
                                          findresult.secondary_category &&
                                          findresult.secondary_category.name}
                                        &emsp;{">"}&emsp;
                                      </div>
                                    )}
                                    {findresult && findresult.name}
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                            </ListGroupItem>
                          )}
                        </div>
                      ))}
                    </ListGroup>
                  </PopoverBody>
                </Popover>
                )}
              <div className="brs-tit">
                Browse for your Resource’s category.
              </div>
              <div className="brs-txt mb-1">
                <IconComponent name="notes" />
                <div className="brs-txt-fil mb-2">
                  <sapn>If you do not see your product’s primary category listed
                  below, you can opt to create one. It will require an approval
                  from the e-Tender, and may take 24 hours to get listed here.
                    <Button
                      color="secondary"
                      className="m-2"
                      size="sm"
                      onClick={this.handlePrimaryCategory}
                      onMouseOver={this.handleClick}
                    >Add Primary Category</Button>
                  </sapn>
                </div>
              </div>
              {this.state.primarymodal === true && (
                <PrimaryModal
                  isOpen={this.state.isOpen}
                  hide={this.modalhide}
                  catValue={this.state.catValue}
                  onChange={this.handlePrimaryValue}
                  onAddCategory={this.onAddCategory}
                  onSearchText={(e) => this.firstInput.focus()}
                />
              )}
              <div className="brs-txt1">
                <IconComponent name="notes" />
                <div className="brs-txt-fil">
                  The category you are trying to add may already exist. Try to
                  find your category with a different spelling.
                </div>
              </div>
              {this.state.successmodal && (
                <div>
                  <SuccessModal
                    isOpen={this.state.isOpen}
                    hide={this.modalsucceshide}
                    value={this.state.catValue}
                  />
                </div>
              )}
              <Button color="secondary" size="sm" className="mb-2">Import</Button>
            </div>
          </Grid.Cell>
          <Grid.Cell span={30}>
            <div className="btr-tst">
              <Button color="link" onClick={this.toggle}>
                Resource category guidelines
              </Button>
              <Toast isOpen={this.state.show}>
                <ToastHeader
                  toggle={this.toggle}
                  style={{ backgroundColor: "#E0F5F5" }}
                >
                  <IconComponent name="tips" />
                  &emsp; Tips
                </ToastHeader>
                <ToastBody style={{ backgroundColor: "#E0F5F5" }}>
                  Search required category name by typing various equivalent
                  words, as this will help you avoid redundancy in the resource
                  catalog. <br></br>Will save you more time when you locate a
                  product.
                </ToastBody>
              </Toast>
            </div>
          </Grid.Cell>
        </Grid>
        <div className="pro-lst-cat">
          <div className="btr-cat-tit heading">All product categories</div>
          {/* <div className="btr-prm-tit">Primary Category</div> */}
          <div className="lst-main">
            <div className="lst2">
              <div className="btr-prm-tit heading">Primary Category</div>
              <div className="btr-lst">
                <ListGroup>
                  {this.state.listitems &&
                    this.state.listitems.length > 0 &&
                    this.state.listitems.map((listitem) => (
                      <div className="res-lst-flw">
                        <div className="res-lst-flw-lst">
                          {this.state.getprimarysearch === listitem.id ? (
                            <ListGroupItem
                              tag="button"
                              action
                              key={listitem.id}
                              className="search-focus"
                              onMouseOver={() =>
                                this.setState(
                                  { primaryid: listitem.id },
                                  () => {
                                    this.handleClick();
                                  }
                                )
                              }
                              // onClick={() =>
                              //   this.setState(
                              //     { primaryid: listitem.id },
                              //     () => {
                              //       this.getSecondarylistItem();
                              //     }
                              //   )
                              // }
                            >
                              {/* {listitem.id} */}
                              {listitem.name}
                            </ListGroupItem>
                          ) : (
                            <ListGroupItem
                              tag="button"
                              action
                              key={listitem.id}
                              className="btr-lst-grp"
                              onMouseOver={() =>
                                this.setState(
                                  { primaryid: listitem.id },
                                  () => {
                                    this.handleClick();
                                  }
                                )
                              }
                              // onClick={() =>
                              //   this.setState(
                              //     { primaryid: listitem.id },
                              //     () => {
                              //       this.getSecondarylistItem();
                              //     }
                              //   )
                              // }
                            >
                              {/* {listitem.id} */}
                              {listitem.name}
                            </ListGroupItem>
                          )}
                        </div>
                        {this.state.getprimarysearch === listitem.id ? (
                          <div className="tag-focus">
                            <div className="tag-focus-btr-lst-icn">
                              <div className="showme">
                                <div class="btr-drp-dwn">
                                  <button
                                    class="dropbtn"
                                    onMouseOver={this.handleClick}
                                  >
                                    <IconComponent
                                      name="threedot"
                                      width={10}
                                      height={10}
                                      fill="rgba(0, 0, 0, 0.5)"
                                    />
                                  </button>
                                  <div class="btr-drp-dwn-content">
                                    <div onClick={this.handlePrimaryResource}>
                                      <IconComponent name="circleplus" />
                                      &ensp; Add a resource
                                    </div>
                                    <div onClick={this.handleCreateSubCatLev1}>
                                      <IconComponent name="circleplus" />
                                      &ensp;Add Sub Category
                                    </div>
                                    <div
                                      onClick={() =>
                                        this.setState(
                                          { primaryid: listitem.id },
                                          () => {
                                            this.handlePrimaryDelete();
                                            this.getlist();
                                          }
                                        )
                                      }
                                    >
                                      <IconComponent name="deleteicon" />
                                      &ensp;
                                      <span style={{ color: "red" }}>
                                        Delete
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div
                                onClick={() =>
                                  this.setState(
                                    {
                                      primaryid: listitem.id,
                                      getprimarysearch: listitem.id
                                    },
                                    () => {
                                      this.getSecondarylistItem();
                                    }
                                  )
                                }
                                className="btr-lst-icn-lefttag"
                              >
                                <IconComponent name="lefttag" />
                              </div>
                            </div>
                          </div>
                        ) : (
                          <div className="btr-lst-icn">
                            <div className="showme">
                              <div class="btr-drp-dwn">
                                <button
                                  class="dropbtn"
                                  onMouseOver={this.handleClick}
                                >
                                  <IconComponent
                                    name="threedot"
                                    width={10}
                                    height={10}
                                    fill="rgba(0, 0, 0, 0.5)"
                                  />
                                </button>
                                <div class="btr-drp-dwn-content">
                                  <div onClick={this.handlePrimaryResource}>
                                    <IconComponent name="circleplus" />
                                    &ensp; Add a resource
                                  </div>
                                  <div onClick={this.handleCreateSubCatLev1}>
                                    <IconComponent name="circleplus" />
                                    &ensp;Add Sub Category
                                  </div>
                                  <div
                                    onClick={() =>
                                      this.setState(
                                        { primaryid: listitem.id },
                                        () => {
                                          this.handlePrimaryDelete();
                                          this.getlist();
                                        }
                                      )
                                    }
                                  >
                                    <IconComponent name="deleteicon" />
                                    &ensp;
                                    <span style={{ color: "red" }}>Delete</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div
                              onClick={() =>
                                this.setState(
                                  { primaryid: listitem.id },
                                  () => {
                                    this.getSecondarylistItem();
                                  }
                                )
                              }
                              className="btr-lst-icn-lefttag"
                            >
                              <IconComponent name="lefttag" />
                            </div>
                          </div>
                        )}
                      </div>
                    ))}
                </ListGroup>
              </div>
            </div>
            {this.state.level1 === true && (
              <div className="lst2">
                <div className="btr-sec-tit heading">
                  Sub Category - Level 1
                </div>
                <div className="btr-lst1">
                  <ListGroup>
                    {this.state.Categorylist1 &&
                      this.state.Categorylist1.length > 0 &&
                      this.state.Categorylist1.map((sublist) => (
                        <div className="res-lst-flw1">
                          <div className="res-lst-flw1-lst1">
                            {sublist.type === "resource" ? (
                              <div>
                                {this.state.getsecondarysearch ===
                                sublist.id ? (
                                  <ListGroupItem
                                    tag="button"
                                    action
                                    className="search-focus-level1"
                                    onMouseOver={() =>
                                      this.setState({ level1Id: sublist.id }, () => {
                                        this.handleClick();
                                      })
                                    }
                                    onClick={this.handleLev1Res}
                                  >
                                    {/* {sublist.id} */}
                                    {sublist.name}
                                  </ListGroupItem>
                                ) : (
                                  <ListGroupItem
                                    tag="button"
                                    action
                                    className="btr-lst1-grp1"
                                    onMouseOver={() =>
                                      this.setState({ level1Id: sublist.id }, () => {
                                        this.handleClick();
                                      })
                                    }
                                    onClick={this.handleLev1Res}
                                  >
                                    {/* {sublist.id} */}
                                    {sublist.name}
                                  </ListGroupItem>
                                )}
                              </div>
                            ) : (
                              <div>
                                {this.state.getsecondarysearch ===
                                sublist.id ? (
                                  <ListGroupItem
                                    tag="button"
                                    action
                                    className="search-focus-level1"
                                    onMouseOver={() =>
                                      this.setState(
                                        { level1Id: sublist.id },
                                        () => {
                                          // this.getResourceListItem();
                                        }
                                      )
                                    }
                                  >
                                    {/* {sublist.id} */}
                                    {sublist.name}
                                  </ListGroupItem>
                                ) : (
                                  <ListGroupItem
                                    tag="button"
                                    action
                                    className="btr-lst1-grp1"
                                    onMouseOver={() =>
                                      this.setState(
                                        { level1Id: sublist.id },
                                        () => {
                                          // this.getResourceListItem();
                                        }
                                      )
                                    }
                                  >
                                    {/* {sublist.id} */}
                                    {sublist.name}
                                  </ListGroupItem>
                                )}
                              </div>
                            )}
                          </div>
                          {this.state.getsecondarysearch === sublist.id ? (
                            <div className="tag-focus-level1">
                              {sublist.type === "secondary_category" ? (
                                <div className="tag-focus-btr-lst1-icn1">
                                  <div className="showicon">
                                    <div class="btr-drp-dwn">
                                      <button
                                        class="dropbtn"
                                        onMouseOver={this.handleClick}
                                      >
                                        <IconComponent
                                          name="threedot"
                                          width={10}
                                          height={10}
                                          fill="rgba(0, 0, 0, 0.5)"
                                        />
                                      </button>
                                      <div class="btr-drp-dwn-content">
                                        <div onClick={this.handleLev1Res}>
                                          <IconComponent name="circleplus" />
                                          &ensp; Add a resource
                                        </div>
                                        <div
                                          onClick={
                                            this.handleCreateSubCatLevel2
                                          }
                                        >
                                          <IconComponent name="circleplus" />
                                          &ensp;Add Sub Category
                                        </div>
                                        <div
                                          onClick={() =>
                                            this.setState(
                                              { level1Id: sublist.id },
                                              () => {
                                                this.handleLevel1Del();
                                                this.getSecondarylistItem();
                                              }
                                            )
                                          }
                                        >
                                          <IconComponent name="deleteicon" />
                                          &ensp;
                                          <span style={{ color: "red" }}>
                                            Delete
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    onClick={() =>
                                      this.setState(
                                        { level1Id: sublist.id },
                                        () => {
                                          this.getResourceListItem();
                                        }
                                      )
                                    }
                                    className="btr-lst1-icn1-lefttag1"
                                  >
                                    <IconComponent name="lefttag" />
                                  </div>
                                </div>
                              ) : (
                                <div
                                  className="res-tag-lib"
                                  onClick={this.handleLev1Res}
                                >
                                  <IconComponent
                                    name="resourcetag"
                                    width="25"
                                    height="40"
                                    fill="grey"
                                  />
                                </div>
                              )}
                            </div>
                          ) : (
                            <div>
                              {sublist.type === "secondary_category" ? (
                                <div className="btr-lst1-icn1">
                                  <div className="showicon">
                                    <div class="btr-drp-dwn">
                                      <button
                                        class="dropbtn"
                                        onMouseOver={this.handleClick}
                                      >
                                        <IconComponent
                                          name="threedot"
                                          width={10}
                                          height={10}
                                          fill="rgba(0, 0, 0, 0.5)"
                                        />
                                      </button>
                                      <div class="btr-drp-dwn-content">
                                        <div onClick={this.handleLev1Res}>
                                          <IconComponent name="circleplus" />
                                          &ensp; Add a resource
                                        </div>
                                        <div
                                          onClick={
                                            this.handleCreateSubCatLevel2
                                          }
                                        >
                                          <IconComponent name="circleplus" />
                                          &ensp;Add Sub Category
                                        </div>
                                        <div
                                          onClick={() =>
                                            this.setState(
                                              { level1Id: sublist.id },
                                              () => {
                                                this.handleLevel1Del();
                                                this.getSecondarylistItem();
                                              }
                                            )
                                          }
                                        >
                                          <IconComponent name="deleteicon" />
                                          &ensp;
                                          <span style={{ color: "red" }}>
                                            Delete
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    onClick={() =>
                                      this.setState(
                                        { level1Id: sublist.id },
                                        () => {
                                          this.getResourceListItem();
                                        }
                                      )
                                    }
                                    className="btr-lst1-icn1-lefttag1"
                                  >
                                    <IconComponent name="lefttag" />
                                  </div>
                                </div>
                              ) : (
                                <div
                                  className="res-tag-lib"
                                  onClick={this.handleLev1Res}
                                >
                                  <IconComponent
                                    name="resourcetag"
                                    width="25"
                                    height="40"
                                    fill="grey"
                                  />
                                </div>
                              )}
                            </div>
                          )}
                        </div>
                      ))}
                  </ListGroup>
                </div>
              </div>
            )}
            {this.state.level2 === true && (
              <div className="lst2">
                <div className="btr-sec-tit heading">
                  Sub Category - Level 2
                </div>
                <div className="btr-lst2">
                  <ListGroup>
                    {this.state.categorylist2.map((list) => (
                      <div className="res-lst-flw2">
                        <div className="res-lst-flw2-lst2">
                          {list.type === "resource" ? (
                            <div>
                              {this.state.getresourcesearch === list.id ? (
                                <ListGroupItem
                                  tag="button"
                                  action
                                  className="search-focus-level2"
                                  onClick={this.handleLev2Res}
                                  onMouseOver={() =>
                                    this.setState(
                                      { level2Id: list.id },
                                      () => { this.handleClick();}
                                    )
                                  }
                                >
                                  {/* {list.id} */}
                                  {list.name}
                                </ListGroupItem>
                              ) : (
                                <ListGroupItem
                                  tag="button"
                                  action
                                  className="btr-lst2-grp2"
                                  onClick={this.handleLev2Res}
                                  onMouseOver={() =>
                                    this.setState(
                                      { level2Id: list.id },
                                      () => { this.handleClick();}
                                    )
                                  }
                                >
                                  {/* {list.id} */}
                                  {list.name}
                                </ListGroupItem>
                              )}
                            </div>
                          ) : (
                            <div>
                              {this.state.getresourcesearch === list.id ? (
                                <ListGroupItem
                                  tag="button"
                                  action
                                  className="search-focus-level2"
                                  // onClick={() => this.handleSubList2(sublist.id)}
                                  onMouseOver={() =>
                                    this.setState(
                                      { level2Id: list.id },
                                      () => {}
                                    )
                                  }
                                >
                                  {/* {list.id} */}
                                  {list.name}
                                </ListGroupItem>
                              ) : (
                                <ListGroupItem
                                  tag="button"
                                  action
                                  className="btr-lst2-grp2"
                                  // onClick={() => this.handleSubList2(sublist.id)}
                                  onMouseOver={() =>
                                    this.setState(
                                      { level2Id: list.id },
                                      () => {}
                                    )
                                  }
                                >
                                  {/* {list.id} */}
                                  {list.name}
                                </ListGroupItem>
                              )}
                            </div>
                          )}
                        </div>
                        {this.state.getresourcesearch === list.id ? (
                          <div className="tag-focus-level2">
                            {list.type === "secondary_category" ? (
                              <div className="tag-focus-btr-lst2-icn2">
                                <div className="showleft">
                                  <div class="btr-drp-dwn">
                                    <button
                                      class="dropbtn"
                                      onMouseOver={this.handleClick}
                                    >
                                      <IconComponent
                                        name="threedot"
                                        width={10}
                                        height={10}
                                        fill="rgba(0, 0, 0, 0.5)"
                                      />
                                    </button>
                                    <div class="btr-drp-dwn-content">
                                      <div onClick={this.handleLev2Res}>
                                        <IconComponent name="circleplus" />
                                        &ensp; Add a resource
                                      </div>
                                      <div
                                        onClick={this.handleCreateSubCatLevel3}
                                      >
                                        <IconComponent name="circleplus" />
                                        &ensp;Add Sub Category
                                      </div>
                                      <div
                                        onClick={() =>
                                          this.setState(
                                            { level2Id: list.id },
                                            () => {
                                              this.handleLevel2Del();
                                              this.getResourceListItem();
                                            }
                                          )
                                        }
                                      >
                                        <IconComponent name="deleteicon" />
                                        &ensp;
                                        <span style={{ color: "red" }}>
                                          Delete
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  // onClick={() => this.getResourceListItem(sublist.id)}
                                  className="btr-lefttag-leve3"
                                  onClick={() =>
                                    this.setState({ level2Id: list.id }, () => {
                                      this.getLevel3Item();
                                    })
                                  }
                                >
                                  <IconComponent name="lefttag" />
                                </div>
                              </div>
                            ) : (
                              <div
                                className="res-tag-lib"
                                onClick={this.handleLev2Res}
                              >
                                <IconComponent
                                  name="resourcetag"
                                  width="25"
                                  height="40"
                                  fill="grey"
                                />
                              </div>
                            )}
                          </div>
                        ) : (
                          <div>
                            {list.type === "secondary_category" ? (
                              <div className="btr-lst2-icn2">
                                <div className="showleft">
                                  <div class="btr-drp-dwn">
                                    <button
                                      class="dropbtn"
                                      onMouseOver={this.handleClick}
                                    >
                                      <IconComponent
                                        name="threedot"
                                        width={10}
                                        height={10}
                                        fill="rgba(0, 0, 0, 0.5)"
                                      />
                                    </button>
                                    <div class="btr-drp-dwn-content">
                                      <div onClick={this.handleLev2Res}>
                                        <IconComponent name="circleplus" />
                                        &ensp; Add a resource
                                      </div>
                                      <div
                                        onClick={this.handleCreateSubCatLevel3}
                                      >
                                        <IconComponent name="circleplus" />
                                        &ensp;Add Sub Category
                                      </div>
                                      <div
                                        onClick={() =>
                                          this.setState(
                                            { level2Id: list.id },
                                            () => {
                                              this.handleLevel2Del();
                                              this.getResourceListItem();
                                            }
                                          )
                                        }
                                      >
                                        <IconComponent name="deleteicon" />
                                        &ensp;
                                        <span style={{ color: "red" }}>
                                          Delete
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  // onClick={() => this.getResourceListItem(sublist.id)}
                                  className="btr-lefttag-leve3"
                                  onClick={() =>
                                    this.setState({ level2Id: list.id }, () => {
                                      this.getLevel3Item();
                                    })
                                  }
                                >
                                  <IconComponent name="lefttag" />
                                </div>
                              </div>
                            ) : (
                              <div
                                className="res-tag-lib"
                                onClick={this.handleLev2Res}
                              >
                                <IconComponent
                                  name="resourcetag"
                                  width="25"
                                  height="40"
                                  fill="grey"
                                />
                              </div>
                            )}
                          </div>
                        )}
                      </div>
                    ))}
                  </ListGroup>
                </div>
              </div>
            )}
            {this.state.level3 === true && (
              <div className="lst2">
                <div className="btr-sec-tit heading">
                  Sub Category - Level 3
                </div>
                <div className="btr-lst3">
                  <ListGroup>
                    {this.state.categorylist3 &&
                      this.state.categorylist3.length > 0 &&
                      this.state.categorylist3.map((list3) => (
                        <div className="res-lst-flw3">
                          <div className="res-lst-flw3-lst3">
                            {list3.type === "resource" ? (
                              <ListGroupItem
                                tag="button"
                                action
                                className="btr-lst3-grp3"
                                onMouseOver={() =>
                                  this.setState({ level3Id: list3.id }, () => {
                                    this.handleClick();
                                  })
                                }
                                onClick={this.handleLev3Res}
                              >
                                {/* {sublist.id} */}
                                {list3.name}
                              </ListGroupItem>
                            ) : (
                              <ListGroupItem
                                tag="button"
                                action
                                className="btr-lst3-grp3"
                                onMouseOver={() =>
                                  this.setState({ level3Id: list3.id }, () => {
                                    this.handleClick();
                                  })
                                }
                                onClick={this.handleLev3Res}
                              >
                                {/* {sublist.id} */}
                                {list3.name}
                              </ListGroupItem>
                            )}
                          </div>
                          {list3.type === "secondary_category" ? (
                            <div className="btr-lst3-icn3">
                              <div className="showbox">
                                <div class="btr-drp-dwn">
                                  <button
                                    class="dropbtn"
                                    onMouseOver={this.handleClick}
                                  >
                                    <IconComponent
                                      name="threedot"
                                      width={10}
                                      height={10}
                                      fill="rgba(0, 0, 0, 0.5)"
                                    />
                                  </button>
                                  <div class="btr-drp-dwn-content">
                                    <div onClick={this.handleLev3Res}>
                                      <IconComponent name="circleplus" />
                                      &ensp; Add a resource
                                    </div>
                                    <div
                                      onClick={this.handleCreateSubCatLevel4}
                                    >
                                      <IconComponent name="circleplus" />
                                      &ensp;Add Sub Category
                                    </div>
                                    <div
                                      onClick={() =>
                                        this.setState(
                                          { level3Id: list3.id },
                                          () => {
                                            this.handleLevel3Del();
                                            this.getLevel3Item();
                                          }
                                        )
                                      }
                                    >
                                      <IconComponent name="deleteicon" />
                                      &ensp;
                                      <span style={{ color: "red" }}>
                                        Delete
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div
                                onClick={() =>
                                  this.setState(
                                    { level3Id: list3.id },
                                    () => {
                                      this.getLevel4Item();
                                    }
                                  )
                                }
                                className="btr-lst1-icn1-lefttag1"
                              >
                                <IconComponent name="lefttag" />
                              </div>
                            </div>
                          ) : (
                            <div
                              className="res-tag-lib"
                              onClick={this.handleLev3Res}
                            >
                              <IconComponent
                                name="resourcetag"
                                width="25"
                                height="40"
                                fill="grey"
                              />
                            </div>
                          )}
                        </div>
                      ))}
                  </ListGroup>
                </div>
              </div>
            )}
            {this.state.level4 === true && (
              <div className="lst2">
              <div className="btr-sec-tit heading">
                Sub Category - Level 4
              </div>
              <div className="btr-lst4">
                <ListGroup>
                  {this.state.categorylist4 && 
                    this.state.categorylist4.length > 0 &&
                    this.state.categorylist4.map((list4) => (
                      <div className="res-lst-flw4">
                        <div className="res-lst-flw4-lst4">
                        {list4.type === "resource" ? (
                              <ListGroupItem
                                tag="button"
                                action
                                className="btr-lst4-grp4"
                                onMouseOver={() =>
                                  this.setState({ level4Id: list4.id }, () => {
                                    this.handleClick();
                                  })
                                }
                                onClick={this.handleLev4Res}
                              >
                                {/* {sublist.id} */}
                                {list4.name}
                              </ListGroupItem>
                            ) : (
                              <ListGroupItem
                                tag="button"
                                action
                                className="btr-lst4-grp4"
                                onMouseOver={() =>
                                  this.setState({ level4Id: list4.id }, () => {
                                    this.handleClick();
                                  })
                                }
                                onClick={this.handleLev4Res}
                              >
                                {/* {sublist.id} */}
                                {list4.name}
                              </ListGroupItem>
                            )}
                        </div>
                        {list4.type === "secondary_category" ? (
                            <div className="btr-lst4-icn4">
                              <div className="showdrop">
                                <div class="btr-drp-dwn">
                                  <button
                                    class="dropbtn"
                                    onMouseOver={this.handleClick}
                                  >
                                    <IconComponent
                                      name="threedot"
                                      width={10}
                                      height={10}
                                      fill="rgba(0, 0, 0, 0.5)"
                                    />
                                  </button>
                                  <div class="btr-drp-dwn-content">
                                    <div 
                                    // onClick={this.handleLev3Res}
                                    >
                                      <IconComponent name="circleplus" />
                                      &ensp; Add a resource
                                    </div>
                                    <div
                                      // onClick={this.handleCreateSubCatLevel4}
                                    >
                                      <IconComponent name="circleplus" />
                                      &ensp;Add Sub Category
                                    </div>
                                    <div
                                      onClick={() =>
                                        this.setState(
                                          { level4Id: list4.id },
                                          () => {
                                            this.handleLevel4Del();
                                            this.getLevel4Item();
                                          }
                                        )
                                      }
                                    >
                                      <IconComponent name="deleteicon" />
                                      &ensp;
                                      <span style={{ color: "red" }}>
                                        Delete
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div
                                // onClick={() =>
                                //   this.setState(
                                //     { level3Id: list3.id },
                                //     () => {
                                //       this.getLevel4Item();
                                //     }
                                //   )
                                // }
                                className="btr-lst1-icn1-lefttag1"
                              >
                                <IconComponent name="lefttag" />
                              </div>
                            </div>
                          ) : (
                            <div
                              className="res-tag-lib"
                              onClick={this.handleLev4Res}
                            >
                              <IconComponent
                                name="resourcetag"
                                width="25"
                                height="40"
                                fill="grey"
                              />
                            </div>
                          )}
                      </div>
                    ))}
                </ListGroup>
              </div>
              </div>
            )}
          </div>
        </div>
        <div>
          {this.state.resoucemodal === true && (
            <ResourceModal
              isOpen={this.state.isOpen}
              hide={this.modalhide}
              resourceName={this.state.resourceName}
              onChange={this.handleResourceValue}
              onAddCategory={this.onAddResource}
              resId={this.state.primaryid}
              resList={this.state.listitems}
              // levelId={this.state.level1Id}
              // catlist={this.state.Categorylist1}
            />
          )}
          {this.state.resoucemodalLevel1 === true && (
            <ResourceModal
              isOpen={this.state.isOpen}
              hide={this.modalhide}
              resourceName={this.state.resourceNameLevel1}
              onChange={this.handleResourceValueLevel1}
              onAddCategory={this.onAddResourceLevel1}
              resId={this.state.primaryid}
              resList={this.state.listitems}
              levelId={this.state.level1Id}
              catlist={this.state.Categorylist1}
            />
          )}
          {this.state.resoucemodalLevel2 === true && (
            <ResourceModal
              isOpen={this.state.isOpen}
              hide={this.modalhide}
              resourceName={this.state.resourceNameLevel2}
              onChange={this.handleResourceValueLevel1}
              onAddCategory={this.onAddResourceLevel2}
              resId={this.state.primaryid}
              resList={this.state.listitems}
              levelId={this.state.level1Id}
              catlist={this.state.Categorylist1}
              level2Id={this.state.level2Id}
              level2list={this.state.categorylist2}
            />
          )}
          {this.state.resoucemodalLevel3 === true && (
            <ResourceModal
              isOpen={this.state.isOpen}
              hide={this.modalhide}
              resourceName={this.state.resourceNameLevel3}
              onChange={this.handleResourceValueLevel1}
              onAddCategory={this.onAddResourceLevel3}
              resId={this.state.primaryid}
              resList={this.state.listitems}
              levelId={this.state.level1Id}
              catlist={this.state.Categorylist1}
              level2Id={this.state.level2Id}
              level2list={this.state.categorylist2}
              level3Id={this.state.level3Id}
              level3list={this.state.categorylist3}
            />
          )}
        </div>
        <div>
          {this.state.submodal && (
            <SubCategoryModal
              isOpen={this.state.isOpen}
              hide={this.modalhide}
              onInputChange={this.onInputChange}
              handleAddvalue={this.handleAddvalue}
              fieldAdd={this.state.fields}
              onAddCategory={this.onAddSubCategorylist1}
              showValue={this.state.primaryid}
              listitemtest={this.state.listitems}
            />
          )}
          {this.state.submodalLevel1 === true && (
            <SubCategoryModal
              isOpen={this.state.isOpen}
              hide={this.modalhide}
              onInputChange={this.onInputChange}
              handleAddvalue={this.handleAddvalue}
              fieldAdd={this.state.fields}
              onAddCategory={this.onAddSubCategorylist2}
              showValue={this.state.primaryid}
              listitemtest={this.state.listitems}
              level1Id={this.state.level1Id}
              secondaryList={this.state.Categorylist1}
            />
          )}
          {this.state.submodalLevel2 === true && (
            <SubCategoryModal
              isOpen={this.state.isOpen}
              hide={this.modalhide}
              onInputChange={this.onInputChange}
              handleAddvalue={this.handleAddvalue}
              fieldAdd={this.state.fields}
              onAddCategory={this.onAddSubCategorylist3}
              showValue={this.state.primaryid}
              listitemtest={this.state.listitems}
              level1Id={this.state.level1Id}
              secondaryList={this.state.Categorylist1}
              level2Id={this.state.level2Id}
              resourcelist={this.state.categorylist2}
            />
          )}
          {this.state.submodalLevel3 === true && (
            <SubCategoryModal
              isOpen={this.state.isOpen}
              hide={this.modalhide}
              onInputChange={this.onInputChange}
              handleAddvalue={this.handleAddvalue}
              fieldAdd={this.state.fields}
              onAddCategory={this.onAddSubCategorylist4}
              showValue={this.state.primaryid}
              listitemtest={this.state.listitems}
              level1Id={this.state.level1Id}
              secondaryList={this.state.Categorylist1}
              level2Id={this.state.level2Id}
              resourcelist={this.state.categorylist2}
              level3Id={this.state.level3Id}
              level3list={this.state.categorylist3}
            />
          )}
        </div>
      </div>
    );
  }
}

ResourceLibrary.propTypes = {
  name: PropTypes.string
};

ResourceLibrary.defaultProps = {
  name: "Resource Library"
};

export default withRouter(ResourceLibrary);
