// Primary Category Modal Component
import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "../../components/Modal";
import { Button } from "reactstrap";
import { Input } from "../../components/Input";
import { IconComponent } from "../../components/Icons";

// Main Component
const PrimaryModal = (props) => {
  const {
    isOpen,
    hide,
    catValue,
    onChange,
    onAddCategory,
    onSearchText
  } = props;
  const [open, setOpen] = useState(false);
  const [showinput, setShowInput] = useState(false);
  const [value, setValue] = useState("");
  const [items, setItems] = useState([]);
  const [showbutton, setShowButton] = useState(false);

  const onInputChange = (value) => {
    if (onChange) {
      onChange(value);
      setShowButton(true);
    }
    else{
      setShowButton(false);
    }
  };

  function handleRender() {
    setShowInput(true);
  }

  return (
    <Modal size="small" isOpen={isOpen} hide={hide}>
      <Modal.Content>
        <div className="prm-mdl">
          <div className="prm-mdl-hdr">
            <div className="prm-tit heading">Add New Primary Category</div>
            <div className="prm-cls" onClick={hide}>
              <IconComponent name="close" />
            </div>
          </div>
          <div className="prm-cnt">
            <div className="prm-cnt-shw">
              <div className="prm-cnt-icn">
                <IconComponent name="infoyel" />
              </div>
              <div className="prm-cnt-des">
                Make sure the <strong>Primary Category</strong> you are trying
                to create <strong>does not exist already.</strong> <br />
                <br />- You may want to{" "}
                <span
                  onClick={onSearchText}
                  style={{ color: "Blue", fontWeight: 700, cursor: "pointer" }}
                >
                  {/* <Button 
                  label="SEARCH"
                  variant="link"
                  /> */}
                  SEARCH
                </span>{" "}
                with a different spelling or terminology.
              </div>
            </div>
            <div className="prm-cnt-txt">
              If you still want to go ahead, you can create one.
            </div>
            {showinput === false && (
              <div className="prm-cnt-add">
                <Button
                  color="link"
                  onClick={handleRender}
                >Add New Primary Category</Button>
              </div>
            )}
            {showinput === true && (
              <div className="prm-cnt-main">
                <div>Name of the primary category</div>
                <div className="prm-cnt-inp">
                  <Input
                    size="small"
                    value={catValue}
                    onChange={onInputChange}
                    onKeyDown={onAddCategory}
                  />
                </div>
                <div className="prm-cnt-rot">
                  {showbutton === true && (
                    <div className="prm-cnt-crt">
                      <Button
                        color="primary"
                        onClick={onAddCategory}
                      >Create</Button>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
      </Modal.Content>
    </Modal>
  );
};

PrimaryModal.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  canClose: PropTypes.bool,
  hide: PropTypes.func,
  catValue: PropTypes.string,
  onChange: PropTypes.func,
  onAddCategory: PropTypes.func,
  onSearchText: PropTypes.func
};

PrimaryModal.defaultProps = {
  name: "Unit Tender",
  isOpen: false,
  canClose: false,
  hide: null,
  catValue: "",
  onchange: null,
  onAddCategory: null,
  onSearchText: null
};

export default PrimaryModal;
