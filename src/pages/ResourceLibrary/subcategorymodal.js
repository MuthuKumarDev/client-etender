// SubCategory Modal Component
import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "../../components/Modal";
import { IconComponent } from "../../components/Icons";
import { Button } from "reactstrap";
// import { Input } from "../../components/Input";
// import { Input } from 'reactstrap';

import _ from "lodash";

// Main Component
const SubCategoryModal = (props) => {
  const {
    isOpen,
    hide,
    showValue,
    onInputChange,
    handleAddvalue,
    onAddCategory,
    listitemtest,
    fieldAdd,
    secondaryList,
    level1Id,
    level2Id,
    resourcelist,
    level3Id,
    level3list,
  } = props;
  const [value, setValue] = useState("");
  const [addinput, setAddInput] = useState(false);

  function handleAdd() {
    // const values = [...fields];
    // values.push({ value: value });
    // setFields(values);
    if (handleAddvalue) {
      handleAddvalue();
    }
  }

  function onInputChangeField(i, event) {
    // const values = [...fields];
    // values[i].label = event.target.value;
    // setFields(values);
    if (onInputChange) {
      onInputChange(i, event);
    }
  }

  const findresult =
    _.find(listitemtest, (opt) => opt.id === showValue) || null;

  const level1result =
    _.find(secondaryList, (opt) => opt.id === level1Id) || null;

  const level2result = _.find(resourcelist, (opt) => opt.id === level2Id) || null;
  const level3result = _.find(level3list, (opt) => opt.id === level3Id) || null;
  return (
    <Modal size="small" isOpen={isOpen} hide={hide}>
      <Modal.Content>
        <div className="sub-mdl">
          <div className="sub-mdl-hdr">
            <div className="sub-tit heading">Add Sub Category</div>
            <div className="sub-cls" onClick={hide}>
              <IconComponent name="close" />
            </div>
          </div>
          <div className="sub-cnt">
            <div className="sub-cnt-top">
              <div className="sub-cnt-tit">
                You are now adding Sub Category OR Sub Categories to
              </div>
              <div className="sub-cnt-flw">
              <div className="sub-cnt-flw-but">
                {findresult && findresult.name}&nbsp;
              </div>
              {level1result !==null && (
              <div className="sub-cnt-flw-but1">
                {'>'}{level1result &&
                  level1result.name
                }
                &nbsp;
              </div>
              )}
              {level2result !==null && (
              <div className="sub-cnt-flw-but1">
                {'>'}{level2result &&
                  level2result.name
                }
                &nbsp;
              </div>
              )}
              {level3result !==null && (
              <div className="sub-cnt-flw-val">
                {'>'}{level3result &&
                  level3result.name
                }
                &nbsp;
              </div>
              )}
              </div>
            </div>
            <div className="sub-cnt-fld">
              {fieldAdd.map((field, idx) => {
                return (
                  <div key={`${field}-${idx}`}>
                    <div className="sub-cnt-tit1">
                      *&ensp;{`Name of Sub Category ${idx + 1}`}
                    </div>
                    <div>
                      <input
                        // size="small"
                        className="sub-cnt-inp1"
                        placeholder="Enter Category"
                        // value={subValue1}
                        value={field.value}
                        onChange={(e) => onInputChangeField(idx, e)}
                      />
                    </div>
                  </div>
                );
              })}
              <div className="sub-cnt-add">
                <Button
                  color="link"
                  onClick={() => handleAdd()}
                >Add more categories</Button>
              </div>
            </div>
          </div>
          <div className="sub-btn">
            <div className="sub-btn-lvl">
              <Button
                color="primary"
                onClick={onAddCategory}
              >Create</Button>
            </div>
          </div>
        </div>
      </Modal.Content>
    </Modal>
  );
};

SubCategoryModal.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  canClose: PropTypes.bool,
  hide: PropTypes.func,
  subValue1: PropTypes.string,
  subValue2: PropTypes.string,
  subValue3: PropTypes.string,
  subValue4: PropTypes.string,
  onChange: PropTypes.func,
  onAddCategory: PropTypes.func,
  onInputChange: PropTypes.func,
  handleAddvalue: PropTypes.func
};

SubCategoryModal.defaultProps = {
  name: "Unit Tender",
  isOpen: false,
  canClose: false,
  hide: null,
  subValue1: "",
  subValue2: "",
  subValue3: "",
  subValue4: "",
  onchange: null,
  onAddCategory: null,
  onInputChange: null,
  handleAddvalue: null
};

export default SubCategoryModal;
