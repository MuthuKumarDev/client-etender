import React, { PureComponent } from "react";
import AllTenders from "../AllTenders";
// import IssuedTenders from "../IssuedTenders";
// import DraftsTenders from "../DraftsTenders";
// import ClosedTenders from "../ClosedTenders";
import { CardComponent } from "../../components/Card";
import "../styles/tenders.scss";
//import {  Button as ButtonComponent} from "../../components/Button";
import { Button } from "reactstrap";
import { HeaderComponent } from "../../components/Header";
import { Row, Col } from "reactstrap";
import "../styles/tenders.scss";
import { InputGroupField } from "../../components/InputGroupField";
import { InputField } from "../../components/InputField/Inputfield";
import { IconComponent } from "../../components/Icons";
import API from "../../api/api";
import { BadgeComponent } from "../../components/Badge";
import { MultiSelectComponent } from "../../components/MultiSelect";
// import { Multiselect } from "multiselect-react-dropdown";
//import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import { DatepickerComponent } from "../../components/DatePicker";
import moment from "moment";
import { Modal } from "../../components/Modal";
import { CheckBox } from "../../components/Checkbox";
import { DropDownComponent as DropDown } from "../../components/DropDownComponent";
import { isBuyer } from "../../common";
import { setTenderListCount } from "../../action";
import { connect } from "react-redux";

import PropTypes from "prop-types";

const mapDispatchToProps = (dispatch) => {
  return {
    setTenderListCount: (tenderListCount) => {
      console.log("dipatch....");
      dispatch(setTenderListCount(tenderListCount));
    }
  };
};

// Main Component
class Tenders extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tenderList: [],
      // tendersList: [
      //   {
      //     // tender_id: " BRN/41755716",
      //     id: "ARN/41755716",
      //     tender_title: "Ports And Inland Water Transport Department",
      //     tender_value: "<5 million AED",
      //     description: "test",
      //     //"(1) providing 2 nos, of minimum 30 ton Bollard pug capacity each tug service on hire basis for Berthing and uriberthing of ships, search and rescue tsar) operations and other services for a period of 3 years with extension of 2 more years, if required",
      //     status: "Open",
      //     project_status: "Job in hand",
      //     service_category: "MEP services",
      //     project_location: "Dubai",
      //     closing_date: "12 Jan 2021",
      //     //is_shortlisted: false,
      //     bids: "0",
      //     questions: "13"
      //   },
      //   {
      //     id: "CRN/41755716",
      //     tender_title: "Ports And Inland Water Transport Department",
      //     tender_value: "<5 million AED",
      //     description:
      //       "(1) providing 2 nos, of minimum 30 ton Bollard pug capacity each tug service on hire basis for Berthing and uriberthing of ships, search and rescue tsar) operations and other services for a period of 3 years with extension of 2 more years, if required",
      //     status: "Withdrawn",
      //     project_status: "Quotation",
      //     service_category: "MEP services",
      //     project_location: "Dubai",
      //     closing_date: "10 Jan 2021",
      //     //is_shortlisted: true,
      //     bids: "5",
      //     questions: "3"
      //   },
      //   {
      //     id: " BRN/41755716",
      //     tender_title: "Ports And Inland Water Transport Department",
      //     tender_value: "<5 million AED",
      //     description:
      //       "(1) providing 2 nos, of minimum 30 ton Bollard pug capacity each tug service on hire basis for Berthing and uriberthing of ships, search and rescue tsar) operations and other services for a period of 3 years with extension of 2 more years, if required",
      //     status: "Closed",
      //     project_status: "Job in hand",
      //     service_category: "MEP services",
      //     project_location: "Dubai",
      //     closing_date: "10 Jan 2021",
      //     //is_shortlisted: true,
      //     bids: "3",
      //     questions: "N/A"
      //   }
      // ],
      tendersList: [],
      filter: "",
      options: [
        { key: "All", cat: "" },
        { key: "Open", cat: "" },
        { key: "Closed", cat: "" },
        { key: "Withdrawn", cat: "" },
        { key: "Hold", cat: "" },
        { key: "Issued", cat: "" }
      ],
      publishedWithin: [
        { key: "1 day", cat: "" },
        { key: "1 week", cat: "" },
        { key: "3 weeks", cat: "" },
        { key: "1 month", cat: "" },
        { key: "3 months", cat: "" },
        { key: "6 months", cat: "" },
        { key: "Custom Range", cat: "" }
      ],
      closingDate: [
        { key: "1 day", cat: "" },
        { key: "1 week", cat: "" },
        { key: "3 weeks", cat: "" },
        { key: "1 month", cat: "" },
        { key: "3 months", cat: "" },
        { key: "6 months", cat: "" },
        { key: "Custom Range", cat: "" }
      ],
      projectValue: [
        // { key: "< 5 millions", cat: "" },
        // { key: "5 - 10 millions", cat: "" },
        // { key: "20 - 50 millions", cat: "" },
        // { key: "> 50 millions", cat: "" }
      ],
      projectLocation: [
        // { key: "Dubai", cat: "" },
        // { key: "Sharjah", cat: "" },
        // { key: "Vietnam", cat: "" },
        // { key: "Philippines", cat: "" },
        // { key: "India", cat: "" },
        // { key: "England", cat: "" }
      ],
      serviceType: [
        // { key: "MEP", cat: "" },
        // { key: "MEP1", cat: "" },
        // { key: "MEP2", cat: "" },
        // { key: "MEP3", cat: "" },
        // { key: "MEP4", cat: "" }
      ],
      sortingOptions: [
        { key: "Latest Addition", cat: "" },
        { key: "Closing Date", cat: "" }
      ],
      // selectedValue: [{key:"test",cat:""}],
      selectedValue: "",
      selectedValueForPublished: [],
      selectedValueForProjectValue: [],
      selectedValueForProjectLocation: [],
      selectedValueForClosingDate: [],
      showDatePicker: false,
      showDatePickerForClosingDate: false,
      filteredValue: "",
      noOfItems: 10,
      status: "",
      filteredData: [],
      //sortingOptions: ["Latest Addition", "Closing Date"],
      selectedFieldValue: "",
      showMore: true,
      startDate: "",
      endDate: "",
      customizedDate: "",
      customizedClosingDate: "",
      closingDateFrom: "",
      closingDateTo: "",
      publishedDateFrom: "",
      publishedDateTo: "",
      noRecordText: "",
      isOpen: false,
      isCheckedBids: false,
      noOfBids: [
        { key: "<5", cat: "" },
        { key: ">5", cat: "" },
        { key: "0", cat: "" }
      ],
      subIndustry: [
        { key: "Real Estate", cat: "" },
        { key: "Manufacturing", cat: "" },
        { key: "Land Scaping", cat: "" },
        { key: "Construction", cat: "" }
      ],
      client: [
        { key: "Ariko", cat: "" },
        { key: "Samsun", cat: "" }
      ],
      consultant: [
        { key: "Khalifa", cat: "" },
        { key: "Stark", cat: "" }
      ],
      displayBidsFilterInScreen: false,
      isCheckedConsultant: false,
      isCheckedClient: false,
      isCheckedSubIndustry: false,
      showBidsFilter: false,
      showClientFilter: false,
      showConsultantFilter: false,
      showSubIndustryFilter: false,
      displaySubIndustryFilterInScreen: false,
      displayClientFilterInScreen: false,
      displayConsultantFilterInScreen: false,
      customStartDateForPublished: "",
      customEndDateForPublished: "",
      customStartDateForClosing: "",
      customEndDateForClosing: "",
      fromTendersScreen: true,
      selectedValueForNumberOfBids: "",
      selectedValueForClient: "",
      selectedValueForConsultant: "",
      selectedValueForSubIndustry: "",
      hideStatusOptions: false,
      dropdownOpen: false,
      dropdownVal: "Select",
      isOpenForPublished: false,
      publishedWithinValue: "Select",
      sOpenForClosingDate: false,
      closingDateValue: "Select",
      isOpenForServiceType: false,
      serviceTypeValue: "Select",
      isOpenForNumberOfBids: false,
      noOfBidsValue: "Select",
      fromAddMore: false,
      isOpenForSorting: false,
      sortValue: "Latest Addition",
      fromBuyerLogin: false,
      inputValue: ""
    };

    this.style = {
      chips: {
        //background: "#fff",
        color: "#000"
      },
      searchBox: {
        flexShrink: 1
      },
      option: {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        color: "#212B36"
      }
    };

    this.singleSlectStyle = {
      optionContainer: {
        display: "none"
      },
      chips: {
        //background: "#fff",
        color: "#000"
      },
      searchBox: {
        flexShrink: 1
      }
    };
    this.singleSlectDisplayStyle = {
      optionListContainer: {
        display: "block"
      },
      chips: {
        //background: "#fff",
        color: "#000"
      },
      searchBox: {
        flexShrink: 1
      }
    };
    this.multiselectRef = React.createRef();
    this.multiselectPublishedRef = React.createRef();
    this.closingDateRef = React.createRef();
    this.projectValueRef = React.createRef();
    this.projectLocationRef = React.createRef();
    this.serviceTypeRef = React.createRef();
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.setRefForClosingDate = this.setRefForClosingDate.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }
  componentDidMount = () => {
    var fromBuyerLogin = isBuyer();
    //console.log("fromBuyerLogin.." + fromBuyerLogin);
    this.setState({ fromBuyerLogin: fromBuyerLogin });

    let params = {
      latest_addition: "desc"
    };
    this.fetchTendersListData(params, "", true);
    document.addEventListener("mousedown", this.handleClickOutside);
  };

  fetchTendersListData = (params, isFromFilterApply, atFirstCall) => {
    API.fetchData(API.Query.All_TENDERS_FILTERS, params).then((data) => {
      console.log("tenders list..", JSON.stringify(data));
      let { tender } = data;
      if (data && data.tender && data.tender.length > 0) {
        this.props.setTenderListCount(data.tender.length);
      }

      var noRecord = "No Record is found for your search.";
      if (data && data.tender && data.tender.length == 0) {
        this.setState({ noRecordText: noRecord });
      } else {
        this.setState({ showMore: true });
      }
      var textFilterValue =
        this.state.filter && this.state.filter.toLowerCase();

      if (isFromFilterApply) {
        if (textFilterValue) {
          var filteredData =
            data &&
            data.tender &&
            data.tender.length > 0 &&
            data.tender.filter((item) => {
              let {
                id,
                tender_title,
                description,
                service_category,
                status,
                client,
                project_status,
                consultant,
                project_value,
                project_location,
                closing_date
              } = item;
              // return Object.keys(item).some((key) => {
              return (
                (tender_title &&
                  tender_title
                    .toString()
                    .toLowerCase()
                    .includes(textFilterValue)) ||
                (id && id.toString().includes(textFilterValue)) ||
                (service_category &&
                  service_category
                    .toString()
                    .toLowerCase()
                    .includes(textFilterValue)) ||
                (description &&
                  description
                    .toString()
                    .toLowerCase()
                    .includes(textFilterValue)) ||
                (status &&
                  status.toString().toLowerCase().includes(textFilterValue)) ||
                (project_status &&
                  project_status
                    .toString()
                    .toLowerCase()
                    .includes(textFilterValue)) ||
                (client &&
                  client.toString().toLowerCase().includes(textFilterValue)) ||
                (consultant &&
                  consultant
                    .toString()
                    .toLowerCase()
                    .includes(textFilterValue)) ||
                (project_value &&
                  project_value.toString().includes(textFilterValue)) ||
                (project_location &&
                  project_location
                    .toString()
                    .toLowerCase()
                    .includes(textFilterValue)) ||
                (closing_date &&
                  moment(closing_date)
                    .format("DD MMM YYYY")
                    .toString()
                    .toLowerCase()
                    .includes(textFilterValue))
              );
              // });
            });
          //console.log("filteredData.." + JSON.stringify(filteredData));
          this.setState({
            filteredData: filteredData,
            //tendersList: filteredData,
            noRecordText: noRecord
          });
        } else {
          this.setState({
            filteredData: data.tender,
            //tendersList: data.tender,
            noRecordText: noRecord
          });
        }
      } else {
        this.setState({
          tendersList: data.tender,
          filteredData: data.tender,
          noRecordText: noRecord
        });
      }
      if (atFirstCall) {
        var temp = null;

        var locationData =
          data &&
          data.tender &&
          data.tender.length > 0 &&
          data.tender.map((item) => {
            // let keyValue = item.project_location && {`key: ${item.project_location},cat: '' ` }
            if (item.project_location != null && item.project_location) {
              temp = {
                key: item.project_location,
                cat: ""
              };
            }
            return temp && temp;
          });
        if (locationData) {
          locationData = locationData.filter((data) => data != null);

          let uniqueData = [
            ...new Map(locationData.map((item) => [item["key"], item])).values()
          ];
          this.setState({ projectLocation: uniqueData });
        }

        var projectValue = null;
        var projectValueData =
          data &&
          data.tender &&
          data.tender.length > 0 &&
          data.tender.map((item) => {
            // let keyValue = item.project_location && {`key: ${item.project_location},cat: '' ` }
            if (item.project_value != null && item.project_value) {
              projectValue = {
                key: item.project_value,
                cat: ""
              };
            }
            return projectValue && projectValue;
          });
        if (projectValueData) {
          projectValueData = projectValueData.filter((data) => data != null);

          let uniqueData = [
            ...new Map(
              projectValueData.map((item) => [item["key"], item])
            ).values()
          ];
          this.setState({ projectValue: uniqueData });
        }

        var serviceType = null;
        var serviceTypeData =
          data &&
          data.tender &&
          data.tender.length > 0 &&
          data.tender.map((item) => {
            // let keyValue = item.project_location && {`key: ${item.project_location},cat: '' ` }
            if (item.service_category != null && item.service_category) {
              serviceType = {
                key: item.service_category,
                cat: ""
              };
            }
            return serviceType && serviceType;
          });
        if (serviceTypeData) {
          serviceTypeData = serviceTypeData.filter((data) => data != null);

          let uniqueData = [
            ...new Map(
              serviceTypeData.map((item) => [item["key"], item])
            ).values()
          ];
          this.setState({ serviceType: uniqueData });
        }

        var client = null;
        var clientData =
          data &&
          data.tender &&
          data.tender.length > 0 &&
          data.tender.map((item) => {
            if (item.client != null && item.client) {
              client = {
                key: item.client,
                cat: ""
              };
            }
            return client && client;
          });
        if (clientData && clientData.length > 0) {
          clientData = clientData.filter((data) => data != null);
          let uniqueValue = [
            ...new Map(clientData.map((item) => [item["key"], item])).values()
          ];
          this.setState({ client: uniqueValue });
        }

        var consultant = null;
        var consultantData =
          data &&
          data.tender &&
          data.tender.length > 0 &&
          data.tender.map((item) => {
            if (item.consultant != null && item.consultant) {
              consultant = {
                key: item.consultant,
                cat: ""
              };
            }
            return consultant && consultant;
          });
        if (consultantData && consultantData.length > 0) {
          consultantData = consultantData.filter((data) => data != null);

          let uniqueValue = [
            ...new Map(
              consultantData.map((item) => [item["key"], item])
            ).values()
          ];
          this.setState({ consultant: uniqueValue });
        }
      }
    });
  };

  onShowMoreClick = () => {
    //this.fetchTendersListData(true);
    this.setState({ noOfItems: this.state.noOfItems + 10, showMore: true });
    let noOfItems = this.state.noOfItems + 10;
    if (this.state.filteredData && this.state.filteredData.length < noOfItems) {
      this.setState({ showMore: false });
    }
    // this.setState({
    //   tendersList: [...this.state.tendersList, ...this.state.tendersList]
    // });
  };

  onChangeInput = (value) => {
    this.setState({ filter: value });
  };

  onSelect = (value, name) => {
    // console.log("value.." + JSON.stringify(value));
    if (name == "status") {
      this.setState({ selectedValue: value });
    }
    if (name == "publishedWithin") {
      this.setState({ selectedValueForPublished: value });
      value.map((data, index) => {
        return data.key == "Custom Range"
          ? this.setState({ showDatePicker: true })
          : this.setState({ showDatePicker: false });
      });
      this.getStartAndEndDate(value, name);
    }
    if (name == "projectvalue") {
      this.setState({ selectedValueForProjectValue: value });
    }
    if (name == "projectlocation") {
      this.setState({ selectedValueForProjectLocation: value });
    }
    if (name == "closingdate") {
      this.setState({ selectedValueForClosingDate: value });
      value.map((data, index) => {
        return data.key == "Custom Range"
          ? this.setState({ showDatePickerForClosingDate: true })
          : this.setState({ showDatePickerForClosingDate: false });
      });

      this.getStartAndEndDate(value, name);
    }
    if (name == "servicetype") {
      this.setState({ selectedValuesForServiceType: value });
    }

    if (name == "sortBy") {
      if (value && value[0].key == "Closing Date") {
        let params = {
          closing_date: "desc"
        };
        this.fetchTendersListData(params);
      }
      if (value && value[0].key == "Latest Addition") {
        let params = {
          latest_addition: "desc"
        };
        this.fetchTendersListData(params);
        // let filteredData = [...this.state.filteredData];
        // var sortedData = [];
        // sortedData = filteredData.sort((a, b) => {
        //   return new Date(b.created_at) - new Date(a.created_at);
        // });
        // if (sortedData) {
        //   this.setState({ filteredData: sortedData });
        // }
      }
    }

    //more filters
    if (name == "noofbids") {
      this.setState({ selectedValueForNumberOfBids: value });
    }
    if (name == "subindustry") {
      this.setState({ selectedValueForSubIndustry: value });
    }
    if (name == "client") {
      this.setState({ selectedValueForClient: value });
    }
    if (name == "consultant") {
      this.setState({ selectedValueForConsultant: value });
    }
  };
  onRemove = (value, name) => {
    if (name == "projectvalue") {
      this.setState({ selectedValueForProjectValue: value });
    }
    if (name == "projectlocation") {
      this.setState({ selectedValueForProjectLocation: value });
    }
    if (name == "subindustry") {
      this.setState({ selectedValueForSubIndustry: value });
    }
    if (name == "client") {
      this.setState({ selectedValueForClient: value });
    }
    if (name == "consultant") {
      this.setState({ selectedValueForConsultant: value });
    }
  };

  getStartAndEndDate = (value, name) => {
    // console.log("inside date.." + value + ".." + name);
    var start = "";
    var end = "";

    // if (value[0].key == "1 day") {
    if (value == "1 day") {
      start = moment.utc().subtract(1, "day").startOf("day");
      end = moment.utc().subtract(1, "day").endOf("day");
    }
    if (value == "1 week") {
      start = moment.utc().subtract(1, "w").startOf("week");
      end = moment.utc().subtract(1, "w").endOf("week");
    }
    if (value == "3 weeks") {
      start = moment.utc().subtract(3, "w").startOf("week");
      end = moment.utc().subtract(1, "w").endOf("week");
    }
    if (value == "1 month") {
      start = moment.utc().subtract(1, "months").startOf("month");
      end = moment.utc().subtract(1, "months").endOf("month");
    }
    if (value == "3 months") {
      start = moment.utc().subtract(3, "months").startOf("month");
      end = moment.utc().subtract(1, "months").endOf("month");
    }
    if (value == "6 months") {
      start = moment.utc().subtract(6, "months").startOf("month");
      end = moment.utc().subtract(1, "months").endOf("month");
    }

    if (name == "publishedWithin" && start && end) {
      this.setState({ publishedDateFrom: start, publishedDateTo: end });
    }
    if (name == "closingDate" && start && end) {
      this.setState({ closingDateFrom: start, closingDateTo: end });
    }
  };
  onListItemClick = (tenderId) => {
    this.props.history.push(`/tenders/${tenderId}`);
  };

  onBidListClick = (tenderId) => {
    this.props.history.push(`/bidlist/${tenderId}`);
  }

  onTenderTitleClick = (event, tenderId) => {
    event.stopPropagation();
    // this.props.history.push(`/tenders/${tenderId}`);
    this.props.history.push({
      pathname: `/tenders/${tenderId}`,
      state: { fromTendersScreen: this.state.fromTendersScreen }
    });
  };

  onStartDateChange = (date) => {
    let formattedDate = moment(date).format("DD-MM-YYYY");

    this.setState({
      startDate: formattedDate,
      customStartDateForPublished: date
    });
  };
  onChangeEndDate = (date) => {
    let formattedDate = moment(date).format("DD-MM-YYYY");
    // let endDateValue = [{key:formattedDate,cat:""}];
    this.setState({ endDate: formattedDate, customEndDateForPublished: date });
  };
  onChangeForClosingStartDate = (date) => {
    // console.log("date..." + JSON.stringify(moment(date).utc()));

    let formattedDate = moment(date).format("DD-MM-YYYY");
    this.setState({
      startDateForClosing: formattedDate,
      customStartDateForClosing: date
    });
  };
  onChangeForClosingEndDate = (date) => {
    let formattedDate = moment(date).format("DD-MM-YYYY");
    this.setState({
      endDateForClosing: formattedDate,
      customEndDateForClosing: date
    });
  };

  onSearchButtonClick = () => {
    let {
      selectedValue,
      publishedDateFrom,
      publishedDateTo,
      closingDateFrom,
      closingDateTo,
      selectedValueForProjectValue,
      selectedValuesForServiceType,
      selectedValueForPublished,
      selectedValueForProjectLocation,
      selectedValueForSubIndustry,
      selectedValueForConsultant,
      selectedValueForClient,
      selectedValueForNumberOfBids,
      filter,
      dropdownVal,
      publishedWithinValue,
      closingDateValue,
      serviceTypeValue,
      noOfBidsValue
    } = this.state;

    //   "client": [""],
    // "consultant": [""],
    // "subintustry": [""]
    let params = {};

    if (filter) {
      params["latest_addition"] = "desc";
    }

    if (dropdownVal) {
      if (dropdownVal == "All") {
        params["latest_addition"] = "desc";
      } else if (dropdownVal != "Select") {
        params["tenderstatus"] = dropdownVal;
      }
    }
    if (publishedDateFrom && publishedDateTo) {
      params["publishedfrom"] = publishedDateFrom;
      params["publishedto"] = publishedDateTo;
    }
    if (closingDateFrom && closingDateTo) {
      params["closingdatefrom"] = closingDateFrom;
      params["closingdateto"] = closingDateTo;
    }
    if (
      selectedValueForProjectValue &&
      selectedValueForProjectValue.length > 0
    ) {
      var valueArray = [];
      for (var key in selectedValueForProjectValue) {
        valueArray.push(selectedValueForProjectValue[key].key);
      }
      params["projectvalue"] = valueArray;
    }
    if (
      selectedValueForProjectLocation &&
      selectedValueForProjectLocation.length > 0
    ) {
      var locationArray = [];
      for (var key in selectedValueForProjectLocation) {
        locationArray.push(selectedValueForProjectLocation[key].key);
      }
      params["projectlocation"] = locationArray;
    }

    if (serviceTypeValue && serviceTypeValue != "Select") {
      params["servicetype"] = serviceTypeValue;
    }
    if (selectedValueForNumberOfBids) {
      //params["numberofbids"] = selectedValueForNumberOfBids[0].key
    }
    if (selectedValueForSubIndustry) {
      let subIndustryArray = [];
      for (var key in selectedValueForSubIndustry) {
        subIndustryArray.push(selectedValueForSubIndustry[key].key);
      }
      params["subintustry"] = subIndustryArray;
    }
    if (selectedValueForClient) {
      let clientArray = [];
      for (var key in selectedValueForClient) {
        clientArray.push(selectedValueForClient[key].key);
      }
      params["client"] = clientArray;
    }
    if (selectedValueForConsultant) {
      let consultantArray = [];
      for (var key in selectedValueForConsultant) {
        consultantArray.push(selectedValueForConsultant[key].key);
      }
      params["consultant"] = consultantArray;
    }
    if (params) {
      this.fetchTendersListData(params, true);
    }
  };

  onApplyButtonClick = () => {
    let {
      startDate,
      endDate,
      customStartDateForPublished,
      customEndDateForPublished,
      selectedValueForPublished
    } = this.state;
    let customizedDate = "";
    if (startDate && endDate) {
      customizedDate = `${startDate} to ${endDate}`;
    } else {
      // customizedDate = "Custom Range";
      customizedDate = "";
    }

    this.setState({
      showDatePicker: false,
      //customizedDate: [{ key: customizedDate, cat: "" }],
      customizedDate: customizedDate,
      // publishedDateFrom: moment
      //   .utc(moment(customStartDateForPublished))
      //   .format(),
      // publishedDateTo: moment.utc(moment(customEndDateForPublished)).format()
      publishedDateFrom: moment(customStartDateForPublished).format(
        "YYYY-MM-DDTHH:mm:ss.SSS[Z]"
      ),
      publishedDateTo: moment(customEndDateForPublished).format(
        "YYYY-MM-DDTHH:mm:ss.SSS[Z]"
      ),
      publishedWithinValue: "Custom Range"
    });
  };
  onApplyButtonClickForClosingDate = () => {
    let {
      startDateForClosing,
      endDateForClosing,
      customStartDateForClosing,
      customEndDateForClosing
    } = this.state;
    let customizedDate = "";
    if (startDateForClosing && endDateForClosing) {
      customizedDate = `${startDateForClosing} to ${endDateForClosing}`;
    } else {
      //customizedDate = "Custom Range";
      customizedDate = "";
    }
    this.setState({
      showDatePickerForClosingDate: false,
      //customizedClosingDate: [{ key: customizedDate, cat: "" }],
      customizedClosingDate: customizedDate,
      closingDateFrom: moment(customStartDateForClosing).format(
        "YYYY-MM-DDTHH:mm:ss.SSS[Z]"
      ),
      //closingDateTo: moment.utc(moment(customEndDateForClosing)).format(),
      closingDateTo: moment(customEndDateForClosing).format(
        "YYYY-MM-DDTHH:mm:ss.SSS[Z]"
      ),
      closingDateValue: "Custom Range"
    });
  };
  resetFilters = () => {
    this.setState({
      filter: "",
      filteredValue: "",
      selectedValueForProjectLocation: "",
      selectedValue: "",
      showMore: true,
      selectedValue: "",
      publishedDateFrom: "",
      publishedDateTo: "",
      closingDateFrom: "",
      closingDateTo: "",
      selectedValueForProjectValue: "",
      selectedValuesForServiceType: "",
      selectedValueForPublished: "",
      selectedValueForClosingDate: "",
      selectedValueForProjectLocation: "",
      selectedValueForClient: "",
      selectedValueForConsultant: "",
      selectedValueForSubIndustry: "",

      startDate: "",
      endDate: "",
      startDateForClosing: "",
      endDateForClosing: "",
      customStartDateForPublished: "",
      customEndDateForPublished: "",
      customStartDateForClosing: "",
      customEndDateForClosing: "",
      customizedClosingDate: "",
      customizedDate: "",
      selectedValueForNumberOfBids: "",
      dropdownVal: "Select",
      publishedWithinValue: "Select",
      closingDateValue: "Select",
      serviceTypeValue: "Select",
      noOfBidsValue: "Select"
    });

    let params = {
      latest_addition: "desc"
    };
    this.fetchTendersListData(params);
  };
  onDraftTendersClick = () => {
    let filteredData = [...this.state.filteredData];
    let tenderListData = [...this.state.tendersList];

    var draftTenderData =
      tenderListData &&
      tenderListData.length > 0 &&
      tenderListData.filter((item) => {
        return (
          item.status &&
          (item.status.toLowerCase() == "drafts" ||
            item.status.toLowerCase() == "draft")
        );
      });
    if (draftTenderData && draftTenderData.length > 0) {
      this.setState({ filteredData: draftTenderData, showMore: true });
    } else {
      this.setState({ showMore: false });
    }
  };

  onShortlistIconClick = (e, index, id) => {
    e.stopPropagation();
    let filteredData = [...this.state.filteredData];
    let isShortlisted =
      filteredData[index].is_shortlisted == true ? false : true;
    let params = {
      tenderId: id,
      is_shortlisted: isShortlisted
    };

    if (filteredData[index].is_shortlisted == true) {
      filteredData[index].is_shortlisted = false;
      this.updateShortlist(params);
    } else {
      this.updateShortlist(params);
      filteredData[index].is_shortlisted = true;
    }
    this.setState({ filteredData: filteredData });
  };

  updateShortlist = (params) => {
    API.fetchData(API.Query.UPDATE_TENDERS_SHORTLIST, params).then((data) => {
      let params = {
        latest_addition: "desc"
      };
      this.fetchTendersListData(params);
    });
  };

  onShortlistedTendersClick = () => {
    let filteredData = [...this.state.filteredData];
    let tenderListData = [...this.state.tendersList];

    var shortlistData =
      tenderListData &&
      tenderListData.length > 0 &&
      tenderListData.filter((item) => {
        return item.is_shortlisted && item.is_shortlisted == true;
      });
    if (shortlistData && shortlistData.length > 0) {
      this.setState({ filteredData: shortlistData, showMore: true });
    } else {
      this.setState({ showMore: false });
    }
  };
  onCheckboxChange = (value) => {
    //console.log("value checkbox" + value);
  };
  onCheckboxChangeForBids = (value) => {
    this.setState({ isCheckedBids: value });
    if (value == true) {
      this.setState({ showBidsFilter: true });
    } else {
      this.setState({ showBidsFilter: false });
    }
  };
  onCheckboxChangeForSubIndustry = (value) => {
    this.setState({ isCheckedSubIndustry: value });
    if (value == true) {
      this.setState({ showSubIndustryFilter: true });
    } else {
      this.setState({ showSubIndustryFilter: false });
    }
  };
  onCheckboxChangeForClient = (value) => {
    // console.log("value checkbox" + value);
    this.setState({ isCheckedClient: value });
    if (value == true) {
      this.setState({ showClientFilter: true });
    } else {
      this.setState({ showClientFilter: false });
    }
  };
  onCheckboxChangeForConsultant = (value) => {
    this.setState({ isCheckedConsultant: value });
    if (value == true) {
      this.setState({ showConsultantFilter: true });
    } else {
      this.setState({ showConsultantFilter: false });
    }
  };

  hideModal = () => {
    this.setState({ isOpen: false, fromAddMore: false });
  };
  moreFiltersClick = () => {
    this.setState({ isOpen: true, fromAddMore: true });
  };
  onSaveButtonClick = () => {
    let {
      isCheckedBids,
      isCheckedClient,
      isCheckedConsultant,
      isCheckedSubIndustry
    } = this.state;
    if (isCheckedBids == true) {
      this.setState({ displayBidsFilterInScreen: true, isOpen: false });
    } else {
      this.setState({
        displayBidsFilterInScreen: false,
        isOpen: false,
        selectedValueForNumberOfBids: ""
      });
    }
    if (isCheckedSubIndustry == true) {
      this.setState({ displaySubIndustryFilterInScreen: true, isOpen: false });
    } else {
      this.setState({
        displaySubIndustryFilterInScreen: false,
        isOpen: false,
        selectedValueForSubIndustry: ""
      });
    }
    if (isCheckedClient == true) {
      this.setState({ displayClientFilterInScreen: true, isOpen: false });
    } else {
      this.setState({
        displayClientFilterInScreen: false,
        isOpen: false,
        selectedValueForClient: ""
      });
    }
    if (isCheckedConsultant == true) {
      this.setState({ displayConsultantFilterInScreen: true, isOpen: false });
    } else {
      this.setState({
        displayConsultantFilterInScreen: false,
        isOpen: false,
        selectedValueForConsultant: ""
      });
    }
    this.setState({ fromAddMore: false });
  };

  handleClickOutside(event) {
    //  alert('You clicked inside of me!' + this.wrapperRef);

    if (
      this.publishedCustomRangeRef &&
      this.publishedCustomRangeRef.contains(event.target)
    ) {
      // alert('outside click!');
    } else {
      this.setState({ showDatePicker: false });
    }
    if (
      this.closingDateCustomRangeRef &&
      this.closingDateCustomRangeRef.contains(event.target)
    ) {
      // alert('outside click');
    } else {
      this.setState({ showDatePickerForClosingDate: false });
    }

    if (
      this.singleSelectDropDownRef &&
      this.singleSelectDropDownRef.contains(event.target)
    ) {
      this.setState({ hideStatusOptions: false });
    } else {
      this.setState({ hideStatusOptions: true });
    }
  }
  setWrapperRef(node) {
    this.publishedCustomRangeRef = node;
  }
  setRefForClosingDate(node) {
    this.closingDateCustomRangeRef = node;
  }
  // setRef = (node, name) => {
  //   if (name == "status") {
  //     this.singleSelectDropDownRef = node;
  //   }
  // };
  toggle = (name) => {
    let { dropdownOpen } = this.state;
    if (name == "status") {
      this.setState({ dropdownOpen: !this.state.dropdownOpen });
    }
    if (name == "publishedWithin") {
      this.setState({ isOpenForPublished: !this.state.isOpenForPublished });
    }
    if (name == "closingDate") {
      this.setState({ isOpenForClosingDate: !this.state.isOpenForClosingDate });
    }
    if (name == "servicetype") {
      this.setState({ isOpenForServiceType: !this.state.isOpenForServiceType });
    }
    if (name == "noofbids") {
      this.setState({
        isOpenForNumberOfBids: !this.state.isOpenForNumberOfBids
      });
    }
    if (name == "sortby") {
      this.setState({
        isOpenForSorting: !this.state.isOpenForSorting
      });
    }
  };
  toggleForMoreFilter = (name) => {
    // console.log("event.name.." + name);
    let { dropdownOpen } = this.state;
    if (name == "status") {
      this.setState({ dropdownOpen: !this.state.dropdownOpen });
    }
    if (name == "publishedWithin") {
      this.setState({ isOpenForPublished: !this.state.isOpenForPublished });
    }
    if (name == "closingDate") {
      this.setState({ isOpenForClosingDate: !this.state.isOpenForClosingDate });
    }
    if (name == "servicetype") {
      this.setState({ isOpenForServiceType: !this.state.isOpenForServiceType });
    }
    if (name == "noofbids") {
      this.setState({
        isOpenForNumberOfBids: !this.state.isOpenForNumberOfBids
      });
    }
  };

  toggleItem = (value, name) => {
    // console.log("inside toggle item value..." + value);
    let {
      dropdownOpen,
      isOpenForPublished,
      isOpenForClosingDate,
      dropdownVal,
      publishedWithinValue,
      closingDateValue,
      isOpenForServiceType,
      isOpenForNumberOfBids
    } = this.state;
    if (name == "status") {
      // console.log("inside status.." + value);
      this.setState({
        dropdownOpen: !this.state.dropdownOpen,
        dropdownVal: value
      });
    }
    if (name == "publishedWithin") {
      this.setState({
        isOpenForPublished: !this.state.isOpenForPublished,
        publishedWithinValue: value
      });
      if (value == "Custom Range") {
        this.setState({ showDatePicker: true, publishedWithinValue: "Select" });
      } else {
        this.setState({ showDatePicker: false });
      }
      this.setState({ customizedDate: "" });
      this.getStartAndEndDate(value, name);
    }
    if (name == "closingDate") {
      this.setState({
        isOpenForClosingDate: !isOpenForClosingDate,
        closingDateValue: value
      });
      if (value == "Custom Range") {
        this.setState({
          showDatePickerForClosingDate: true,
          closingDateValue: "Select"
        });
      } else {
        this.setState({ showDatePickerForClosingDate: false });
      }
      this.setState({ customizedClosingDate: "" });

      this.getStartAndEndDate(value, name);
    }
    if (name == "servicetype") {
      this.setState({
        isOpenForServiceType: !isOpenForServiceType,
        serviceTypeValue: value
      });
    }
    if (name == "noofbids") {
      this.setState({
        isOpenForNumberOfBids: !isOpenForNumberOfBids,
        noOfBidsValue: value
      });
    }
    if (name == "sortby") {
      this.setState({
        isOpenForSorting: !this.state.isOpenForSorting,
        sortValue: value
      });
      if (value == "Closing Date") {
        let params = {
          closing_date: "desc"
        };
        this.fetchTendersListData(params);
      }
      if (value == "Latest Addition") {
        let params = {
          latest_addition: "desc"
        };
        this.fetchTendersListData(params);
      }
    }
  };

  toggleItemForMoreFilter = (value, name) => {
    let {
      dropdownOpen,
      isOpenForPublished,
      isOpenForClosingDate,
      dropdownVal,
      publishedWithinValue,
      closingDateValue,
      isOpenForServiceType,
      isOpenForNumberOfBids
    } = this.state;
    if (name == "status") {
      // console.log("inside status.." + value);
      this.setState({
        dropdownOpen: !this.state.dropdownOpen,
        dropdownVal: value
      });
    }
    if (name == "publishedWithin") {
      this.setState({
        isOpenForPublished: !this.state.isOpenForPublished,
        publishedWithinValue: value
      });
      if (value == "Custom Range") {
      } else {
        this.setState({ showDatePicker: false });
      }
      this.setState({ customizedDate: "" });

      this.getStartAndEndDate(value, name);
    }
    if (name == "closingDate") {
      this.setState({
        isOpenForClosingDate: !isOpenForClosingDate,
        closingDateValue: value
      });
      if (value == "Custom Range") {
        // this.setState({ showDatePickerForClosingDate: true });
      } else {
        this.setState({ showDatePickerForClosingDate: false });
      }
      this.setState({ customizedClosingDate: "" });

      this.getStartAndEndDate(value, name);
    }
    if (name == "servicetype") {
      this.setState({
        isOpenForServiceType: !isOpenForServiceType,
        serviceTypeValue: value
      });
    }
    if (name == "noofbids") {
      this.setState({
        isOpenForNumberOfBids: !isOpenForNumberOfBids,
        noOfBidsValue: value
      });
    }
  };
  // onSearch = (value) => {
  //   console.log("value in..." + value)
  //   this.setState({ inputValue: value})
  // }
  onSearchIconClick = () => {
    let {
      filter,
      dropdownVal,
      publishedWithinValue,
      closingDateValue,
      selectedValueForProjectLocation,
      selectedValueForProjectValue,
      serviceTypeValue,
      noOfBidsValue,
      selectedValueForClient,
      selectedValueForConsultant,
      selectedValueForSubIndustry
    } = this.state;
    if (
      filter ||
      (dropdownVal && dropdownVal != "Select") ||
      (publishedWithinValue && publishedWithinValue != "Select") ||
      (closingDateValue && closingDateValue != "Select") ||
      (selectedValueForProjectLocation &&
        selectedValueForProjectLocation.length > 0) ||
      (selectedValueForProjectValue &&
        selectedValueForProjectValue.length > 0) ||
      (serviceTypeValue && serviceTypeValue != "Select") ||
      (noOfBidsValue && noOfBidsValue != "Select") ||
      (selectedValueForClient && selectedValueForClient.length > 0) ||
      (selectedValueForConsultant && selectedValueForConsultant.length > 0) ||
      (selectedValueForSubIndustry && selectedValueForSubIndustry.length > 0)
    ) {
      this.onSearchButtonClick();
    }
  };
  render() {
    // console.log("filteredData.." + JSON.stringify(this.state.filteredData));
    const {
      filter,
      tendersList,
      selectedValue,
      selectedValueForPublished,
      options,
      publishedWithin,
      closingDate,
      projectLocation,
      projectValue,
      serviceType,
      selectedValueForClosingDate,
      selectedValueForProjectValue,
      selectedValueForProjectLocation,
      selectedValuesForServiceType,
      showDatePicker,
      showDatePickerForClosingDate,
      filteredValue,
      noOfItems,
      filteredData,
      sortingOptions,
      selectedFieldValue,
      startDate,
      endDate,
      showMore,
      customizedDate,
      customizedClosingDate,
      noRecordText,
      isCheckedBids,
      displayBidsFilterInScreen,
      isCheckedConsultant,
      isCheckedClient,
      isCheckedSubIndustry,
      showClientFilter,
      showConsultantFilter,
      showSubIndustryFilter,
      displaySubIndustryFilterInScreen,
      displayClientFilterInScreen,
      displayConsultantFilterInScreen,
      subIndustry,
      client,
      consultant,
      noOfBids,
      selectedValueForSubIndustry,
      selectedValueForClient,
      selectedValueForConsultant,
      selectedValueForNumberOfBids,
      isOpenForPublished,
      publishedWithinValue,
      closingDateValue,
      isOpenForClosingDate,
      isOpenForServiceType,
      serviceTypeValue,
      isOpenForNumberOfBids,
      noOfBidsValue,
      dropdownVal,
      fromAddMore,
      isOpenForSorting,
      sortValue,
      fromBuyerLogin
    } = this.state;

    var isDisabled = true;

    if (
      filter ||
      (dropdownVal && dropdownVal != "Select") ||
      (publishedWithinValue && publishedWithinValue != "Select") ||
      (closingDateValue && closingDateValue != "Select") ||
      (selectedValueForProjectLocation &&
        selectedValueForProjectLocation.length > 0) ||
      (selectedValueForProjectValue &&
        selectedValueForProjectValue.length > 0) ||
      (serviceTypeValue && serviceTypeValue != "Select") ||
      (noOfBidsValue && noOfBidsValue != "Select") ||
      (selectedValueForClient && selectedValueForClient.length > 0) ||
      (selectedValueForConsultant && selectedValueForConsultant.length > 0) ||
      (selectedValueForSubIndustry && selectedValueForSubIndustry.length > 0)
    ) {
      isDisabled = false;
    }

    var isDisabledSaveButton = true;
    if (
      isCheckedConsultant ||
      isCheckedBids ||
      isCheckedClient ||
      isCheckedSubIndustry
    ) {
      isDisabledSaveButton = false;
    }

    return (
      <div className="tenderListMainDiv">
        {/* <AllTenders status={this.props.match.params.status}/> */}

        <div className="d-flex row tenderHeadingMainDiv">
          <div className="heading tenderHeading col-12 col-md-4 col-lg-6">
            Tenders{" "}
            <span className="tendersIcon">
              {<IconComponent name="tenders" />}
            </span>
          </div>
          <div className="d-flex shortlistButtonMainDiv col-12 col-md-8 col-lg-6">
            <div onClick={this.onShortlistedTendersClick}>
              <Button color="link" size="sm">
                Shortlisted Tenders
              </Button>
            </div>
            {fromBuyerLogin ? (
              <>
                <div
                  className="draftsTenders"
                  onClick={this.onDraftTendersClick}
                >
                  <Button color="link" size="sm">
                    Draft Tenders
                  </Button>
                </div>
                <div>
                  <Link to="/tenders/create">
                    <Button color="primary" size="sm">
                      Create New Tender
                    </Button>
                  </Link>
                </div>
              </>
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="serachAndFiltersMainDiv">
          <Row className="ml-0 mb-2 pl-0">
            <Col md="6" className="pr-0 pl-0">
              <InputGroupField
                addOnType="append"
                iconName="search"
                type="text"
                name="search"
                id="searchField"
                placeholder="Search by Tender ID, Tender Title, project name, etc.... "
                handleInputChange={this.onChangeInput}
                value={this.state.filter}
                classNames="searchInput"
                onSearchIconClick={this.onSearchIconClick}
              />
            </Col>
          </Row>
          <div className="d-flex mt-4 filterHeadingMainDiv row">
            <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
              <div className="pl-0 pb-1">Tender Status</div>
              <div
                className="pr-0 pl-0 filterSubDiv"
                // ref={(node) => this.setRef(node, "status")}
              >
                <DropDown
                  options={options}
                  //isOpen={this.state.dropdownOpen}
                  isOpen={
                    this.state.isOpen == false ? this.state.dropdownOpen : ""
                  }
                  toggleItem={this.toggleItem}
                  toggle={this.toggle}
                  dropdownVal={this.state.dropdownVal}
                  direction="down"
                  size="lg"
                  name="status"
                  fromAddMore={fromAddMore}
                  icon={<IconComponent name="downarrow" />}
                  classNames={
                    this.state.dropdownVal == "Select"
                      ? {
                          dropdownStyle: "defaultDropdownDiv",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                      : {
                          dropdownStyle: "dropdownDiv",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                  }
                />
              </div>
            </div>
            <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
              <div className="pb-1">Published within</div>
              <div className="pr-0">
                <DropDown
                  options={publishedWithin}
                  isOpen={this.state.isOpen == false ? isOpenForPublished : ""}
                  toggleItem={this.toggleItem}
                  toggle={this.toggle}
                  dropdownVal={
                    customizedDate ? customizedDate : publishedWithinValue
                  }
                  direction="down"
                  size="lg"
                  name="publishedWithin"
                  fromAddMore={fromAddMore}
                  icon={<IconComponent name="upanddownarrow" />}
                  classNames={
                    publishedWithinValue == "Select"
                      ? {
                          dropdownStyle: "defaultDropdownDiv",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                      : customizedDate
                      ? {
                          dropdownStyle: "dropdownDivForCustomRange",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                      : {
                          dropdownStyle: "dropdownDiv",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                  }
                />
              </div>
            </div>
            <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
              <div className="pb-1">Closing date</div>
              <div className="pr-0">
                <DropDown
                  options={closingDate}
                  isOpen={
                    this.state.isOpen == false ? isOpenForClosingDate : ""
                  }
                  toggleItem={this.toggleItem}
                  toggle={this.toggle}
                  dropdownVal={
                    customizedClosingDate
                      ? customizedClosingDate
                      : closingDateValue
                  }
                  direction="down"
                  size="lg"
                  name="closingDate"
                  fromAddMore={fromAddMore}
                  icon={<IconComponent name="upanddownarrow" />}
                  classNames={
                    closingDateValue == "Select"
                      ? {
                          dropdownStyle: "defaultDropdownDiv",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                      : customizedClosingDate
                      ? {
                          dropdownStyle: "dropdownDivForCustomRange",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                      : {
                          dropdownStyle: "dropdownDiv",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                  }
                />
              </div>
            </div>
            <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
              <div className="pb-1">Project Value (AED)</div>
              <div className="pr-0">
                <MultiSelectComponent
                  options={projectValue}
                  displayValue="key"
                  showCheckbox={true}
                  onSelect={this.onSelect}
                  onRemove={this.onRemove}
                  styles={this.style}
                  closeIcon=""
                  selectedValues={selectedValueForProjectValue}
                  // selectedValues={selectedValueForProjectValue && selectedValueForProjectValue.length > 0 && selectedValueForProjectValue.slice(0,1)}
                  ref={this.projectValueRef}
                  name="projectvalue"
                  singleSelect={false}
                />
              </div>
            </div>
            <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
              <div className="pb-1">Project Location</div>
              <div className="pr-0">
                <MultiSelectComponent
                  options={projectLocation}
                  displayValue="key"
                  showCheckbox={true}
                  onSelect={this.onSelect}
                  onRemove={this.onRemove}
                  styles={this.style}
                  closeIcon=""
                  ref={this.projectLocationRef}
                  selectedValues={selectedValueForProjectLocation}
                  name="projectlocation"
                  singleSelect={false}
                />
              </div>
            </div>
          </div>
          <div className="d-flex row">
            <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
              <div className="mt-3 pl-0 filterHeadingMainDiv fontStyle">
                Service type
              </div>
              <div className="mt-1 mb-3 pr-0 pl-0 filterSubDiv">
                <DropDown
                  options={serviceType}
                  isOpen={
                    this.state.isOpen == false ? isOpenForServiceType : ""
                  }
                  toggleItem={this.toggleItem}
                  toggle={this.toggle}
                  dropdownVal={serviceTypeValue}
                  direction="down"
                  size="lg"
                  name="servicetype"
                  icon={<IconComponent name="downarrow" />}
                  fromAddMore={fromAddMore}
                  classNames={
                    serviceTypeValue == "Select"
                      ? {
                          dropdownStyle: "defaultDropdownDiv",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                      : {
                          dropdownStyle: "dropdownDiv",
                          dropDownMenu: "dropDownMenu",
                          dropDownItem: "dropDownItem"
                        }
                  }
                />
                {/* <MultiSelectComponent
                  options={serviceType}
                  displayValue="key"
                  showCheckbox={true}
                  onSelect={this.onSelect}
                  styles={this.style}
                  closeIcon=""
                  selectedValues={selectedValuesForServiceType}
                  ref={this.serviceTypeRef}
                  singleSelect={true}
                  name="servicetype"
                  onRemove={this.onRemove}
                /> */}
              </div>
            </div>

            {displayBidsFilterInScreen ? (
              <div className="col-12 col-sm-6 col-md-6 col-lg-2 pr-0 filtersDiv">
                <div className="mt-3 filterHeadingMainDiv fontStyle ">
                  Number of Bids
                </div>
                <div className="mt-1 mb-3 pr-0 filterSubDiv">
                  <DropDown
                    options={noOfBids}
                    isOpen={isOpenForNumberOfBids}
                    toggleItem={this.toggleItem}
                    toggle={this.toggle}
                    dropdownVal={noOfBidsValue}
                    direction="down"
                    size="lg"
                    name="noofbids"
                    icon={<IconComponent name="downarrow" />}
                    fromAddMore={fromAddMore}
                    classNames={
                      noOfBidsValue == "Select"
                        ? {
                            dropdownStyle: "defaultDropdownDiv",
                            dropDownMenu: "dropDownMenu",
                            dropDownItem: "dropDownItem"
                          }
                        : {
                            dropdownStyle: "dropdownDiv",
                            dropDownMenu: "dropDownMenu",
                            dropDownItem: "dropDownItem"
                          }
                    }
                  />
                </div>
              </div>
            ) : (
              ""
            )}
            {displaySubIndustryFilterInScreen ? (
              <div className="col-12 col-sm-6 col-md-6 col-lg-2 pr-0 filtersDiv">
                <div className="mt-3 filterHeadingMainDiv fontStyle">
                  Sub Industry
                </div>
                <div className="mt-1 mb-3 pr-0 filterSubDiv">
                  <MultiSelectComponent
                    options={subIndustry}
                    displayValue="key"
                    onSelect={this.onSelect}
                    styles={this.style}
                    closeIcon=""
                    selectedValues={selectedValueForSubIndustry}
                    ref={this.serviceTypeRef}
                    name="subindustry"
                    onRemove={this.onRemove}
                    singleSelect={false}
                    showCheckbox={true}

                    // singleSelect={false}
                  />
                </div>
              </div>
            ) : (
              ""
            )}
            {displayClientFilterInScreen ? (
              <div className="col-12 col-sm-6 col-md-6 col-lg-2 pr-0 filtersDiv">
                <div className="mt-3 filterHeadingMainDiv fontStyle">
                  Client
                </div>
                <div className="mt-1 mb-3 pr-0 filterSubDiv">
                  <MultiSelectComponent
                    options={client}
                    displayValue="key"
                    onSelect={this.onSelect}
                    styles={this.style}
                    closeIcon=""
                    selectedValues={selectedValueForClient}
                    //ref={this.serviceTypeRef}
                    name="client"
                    onRemove={this.onRemove}
                    singleSelect={false}
                    showCheckbox={true}

                    // singleSelect={false}
                  />
                </div>
              </div>
            ) : (
              ""
            )}
            {displayConsultantFilterInScreen ? (
              <div className="col-12 col-sm-6 col-md-6 col-lg-2 pr-0 filtersDiv">
                <div className="mt-3 filterHeadingMainDiv fontStyle">
                  Consultant
                </div>
                <div className="mt-1 mb-3 pr-0 filterSubDiv">
                  <MultiSelectComponent
                    options={consultant}
                    displayValue="key"
                    onSelect={this.onSelect}
                    styles={this.style}
                    closeIcon=""
                    selectedValues={selectedValueForConsultant}
                    //ref={this.serviceTypeRef}
                    name="consultant"
                    onRemove={this.onRemove}
                    singleSelect={false}
                    showCheckbox={true}
                    // singleSelect={false}
                  />
                </div>
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className="d-flex filtersDiv datePickerDiv"
            ref={this.setWrapperRef}
          >
            {showDatePicker && (
              <div className="datePickerMainDiv">
                <>
                  {" "}
                  <div className="d-flex fromToMainDiv">
                    <div className="datepickerFromButton">
                      <Button variant="date" block size="sm" color="primary">
                        FROM
                      </Button>
                    </div>
                    <div className="datepickerToButton">
                      <Button variant="date" block size="sm" color="primary">
                        TO
                      </Button>
                    </div>
                  </div>
                  <div className="d-flex">
                    <DatepickerComponent
                      selected={this.state.selected}
                      onChange={this.onChangeDate}
                      isFromTenders={true}
                      onChange={this.onStartDateChange}
                      onChangeEndDate={this.onChangeEndDate}
                      maxDate={new Date()}
                      showYearDropdown={true}
                    />
                  </div>
                  <div className="mt-1 d-flex justify-content-end applyButtonDiv">
                    <div className="col-2">
                      {" "}
                      <Button color="primary" onClick={this.onApplyButtonClick}>
                        Apply
                      </Button>
                    </div>
                  </div>
                </>
              </div>
            )}

            {showDatePickerForClosingDate && (
              <div
                className="datePickerMainDivForClosingDate"
                ref={this.setRefForClosingDate}
              >
                <>
                  {" "}
                  <div className="d-flex fromToMainDiv">
                    <div className="datepickerFromButton">
                      <Button variant="date" block size="sm" color="primary">
                        FROM
                      </Button>
                    </div>
                    <div className="datepickerToButton">
                      <Button variant="date" block size="sm" color="primary">
                        TO
                      </Button>
                    </div>
                  </div>
                  <div className="d-flex">
                    <DatepickerComponent
                      selected={this.state.selected}
                      onChange={this.onChangeClosingDate}
                      isFromTenders={true}
                      onChange={this.onChangeForClosingStartDate}
                      onChangeEndDate={this.onChangeForClosingEndDate}
                      showYearDropdown={true}
                    />
                  </div>
                  <div className="mt-1 d-flex justify-content-end applyButtonDiv">
                    <div className="col-2">
                      {" "}
                      <Button
                        color="primary"
                        onClick={this.onApplyButtonClickForClosingDate}
                      >
                        Apply
                      </Button>
                    </div>
                  </div>
                </>
              </div>
            )}
          </div>

          {/* <div> */}
          <div className="d-flex searchButtonMainDiv">
            <div className="mr-2">
              <Button
                color="primary"
                size="sm"
                onClick={this.onSearchButtonClick}
                disabled={isDisabled}
              >
                Apply Filter
              </Button>
            </div>
            <div>
              <Button
                variant="secondary"
                size="sm"
                outline
                onClick={this.resetFilters}
              >
                Clear All
              </Button>
            </div>
            <div className="d-flex">
              <div className="addMoreIcon">
                <IconComponent name="more_filter" />
              </div>
              <div onClick={this.moreFiltersClick} className="moreFilterStyle">
                <Button color="link" size="sm">
                  more filters
                </Button>
              </div>
            </div>
          </div>
        </div>

        {filteredData && filteredData.length > 0 && (
          <div className="d-flex sortMainDiv">
            <div className="noOfTendersDiv">
              <span className="noOfTenders">
                {filteredData && filteredData.length}
              </span>{" "}
              <span>{filteredData.length == 1 ? "Tender" : "Tenders"} </span>{" "}
              listed matching your requirement
            </div>

            <div>
              <div className="d-flex sortByMainDiv">
                <div className="sortBy">Sort By</div>

                <div className="sortByDropdownDiv">
                  <DropDown
                    options={sortingOptions}
                    isOpen={isOpenForSorting}
                    toggleItem={this.toggleItem}
                    toggle={this.toggle}
                    dropdownVal={sortValue}
                    direction="down"
                    size="lg"
                    name="sortby"
                    icon={<IconComponent name="downarrow" />}
                    fromAddMore={fromAddMore}
                    classNames={
                      sortValue == "Select"
                        ? {
                            dropdownStyle: "defaultDropdownDivForSort",
                            dropDownMenu: "dropDownMenu",
                            dropDownItem: "dropDownItem"
                          }
                        : {
                            dropdownStyle: "dropdownDivForSort",
                            dropDownMenu: "dropDownMenu",
                            dropDownItem: "dropDownItem"
                          }
                    }
                  />
                </div>
              </div>
            </div>
          </div>
        )}
        <div className="listItemMainDiv">
          {tendersList && filteredData && filteredData.length > 0 ? (
            filteredData
              .slice(0, this.state.noOfItems)
              .map((tenderData, index) => {
                let {
                  tender_title,
                  id,
                  tender_value,
                  service_category,
                  description,
                  status,
                  project_status,
                  closing_date,
                  project_location,
                  is_shortlisted,
                  bids_list_aggregate,
                  questions_and_answer_aggregate,
                  questions,
                  client,
                  consultant,
                  project_value,
                  closeDate
                } = tenderData;
                return (
                  <div
                    onClick={() => this.onListItemClick(tenderData.id)}
                    className="cursorPointer"
                  >
                    <div
                      className={
                        status != null &&
                        status &&
                        status.toLowerCase() == "open"
                          ? `listItemStyle openTenderStyle`
                          : status && status.toLowerCase() == "withdrawn"
                          ? `listItemStyle withdrawnTenderStyle`
                          : status && status.toLowerCase() == "closed"
                          ? `listItemStyle closedTenderStyle`
                          : status && status.toLowerCase() == "hold"
                          ? "listItemStyle withdrawnTenderStyle"
                          : "listItemStyle defaultTenderStyle"
                      }
                    >
                      <Row className="itemRowStyle">
                        <Col md="9" lg="9">
                          <div className="d-flex">
                            <div
                              onClick={(e) =>
                                this.onShortlistIconClick(e, index, id)
                              }
                            >
                              <IconComponent
                                name={
                                  is_shortlisted
                                    ? "shortlisted"
                                    : "not_shortlisted"
                                }
                              />

                              {/* {is_shortlisted ? (
                                <IconComponent name="shortlisted" />
                              ) : (
                                <IconComponent name="not_shortlisted" />
                              )} */}
                            </div>
                            <div
                              className="tenderTilte cursorPointer"
                              onClick={(e) =>
                                this.onTenderTitleClick(e, tenderData.id)
                              }
                            >
                              {tender_title ? tender_title : ""}
                            </div>
                            <div className="tenderIdStyle">
                              Tender ID : {id ? id : ""}
                            </div>
                          </div>
                        </Col>

                        {fromBuyerLogin ? (
                          <Col md="3" lg="3" className="text-align-right">
                            <div className="d-flex">
                              {bids_list_aggregate ? (
                                <div onClick={(e) => {
                                  e.stopPropagation();
                                  if(bids_list_aggregate.aggregate &&
                                    bids_list_aggregate.aggregate.count){
                                      this.onBidListClick(tenderData.id);
                                    }
                                }}>
                                  <BadgeComponent
                                    className="cursorPointer"
                                    value={
                                      <>
                                        <span className="bidsStyle">
                                            {bids_list_aggregate.aggregate &&
                                              bids_list_aggregate.aggregate.count}
                                          </span>{" "}
                                          <span className="bidsTextStyle">
                                            bids
                                          </span>
                                      </>
                                    }
                                    badgeStyles={
                                      bids_list_aggregate.aggregate &&
                                      bids_list_aggregate.aggregate.count == 0
                                        ? "zeroBidBadge"
                                        : bids_list_aggregate.aggregate &&
                                          bids_list_aggregate.aggregate.count <
                                            5
                                        ? "lessthanFiveBidBadge"
                                        : bids_list_aggregate.aggregate &&
                                          bids_list_aggregate.aggregate.count >=
                                            5
                                        ? "moreThanFiveBidBadge"
                                        : ""
                                    }
                                  />
                                </div>
                              ) : (
                                ""
                              )}
                              {questions_and_answer_aggregate &&
                              questions_and_answer_aggregate.count &&
                              (questions_and_answer_aggregate.count.count ==
                                0 ||
                                questions_and_answer_aggregate.count.count) ? (
                                <div>
                                  <Button
                                    // variant={
                                    //   questions == "N/A"
                                    //     ? "normalOutline"
                                    //     : "outline"
                                    // }
                                    outline
                                    color="info"
                                    // label={ `${questions_and_answer_aggregate.count.count} new questions`
                                    // }
                                  >{questions_and_answer_aggregate.count.count} new questions
                                   
                                  </Button>
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                          </Col>
                        ) : (
                          ""
                        )}
                      </Row>
                      <Row>
                        <Col md="12">
                          <div className="categoryDiv">
                            Category :
                            <span className="categoryValue">
                              {" "}
                              {service_category ? service_category : ""}
                            </span>
                          </div>
                        </Col>
                      </Row>
                      <Row className="">
                        <Col md="9" lg="9">
                          <div className="d-flex">
                            <div>
                              {description ? (
                                <div
                                  className="smr-par"
                                  dangerouslySetInnerHTML={{
                                    __html: description
                                  }}
                                ></div>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                        </Col>
                        <Col md="3" lg="3" className="">
                          <div className="d-flex locatioDiv">
                            <div className="locationStyle">
                              <IconComponent name="location" />
                            </div>
                            <div>
                              {project_location ? project_location : ""}
                            </div>
                          </div>
                          <div className="d-flex">
                            <div className="closingDate">
                              <IconComponent name="closing_date" />
                            </div>
                            <div>
                              {closing_date
                                ? moment(closing_date).format("DD MMM YYYY")
                                : ""}
                            </div>
                          </div>
                        </Col>
                      </Row>
                      <Row className="itemStyle">
                        <Col md="9" lg="9">
                          <div className="d-flex">
                            <div className="lastRowStyle">
                              Tender Status :
                              <span
                                className={
                                  status && status.toLowerCase() == "open"
                                    ? `openTenderText`
                                    : status &&
                                      status.toLowerCase() == "withdrawn"
                                    ? status && `withdrawnTenderText`
                                    : status && status.toLowerCase() == "closed"
                                    ? `closedTenderText`
                                    : status && status.toLowerCase() == "hold"
                                    ? "withdrawnTenderText"
                                    : ""
                                }
                              >
                                {" "}
                                {status ? status : ""}
                              </span>
                            </div>
                            <div className="lastRowStyle">
                              Project Status :
                              <span className="font-weight-bold">
                                {" "}
                                {project_status ? project_status : ""}
                              </span>
                            </div>
                            <div className="lastRowStyle">
                              Client :{" "}
                              <span className="font-weight-bold">
                                {client ? client : ""}
                              </span>
                            </div>
                            <div>
                              Consultant :{" "}
                              <span className="font-weight-bold">
                                {consultant ? consultant : ""}
                              </span>
                            </div>
                          </div>
                        </Col>
                        <Col md="3" lg="3" className="text-align-right">
                          <div className="d-flex">
                            <div className="tenderValue">
                              <IconComponent name="tender_value" />
                            </div>
                            <div>{project_value ? project_value : ""}</div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </div>
                );
              })
          ) : (
            <div align="center">{noRecordText ? noRecordText : ""} </div>
          )}
        </div>
        {showMore == false ? (
          <div align="center" className="mt-3">
            No More Data
          </div>
        ) : filteredData && filteredData.length > 0 ? (
          <div className="d-flex justify-content-center showMoreButtonDiv">
            <div className="col-6 col-sm-4 col-md-3 col-lg-2">
              <Button color="primary" size="sm" onClick={this.onShowMoreClick}>
                Show More
              </Button>
            </div>
          </div>
        ) : (
          ""
        )}
        {/* </div> */}
        <Modal size="big" isOpen={this.state.isOpen} hide={this.hideModal}>
          <Modal.Content>
            <div className="modalMainDiv">
              <div className="modalHeader">
                <div className="d-flex">
                  <div className="addFilter heading">Add filter</div>
                  <div className="closeIcon" onClick={this.hideModal}>
                    <IconComponent name="close" />
                  </div>
                </div>
                <div className="modalSubHeadingText">
                  PICK FROM OVER 10 FILTERS
                </div>
              </div>

              <input
                id="replace"
                name="replace"
                value="replace"
                style={{ visibility: "hidden", height: 0 }}
              ></input>

              <div className="d-flex mt-2 filterHeadingMainDiv row ml-0">
                <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
                  <div className="pl-0 pb-1">Tender Status</div>
                  <div className="pr-0 pl-0">
                    <DropDown
                      options={options}
                      isOpen={this.state.dropdownOpen}
                      toggleItemForMoreFilter={this.toggleItemForMoreFilter}
                      toggleForMoreFilter={this.toggleForMoreFilter}
                      dropdownVal={this.state.dropdownVal}
                      direction="down"
                      size="lg"
                      name="status"
                      icon={<IconComponent name="upanddownarrow" />}
                      fromAddMore={fromAddMore}
                      classNames={
                        this.state.dropdownVal == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                </div>
                <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
                  <div className="pb-1">Published within</div>
                  <div className="pr-0">
                    <DropDown
                      options={publishedWithin}
                      isOpen={isOpenForPublished}
                      //toggleItem={this.toggleItem}
                      //toggle={this.toggle}
                      dropdownVal={
                        customizedDate ? customizedDate : publishedWithinValue
                      }
                      direction="down"
                      size="lg"
                      name="publishedWithin"
                      icon={<IconComponent name="upanddownarrow" />}
                      fromAddMore={fromAddMore}
                      toggleItemForMoreFilter={this.toggleItemForMoreFilter}
                      toggleForMoreFilter={this.toggleForMoreFilter}
                      classNames={
                        publishedWithinValue == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : customizedDate
                          ? {
                              dropdownStyle: "dropdownDivForCustomRange",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                </div>
                <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
                  <div className="pb-1">Closing date</div>
                  <div className="pr-0">
                    <DropDown
                      options={closingDate}
                      isOpen={isOpenForClosingDate}
                      //toggleItem={this.toggleItem}
                      //toggle={this.toggle}
                      dropdownVal={
                        customizedClosingDate
                          ? customizedClosingDate
                          : closingDateValue
                      }
                      direction="down"
                      size="lg"
                      fromAddMore={fromAddMore}
                      toggleItemForMoreFilter={this.toggleItemForMoreFilter}
                      toggleForMoreFilter={this.toggleForMoreFilter}
                      icon={<IconComponent name="upanddownarrow" />}
                      name="closingDate"
                      classNames={
                        closingDateValue == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : customizedClosingDate
                          ? {
                              dropdownStyle: "dropdownDivForCustomRange",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                </div>
                <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0 disableFocus">
                  <div className="pb-1">Project Value </div>
                  <div className="pr-0 ">
                    <MultiSelectComponent
                      options={projectValue}
                      displayValue="key"
                      showCheckbox={true}
                      onSelect={this.onSelect}
                      onRemove={this.onRemove}
                      styles={this.style}
                      closeIcon=""
                      selectedValues={selectedValueForProjectValue}
                      // selectedValues={selectedValueForProjectValue && selectedValueForProjectValue.length > 0 && selectedValueForProjectValue.slice(0,1)}
                      ref={this.projectValueRef}
                      name="projectvalue"
                    />
                  </div>
                </div>
                <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0">
                  <div className="pb-1">Project Location</div>
                  <div className="pr-0">
                    <MultiSelectComponent
                      options={projectLocation}
                      displayValue="key"
                      showCheckbox={true}
                      onSelect={this.onSelect}
                      onRemove={this.onRemove}
                      styles={this.style}
                      closeIcon=""
                      ref={this.projectLocationRef}
                      selectedValues={selectedValueForProjectLocation}
                      name="projectlocation"
                    />
                  </div>
                </div>
              </div>
              <div className="d-flex filtersList row ml-0">
                {/* <div className="col-2 mt-3 filterHeadingMainDiv fontStyle">
                  Service type
                </div> */}
                <div className="filtersDiv col-12 col-sm-6 col-md-6 col-lg-2 pr-0 serviceTypeDiv">
                  <div className="mt-3 pl-0 filterHeadingMainDiv fontStyle">
                    Service type
                  </div>
                  <div className="mt-1 mb-3 pr-0 pl-0 filterSubDiv">
                    <DropDown
                      options={serviceType}
                      isOpen={isOpenForServiceType}
                      //toggleItem={this.toggleItem}
                      //toggle={this.toggle}
                      dropdownVal={serviceTypeValue}
                      direction="down"
                      size="lg"
                      name="servicetype"
                      icon={<IconComponent name="downarrow" />}
                      fromAddMore={fromAddMore}
                      toggleItemForMoreFilter={this.toggleItemForMoreFilter}
                      toggleForMoreFilter={this.toggleForMoreFilter}
                      classNames={
                        serviceTypeValue == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                </div>
                {this.state.showBidsFilter ? (
                  // <div className="col-2 mt-3 filterHeadingMainDiv fontStyle">
                  //   Number of Bids{" "}
                  // </div>
                  <div className="col-12 col-sm-6 col-md-6 col-lg-2 pr-0 filtersDiv">
                    <div className="mt-3 filterHeadingMainDiv fontStyle ">
                      Number of Bids
                    </div>
                    <div className="mt-1 mb-3 pr-0 filterSubDiv">
                      <DropDown
                        options={noOfBids}
                        isOpen={isOpenForNumberOfBids}
                        //toggleItem={this.toggleItem}
                        //toggle={this.toggle}
                        dropdownVal={noOfBidsValue}
                        direction="down"
                        size="lg"
                        name="noofbids"
                        icon={<IconComponent name="downarrow" />}
                        fromAddMore={fromAddMore}
                        toggleItemForMoreFilter={this.toggleItemForMoreFilter}
                        toggleForMoreFilter={this.toggleForMoreFilter}
                        classNames={
                          noOfBidsValue == "Select"
                            ? {
                                dropdownStyle: "defaultDropdownDiv",
                                dropDownMenu: "dropDownMenu",
                                dropDownItem: "dropDownItem"
                              }
                            : {
                                dropdownStyle: "dropdownDiv",
                                dropDownMenu: "dropDownMenu",
                                dropDownItem: "dropDownItem"
                              }
                        }
                      />
                    </div>
                  </div>
                ) : (
                  ""
                )}
                {this.state.showSubIndustryFilter ? (
                  <div className="col-12 col-sm-6 col-md-6 col-lg-2 pr-0 filtersDiv">
                    <div className="mt-3 filterHeadingMainDiv fontStyle">
                      Sub Industry
                    </div>
                    <div className="mt-1 mb-3 pr-0 filterSubDiv">
                      <MultiSelectComponent
                        options={subIndustry}
                        displayValue="key"
                        onSelect={this.onSelect}
                        styles={this.style}
                        closeIcon=""
                        selectedValues={selectedValueForSubIndustry}
                        ref={this.serviceTypeRef}
                        name="subindustry"
                        onRemove={this.onRemove}
                        singleSelect={false}
                        showCheckbox={true}
                        // singleSelect={false}
                      />
                    </div>
                  </div>
                ) : (
                  ""
                )}
                {this.state.showClientFilter ? (
                  <div className="col-12 col-sm-6 col-md-6 col-lg-2 pr-0 filtersDiv">
                    <div className="mt-3 filterHeadingMainDiv fontStyle">
                      Client
                    </div>
                    <div className="mt-1 mb-3 pr-0 filterSubDiv">
                      <MultiSelectComponent
                        options={client}
                        displayValue="key"
                        onSelect={this.onSelect}
                        styles={this.style}
                        closeIcon=""
                        selectedValues={selectedValueForClient}
                        //ref={this.serviceTypeRef}
                        name="client"
                        onRemove={this.onRemove}
                        singleSelect={false}
                        showCheckbox={true}
                        // singleSelect={false}
                      />
                    </div>
                  </div>
                ) : (
                  ""
                )}
                {this.state.showConsultantFilter ? (
                  // <div className="col-2 mt-3 filterHeadingMainDiv fontStyle">
                  //   Consultant
                  // </div>
                  <div className="col-12 col-sm-6 col-md-6 col-lg-2 pr-0 filtersDiv">
                    <div className="mt-3 filterHeadingMainDiv fontStyle">
                      Consultant
                    </div>
                    <div className="mt-1 mb-3 pr-0 filterSubDiv">
                      <MultiSelectComponent
                        options={consultant}
                        displayValue="key"
                        onSelect={this.onSelect}
                        styles={this.style}
                        closeIcon=""
                        selectedValues={selectedValueForConsultant}
                        //ref={this.serviceTypeRef}
                        name="consultant"
                        onRemove={this.onRemove}
                        singleSelect={false}
                        showCheckbox={true}
                        // singleSelect={false}
                      />
                    </div>
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="d-flex filtersDiv"></div>

              <div className="d-flex">
                <div className="filterCheckboxMainDiv col-3">
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        onCheckboxChange={this.onCheckboxChange}
                        isChecked={true}
                        isDisabled={true}
                      />
                    </div>
                    <div className="disabledFilterText">Tender Status</div>
                  </div>
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        isChecked={true}
                        isDisabled={true}
                      />
                    </div>
                    <div className="disabledFilterText">Published Within</div>
                  </div>
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        isChecked={true}
                        isDisabled={true}
                      />
                    </div>
                    <div className="disabledFilterText">Closing Date</div>
                  </div>
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        isChecked={true}
                        isDisabled={true}
                      />
                    </div>
                    <div className="disabledFilterText">Project Value</div>
                  </div>
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        isChecked={true}
                        isDisabled={true}
                      />
                    </div>
                    <div className="disabledFilterText">Project Location</div>
                  </div>
                </div>

                <div className="filterCheckboxMainDiv col-4">
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        onCheckboxChange={this.onCheckboxChange}
                        isChecked={true}
                        isDisabled={true}
                      />
                    </div>
                    <div className="disabledFilterText">Service Type</div>
                  </div>
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        isChecked={isCheckedBids}
                        // isDisabled={true}
                        // checked={}
                        onCheckboxChange={this.onCheckboxChangeForBids}
                      />
                    </div>
                    Number of Bids
                  </div>
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        isChecked={isCheckedSubIndustry}
                        // isDisabled={true}
                        // checked={}
                        onCheckboxChange={this.onCheckboxChangeForSubIndustry}
                      />
                    </div>
                    <div>Sub Industry</div>
                  </div>
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        isChecked={isCheckedClient}
                        onCheckboxChange={this.onCheckboxChangeForClient}
                      />
                    </div>
                    <div>Client</div>
                  </div>
                  <div className="d-flex checkBoxWithContent">
                    <div>
                      <CheckBox
                        variant="defaultCheckbox"
                        isChecked={isCheckedConsultant}
                        onCheckboxChange={this.onCheckboxChangeForConsultant}
                      />
                    </div>
                    <div>Consultant</div>
                  </div>
                </div>
              </div>
              <div className="mt-1 d-flex saveButton">
                <div className="saveButtonSubDiv col-2">
                  <div className="saveButtonDiv">
                    {" "}
                    <Button
                      color="primary"
                      size="sm"
                      onClick={this.onSaveButtonClick}
                      isDisabled={isDisabledSaveButton}
                    >
                      Save
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}
Tenders.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  canClose: PropTypes.bool,
  hide: PropTypes.func
};
Tenders.defaultProps = {
  //name: "Unit Tender",
  isOpen: true,
  canClose: false,
  hide: null
};

export default connect(null, mapDispatchToProps)(Tenders);
