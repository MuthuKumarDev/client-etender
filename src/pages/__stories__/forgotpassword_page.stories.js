//Forgot Password Page Stories
import React from "react";
import { withKnobs } from "@storybook/addon-knobs";

import ForgotPassword from "../Forgot_Password";

export default {
  title: "Pages",
  component: ForgotPassword,
  decorators: [withKnobs]
};

export const forgotPassswordPage = () => {
  return <ForgotPassword />;
};
