// Core - Grid Stories
import React from "react";
import { withKnobs } from "@storybook/addon-knobs";

import CreateTender from "../Create_Tender";

export default {
  title: "Pages",
  component: CreateTender,
  decorators: [withKnobs]
};

export const CreateTenderPage = () => {
  return <CreateTender />;
};
