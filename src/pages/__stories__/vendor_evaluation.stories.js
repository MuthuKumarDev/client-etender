// Vendor Evaluation Page Stories
import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import VendorEvaluation from "../Vendor_Evaluation";

export default {
  title: "Pages",
  component: VendorEvaluation,
  decorators: [withKnobs]
};

export const VendorEvaluationPage = () => {
  return <VendorEvaluation />;
};
