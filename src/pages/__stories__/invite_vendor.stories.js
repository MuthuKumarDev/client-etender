// Invite_Vendor Stories
import React from "react";
import { withKnobs } from "@storybook/addon-knobs";

import InviteVendor from "../Invite_vendor";

export default {
  title: "Pages",
  component: InviteVendor,
  decorators: [withKnobs]
};

export const InviteVendorPage = () => {
  return <InviteVendor />;
};
