// Buyer Login Stories
import React from "react";
import { withKnobs } from "@storybook/addon-knobs";

import BuyerLoginPage from "../Buyer_Login";

export default {
  title: "Pages",
  component: BuyerLoginPage,
  decorators: [withKnobs]
};

export const buyerLoginPage = () => {
  return <BuyerLoginPage />;
};
