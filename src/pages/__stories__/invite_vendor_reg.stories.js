// Invite Vendor Register Stories
import React from "react";
import { withKnobs } from "@storybook/addon-knobs";

import InviteVendorRegister from "../Invite_Vendor_Register";

export default {
    title: "Pages",
    component: InviteVendorRegister,
    decorators: [withKnobs]
};

export const inviteVendorRegister = () => {
    return <InviteVendorRegister />;
};
