// Tender Summaryt Page Stories
import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import TenderSummaryPage from "../Tender_Summary";

export default {
  title: "Pages",
  component: TenderSummaryPage,
  decorators: [withKnobs]
};

export const tenderSummaryPage = () => {
  return <TenderSummaryPage />;
};
