// Tender Summary Page
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
//import { Button } from "../../components/Button";
import { Input } from "../../components/Input";
import { Grid } from "../../components/Grid";
import { Button, Toast, ToastBody, ToastHeader, FormGroup } from "reactstrap";
import { IconComponent } from "../../components/Icons";
//import { tenderData } from "../helpers/mock_data";
import API from "../../api/api";
import { withRouter } from "react-router";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { notification } from "antd";
import { DropDownComponent as DropDown } from "../../components/DropDownComponent";
import { Avatar } from "../../components/Avatar";
import moment from "moment";
import { isBuyer } from "../../common";
import { Modal } from "../../components/Modal";
import { AttachFile } from "../../components/AttachFile";
// import CreateBid from "./createbid"
import { TextEditer } from "../../components/TextEditer";
import { Popover } from "antd";

const FormData = require("form-data");

const documentLists = [
  // {
  //   id: "doc-1",
  //   title: "Civil works general arrangement detailed sections sheet 7.pdf"
  // },
  // {
  //   id: "doc-2",
  //   title: "Civil works general arrangement detailed sections sheet 3-5.pdf"
  // },
  // {
  //   id: "doc-3",
  //   title: "Civil works general arrangement detailed sections sheet 1&2.pdf"
  // },
  // {
  //   id: "doc-4",
  //   title: "Specification details.doc"
  // }
];

const Document = ({ documentList }) => {
  const { title } = documentList;
  return (
    <div className="doc-tlt">
      <a href="doc">{title}</a>
    </div>
  );
};

const DocumentLists = ({ documentLists }) => (
  <div className="doc-lit">
    {documentLists.map((documentList) => (
      <Document key={`${documentList.id}`} documentList={documentList} />
    ))}
  </div>
);

class TenderSummaryPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeMenu: "tenderterms",
      isOpen: false,
      ishide: false,
      anchorEl: null,
      tender: null,
      changetab: false,
      show: false,
      bidpage: false,
      tenderId: 0,
      status: "",
      tendertype: "",
      publishId: 0,
      questionmodal: false,
      questioninput: "",
      fileLists: [],
      fromBuyerLogin: false,
      setInterest: false,
      options: [
        { key: "Questions by you", cat: "" },
        { key: "Questions by others", cat: "" }
      ],
      dropdownValue: "Questions by you",
      isOpenDropdown: false,
      postDisabled: false,
      bidsubmitteddata: [],
      questionsAndAnswersList: [
        // {
        //   id: 1,
        //   created_at: "2021-02-02T07:15:58.110394+00:00",
        //   question:
        //     "We can only supply to your major site.  Your project happens in 4 locations, and having to supply to 4 different places will increase shipping charges.  Can you be more specific on this?",
        //   replies: [
        //     {
        //       answer:
        //         "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Facilisi mattis adipiscing velit non feugiat lacus, et nisi, tellus. Lobortis risus gravida aenean mattis lacus, dictumst vulputate."
        //     }
        //   ]
        // },
        // {
        //   id: 2,
        //   created_at: "2021-02-05T08:15:58.110394+00:00",
        //   question:
        //     "We can only supply to your major site. Can you be more specific on this?",
        //   replies: [
        //     {
        //       answer:
        //         "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Facilisi mattis adipiscing velit non feugiat lacus, et nisi, tellus. Lobortis risus gravida aenean mattis lacus, dictumst vulputate."
        //     },
        //     {
        //       answer:
        //         "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
        //     }
        //   ]
        // },
        // {
        //   id: 3,
        //   created_at: "2021-01-04T09:15:58.110394+00:00",
        //   question:
        //     "We can only supply to your major site.  Your project happens in 4 locations, ?",
        //   replies: [
        //     {
        //       answer:
        //         "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Facilisi mattis adipiscing velit non feugiat lacus, et nisi, tellus. Lobortis risus gravida aenean mattis lacus, dictumst vulputate."
        //     }
        //   ]
        // }
      ],
      userIdForVendor: "",
      noDataText: "",
      fromBuyerLogin: false,
      questionsAndAnswersListByOthers: [],
      isVisibleAnswer: true,
      buyerUserId: "",
      buyerOptions: [{ key: "All Questions", cat: "" }],
      buyerDropdownValue: "All Questions"
      // selectedAnswerIndex: null,
    };
  }

  componentDidMount() {
    var fromBuyerLogin = isBuyer();
    //console.log("fromBuyerLogin.." + fromBuyerLogin);
    this.setState({ fromBuyerLogin: fromBuyerLogin });

    if (
      this.props &&
      this.props.location &&
      this.props.location.state &&
      this.props.location.state.fromTendersScreen
    ) {
      // console.log("props.. inside" + this.props.location.state.fromTendersScreen )
      window.location.href = "#q&a";
      this.setState({ activeMenu: "q&a" });
    }
    var userId = localStorage.getItem("userIdForVendor");
    var buyerUserId = localStorage.getItem("userIdForBuyer");
    this.setState({ userIdForVendor: userId, buyerUserId: buyerUserId });

    const { tenderId } = this.props.match.params;
    let tenderData = this.props.location;
    let { tender } = tenderData;
    // console.log("Tender Value", tender);
    // console.log("TenderData value", tenderData);
    // console.log(` Tender ID = ${tenderId}`);
    if (tenderId && tender === undefined) {
      API.fetchData(API.Query.TENDER_BY_ID, { tenderId }).then((data) => {
        console.log("tenders list", data.tender);
        // let { tender } = data.tender;
        this.setState({
          tender: data.tender,
          status: data.tender && data.tender.status,
          tendertype: data.tender && data.tender.tender_type,
        });
        console.log("statetender", this.state.status);
        console.log("tendertype", this.state.tendertype)
        this.fetchQuestionsAndAnswersData(userId, data && data.tender.id);
      });
      // this.setState({
      //   tender: tender
      // });
    } else if (tenderId && tender) {
      this.setState({
        tender: tender
      });
      // this.fetchQuestionsAndAnswersData(userId, data.tender.id);
    } else if (tender) {
      this.setState({
        tender: tender
      });
      // this.fetchQuestionsAndAnswersData(userId, data.tender.id);
    }
    // console.log("data", tenderId)
    API.fetchData("get_bid_submited", {
        tenderid: tenderId,
        vendorid: "Jh8ft2suW4QG4yUkYzGMEXMgDun1"
    }).then((res) => {
      console.log("valuestest", res.data)

      this.setState({
        bidsubmitteddata: res.data
      })
      console.log("datatest", this.state.bidsubmitteddata)
    })
    
    if (!fromBuyerLogin) {
      API.fetchData(API.Query.GET_TENDER_SHOW_INTEREST, {
        tenderid: tenderId,
        vendorid: userId
      })
        .then((data) => {
          console.log("show intrest status", data);
          // this.setState({ setInterest: !!data });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  /* Onclick Function */
  _onChangeTab = (activeMenu) => {
    this.setState({ activeMenu });
  };

  handleEdit = (props) => {
    const { tenderId } = this.props.match.params;
    const { history } = this.props;
    let tender = this.state;
    if (tenderId && tender === undefined) {
      this.props.history.push(`/tenders/edit/${tenderId}`, tender);
    } else if (tenderId && tender) {
      history.push({ pathname: `/tenders/edit/${tenderId}`, tender });
    } else if (tender) {
      history.push({ pathname: "/tenders/create", tender });
    }
  };

  handleInvite = (props) => {
    this.props.history.push(`/invitevendor`);
  };

  handleDiscard = (props) => {
    this.props.history.push(`/tenders/create`);
  };

  handleChange = () => {
    const { show } = this.state;
    const { tenderId } = this.props.match.params;
    API.fetchData(API.Query.UPDATE_TENDER_STATUS_TENDOR_BY_ID, {
      tenderId: tenderId,
      tenderstatus: "open"
    }).then((res) => {
      // console.log(res);
    });

    this.setState({
      changetab: true,
      show: !show
    });
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleSave = () => {
    // console.log("Cehck", this.state.tender);
    API.fetchData(API.Query.CREATE_TENDER, { data: this.state.tender }).then(
      ({ data }) => {
        if (data) {
          let { id } = data;
          // console.log("id", id);
          if (id) {
            this.props.history.push(`/tenders/${id}`);
          }
        }
        // console.log("idcheck", data.tender_id);

        notification.success({
          message: "Tender Saved"
        });
        console.log("Tender data to be submit", data);
      }
    );
  };

  handlePublish = () => {
    const { tenderId } = this.props.match.params;
    // console.log("tenderid PUblish", tenderId);
    if (tenderId) {
      API.fetchData(API.Query.UPDATE_TENDER_STATUS_TENDOR_BY_ID, {
        tenderId: tenderId,
        tenderstatus: "open"
      }).then((res) => {
        // console.log(res);
      });
    } else {
      notification.error({
        message: "Fill the details to publish"
      });
    }
  };

  handleduplicate = () => {
    // const { tenderId } = this.props.match.params;
    const { history } = this.props;
    let tender = this.state;

    history.push({ pathname: "/tenders/create", tender });
  };
  toggle = (name) => {
    if (name == "questions") {
      this.setState({ isOpenDropdown: !this.state.isOpenDropdown });
    }
    if (name == "questionsForBuyers") {
      this.setState({
        isQuestionsOpenDropdown: !this.state.isQuestionsOpenDropdown
      });
    }
  };

  toggleItem = (value, name) => {
    // console.log("inside status.." + value);
    // console.log("userIdForVendor..." + this.state.userIdForVendor);
    // console.log("tender id.." + this.state.tender.id);

    this.setState({
      isOpenDropdown: !this.state.isOpenDropdown,
      dropdownValue: value,
      noDataText: ""
    });
    if (value == "Questions by you") {
      this.fetchQuestionsAndAnswersData(
        this.state.userIdForVendor,
        this.state.tender.id
      );
    } else if (value == "Questions by others") {
      this.fetchQuestionsAndAnswersData("", this.state.tender.id);
    } else if (value == "All Questions") {
      console.log("inside..." + value);
      this.fetchQuestionsAndAnswersData("", this.state.tender.id);
    }
  };

  fetchQuestionsAndAnswersData = (userId, id) => {
    console.log("userid.." + userId);
    let params = {};
    if (id) {
      params["tenderid"] = id;
    }
    if (userId) {
      params["userid"] = userId;
    }

    // console.log("params..." + JSON.stringify(params));
    API.fetchData(API.Query.GET_QUESTION_AND_ANSWER, params).then(
      (responseData) => {
        console.log(
          "questions and answers response",
          JSON.stringify(responseData)
        );
        if (responseData && responseData.data && responseData.data.length > 0) {
          this.setState({
            questionsAndAnswersList: responseData.data,
            noDataText: ""
          });
        } else if (
          responseData &&
          responseData.data &&
          responseData.data.length == 0
        ) {
          // console.log("inside else..." + responseData.data.length);
          this.setState({ noDataText: "No Data Found" });
        }
      }
    );
  };

  fetchOthersQuestionsAndAnswersData = (userid, id) => {
    let params = {};
    if (id) {
      params["tenderid"] = id;
    }

    // console.log("params..." + JSON.stringify(params));
    API.fetchData(API.Query.GET_QUESTION_AND_ANSWER, params).then(
      (responseData) => {
        console.log(
          "questions and answers response",
          JSON.stringify(responseData)
        );
        if (responseData && responseData.data && responseData.data.length > 0) {
          this.setState({ questionsAndAnswersListByOthers: responseData.data });
        } else if (
          responseData &&
          responseData.data &&
          responseData.data.length == 0
        ) {
          // console.log("inside else..." + responseData.data.length);
          this.setState({ noDataText: "No Data Found" });
        }
      }
    );
  };

  handleCreateBid = () => {
    if (this.state.tendertype === "Lumpsum Unit Rate") {
      const { tenderId } = this.props.match.params;
      this.props.history.push(`/createbid_unitrate/${tenderId}`);
    } else {
      const { tenderId } = this.props.match.params;
      this.props.history.push(`/createbid/${tenderId}`);
    }
  };

  handleBidList = () => {
    const { tenderId } = this.props.match.params;
    this.props.history.push(`/BidList/${tenderId}`);
  };

  handleAskQuestion = () => {
    this.setState({
      questionmodal: true,
      isOpen: true
    });
  };

  handleQuestionPost = (status) => {
    let { fileLists } = this.state;
    console.log("Post", fileLists);
    const filedata = fileLists[0].name;
    const { tenderId } = this.props.match.params;
    console.log("Inptvlaue", this.state.questioninput);
    // const formData = new FormData();
    // formData.append("queryName", "create_question_and_answer");
    // formData.append("userid", "11221");
    // formData.append("tenderid", "516");
    // formData.append("type", "bid");
    // formData.append("question", this.state.questioninput);
    for (let index = 0; index < fileLists.length; index++) {
      "fileurl" + [index], fileLists[index];
    }

    let params = {
      question: this.state.questioninput,
      userid: this.state.userIdForVendor,
      tenderid: this.state.tender && this.state.tender.id,
      fileurl: filedata,
      status: status

      //fileurl: ""
    };

    API.fetchData(API.Query.CREATE_QUESTION_AND_ANSWER, {
      data: params
    }).then((response) => {
      console.log("response for create questions", JSON.stringify(response));
      if (response) {
        this.modalhide();
        this.fetchQuestionsAndAnswersData(
          this.state.userIdForVendor,
          this.state.tender.id
        );
        this.fetchOthersQuestionsAndAnswersData(
          this.state.userIdForVendor,
          this.state.tender.id
        );
        this.setState({ questioninput: "" });
      }
    });

    // API.fetchformData(formData)
    //   .then((res) => {
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     console.log(err.message);
    //   });

    // this.modalhide();
    // this.setState({
    //   questionmodal : false
    // })
  };
  handleAnswerPost = (status, id) => {
    console.log("userIdForVendor.." + this.state.userIdForVendor);
    var replyText = this.state.replyText.replace(/<(.|\n)*?>/g, "");

    if (this.state.replyId) {
      console.log("inside edit..");
      console.log("reply.." + replyText);
      let params = {
        questionid: this.state.questionId,
        answer: replyText,
        userid: this.state.buyerUserId,
        status: status
      };
      API.fetchData(API.Query.UPDATE_REPLY, {
        replyid: this.state.replyId,
        data: params
      }).then((response) => {
        console.log("response for update reply", JSON.stringify(response));
        if (response) {
          this.fetchQuestionsAndAnswersData(
            this.state.buyerUserId,
            this.state.tender.id
          );
          this.setState({ isVisibleAnswer: false, replyId: "" });
        } else {
          this.setState({ isVisibleAnswer: false, replyId: "" });
        }
      });
    } else {
      let params = {
        questionid: id,
        answer: replyText,
        userid: this.state.buyerUserId,
        status: status
      };

      API.fetchData(API.Query.CREATE_REPLY, {
        data: params
      }).then((response) => {
        console.log("response for create reply", JSON.stringify(response));
        if (response) {
          // this.fetchQuestionsAndAnswersData(
          //   this.state.userIdForVendor,
          //   this.state.tender.id
          // );
          this.fetchQuestionsAndAnswersData(
            // this.state.buyerUserId,
            "",
            this.state.tender.id
          );
          this.setState({ isVisibleAnswer: false });
        } else {
          this.setState({ isVisibleAnswer: false });
        }
      });
    }
  };

  onIgnoreButtonClick = (id) => {
    let params = {
      status: "ignore"
    };

    API.fetchData(API.Query.UPDATE_QUESTION_AND_ANSWER, {
      questionid: id,
      data: params
    }).then((response) => {
      console.log("response for ignore", JSON.stringify(response));
      if (response) {
        // this.fetchQuestionsAndAnswersData(
        //   this.state.userIdForVendor,
        //   this.state.tender.id
        // );
        this.fetchQuestionsAndAnswersData(
          // this.state.buyerUserId,
          "",
          this.state.tender.id
        );
        this.setState({ isVisibleAnswer: false });
      } else {
        this.setState({ isVisibleAnswer: false });
      }
    });
  };

  onTextEditChange = (value) => {
    this.setState({ replyText: value });
  };
  // onSaveDraftButtonClick = () => {
  //   let params = {
  //     question: this.state.questioninput,
  //     userid: this.state.userIdForVendor,
  //     tenderid: this.state.tender && this.state.tender.id,
  //     fileurl: "test"
  //     //status: "drafts"

  //     //fileurl: ""
  //   };

  //   API.fetchData(API.Query.UPDATE_QUESTION_AND_ANSWER, {
  //     data: params
  //   }).then((response) => {
  //     console.log("response for create questions", JSON.stringify(response));
  //     if (response) {
  //       this.modalhide();

  //     }
  //   });

  // }
  onEditAnswerButtonClick = (id, replyData, questionId, index) => {
    console.log("inside edit..." + id + replyData.answer + questionId);
    this.setState({
      replyId: id,
      isVisibleAnswer: true,
      replyText: replyData.answer,
      questionId: questionId,
      selectedAnswerIndex: index
    });
  };

  handleShowInterest = () => {
    const { tenderId } = this.props.match.params;
    let { userIdForVendor } = this.state;
    // console.log("idvalue", tenderId);
    API.fetchData(API.Query.SHOW_INTREST_TENDER, {
      data: {
        tenderid: tenderId,
        vendorid: userIdForVendor,
        is_intrested: "active"
      }
    })
      .then((data) => {
        // console.log(data);
        this.setState({ setInterest: true });
      })
      .catch((err) => {
        // console.log(err);
      });
  };

  handleInputQuestion = (event) => {
    this.setState({ questioninput: event.target.value });
  };

  modalhide = () => {
    this.setState({
      isOpen: false
    });
  };

  handleFileChange = (files) => {
    let { fileLists } = this.state;
    this.setState(fileLists);
    for (let i = 0; i < files.length; i++) {
      // console.log("file Name", files[i].name);
      fileLists.push(files[i]);
    }
  };
  onAnswerButtonClick = (index) => {
    console.log("inside index.." + index);
    this.setState({ selectedAnswerIndex: index, isVisibleAnswer: true });
  };
  handleAnswerVisibleChange = (visible) => {
    console.log("inside visible .." + visible);
    this.setState({ isVisibleAnswer: visible });
  };

  hideAnswers = () => {
    this.setState({
      isVisibleAnswer: false
    });
  };
  onDeleteButtonClick = (replyId) => {
    API.fetchData(API.Query.DELETE_QUESTION_REPLIES, {
      replyid: replyId
    }).then((response) => {
      console.log("response for delete reply", JSON.stringify(response));
      if (response) {
        this.fetchQuestionsAndAnswersData("", this.state.tender.id);
      }
    });
  };
  render() {
    console.log("isVisibleAnswer.." + this.state.isVisibleAnswer);
    let {
      tender,
      options,
      dropdownValue,
      isOpenDropdown,
      questionsAndAnswersList,
      noDataText,
      questionsList,
      questionsAndAnswersListByOthers,
      replyText,
      buyerOptions,
      buyerDropdownValue,
      isQuestionsOpenDropdown
    } = this.state;
    const { fromBuyerLogin } = this.state;

    console.log("inside View summary", tender);
    const open = Boolean(this.state.anchorEl);
    // console.log("questionsAndAnswersList.." + JSON.stringify(questionsAndAnswersList))

    // console.log("DAta show", this.state.status);
    let created =
      tender &&
      tender.created_at &&
      moment(tender.created_at).format("DD MMM YYYY");
    let deadline =
      tender &&
      tender.deadline &&
      moment(tender.deadline).format("DD MMM YYYY");
    console.log("created and deadline ", { created, deadline });

    return (
      <div className="smr-wrp">
        <Grid>
          <Grid.Cell>
            <div className="ten-smr">
              {fromBuyerLogin ? (
                <div className="smr-hdr p-2">
                  <div className="smr-tit">
                    <div className="smr-hdr-nme heading">Tender Summary</div>
                  </div>
                  {this.state.changetab === false && (
                    <div className="smr-btn">
                      {/* <div>
                      <Button label="Discard" color="link" size="big" onClick={this.handleDiscard}/>
                    </div> */}
                      <div>
                        {this.state.status === "draft" ||
                        this.state.status === "drafts" ||
                        this.state.status === "open" ? (
                          // <div>
                          //   <Button label="Show" color="link" size="big" />
                          // </div>
                          <div className="drt-btn">
                            <div className="mr-2">
                              <Button
                                outline
                                size="sm"
                                color="secondary"
                                onClick={this.handleEdit}
                              >
                                &ensp;
                                <IconComponent name="edit" className="mr-2" />
                                Edit
                              </Button>
                            </div>
                            <div className="mr-2">
                              <Button
                                color="primary"
                                size="sm"
                                onClick={this.handleChange}
                              >
                                Publish Tender
                              </Button>
                            </div>
                          </div>
                        ) : (
                          <div className="drt-btn">
                            <div className="mr-2">
                              <Button
                                size="sm"
                                color="link"
                                onClick={this.handleDiscard}
                              >
                                Discard
                              </Button>
                            </div>
                            <div className="mr-2">
                              <Button
                                outline
                                size="sm"
                                color="secondary"
                                onClick={this.handleEdit}
                              >
                                &ensp;
                                <IconComponent name="edit" />
                                Edit
                              </Button>
                            </div>
                            <div className="mr-2">
                              <Button
                                color="primary"
                                size="sm"
                                onClick={this.handleSave}
                              >
                                Save
                              </Button>
                            </div>
                            <div className="mr-2">
                              <Button
                                color="primary"
                                size="sm"
                                onClick={this.handlePublish}
                              >
                                Publish Tender
                              </Button>
                            </div>
                          </div>
                        )}
                      </div>
                      <div>
                        {/* <Button
                        outline
                        color="secondary"
                        style={{ width: 75, height: 25 }}
                        onClick={this.handleEdit}
                      >
                        &ensp;
                        <IconComponent name="edit" />
                        Edit
                      </Button> */}
                      </div>
                      {/* <div>
                      <Button label="Save" color="primary" size="big" />
                    </div> */}
                      {/* <div>
                      <Button
                        label="Publish Tender"
                        color="primary"
                        size="big"
                        onClick={this.handleChange}
                      />
                    </div> */}
                    </div>
                  )}
                  <div>
                    {this.state.changetab === true && (
                      <div className="pub-pug">
                        <Toast isOpen={this.state.show} className="tst-min">
                          <ToastHeader
                            toggle={this.handleChange}
                            className="tst-min-hed"
                            // style={{ backgroundColor: "#212B36", color: "#fff" }}
                          ></ToastHeader>
                          <ToastBody
                            style={{
                              backgroundColor: "#212B36",
                              color: "#fff"
                            }}
                          >
                            Tender Published Succesfully
                          </ToastBody>
                        </Toast>
                        <div>
                          <Button
                            outline
                            size="sm"
                            color="secondary"
                            onClick={this.handleEdit}
                          >
                            &ensp;
                            <IconComponent name="edit" />
                            Edit
                          </Button>
                        </div>
                        <div>
                          <Button
                            color="primary"
                            size="sm"
                            onClick={this.handleInvite}
                          >
                            Invite Vendor
                          </Button>
                        </div>
                        &ensp;
                        <div>
                          <IconButton
                            className="MuiIconButton-root"
                            aria-label="more"
                            aria-controls="long-menu"
                            aria-haspopup="true"
                            onClick={this.handleClick}
                          >
                            <IconComponent
                              name="threedot"
                              width={15}
                              height={15}
                              fill="rgba(0, 0, 0, 0.5)"
                            />
                          </IconButton>
                          <div className="ten-mnu">
                            <Menu
                              id="long-menu"
                              anchorEl={this.state.anchorEl}
                              keepMounted
                              open={open}
                              onClose={this.handleClose}
                            >
                              <MenuItem onClick={this.handleduplicate}>
                                <IconComponent
                                  name="profile"
                                  fill="#B7BCC0"
                                  width="20"
                                  height="20"
                                />
                                &ensp;Duplicate
                              </MenuItem>
                            </Menu>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              ) : (
                <div className="ven-log-shw"></div>
              )}
              <div className="cnt-sum">
                <Grid break="lg">
                  <Grid.Cell span={68}>
                    <div className="sum-det">
                      <div className="tab-lnk">
                        <a
                          href="#tenderoverview heading"
                          className="ten-ovr-vew"
                        >
                          Tender Overview
                        </a>
                        <a href="#tenderterms heading" className="ten-ovr-vew">
                          Tender Terms
                        </a>
                        <a href="#technical heading" className="ten-ovr-vew">
                          Technical
                        </a>
                        <a href="#addendum heading" className="ten-ovr-vew">
                          Addendum
                        </a>
                        <a href="#q&a" className="ten-ovr-vew heading">
                          Questions and Answers
                        </a>
                      </div>
                      <div id="tenderoverview">
                        <Grid>
                          <div className="sum-det-hed heading">
                            {tender && tender.tender_title}
                          </div>
                        </Grid>
                        <Grid>
                          <div className="ted-det">
                            <div className="ted-det-val">
                              <Grid break="md">
                                <Grid.Cell span={30}>
                                  <div className="val-idy-hed">Tender Id</div>
                                  <div className="det-idy">
                                    {tender &&
                                      (tender.id ||
                                        (tender.tender_id === 0
                                          ? ""
                                          : tender.tender_id))}
                                  </div>
                                </Grid.Cell>
                                <Grid.Cell span={56}>
                                  <div className="prj-nme-hed">
                                    Project Name
                                  </div>
                                  <div className="det-nme">
                                    {tender && tender.project_name}
                                  </div>
                                </Grid.Cell>
                              </Grid>
                            </div>
                            <div className="ted-prj-det">
                              <Grid break="md">
                                <Grid.Cell span={30}>
                                  <div className="prj-val-hed">
                                    Project Value
                                  </div>
                                  <div className="prj-val">
                                    {tender && tender.project_value}
                                  </div>
                                </Grid.Cell>
                                <Grid.Cell span={30}>
                                  <div className="prj-loc-hed">
                                    Project Location
                                  </div>
                                  <div className="prj-loc">
                                    {tender && tender.project_location}
                                  </div>
                                </Grid.Cell>
                                <Grid.Cell span={20}>
                                  <div className="prj-sts-hed">
                                    Project Status
                                  </div>
                                  <div className="prj-sts">
                                    {tender && tender.project_status}
                                  </div>
                                </Grid.Cell>
                              </Grid>
                            </div>
                            <div className="ted-tpy-det">
                              <Grid break="md">
                                <Grid.Cell span={30}>
                                  <div className="tpy-det-hed">Tender Type</div>
                                  <div className="ted-tpy">
                                    {tender && tender.tender_type}
                                  </div>
                                </Grid.Cell>
                                <Grid.Cell span={57}>
                                  <div className="ted-val-nme">
                                    Tender Value
                                  </div>
                                  <div className="ted-typ-val">
                                    {tender && tender.tender_value}
                                  </div>
                                </Grid.Cell>
                              </Grid>
                            </div>
                          </div>
                        </Grid>
                        <Grid>
                          <div className="ten-smr-par">
                            <Grid>
                              <div className="ten-par">
                                <div
                                  className="smr-par"
                                  dangerouslySetInnerHTML={{
                                    __html: tender && tender.description
                                  }}
                                ></div>
                              </div>
                            </Grid>
                          </div>
                        </Grid>
                      </div>
                      <div id="tenderterms">
                        <div className="ten-trm heading">Tender Terms</div>
                        <div className="ten-pge-brk"></div>
                        <Grid break="lg">
                          <Grid.Cell span={30}>Scope of Work</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.scope_work}
                          </Grid.Cell>
                        </Grid>
                        <div className="ten-pge-brk"></div>
                        <Grid break="lg">
                          <Grid.Cell span={30}>Exclusions</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.exclution}
                          </Grid.Cell>
                        </Grid>
                        <div className="ten-pge-brk"></div>
                        <Grid break="lg">
                          <Grid.Cell span={30}>Validity</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.validity}
                          </Grid.Cell>
                        </Grid>
                        <div className="ten-pge-brk"></div>
                        <Grid break="lg">
                          <Grid.Cell span={30}>Delivery</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.delivery}
                          </Grid.Cell>
                        </Grid>
                        <div className="ten-pge-brk"></div>
                        <Grid break="lg">
                          <Grid.Cell span={30}>Price Basis</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.price_basis}
                          </Grid.Cell>
                        </Grid>
                        <div className="ten-pge-brk"></div>
                        {/* <Grid break="lg">
                          <Grid.Cell span={30}>Payment Terms</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.payment_terms}
                          </Grid.Cell>
                        </Grid> */}
                        <div className="ten-pge-brk"></div>
                        <Grid break="lg">
                          <Grid.Cell span={30}>Warranty</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.warranty}
                          </Grid.Cell>
                        </Grid>
                        <div className="ten-pge-brk"></div>
                        <Grid break="lg">
                          <Grid.Cell span={30}>Remarks</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.remarks}
                          </Grid.Cell>
                        </Grid>
                      </div>

                      <Grid>
                        <div id="technical">
                          <div className="smr-ten-doc">
                            <Grid>
                              <div className="smr-doc-nme heading">
                                Technical
                                <span className="smr-cnt">
                                  ({tender && tender.technical_files.length}{" "}
                                  documents attached)
                                </span>
                              </div>
                            </Grid>
                            <Grid>
                              <div className="lst">
                                {tender &&
                                  tender.technical_files.map((document) => (
                                    <div>
                                      <a
                                        class="cge-lnk"
                                        key={`${document.id}`}
                                        href={document.url}
                                      >
                                        {document.name}
                                      </a>
                                    </div>
                                  ))}
                              </div>
                            </Grid>
                          </div>
                        </div>
                      </Grid>
                      {/* <div id="q&a">
                        <div className="smr-doc-nme">
                          Questions {"&"} Answers
                        </div>
                      </div> */}
                      {fromBuyerLogin == false ? (
                        <div id="q&a" classname="qanda">
                          <div className="questionsAndAnswersText smr-ten-doc pb-2 heading">
                            Questions and Answers{" "}
                          </div>
                          <Grid break="lg">
                            <Grid.Cell span={40}>
                              {questionsAndAnswersList &&
                                questionsAndAnswersList.length == 0 && (
                                  <div>
                                    No questions have been asked by you yet.{" "}
                                  </div>
                                )}
                              <div className="w-100 mt-3 mb-3">
                                <Button
                                  color="primary"
                                  size="sm"
                                  block
                                  className="mt-2"
                                  onClick={this.handleAskQuestion}
                                >
                                  Ask a question
                                </Button>
                              </div>

                              <DropDown
                                options={options}
                                //isOpen={this.state.dropdownOpen}
                                isOpen={isOpenDropdown}
                                toggleItem={this.toggleItem}
                                toggle={this.toggle}
                                dropdownVal={dropdownValue}
                                direction="down"
                                size="md"
                                name="questions"
                                fromAddMore={false}
                                icon={<IconComponent name="dropdownarrow" />}
                                classNames={
                                  dropdownValue == "Select"
                                    ? {
                                        dropdownStyle:
                                          "defaultDropdownDivStyle",
                                        dropDownMenu: "dropDownMenuStyle",
                                        dropDownItem: "dropDownItemStyle"
                                      }
                                    : {
                                        dropdownStyle: "dropdownDivStyle",
                                        dropDownMenu: "dropDownMenuStyle",
                                        dropDownItem: "dropDownItemStyle"
                                      }
                                }
                              />
                            </Grid.Cell>

                            <Grid.Cell span={60}>
                              {/* {tender && tender.scope_work} */}
                            </Grid.Cell>
                            <Grid break="lg">
                              <Grid.Cell span={35}>
                                {questionsAndAnswersList &&
                                  questionsAndAnswersList.length > 0 &&
                                  questionsAndAnswersList.map((data) => {
                                    return (
                                      <div className="questionsMainDiv">
                                        <div className="questionDate">
                                          You asked this question on{" "}
                                          {data.created_at &&
                                            moment(data.created_at).format(
                                              "DD MMMM YYYY, HH:mm"
                                            )}{" "}
                                          hrs
                                        </div>
                                        <div className="questionText">
                                          {data.question ? data.question : ""}
                                        </div>
                                        <div className="replyLinkText">
                                          {data &&
                                            data.replies &&
                                            data.replies.length}{" "}
                                          {data &&
                                          data.replies &&
                                          data.replies.length == 1
                                            ? "reply"
                                            : data.replies.length == 0 ||
                                              data.replies.length > 1
                                            ? "replies"
                                            : ""}
                                        </div>
                                        {data.replies &&
                                          data.replies.length > 0 &&
                                          data.replies.map((replyData) => {
                                            return (
                                              <div className="d-flex replyMainDiv ">
                                                {/* <img src={} */}
                                                <div>
                                                  <Avatar
                                                    image=""
                                                    size="normal"
                                                  />
                                                </div>

                                                <div className="replyText">
                                                  {" "}
                                                  {replyData.answer
                                                    ? replyData.answer
                                                    : ""}
                                                </div>
                                              </div>
                                            );
                                          })}
                                      </div>
                                    );
                                  })}
                                <div align="center" className="mt-3">
                                  {noDataText ? noDataText : ""}
                                </div>
                              </Grid.Cell>
                            </Grid>
                          </Grid>
                        </div>
                      ) : fromBuyerLogin == true ? (
                        <div id="q&a" classname="qanda">
                          <div className="questionsAndAnswersText smr-ten-doc pb-2 heading">
                            Questions and Answers{" "}
                            <div className="col-6 col-md-3 pl-1">
                              <DropDown
                                options={buyerOptions}
                                //isOpen={this.state.dropdownOpenForQuestions}
                                isOpen={isQuestionsOpenDropdown}
                                toggleItem={this.toggleItem}
                                toggle={this.toggle}
                                dropdownVal={buyerDropdownValue}
                                direction="down"
                                size="md"
                                name="questionsForBuyers"
                                fromAddMore={false}
                                icon={<IconComponent name="dropdownarrow" />}
                                classNames={
                                  dropdownValue == "Select"
                                    ? {
                                        dropdownStyle:
                                          "defaultDropdownDivStyle",
                                        dropDownMenu: "dropDownMenuStyle",
                                        dropDownItem: "dropDownItemStyle"
                                      }
                                    : {
                                        dropdownStyle: "dropdownDivStyle",
                                        dropDownMenu: "dropDownMenuStyle",
                                        dropDownItem: "dropDownItemStyle"
                                      }
                                }
                              />
                            </div>
                            {questionsAndAnswersList &&
                              questionsAndAnswersList.length > 0 &&
                              questionsAndAnswersList.map((data, index) => {
                                return data.status && data.status == "post" ? (
                                  <div className="questionsMainDivBuyer d-flex">
                                    <div>
                                      {" "}
                                      <div className="vendorUserImage">
                                        <Avatar image="" size="normal" />
                                      </div>
                                    </div>
                                    <div>
                                      <div className="questionDate">
                                        <span>
                                          {data.user_details &&
                                            data.user_details.name &&
                                            data.user_details.name}{" "}
                                          asked this question on{" "}
                                          {data.created_at &&
                                            moment(data.created_at).format(
                                              "DD MMMM YYYY, HH:mm"
                                            )}{" "}
                                          hrs
                                        </span>
                                        {/* <span
                                          className="closeIconInView"
                                          onClick={this.hideAnswers}
                                        >
                                          <IconComponent name="close" />
                                        </span> */}
                                      </div>
                                      <div className="questionTextInBuyer mt-2">
                                        {data.question ? data.question : ""}
                                      </div>
                                      <div>
                                        {data && data.replies.length == 0 ? (
                                          <div className="d-flex">
                                            <div
                                              onClick={() =>
                                                this.onAnswerButtonClick(index)
                                              }
                                              className="mr-4"
                                            >
                                              <span className="answerIcon cursorPointer">
                                                <IconComponent name="reply" />
                                              </span>
                                              <span className="replyLinkText cursorPointer">
                                                Answer
                                              </span>
                                            </div>
                                            <div
                                              onClick={() =>
                                                this.onIgnoreButtonClick(
                                                  data.id
                                                )
                                              }
                                            >
                                              <span className="answerIcon cursorPointer">
                                                <IconComponent name="ignore" />
                                              </span>

                                              <span className="replyLinkText cursorPointer">
                                                Ignore
                                              </span>
                                            </div>
                                          </div>
                                        ) : (
                                          ""
                                        )}
                                        <Popover
                                          content={
                                            <div className="pl-3 pr-3 pb-5">
                                              <div
                                                className="closeIconInView"
                                                onClick={this.hideAnswers}
                                              >
                                                <IconComponent name="close" />
                                              </div>
                                              <FormGroup>
                                                <TextEditer
                                                  id="text-editor_answers"
                                                  value={this.state.replyText}
                                                  onChange={
                                                    this.onTextEditChange
                                                  }
                                                />
                                              </FormGroup>

                                              <div className="d-flex">
                                                <div>
                                                  <Button
                                                    color="primary"
                                                    size="sm"
                                                    onClick={() =>
                                                      this.handleAnswerPost(
                                                        "post",
                                                        data.id
                                                      )
                                                    }
                                                    disabled={
                                                      replyText ? false : true
                                                    }
                                                  >
                                                    Post
                                                  </Button>
                                                </div>
                                                <div>
                                                  <Button
                                                    color="link"
                                                    size="sm"
                                                    disabled={
                                                      replyText ? false : true
                                                    }
                                                    onClick={() =>
                                                      this.handleAnswerPost(
                                                        "drafts",
                                                        data.id
                                                      )
                                                    }
                                                  >
                                                    Save Draft
                                                  </Button>
                                                </div>
                                              </div>
                                            </div>
                                          }
                                          // title="Title"
                                          trigger="click"
                                          visible={
                                            index ==
                                            this.state.selectedAnswerIndex
                                              ? this.state.isVisibleAnswer
                                              : ""
                                          }
                                          placement="bottomLeft"
                                          onVisibleChange={
                                            this.handleAnswerVisibleChange
                                          }
                                          overlayClassName="popoverStyle"
                                        ></Popover>
                                      </div>

                                      {/* <div className="replyLinkText">
                                          {data &&
                                            data.replies &&
                                            data.replies.length}{" "}
                                          {data &&
                                          data.replies &&
                                          data.replies.length == 1
                                            ? "reply"
                                            : data.replies.length == 0 ||
                                              data.replies.length > 1
                                            ? "replies"
                                            : ""}
                                        </div> */}
                                      {data.replies &&
                                        data.replies.length > 0 &&
                                        data.replies.map((replyData) => {
                                          return (
                                            <>
                                              <div className="answerDate pt-3">
                                                {replyData.buyer_details &&
                                                  replyData.buyer_details
                                                    .name &&
                                                  replyData.buyer_details
                                                    .name}{" "}
                                                answered this question on{" "}
                                                {replyData.created_at &&
                                                  moment(
                                                    replyData.created_at
                                                  ).format(
                                                    "DD MMMM YYYY, HH:mm"
                                                  )}{" "}
                                                hrs
                                              </div>
                                              <div className="">
                                                {/* <img src={} */}
                                                {/* <div>
                                                <Avatar
                                                  image=""
                                                  size="normal"
                                                />
                                              </div> */}

                                                <div className="replyTextInBuyer">
                                                  {" "}
                                                  {replyData.answer
                                                    ? replyData.answer
                                                    : ""}
                                                </div>
                                              </div>
                                              <div className="d-flex pt-2">
                                                <div
                                                  onClick={() =>
                                                    this.onEditAnswerButtonClick(
                                                      replyData.id,
                                                      replyData,
                                                      data.id,
                                                      index
                                                    )
                                                  }
                                                  className="mr-4"
                                                >
                                                  <span className="answerIcon">
                                                    <IconComponent name="reply" />
                                                  </span>

                                                  <span className="replyLinkText cursorPointer">
                                                    Edit Answer
                                                  </span>
                                                </div>
                                                <div
                                                  onClick={() =>
                                                    this.onDeleteButtonClick(
                                                      replyData.id
                                                    )
                                                  }
                                                >
                                                  {" "}
                                                  <span className="answerIcon">
                                                    <IconComponent name="remove" />
                                                  </span>
                                                  <span className="replyLinkText cursorPointer">
                                                    Delete
                                                  </span>
                                                </div>
                                              </div>
                                            </>
                                          );
                                        })}
                                    </div>
                                  </div>
                                ) : (
                                  ""
                                );
                              })}
                          </div>
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </Grid.Cell>
                  <Grid.Cell span={30}>
                    {/* {tender && <div className="usr-info"> */}
                    {fromBuyerLogin ? (
                      <div>
                        {/* {this.state.status === "draft" ||
                        this.state.status === "drafts" ||
                        this.state.status === "open" ? (
                          <div className="ten-bid-btn">
                            <Button
                              label="5 bids"
                              onClick={this.handleBidList}
                            />
                          </div>
                        ) : (
                          ""
                        )} */}
                        <div className="usr-info">
                          <Grid>
                            <div className="usr-info-key">
                              <div className="info-key-hed heading">
                                Key Information
                              </div>
                              <div className="info-isu">
                                <div className="ifo-isu-hed">
                                  Tender Issue date
                                </div>
                                <div className="info-isu-dte">{created}</div>
                              </div>
                              <div className="info-res">
                                <div className="info-res-hed">
                                  Response Deadline date
                                </div>
                                <div className="info-res-dte">{deadline}</div>
                              </div>
                            </div>
                          </Grid>
                          <Grid>
                            <div className="usr-info-cnt">
                              <div className="info-cnt-hed heading">
                                Contact Information
                              </div>
                              {tender && (
                                <div>
                                  {tender.buyer_contact_information &&
                                    tender.buyer_contact_information.name && (
                                      <div className="info-cnt">
                                        <div className="cnt-hed">
                                          Contact Person
                                        </div>
                                        <div className="cnt-nme">
                                          {
                                            tender.buyer_contact_information
                                              .name
                                          }
                                        </div>
                                      </div>
                                    )}
                                  {tender.buyer_contact_information &&
                                    tender.buyer_contact_information
                                      .phonenumber && (
                                      <div className="info-mbl">
                                        <div className="info-mbl-hed">
                                          Mobile
                                        </div>
                                        <div className="mbl-val">
                                          {
                                            tender.buyer_contact_information
                                              .phonenumber
                                          }
                                        </div>
                                      </div>
                                    )}
                                  {tender.buyer_contact_information &&
                                    tender.buyer_contact_information.email && (
                                      <div className="info-mil">
                                        <div className="info-mil-hed">
                                          email
                                        </div>
                                        <div className="mil-val">
                                          {
                                            tender.buyer_contact_information
                                              .email
                                          }
                                        </div>
                                      </div>
                                    )}
                                </div>
                              )}
                            </div>
                          </Grid>
                          <Grid>
                            <div className="usr-pay-cnt">
                              <div className="info-pay-hed heading">
                                Payment Terms
                              </div>
                              <div className="pay-cnt">
                                <div className="pay-pra">
                                  {/* <strong>10% </strong>OR <strong>15% </strong>OR{" "}
                              <strong>20%</strong> */}
                                  {tender && tender.payment_terms}
                                </div>
                                <div className="pay-pra">
                                  {/* Advance against guarantee{" "} */}
                                </div>
                                {/* <div className="pay-pra">Mode of Payment : CDC</div> */}
                              </div>
                              <div className="pay-blc">
                                <div className="pay-blc-pra">
                                  {/* Balance payment (upon invoice receipt) */}
                                </div>
                                <div className="pay-blc-pra">
                                  {/* Letter of Credit (LC) */}
                                </div>
                                <div className="pay-blc-pra">
                                  {/* after <strong>30 days </strong>OR{" "}
                              <strong>60 days </strong>
                              OR <strong>90 days</strong> */}
                                </div>
                              </div>
                              <div className="pay-ret">
                                {/* <div className="pay-ret-val">Retention()</div> */}
                                <div className="pay-ret-val">
                                  {/* <strong>10%</strong> */}
                                </div>
                              </div>
                            </div>
                          </Grid>
                        </div>
                      </div>
                    ) : (
                      <div>
                        {this.props.location.state > 0 ? (
                          <div>
                            <div className="ven-grd">
                              <Button outline color="secondary" disabled={true}>
                                Create Bid
                              </Button>
                              &emsp;
                              <Button color="primary" disabled={true}>
                                Show Interest
                              </Button>
                            </div>
                            <div className="ven-grd-suc-sho">
                              You submitted your bid for this tender on <br />
                              {this.state.bidsubmitteddata &&
                              this.state.bidsubmitteddata.length > 0 &&
                              this.state.bidsubmitteddata.map((data) => (
                                data.created_at &&
                                  moment(data.created_at).format(
                                    "DD MMMM YYYY, HH:mm"
                                  )
                              ))} &nbsp;hrs
                            </div>
                          </div>
                        ) : (
                          <div>
                            <div className="row d-flex mt-2 mb-4">
                              <div className="col-auto"></div>
                              <Button
                                color="primary"
                                size="sm"
                                className="col ml-1"
                                onClick={this.handleCreateBid}
                              >
                                Create Bid
                              </Button>
                              <div className="col">
                                {this.state.setInterest === false ? (
                                  <Button
                                    className="mr-3"
                                    color="primary"
                                    size="sm"
                                    onClick={this.handleShowInterest}
                                  >
                                    Show Interest
                                  </Button>
                                ) : (
                                  <Button
                                    className="mr-3"
                                    color="discard"
                                    size="sm"
                                    disabled
                                  >
                                    Interested
                                  </Button>
                                )}
                              </div>
                            </div>
                          </div>
                        )}
                        <div className="w-100 mt-3 mb-3 pr-3">
                          <Button
                            color="primary"
                            size="sm"
                            block
                            className="mt-2"
                            onClick={this.handleAskQuestion}
                          >
                            Ask a question
                          </Button>
                        </div>
                        {this.state.questionmodal === true && (
                          // console.log(this.state.questionmodal)
                          <div>
                            <Modal
                              size="medium"
                              isOpen={this.state.isOpen}
                              hide={this.modalhide}
                            >
                              <Modal.Content>
                                <div className="qus-mdl">
                                  <div className="qus-mdl-hdr">
                                    <div className="qus-tit heading">
                                      Ask a question to buyer
                                    </div>
                                    <div
                                      className="qus-cls"
                                      onClick={this.modalhide}
                                    >
                                      <IconComponent name="close" />
                                    </div>
                                  </div>
                                  <div className="qus-cnt">
                                    <div className="qus-cnt-shw">
                                      <div className="qus-cnt-icn"></div>
                                      <div className="qus-cnt-des">
                                        <strong>
                                          Before you ask a question
                                        </strong>
                                        <br /> <br /> - See if your question has
                                        already been answered. You may want to
                                        check{" "}
                                        <span className="questionByOthersText">
                                          ‘Questions by Others’{" "}
                                        </span>
                                        section <br></br>- To get answers
                                        quickly, keep your questions short and
                                        to the point. <br></br>- Wherever
                                        needed, feel free to provide reference
                                        or attachment
                                      </div>
                                    </div>
                                    <div>
                                      <input
                                        placeholder="Enter Question"
                                        type="text"
                                        name="fname1"
                                        value={this.state.questioninput}
                                        onChange={this.handleInputQuestion}
                                        autofocus
                                        className="inp-bot"
                                      />
                                    </div>
                                    <div className="att-file-flw">
                                      <AttachFile
                                        onChange={this.handleFileChange}
                                      />
                                    </div>
                                  </div>
                                  <div className="d-flex">
                                    <div>
                                      <Button
                                        color="primary"
                                        size="sm"
                                        onClick={() =>
                                          this.handleQuestionPost("post")
                                        }
                                      >
                                        Post
                                      </Button>
                                    </div>
                                    <div>
                                      <Button
                                        color="link"
                                        size="sm"
                                        onClick={() =>
                                          this.handleQuestionPost("drafts")
                                        }
                                      >
                                        Save Draft
                                      </Button>
                                    </div>
                                  </div>
                                </div>
                              </Modal.Content>
                            </Modal>
                          </div>
                        )}
                        <div className="usr-info">
                          <Grid>
                            <div className="usr-info-key">
                              <div className="info-key-hed">
                                Key Information
                              </div>
                              <div className="info-isu">
                                <div className="ifo-isu-hed">
                                  Tender Issue date
                                </div>
                                <div className="info-isu-dte">{created}</div>
                              </div>
                              <div className="info-res">
                                <div className="info-res-hed">
                                  Response Deadline date
                                </div>
                                <div className="info-res-dte">{deadline}</div>
                              </div>
                            </div>
                          </Grid>
                          <Grid>
                            <div className="usr-info-cnt">
                              <div className="info-cnt-hed">
                                Contact Information
                              </div>
                              <div className="info-cnt">
                                <div className="cnt-hed">Contact Person</div>
                                <div className="cnt-nme">
                                  {tender && tender.contact_person}
                                </div>
                              </div>
                              <div className="info-mbl">
                                <div className="info-mbl-hed">Mobile</div>
                                <div className="mbl-val">
                                  {tender && tender.mobile}
                                </div>
                              </div>
                              <div className="info-mil">
                                {/* <div className="info-mil-hed">email</div> */}
                                <div className="mil-val">
                                  {/* {tender && tender.email} */}
                                </div>
                              </div>
                            </div>
                          </Grid>
                          <Grid>
                            <div className="usr-pay-cnt">
                              <div className="info-pay-hed">Payment Terms</div>
                              <div className="pay-cnt">
                                <div className="pay-pra">
                                  {/* <strong>10% </strong>OR <strong>15% </strong>OR{" "}
                              <strong>20%</strong> */}
                                  {tender && tender.payment_terms}
                                </div>
                                <div className="pay-pra">
                                  {/* Advance against guarantee{" "} */}
                                </div>
                                {/* <div className="pay-pra">Mode of Payment : CDC</div> */}
                              </div>
                              <div className="pay-blc">
                                <div className="pay-blc-pra">
                                  {/* Balance payment (upon invoice receipt) */}
                                </div>
                                <div className="pay-blc-pra">
                                  {/* Letter of Credit (LC) */}
                                </div>
                                <div className="pay-blc-pra">
                                  {/* after <strong>30 days </strong>OR{" "}
                              <strong>60 days </strong>
                              OR <strong>90 days</strong> */}
                                </div>
                              </div>
                              <div className="pay-ret">
                                {/* <div className="pay-ret-val">Retention()</div> */}
                                <div className="pay-ret-val">
                                  {/* <strong>10%</strong> */}
                                </div>
                              </div>
                            </div>
                          </Grid>
                        </div>
                        <div className="mt-4 pl-3">
                          <div className="questionsByYouText heading">
                            Questions by you
                          </div>
                          <div>
                            {questionsAndAnswersList &&
                              questionsAndAnswersList.length > 0 &&
                              questionsAndAnswersList.map((data) => {
                                return (
                                  <div className="pt-2">
                                    <a className="questionsList">
                                      {data.question}
                                    </a>
                                  </div>
                                );
                              })}
                          </div>

                          <div className="questionsByYouText heading mt-5">
                            Top Questions by others
                          </div>
                          <div>
                            {questionsAndAnswersListByOthers &&
                              questionsAndAnswersListByOthers.length > 0 &&
                              questionsAndAnswersListByOthers
                                .slice(0, 5)
                                .map((data) => {
                                  return (
                                    <div className="pt-2">
                                      <a className="questionsList">
                                        {data.question}
                                      </a>
                                    </div>
                                  );
                                })}
                          </div>

                          <div className="mt-5">
                            Please <span className="linkStyle">Submit PQ </span>
                            to get approved.{" "}
                          </div>
                        </div>
                      </div>
                    )}
                  </Grid.Cell>
                </Grid>
                {fromBuyerLogin == false ? (
                  <div className="mt-5 disclaimerMainDiv">
                    <div className="pt-3">
                      <span className="disclaimer">Disclaimer : </span>{" "}
                      <span className="disclaimerText">
                        We take all possible care for accurate & authentic
                        tender information, however suppliers / vendors are
                        requested to refer Original source of Tender Notice /
                        Tender Document before taking any call regarding this
                        tender.
                      </span>
                    </div>
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </Grid.Cell>
        </Grid>
      </div>
    );
  }
}

TenderSummaryPage.prototypes = {
  name: PropTypes.string,
  filedata: PropTypes.array
};

TenderSummaryPage.defaultProps = {
  name: "Page",
  filedata: []
};

export default withRouter(TenderSummaryPage);
