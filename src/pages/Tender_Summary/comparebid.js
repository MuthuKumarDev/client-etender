// Compare Bid - page

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Button } from "../../components/Button";
import { Grid } from "../../components/Grid";
import { IconComponent } from "../../components/Icons";
import API from "../../api/api";
import { withRouter } from "react-router";
import { notification } from "antd";
import { FileUpload } from "../../components/FileUpload";
import { CheckBox } from "../../components/Checkbox";
import { MultiSelectComponent } from "../../components/MultiSelect";

class CompareBids extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      sortingOptions: [
        { key: "Custom Weight", cat: "" },
        { key: "Equal Weight", cat: "" }
      ]
    };
  }

  onSelect = (value, name) => {
    if (name == "location") {
      this.setState({ selectedValue: value });
    }
  };

  handleTender = () => {
    this.props.history.push(`/tenders`);
  };

  render() {
    const { sortingOptions } = this.state;
    return (
      <div className="cmp-bid">
        <div className="cmp-bid-top">
          <div className="cmp-bid-top-tit">Comparison of bids</div>
          <div className="cmp-bid-top-btns">
            <div>
              <Button label="Share Report" variant="primary" />
            </div>
            <div>
              <Button
                label="Go Back Tenders"
                variant="link"
                onClick={this.handleTender}
              />
            </div>
          </div>
        </div>
        <div className="bid-ovr-viw">
          <div className="bid-ovr-viw-bod">
            <div className="bid-ovr-viw-btns">
              <button className="bid-ovr-viw-btn1">Comparison Overview</button>
              <button className="bid-ovr-viw-btn1">Technical Compilance</button>
              <button className="bid-ovr-viw-btn1">
                Commercial Compliance
              </button>
            </div>
          </div>
          <div className="bid-bot-tit">
            <div className="bid-bot-tit-val">Aluminium Louvers</div>
            <div className="bid-bot-tit-tit">Weight Mode</div>
            <div className="bid-bot-tit-mul">
              <MultiSelectComponent
                options={sortingOptions}
                displayValue="key"
                showCheckbox={true}
                onSelect={this.onSelect}
                styles={this.style}
                closeIcon=""
                singleSelect={true}
                // selectedValues={this.state.selectedValue}
                //ref={this.serviceTypeRef}
                name="sortBy"
              />
            </div>
          </div>
          <div className="bid-cmp-tab">
            <div></div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(CompareBids);
