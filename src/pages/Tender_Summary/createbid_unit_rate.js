// Create Bid Unit Rate
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Button } from "../../components/Button";
import { Grid } from "../../components/Grid";
import { IconComponent } from "../../components/Icons";
import API from "../../api/api";
import { withRouter } from "react-router";
import { notification, Table, Rate } from "antd";
import moment from "moment";
import { Toast, ToastBody, ToastHeader } from "reactstrap";

import { UnitRatecolumn, UnitRatedataSource } from "../helpers/tabledata";

class CreateBidUnitRate extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tender: null,
      tenderId: 0,
      status: "",
      showtech: false,
      setInterest: false
    };
  }

  componentDidMount() {
    const { tenderId } = this.props.match.params;
    let tenderData = this.props.location;
    let { tender } = tenderData;
    console.log("Tender Value", tender);
    console.log("TenderData value", tenderData);
    console.log(` Tender ID = ${tenderId}`);
    if (tenderId) {
      API.fetchData(API.Query.TENDER_BY_ID, { tenderId }).then((data) => {
        console.log("tenders list", data.tender);
        // let { tender } = data.tender;
        this.setState({
          tender: data.tender
          //   status: data.tender.status
        });
        console.log("statetender", this.state.status);
      });
    } else if (tender) {
      this.setState({
        tender: tender
      });
    }
  }

  handleTechnical = () => {
    this.setState({
      showtech: true
    });
  };

  handleShowInterest = () => {
    const { tenderId } = this.props.match.params;
    console.log("idvalue", tenderId);
    API.fetchData(API.Query.SHOW_INTREST_TENDER, {
      data: {
        tenderid: tenderId,
        vendorid: "Jh8ft2suW4QG4yUkYzGMEXMgDun1",
        is_intrested: "active"
      }
    })
      .then((data) => {
        console.log(data);
        this.setState({ setInterest: true });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    let { tender } = this.state;

    return (
      <div className="crt-bid-unt">
        <Grid>
          <div className="crt-bid-unt-tit">{tender && tender.tender_title}</div>
        </Grid>
        <div className="crt-bid-unt-str">
          <Grid>
            <Grid.Cell span={43}>
              <div className="crt-bid-unt-brk"></div>
              <Grid break="lg">
                <Grid.Cell span={60}>Tender Date</Grid.Cell>
                <Grid.Cell span={40}>
                  <strong>
                    {tender &&
                      tender.created_at &&
                      moment(tender.created_at).format("DD/MMMM/YYYY")}
                  </strong>
                </Grid.Cell>
              </Grid>
              <div className="crt-bid-unt-brk"></div>
              <Grid break="lg">
                <Grid.Cell span={60}>Response Deadline</Grid.Cell>
                <Grid.Cell span={40}>
                  <strong>
                    {tender &&
                      tender.deadline &&
                      moment(tender.deadline).format("DD/MMMM/YYYY")}
                  </strong>
                </Grid.Cell>
              </Grid>
              <div className="crt-bid-unt-brk"></div>
              <Grid break="lg">
                <Grid.Cell span={60}>Status</Grid.Cell>
                <Grid.Cell span={40}>
                  <strong>{tender && tender.status}</strong>
                </Grid.Cell>
              </Grid>
              <div className="crt-bid-unt-brk"></div>
              <Grid break="lg">
                <Grid.Cell span={60}>Sector</Grid.Cell>
                <Grid.Cell span={40}>
                  <strong>{tender && tender.sector}</strong>
                </Grid.Cell>
              </Grid>
              <div className="crt-bid-unt-brk"></div>
              <Grid break="lg">
                <Grid.Cell span={60}>Tender Type</Grid.Cell>
                <Grid.Cell span={40}>
                  <strong>{tender && tender.status}</strong>
                </Grid.Cell>
              </Grid>
              <div className="crt-bid-unt-brk"></div>
              {/* <Grid break="lg">
                          <Grid.Cell span={30}>Payment Terms</Grid.Cell>
                          <Grid.Cell span={70}>
                            {tender && tender.payment_terms}
                          </Grid.Cell>
                        </Grid> */}
              <div className="crt-bid-unt-brk"></div>
              <Grid break="lg">
                <Grid.Cell span={60}>Nature</Grid.Cell>
                <Grid.Cell span={40}>
                  <strong>{tender && tender.tender_type}</strong>
                </Grid.Cell>
              </Grid>
              <div className="crt-bid-unt-brk"></div>
              <Grid break="lg">
                <Grid.Cell span={60}>Project Value (AED)</Grid.Cell>
                <Grid.Cell span={40}>
                  <strong>{tender && tender.project_value}</strong>
                </Grid.Cell>
              </Grid>
            </Grid.Cell>
            <Grid.Cell span={23} vAlign="end">
              Max Allowed Vendors : <strong>{tender && tender.remarks}</strong>
            </Grid.Cell>
            <Grid.Cell span={33} sAlign="center">
              {this.state.setInterest === false && (
                <div className="crt-bid-unt-btn">
                  <Button
                    label="Express Interest"
                    variant="primary"
                    onClick={this.handleShowInterest}
                  />
                </div>
              )}
              {this.state.setInterest === true && (
                <div className="crt-bid-unt-btn">
                  <Button label="interested" variant="secondary" isDisabled={true} />
                </div>
              )}
              <div>
                <Toast style={{ width: 300, marginTop: 15, marginLeft: 50 }}>
                  <ToastHeader
                    style={{
                      backgroundColor: "#E0F5F5"
                    }}
                  >
                    <IconComponent name="tips" />
                  </ToastHeader>
                  <ToastBody style={{ backgroundColor: "#E0F5F5" }}>
                    Please be sure that you give the compliance according to the
                    specfication. Any non-compliance would affect your chances
                    of winning this contract.
                    <br />
                    <br /> This might also affect your rating given by our
                    portal, and as well as the buyer.
                  </ToastBody>
                </Toast>
              </div>
            </Grid.Cell>
          </Grid>
          <div className="crt-bid-sav">
            <div className="crt-bid-sav-btn">
              <Button label="save" variant="primary" />
            </div>
          </div>
          <div className="crt-bid-tab-long">
            <div className="crt-bid-tab">
              <div className="crt-bid-tab-key">Overview</div>
              <div className="crt-bid-tab-key">Commercial</div>
              <div className="crt-bid-tab-key" onClick={this.handleTechnical}>
                Technical
              </div>
              <div className="crt-bid-tab-key">Attachments</div>
              <div className="crt-bid-tab-key">Questions and Answers</div>
              <div className="crt-bid-tab-key">Addendum</div>
            </div>
          </div>
          {this.state.showtech === true && (
            <div>
              <div className="crt-bid-itm-val">Number of Items :</div>
              <div className="crt-bid-bod-bdr">
                <div className="crt-bid-bod-bdr-tti">1. Aluminium Lovers</div>
                <div className="crt-bid-bod-bdr-flw">
                  <IconComponent name="download" />
                  &emsp;
                  <IconComponent name="deleteicon" />
                  &emsp;
                  <IconComponent
                    name="threedot"
                    width={12}
                    height={15}
                    fill="grey"
                  />
                  &emsp;
                </div>
              </div>
              <div className="crt-bid-spec-tab">
                <Table
                  dataSource={UnitRatedataSource}
                  columns={UnitRatecolumn}
                  size="small"
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withRouter(CreateBidUnitRate);
