import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Button } from "../../components/Button";
import { Grid } from "../../components/Grid";
import { IconComponent } from "../../components/Icons";
import API from "../../api/api";
import { withRouter } from "react-router";
import { notification } from "antd";
import { FileUpload } from "../../components/FileUpload";
import { CheckBox } from "../../components/Checkbox";
import { Table, Pagination } from "antd";
import moment from 'moment';
import { bidColumn } from "../helpers/tabledata";

class BidList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeMenu: "tenderterms",
      anchorEl: null,
      tender: null,
      changetab: false,
      show: false,
      bidpage: false,
      tenderId: 0,
      status: "",
      publishId: 0,
      fileLists: []
    };
  }

  componentDidMount() {
    const { tenderId } = this.props.match.params;
    let tenderData = this.props.location;
    let { tender } = tenderData;
    console.log("Tender Value", tender);
    console.log("TenderData value", tenderData);
    console.log(` Tender ID = ${tenderId}`);
    if (tenderId) {
      API.fetchData(API.Query.TENDER_BY_ID, { tenderId }).then((data) => {
        console.log("tenders list", data.tender);
        // let { tender } = data.tender;
        this.setState({
          tender: data.tender
          //   status: data.tender.status
        });
        console.log("statetender", this.state.status);
      });
    } else if (tender) {
      this.setState({
        tender: tender
      });
    }
  }

  handleGoBack = () => {
    const { tenderId } = this.props.match.params;
    this.props.history.push(`/tenders/${tenderId}`);
  }

  handleCompareBid = () => {
    this.props.history.push(`/comparebid`)
  }

  render() {
    let { tender, fileLists } = this.state;
    let bidCount = (tender && tender.bids_list_aggregate && tender.bids_list_aggregate.aggregate && tender.bids_list_aggregate.aggregate.count)?tender.bids_list_aggregate.aggregate.count:0;
    let bidList = tender && tender.bids_list;
    let bids = [];
    if(bidList){
      bidList.map(item=>{
        if(item.vendor_details && item.vendor_details.length){
          item.submited = moment(item.created_at).format('DD/MM/YYYY');
          let vendor = item.vendor_details[0];
          let bid = {
            ...item,
            vendor_name :vendor.name,
            vendor_phone : vendor.phonenumber,
            vendor_email : vendor.email
          }
          bids.push(bid);
        }else{
          bids.push(item);
        }
      })
    }
    console.log('current bid list', bids);
    return (
      <div className="cnt-sum">
        <div className="bid-lst-tit">
            Bids({bidCount})
        </div>
        <div className="inv-ven-tab">
          <Table
            dataSource={bidList}
            columns={bidColumn}
            size="small"
            bordered
          />
        </div>
        <div className="lst-bid-buttons">
            <div className="lst-bid-buttons-btn">
                <Button 
                    label="Go Back"
                    variant="link"
                    onClick={this.handleGoBack}
                />
            </div>
            <div className="lst-bid-buttons-btn1">
                <Button
                    label="Compare Bids"
                    variant="primary"
                    onClick={this.handleCompareBid}
                />
            </div>
        </div>
      </div>
    );
  }
}

export default withRouter(BidList);
