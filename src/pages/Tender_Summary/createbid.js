// Create Bid - Page

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
//import { Button } from "../../components/Button";
import { Button } from "reactstrap";
import { Grid } from "../../components/Grid";
import { IconComponent } from "../../components/Icons";
import API from "../../api/api";
import { withRouter } from "react-router";
import { notification } from "antd";
import { FileUpload } from "../../components/FileUpload";
import { Checkbox } from "antd";
import { withFormik, Field } from "formik";
import * as Yup from "yup";
import { FormFeedback, FormGroup, Form, Input, Label } from "reactstrap";
const clonedeep = require("lodash.clonedeep");

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape({
    validity: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("validity is required."),
    scope_work: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Scope of Work is required."),
    exclution: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Exclusions is required."),
    delivery: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Delivery is required."),
    price_basis: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Price Basis is required."),
    payment_terms: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Payment Terms is required."),
    warranty: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Warranty is required."),
    remarks: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Remarks is required.")
  }),

  mapPropsToValues: ({ user }) => ({
    ...user
  }),
  displayName: "CreateForm"
});

const TextInput = ({
  type,
  id,
  label,
  error,
  size,
  value,
  onChange,
  className,
  ...props
}) => {
  return (
    <div className={className}>
      <FormGroup>
        <Input
          id={id}
          type={type}
          size={size}
          value={value}
          onChange={onChange}
          {...props}
          invalid={!!error}
        />
        <FormFeedback invalid>{error}</FormFeedback>
      </FormGroup>
    </div>
  );
};

class CreateForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tender: null,
      tenderterms: null,
      tenderId: 0,
      fileLists: [],
      checked: [],
      value: "",
      bidmappingdetails: null
    };
  }

  componentDidMount() {
    const { tenderId } = this.props.match.params;
    let tenderData = this.props.location;
    let { tender } = tenderData;
    console.log("Tender Value", tender);
    console.log("TenderData value", tenderData);
    console.log(` Tender ID = ${tenderId}`);
    if (tenderId) {
      API.fetchData(API.Query.TENDER_BY_ID, { tenderId }).then((data) => {
        console.log("tenders list", data.tender);
        // let { tender } = data.tender;
        this.setState(
          {
            tender: data.tender
            //   status: data.tender.status
          },
          () => {
            this.getTenderValue();
          }
        );
        // console.log("statetender", this.state.status);
      });
    } else if (tender) {
      this.setState({
        tender: tender
      });
      this.getTenderValue();
    }

    API.fetchData("get_bid_details", {
      tenderid: tenderId,
      vendorid: "Jh8ft2suW4QG4yUkYzGMEXMgDun1"
    }).then((res) => {
      // let { values } = this.props;
      console.log("getbiddetails", res.bid_maping_vendor[0]);
      this.setState({ bidmappingdetails: res.bid_maping_vendor[0] });
      this.getTenderValue();
    });
  }

  getTenderValue() {
    let { bidmappingdetails } = this.state;
    // console.log(this.state.bidmappingdetails.scope_work)
    let { values } = this.props;
    if (bidmappingdetails) {
      values.scope_work = this.state.bidmappingdetails.scope_work;
      values.exclution = this.state.bidmappingdetails.exclution;
      values.validity = this.state.bidmappingdetails.validity;
      values.delivery = this.state.bidmappingdetails.delivery;
      values.price_basis = this.state.bidmappingdetails.price_basis;
      values.payment_terms = this.state.bidmappingdetails.payment_terms;
      values.warranty = this.state.bidmappingdetails.warranty;
      values.remarks = this.state.bidmappingdetails.remarks;
      this.setState({ values });
      console.log("Editvlaueid", values);
    }
  }

  handleTenders = () => {
    this.props.history.goBack();
  };

  handleFileChange = (files) => {
    let { fileLists } = this.state;
    this.setState(fileLists);
    for (let i = 0; i < files.length; i++) {
      console.log("file Name", files[i].name);
      fileLists.push(files[i]);
    }
  };

  handleSaveBid = (payload) => {
    let { tender, fileLists } = this.state;
    let tenderId = this.props.match.params.tenderId;
    let data = payload;
    const cloneData = clonedeep(payload);
    console.log("data", data);
    console.log("data value", cloneData);

    if (tenderId) {
      tender.scope_work = data.scope_work;
      tender.exclution = data.exclution;
      tender.validity = data.validity;
      tender.delivery = data.delivery;
      tender.price_basis = data.price_basis;
      tender.payment_terms = data.payment_terms;
      tender.warranty = data.warranty;
      tender.remarks = data.remarks;
      console.log("datatenderterms");
    } else {
      data.validity = data.validity;
      data.scope_work = data.scope_work;
      data.delivery = data.delivery;
      data.price_basis = data.price_basis;
      data.payment_terms = data.payment_terms;
      data.remarks = data.remarks;
      data.warranty = data.warranty;
      data.exclution = data.exclution;
    }
    if (
      data.validity === "" ||
      data.scope_work === "" ||
      data.delivery === "" ||
      data.price_basis === "" ||
      data.payment_terms === "" ||
      data.warranty === "" ||
      data.remarks === "" ||
      data.exclution === ""
    ) {
      console.log("invalid");
      notification.error({
        message: "Enter the Terms"
      });
    }
    if (this.state.bidmappingdetails !== undefined) {
      API.fetchData("update_bids", {
        id: this.state.bidmappingdetails.id,
        data: {
          tenderid: tenderId,
          userid: "Jh8ft2suW4QG4yUkYzGMEXMgDun1",
          type: "bid",
          scope_work: tender.scope_work,
          exclution: tender.exclution,
          validity: tender.validity,
          delivery: tender.delivery,
          price_basis: tender.price_basis,
          payment_terms: tender.payment_terms,
          warranty: tender.warranty,
          remarks: tender.remarks
        }
      }).then((res) => {
        console.log("UpdateAPICreate", res);
      });
      console.log("inside the tender", tender);
    }
    if (fileLists.length === 0) {
      notification.error({
        message: "Please Choose a File"
      });
    }
    if (fileLists.length > 0 && this.state.bidmappingdetails === undefined) {
      let { tenderId } = this.props.match.params;
      const filedata = fileLists[0].name;
      console.log(tenderId);
      console.log("Filelist data", fileLists);
      for (let index = 0; index < fileLists.length; index++) {
        "fileurl" + [index], fileLists[index];
      }
      API.fetchData("create_bid", {
        data: {
          tenderid: tenderId,
          userid: "Jh8ft2suW4QG4yUkYzGMEXMgDun1",
          type: "bid",
          scope_work: data.scope_work,
          exclution: data.exclution,
          validity: data.validity,
          delivery: data.delivery,
          price_basis: data.price_basis,
          payment_terms: data.price_basis,
          warranty: data.warranty,
          remarks: data.remarks
        }
      }).then((res) => {
        API.fetchData("create_bid_details", {
          data: {
            bidid: 77,
            filename: filedata,
            fileurl: filedata,
            worklist_propertyid: "12",
            property_value: "test"
          }
        });
        console.log("Tstvalue", res);
      });
    }
  };

  handleBidSubmit = () => {
    if (this.state.fileLists.length > 0) {
      const { tenderId } = this.props.match.params;
      this.props.history.push(
        `/tenders/${tenderId}`,
        this.state.fileLists.length
      );
    } else {
      notification.error({
        message: "Please Choose a File"
      });
    }
  };

  handleCheckOnchange = (value) => {
    this.setState({ checked: value });
  };

  render() {
    let { tender, fileLists } = this.state;
    const CheckboxGroup = Checkbox.Group;
    const {
      values,
      touched,
      errors,
      dirty,
      handleChange,
      handleBlur,
      handleSubmit,
      handleReset,
      isSubmitting,
      setFieldValue,
      setValue
    } = this.props;

    return (
      <div className="cnt-sum">
        <Form onSubmit={handleSubmit}>
          {/* <TenderSummaryPage filedata={this.state.fileData} /> */}
          <Grid break="lg">
            <Grid.Cell span={68}>
              <div className="sum-det">
                <div className="tab-lnk">
                  <a href="#tenderoverview" className="ten-ovr-vew">
                    Tender Overview
                  </a>
                  <a href="#tenderterms" className="ten-ovr-vew">
                    Tender Terms
                  </a>
                  <a href="#technical" className="ten-ovr-vew">
                    Technical
                  </a>
                  <a href="#addendum" className="ten-ovr-vew">
                    Addendum
                  </a>
                  <a href="#q&a" className="ten-ovr-vew">
                    Questions and Answers
                  </a>
                </div>
                <div id="tenderoverview">
                  <Grid>
                    <div className="sum-det-hed">
                      {tender && tender.tender_title}
                    </div>
                  </Grid>
                  <Grid>
                    <div className="ted-det">
                      <div className="ted-det-val">
                        <Grid break="md">
                          <Grid.Cell span={30}>
                            <div className="val-idy-hed">Tender Id</div>
                            <div className="det-idy">{tender && tender.id}</div>
                          </Grid.Cell>
                          <Grid.Cell span={56}>
                            <div className="prj-nme-hed">Project Name</div>
                            <div className="det-nme">
                              {tender && tender.project_name}
                            </div>
                          </Grid.Cell>
                        </Grid>
                      </div>
                      <div className="ted-prj-det">
                        <Grid break="md">
                          <Grid.Cell span={30}>
                            <div className="prj-val-hed">Project Value</div>
                            <div className="prj-val">
                              {tender && tender.project_value}
                            </div>
                          </Grid.Cell>
                          <Grid.Cell span={30}>
                            <div className="prj-loc-hed">Project Location</div>
                            <div className="prj-loc">
                              {tender && tender.project_location}
                            </div>
                          </Grid.Cell>
                          <Grid.Cell span={20}>
                            <div className="prj-sts-hed">Project Status</div>
                            <div className="prj-sts">
                              {tender && tender.project_status}
                            </div>
                          </Grid.Cell>
                        </Grid>
                      </div>
                      <div className="ted-tpy-det">
                        <Grid break="md">
                          <Grid.Cell span={30}>
                            <div className="tpy-det-hed">Tender Type</div>
                            <div className="ted-tpy">
                              {tender && tender.tender_type}
                            </div>
                          </Grid.Cell>
                          <Grid.Cell span={57}>
                            <div className="ted-val-nme">Tender Value</div>
                            <div className="ted-typ-val">
                              {tender && tender.tender_value}
                            </div>
                          </Grid.Cell>
                        </Grid>
                      </div>
                    </div>
                  </Grid>
                  <Grid>
                    <div className="ten-smr-par">
                      <Grid>
                        <div className="ten-par">
                          <div
                            className="smr-par"
                            dangerouslySetInnerHTML={{
                              __html: tender && tender.description
                            }}
                          ></div>
                        </div>
                      </Grid>
                    </div>
                  </Grid>
                </div>
              </div>
            </Grid.Cell>
            <Grid.Cell span={30}>
              <div className="crt-bid">
                <div className="go-back-txt">Go Back To</div>
                <Button size="sm" color="link" onClick={this.handleTenders}>
                  Tender Details
                </Button>
              </div>
              <div className="crt-inf">
                <Grid>
                  <div className="usr-info-key">
                    <div className="shw-war">Warning</div>
                  </div>
                </Grid>
              </div>
            </Grid.Cell>
          </Grid>
          <div className="ten-cnt-bid">
            <div id="tenderterms">
              <div className="ten-trm">Tender Terms</div>
              <Grid>
                <Grid.Cell span={50}>
                  <div className="ten-pge-brk"></div>
                  <Grid break="lg">
                    <Grid.Cell span={50}>Scope of Work</Grid.Cell>
                    <Grid.Cell span={50}>
                      {tender && tender.scope_work}
                    </Grid.Cell>
                  </Grid>
                  <div className="ten-pge-brk"></div>
                  <Grid break="lg">
                    <Grid.Cell span={50}>Validity</Grid.Cell>
                    <Grid.Cell span={50}>{tender && tender.validity}</Grid.Cell>
                  </Grid>
                  <div className="ten-pge-brk"></div>
                  <Grid break="lg">
                    <Grid.Cell span={50}>Delivery</Grid.Cell>
                    <Grid.Cell span={50}>{tender && tender.delivery}</Grid.Cell>
                  </Grid>
                  <div className="ten-pge-brk"></div>
                  <Grid break="lg">
                    <Grid.Cell span={50}>Price Basis</Grid.Cell>
                    <Grid.Cell span={50}>
                      {tender && tender.price_basis}
                    </Grid.Cell>
                  </Grid>
                  <div className="ten-pge-brk"></div>
                  <Grid break="lg">
                    <Grid.Cell span={50}>Payment Terms</Grid.Cell>
                    <Grid.Cell span={50}>
                      {tender && tender.payment_terms}
                    </Grid.Cell>
                  </Grid>
                  <div className="ten-pge-brk"></div>
                  <Grid break="lg">
                    <Grid.Cell span={50}>Warranty</Grid.Cell>
                    <Grid.Cell span={50}>{tender && tender.warranty}</Grid.Cell>
                  </Grid>
                  <div className="ten-pge-brk"></div>
                  <Grid break="lg">
                    <Grid.Cell span={50}>Remarks</Grid.Cell>
                    <Grid.Cell span={50}>{tender && tender.remarks}</Grid.Cell>
                  </Grid>
                  <div className="ten-pge-brk"></div>
                  <Grid break="lg">
                    <Grid.Cell span={50}>Exclusions</Grid.Cell>
                    <Grid.Cell span={50}>
                      {tender && tender.exclution}
                    </Grid.Cell>
                  </Grid>
                </Grid.Cell>
                <Grid.Cell span={47}>
                  <div className="ten-pge-area1"></div>
                  <TextInput
                    id="scope_work"
                    type="textarea"
                    placeholder="Scope of Work"
                    error={touched.scope_work && errors.scope_work}
                    size="medium"
                    value={values.scope_work}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="ten-pge-area"></div>
                  <TextInput
                    id="validity"
                    type="textarea"
                    placeholder="validity"
                    error={touched.validity && errors.validity}
                    size="medium"
                    value={values.validity}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="ten-pge-area"></div>
                  <TextInput
                    id="delivery"
                    type="textarea"
                    placeholder="delivery"
                    error={touched.delivery && errors.delivery}
                    size="medium"
                    value={values.delivery}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="ten-pge-area"></div>
                  <TextInput
                    id="price_basis"
                    type="textarea"
                    placeholder="price_basis"
                    error={touched.price_basis && errors.price_basis}
                    size="medium"
                    value={values.price_basis}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="ten-pge-area"></div>
                  <TextInput
                    id="payment_terms"
                    type="textarea"
                    placeholder="payment_terms"
                    error={touched.payment_terms && errors.payment_terms}
                    size="medium"
                    value={values.payment_terms}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="ten-pge-area"></div>
                  <TextInput
                    id="warranty"
                    type="textarea"
                    placeholder="warranty"
                    error={touched.warranty && errors.warranty}
                    size="medium"
                    value={values.warranty}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="ten-pge-area"></div>
                  <TextInput
                    id="remarks"
                    type="textarea"
                    placeholder="remarks"
                    error={touched.remarks && errors.remarks}
                    size="medium"
                    value={values.remarks}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="ten-pge-area"></div>
                  <TextInput
                    id="exclution"
                    type="textarea"
                    placeholder="exclution"
                    error={touched.exclution && errors.exclution}
                    size="medium"
                    value={values.exclution}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Grid.Cell>
                <Grid.Cell span={3}></Grid.Cell>
              </Grid>
            </div>
            <Grid>
              <div id="technical">
                <div className="smr-ten-doc">
                  <div className="smr-doc-nme">Technical</div>
                </div>
                <div>
                  <FileUpload onChange={this.handleFileChange} />
                </div>
                {this.state.checked.length === 2 ? (
                  <div>
                    <Button
                      color="primary"
                      size="sm"
                      className="mr-2"
                      onClick={this.handleBidSubmit}
                    >
                      Submit Bid
                    </Button>
                    <Button
                      color="primary"
                      size="sm"
                      onClick={() => {
                        this.handleSaveBid(values);
                      }}
                    >
                      Save Bid
                    </Button>
                  </div>
                ) : (
                  ""
                )}
                <CheckboxGroup
                  onChange={this.handleCheckOnchange}
                  className="chk-lst-flw"
                >
                  <div className="bid-chk">
                    <Checkbox value="article" />
                    &ensp;All&nbsp;
                    <a href="" target="_blank">
                      <span style={{ color: "#4A42ED", cursor: "pointer" }}>
                        required documents
                      </span>
                    </a>
                    &nbsp; are valid and upto date.
                  </div>
                  <div className="bid-chk2">
                    <Checkbox value="article1" />
                    &ensp; By clicking on ‘Submit Bid’ button I agree to the{" "}
                    &nbsp;
                    <a href="" target="_blank">
                      <span style={{ color: "#4A42ED", cursor: "pointer" }}>
                        terms and conditons
                      </span>
                    </a>
                    &nbsp; of this E-Tender Port.
                  </div>
                </CheckboxGroup>
              </div>
            </Grid>
          </div>
        </Form>
      </div>
    );
  }
}

const MyEnhancedForm = formikEnhancer(withRouter(CreateForm));

// Main Component
class CreateBid extends PureComponent {
  render() {
    return (
      <div>
        <MyEnhancedForm
          user={{
            project_name: "",
            tender_title: "",
            tender_value: "",
            project_id: "",
            tender_id: "",
            validity: "",
            scope_work: "",
            delivery: "",
            price_basis: "",
            payment_terms: "",
            warranty: "",
            remarks: "",
            exclution: ""
          }}
        />
      </div>
    );
  }
}

export default withRouter(CreateBid);
