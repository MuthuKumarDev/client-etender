// VendorList Page Component
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import { Table, Pagination } from "antd";
import { Input } from "../../components/Input";
import { Grid } from "../../components/Grid";
import { MultiSelectComponent } from "../../components/MultiSelect";
import API from "../../api/api";
import _ from "lodash";
import { Link } from "react-router-dom";
import { IconComponent } from "../../components/Icons";
import { DropDownComponent as DropDown } from "../../components/DropDownComponent";

import { columns } from "../helpers/tabledata";

// Main Component
export default class VendorList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      vendors: [],
      searchText: "",
      location: [
        { key: "UAE", cat: "" },
        { key: "INDIA", cat: "" },
        { key: "ENGLAND", cat: "" }
      ],
      projectValue: [
        { key: "10- 12 million AED", cat: "" },
        { key: "13- 15 million AED", cat: "" },
        { key: "17- 20 million AED", cat: "" }
      ],
      sector: [
        { key: "Service", cat: "" },
        { key: "Trading", cat: "" },
        { key: "Manufacturing", cat: "" }
      ],
      morefilters: [
        { key: "Rating", cat: "" },
        { key: "Annual Turnover", cat: "" },
        { key: "Certificate", cat: "" },
        { key: "Consultant", cat: "" },
        { key: "Last Logged in", cat: "" },
        { key: "Ship to location", cat: "" }
      ],
      sortingOptions: [
        { key: "Latest Addition", cat: "" },
        { key: "Closing Date", cat: "" }
      ],
      isOpenForSorting: false,
      sortValue: "Latest Addition",
      fromAddMore: false,
      isOpen: false,
      dropdownOpen: false,
      dropdownVal: "Select",
      isOpenForPublished: false,
      customizedDate: "",
      publishedWithinValue: "Select",
      customizedClosingDate: "",
      closingDateValue: "Select",
      isOpenForServiceType: false,
      serviceTypeValue: "Select"
    };

    this.style = {
      chips: {
        background: "#fff",
        color: "#000"
      },
      searchBox: {},
      multiselectContainer: {
        width: 180
      }
    };
  }

  componentDidMount() {
    API.fetchData(API.Query.GET_VENDORS).then((data) => {
      console.log("vendor data", data.vendors);

      this.setState({
        vendors: data.vendors
      });
    });
  }

  onSelect = (value, name) => {
    if (name == "status") {
      this.setState({ selectedValue: value });
    }
    if (name == "projectvalue") {
      this.setState({ selectedValueForProjectValue: value });
    }
    if (name == "sector") {
      this.setState({ selectedValueForSector: value });
    }
    if (name == "morefilters") {
      this.setState({ selectedValueForMoreFilters: value });
    }
  };

  onChangeSearchText = (event) => {
    // console.log("Onchange text", searchText);
    this.setState({ searchText: event.target.value });
  };

  toggleItem = (value, name) => {
    // console.log("inside toggle item value..." + value);
    let {
      dropdownOpen,
      isOpenForPublished,
      isOpenForClosingDate,
      dropdownVal,
      publishedWithinValue,
      closingDateValue,
      isOpenForServiceType,
      isOpenForNumberOfBids
    } = this.state;
    if (name == "status") {
      // console.log("inside status.." + value);
      this.setState({
        dropdownOpen: !this.state.dropdownOpen,
        dropdownVal: value
      });
    }
    if (name == "publishedWithin") {
      this.setState({
        isOpenForPublished: !this.state.isOpenForPublished,
        publishedWithinValue: value
      });
      this.setState({ customizedDate: "" });
    }
    if (name == "closingDate") {
      this.setState({
        isOpenForClosingDate: !isOpenForClosingDate,
        closingDateValue: value
      });
      this.setState({ customizedClosingDate: "" });
    }
    if (name == "servicetype") {
      this.setState({
        isOpenForServiceType: !isOpenForServiceType,
        serviceTypeValue: value
      });
    }
    if (name == "noofbids") {
      this.setState({
        isOpenForNumberOfBids: !isOpenForNumberOfBids,
        noOfBidsValue: value
      });
    }
    if (name == "sortby") {
      this.setState({
        isOpenForSorting: !this.state.isOpenForSorting,
        sortValue: value
      });
      if (value == "Closing Date") {
        let params = {
          closing_date: "desc"
        };
      }
      if (value == "Latest Addition") {
        let params = {
          latest_addition: "desc"
        };
      }
    }
  };

  toggle = (name) => {
    let { dropdownOpen } = this.state;
    if (name == "status") {
      this.setState({ dropdownOpen: !this.state.dropdownOpen });
    }
    if (name == "publishedWithin") {
      this.setState({ isOpenForPublished: !this.state.isOpenForPublished });
    }
    if (name == "closingDate") {
      this.setState({ isOpenForClosingDate: !this.state.isOpenForClosingDate });
    }
    if (name == "servicetype") {
      this.setState({ isOpenForServiceType: !this.state.isOpenForServiceType });
    }
    if (name == "noofbids") {
      this.setState({
        isOpenForNumberOfBids: !this.state.isOpenForNumberOfBids
      });
    }
    if (name == "sortby") {
      this.setState({
        isOpenForSorting: !this.state.isOpenForSorting
      });
    }
  };

  render() {
    const {
      location,
      projectValue,
      sector,
      morefilters,
      sortingOptions,
      sortValue,
      fromAddMore,
      isOpenForSorting,
      isOpenForPublished,
      customizedDate,
      publishedWithinValue,
      isOpenForClosingDate,
      customizedClosingDate,
      closingDateValue,
      isOpenForServiceType,
      serviceTypeValue
    } = this.state;

    console.log("State Vendor", this.state.vendors);

    // var managerNames = ["Master", "Dubai"];

    // var Managers = _.filter(this.state.vendors, (i) => {
    //   console.log("Test", i);
    //   return managerNames.indexOf(i.company_name) > -1;
    // });

    // console.log("Managers", Managers);

    const results = this.state.vendors.filter(
      (vendor) =>
        vendor.company_name &&
        (vendor.company_name.toLowerCase().includes(this.state.searchText) ||
          vendor.company_name.includes(this.state.searchText) ||
          vendor.company_name.toUpperCase().includes(this.state.searchText))
    );

    const dataSource =
      results &&
      results.length > 0 &&
      results.map((vendor) => ({
        key: vendor.id,
        vId: vendor.vendor_id,
        company: vendor.company_name,
        location: vendor.location,
        rating: vendor.rating,
        phNumber: vendor.phone,
        email: vendor.email,
        businesstype: vendor.business_sector
      }));

    return (
      <div className="ven-lst">
        <Grid>
          <Grid.Cell>
            <div className="ven-lst-top">
              <div className="ven-lst-top-hdr">
                <div className="hdr-tit">
                  <div className="smr-hdr-nme heading">
                    Amtrex Technical Services - Vendors
                  </div>
                </div>
                <div className="smr-btn">
                  <div>
                    <Link to="/vendorregistor">
                      <Button color="primary" size="md">
                        Invite Vendor
                      </Button>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="ven-lst-pge">
                <div className="ven-lst-srh">
                  <div class="main">
                    <div class="form-group has-search">
                      <div className="icn-main">
                        <IconComponent name="search" />
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        name="search"
                        placeholder="Search Vendors"
                        onChange={this.onChangeSearchText}
                      ></input>
                    </div>
                  </div>
                  <div className="ven-lst-sel">
                    <DropDown
                      options={location}
                      isOpen={
                        this.state.isOpen == false
                          ? this.state.dropdownOpen
                          : ""
                      }
                      toggleItem={this.toggleItem}
                      toggle={this.toggle}
                      dropdownVal={this.state.dropdownVal}
                      direction="down"
                      size="lg"
                      fromAddMore={fromAddMore}
                      icon={<IconComponent name="downarrow" />}
                      name="status"
                      classNames={
                        this.state.dropdownVal == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                  <div className="ven-lst-sel1">
                    <DropDown
                      options={projectValue}
                      isOpen={
                        this.state.isOpen == false ? isOpenForPublished : ""
                      }
                      toggleItem={this.toggleItem}
                      toggle={this.toggle}
                      dropdownVal={
                        customizedDate ? customizedDate : publishedWithinValue
                      }
                      direction="down"
                      size="lg"
                      fromAddMore={fromAddMore}
                      icon={<IconComponent name="downarrow" />}
                      name="publishedWithin"
                      classNames={
                        publishedWithinValue == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : customizedDate
                          ? {
                              dropdownStyle: "dropdownDivForCustomRange",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                  <div className="ven-lst-sel2">
                    <DropDown
                      options={sector}
                      isOpen={
                        this.state.isOpen == false ? isOpenForClosingDate : ""
                      }
                      toggleItem={this.toggleItem}
                      toggle={this.toggle}
                      dropdownVal={
                        customizedClosingDate
                          ? customizedClosingDate
                          : closingDateValue
                      }
                      direction="down"
                      size="lg"
                      name="closingDate"
                      fromAddMore={fromAddMore}
                      icon={<IconComponent name="downarrow" />}
                      classNames={
                        closingDateValue == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : customizedClosingDate
                          ? {
                              dropdownStyle: "dropdownDivForCustomRange",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                  <div className="ven-lst-sel3">
                    <DropDown
                      options={morefilters}
                      isOpen={
                        this.state.isOpen == false ? isOpenForServiceType : ""
                      }
                      toggleItem={this.toggleItem}
                      toggle={this.toggle}
                      dropdownVal={serviceTypeValue}
                      direction="down"
                      size="lg"
                      name="servicetype"
                      icon={<IconComponent name="downarrow" />}
                      fromAddMore={fromAddMore}
                      classNames={
                        serviceTypeValue == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                </div>
                <div className="ven-lst-shw">
                  <div className="ven-lst-shw-txt">
                    Showing {this.state.vendors.length} Vendors
                  </div>
                  <div className="ven-lst-shw-box">
                    <div className="d-flex sortByMainDiv">
                      <div className="sortBy">Sort By</div>
                      <div className="sortByDropdownDiv">
                        <DropDown
                          options={sortingOptions}
                          isOpen={isOpenForSorting}
                          toggleItem={this.toggleItem}
                          toggle={this.toggle}
                          dropdownVal={sortValue}
                          direction="down"
                          size="lg"
                          name="sortby"
                          icon={<IconComponent name="downarrow" />}
                          fromAddMore={fromAddMore}
                          classNames={
                            sortValue == "Select"
                              ? {
                                  dropdownStyle: "defaultDropdownDivForSort",
                                  dropDownMenu: "dropDownMenu",
                                  dropDownItem: "dropDownItem"
                                }
                              : {
                                  dropdownStyle: "dropdownDivForSort",
                                  dropDownMenu: "dropDownMenu",
                                  dropDownItem: "dropDownItem"
                                }
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="ven-lst-tab">
                  <Table
                    dataSource={dataSource}
                    columns={columns}
                    size="small"
                    bordered
                    pagination={false}
                  />
                </div>
                <div className="ven-lst-pag">
                  <Pagination simple defaultCurrent={1} total={100} />
                </div>
              </div>
            </div>
          </Grid.Cell>
        </Grid>
      </div>
    );
  }
}

VendorList.propTypes = {
  name: PropTypes.string
};

VendorList.defaultProps = {
  name: "Invite Tender"
};
