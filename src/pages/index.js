import Dashboard from "./Dashboard";
import Login from "./Buyer_Login";
import SupplierLogin from "./Supplier_Login";
import Tenders from "./Tenders";
import TenderSummary from "./Tender_Summary";
import CreateTender from "./Create_Tender";
import InviteVendorRegister from "./Invite_Vendor_Register";
import VendorList from "./Vendor_List";
import ResourceLibrary from "./ResourceLibrary";
import InviteVendor from "./Invite_vendor";
import ForgotPassword from "./Forgot_Password";
import CreateBid from "./Tender_Summary/createbid";
import CreateBidUnitRate from "./Tender_Summary/createbid_unit_rate";
import BidList from "./Tender_Summary/bidlist";
import CompareBids from "./Tender_Summary/comparebid";
import FormExample from "./Form";

export { 
    Dashboard,
    Login,
    SupplierLogin,
    Tenders,
    TenderSummary,
    CreateTender,
    InviteVendorRegister,
    VendorList,
    ResourceLibrary,
    InviteVendor,
    ForgotPassword,
    CreateBid,
    CreateBidUnitRate,
    BidList,
    CompareBids,
    FormExample
};