// Tender Summary data
const projectStatusLists = [
  {
    value: "option1",
    label: "Design and Build"
  },
  {
    value: "option2",
    label: "Job in Hand"
  },
  {
    value: "option3",
    label: "Tender"
  }
];

const projectNumberLists = [
  {
    value: "option1",
    label: "1"
  },
  {
    value: "option2",
    label: "2"
  }
];

const projectValueLists = [
  {
    value: "option1",
    label: "10- 12 million AED"
  },
  {
    value: "option2",
    label: "13- 15 million AED"
  },
  {
    value: "option3",
    label: "17- 20 million AED"
  }
];

const projectLocationLists = [
  {
    value: "option1",
    label: "UAE"
  },
  {
    value: "option2",
    label: "INDIA"
  },
  {
    value: "option3",
    label: "ENGLAND"
  }
];

const tenderTypeLists = [
  {
    value: "option1",
    label: "Lumpsum Material"
  },
  {
    value: "option2",
    label: "Lumpsum Unit Rate"
  }
];

const businessTypeLists = [
  {
    value: "option1",
    label: "Service"
  },
  {
    value: "option2",
    label: "Trading"
  },
  {
    value: "option3",
    label: "Manufacturing",
  }
];

const serviceCategoryLists = [
  {
    value: "option1",
    label: "MEP"
  },
  {
    value: "option2",
    label: "MEP"
  }
];

const SingleContactList = [
  {
    value: "option1",
    label: "Rengarajan"
  },
  {
    value: "option2",
    label: "John"
  }
];

const tenderData= {
  "tender": {
    "business_sector": "IT",
    "business_type": "Product",
    "contract_period": "1 month",
    "created_at": "2021-01-06T08:50:52.127757+00:00",
    "deadline": null,
    "description": "tender description",
    "id": 20,
    "issue_date": null,
    "nature": null,
    "preferences": null,
    "project_id": 10,
    "project_location": "INDIA",
    "project_name": "Etender",
    "project_status": "completed",
    "project_value": "10- 12 million AED",
    "sector": null,
    "service_category": "MEP",
    "set_payment_terms": "payment terms",
    "status": "open",
    "tender_date": null,
    "tender_id": 10,
    "tender_title": "This is a new tender!",
    "tender_type": "Lumpsum Material",
    "tender_value": "2",
    "type_of_contract": "test type",
    "updated_at": "2021-01-06T08:50:52.127757+00:00"
  }
}



export {
  tenderData,
  projectStatusLists,
  projectNumberLists,
  projectValueLists,
  projectLocationLists,
  tenderTypeLists,
  businessTypeLists,
  serviceCategoryLists,
  SingleContactList
};

/* {
  "data":{
    business_sector: "", 
    business_type: "", 
    contact_person: "", 
    contract_period: "", 
    created_at: "", 
    custom_level: "", 
    deadline: "", 
    delivery: "", 
    description: "", 
    email: "", 
    exclusions: "", 
    id: 10, 
    issue_date: "", 
    mobile: 10, 
    nature: "", 
    payment_terms: "", 
    preferences: false, 
    price_basis: "", 
    project_id: 10, 
    project_location: "", 
    project_name: "", 
    project_status: "", 
    project_value: 10, 
    remarks: "", 
    scope_work: "", 
    sector: "", 
    service_category: "", 
    set_payment_terms: "", 
    status: "", 
    tender_date: "", 
    tender_id: 10, 
    tender_title: "", 
    tender_type: "", 
    tender_value: 10, 
    type_of_contract: ""}
} */