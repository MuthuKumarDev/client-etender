import { Rate, Checkbox } from "antd";

// const dataSource = [
//     {
//       key: "1",
//       vId: "V1001",
//       company: "Air Master Equipments Emirates LLC",
//       location: "Ajman, UAE",
//       phNumber: "04-2238016",
//       email: "jask@g.com",
//       businesstype: "Manufacturing"
//     },
//     {
//       key: "2",
//       vId: "V1002",
//       company: "Al Arabia For Technical Supplies & Contracts LLC",
//       location: "Dubai, UAE",
//       phNumber: "04-2234745",
//       email: "alzareenswg2010@outlook.com",
//       businesstype: "Services"
//     },
//     {
//       key: "3",
//       vId: "V1003",
//       company: "Al Falak Al Dahabi Technical Services LLC",
//       location: "Dubai, UAE",
//       phNumber: "04-2238456",
//       email: "jask@g.com",
//       businesstype: "Manufacturing"
//     },
//     {
//       key: "4",
//       vId: "V1004",
//       company: "Al Hathboor Elecricals LLC",
//       location: "Dubai, UAE",
//       phNumber: "04-2238123",
//       email: "jask@g.com",
//       businesstype: "Services"
//     },
//     {
//       key: "5",
//       vId: "V1005",
//       company: "Al Jarah Al Baraqa General Trading",
//       location: "Dubai, UAE",
//       phNumber: "04-2238478",
//       email: "jask@g.com",
//       businesstype: "Services"
//     },
//     {
//       key: "6",
//       vId: "V1006",
//       company: "Al Raha International Building Materials Trading LLC",
//       location: "Dubai, UAE",
//       phNumber: "04-2238452",
//       email: "jask@g.com",
//       businesstype: "Services"
//     },
//     {
//       key: "7",
//       vId: "V1007",
//       company: "Al Sagr National Insurance Co. LLC",
//       location: "Bur Dubai, UAE",
//       phNumber: "04-2238785",
//       email: "jask@g.com",
//       businesstype: "Services"
//     },
//     {
//       key: "8",
//       vId: "V1008",
//       company: "Al Zareen Swithgear LLC",
//       location: "Dubai, UAE",
//       phNumber: "04-2238632",
//       email: "jask@g.com",
//       businesstype: "Services"
//     },
//     {
//       key: "9",
//       vId: "V1003",
//       company: "Anixter Middle East FZE",
//       location: "Dubai, UAE",
//       phNumber: "04-2238452",
//       email: "jask@g.com",
//       businesstype: "Services"
//     },
//     {
//       key: "10",
//       vId: "V1010",
//       company: "Bahri & Mazroei Trading Co. LLC",
//       location: "Dubai, UAE",
//       phNumber: "04-2238452",
//       email: "jask@g.com",
//       businesstype: "Services"
//     }
// ];

const columns = [
  {
    title: "Vendor Id",
    dataIndex: "vId",
    key: "name",
    width: 90,
    render: (_text) => (
      <div className="inv-ren-chk">
        <span>{_text}</span>
      </div>
    )
  },
  {
    title: "Company Name",
    dataIndex: "company",
    key: "company",
    align: "center",
    width: 290
  },
  {
    title: "Location",
    dataIndex: "location",
    key: "location",
    align: "center",
    width: 140
  },
  {
    title: "Rating",
    dataIndex: "rating",
    key: "rating",
    render: (text) => <Rate defaultValue="1" />,
    width: 150,
    align: "center"
  },
  {
    title: "Rating",
    dataIndex: "rating",
    key: "rating",
    width: 100,
    align: "center"
  },
  {
    title: "Phone",
    dataIndex: "phNumber",
    key: "phNumber",
    align: "center",
    width: 100
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
    align: "center",
    width: 170
  },
  {
    title: "Business Category",
    dataIndex: "businesstype",
    key: "businesstype",
    align: "center",
    width: 120
  }
];

const inviteColumn = [
  {
    title: "Vendor Id",
    dataIndex: "vId",
    key: "name",
    width: 140,
    align: "center",
    render: (_text, index) => (
      <div className="inv-ren-stat">
        <span
        style={{marginTop: "-20px"}}
        >
          <Checkbox value={`Bearer ${index + 1}`}></Checkbox>
        </span>
        <span
          style={{
            marginLeft: "12px"
          }}
        >
          {_text}
        </span>
      </div>
    )
  },
  {
    title: "Company Name",
    dataIndex: "company",
    key: "company",
    align: "center",
    width: 290
  },
  {
    title: "Location",
    dataIndex: "location",
    key: "location",
    align: "center",
    width: 140
  },
  {
    title: "Rating",
    dataIndex: "rating",
    key: "rating",
    render: (text) => <Rate defaultValue="1" />,
    width: 150,
    align: "center"
  },
  {
    title: "Phone",
    dataIndex: "phNumber",
    key: "phNumber",
    align: "center",
    width: 100
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
    align: "center",
    width: 170
  },
  {
    title: "Business Category",
    dataIndex: "businesstype",
    key: "businesstype",
    align: "center",
    width: 120
  }
];

const UnitRatecolumn = [
  {
    title: "Specification requirement (as approved by client)",
    dataIndex: "spec",
    key: "spec",
    align: "center",
    width: 130
  },
  {
    title: "",
    dataIndex: "cell1",
    key: "cell1",
    width: 140,
    render: (text) => (
      <div>
        <button className="tbl-btn-unt">Comply</button>
      </div>
    ),
    align: "center"
  },
  {
    title: "",
    dataIndex: "cell2",
    key: "cell2",
    width: 140,
    render: (text) => (
      <div>
        <button className="tbl-btn-unt">Comply+</button>
      </div>
    )
  },
  {
    title: "",
    dataIndex: "cell3",
    key: "cell3",
    width: 140,
    render: (text) => (
      <div>
        <button className="tbl-btn-unt">Comply with deviation</button>
      </div>
    )
  },
  {
    title: "",
    dataIndex: "cell4",
    key: "cell4",
    width: 140,
    render: (text) => (
      <div>
        <button className="tbl-btn-unt">Do not comply</button>
      </div>
    )
  }
];

const UnitRatedataSource = [
  {
    key: "spec"
    // spec: 'Pro',
  },
  {
    key: "cell1"
    // cell1: 'John',
  },
  {
    key: "cell2"
    // cell2: 'John',
  },
  {
    key: "cell3"
    // cell2: 'John',
  },
  {
    key: "cell4"
    // cell2: 'John',
  }
];
const bidColumn = [
  {
    title: "Bid Id",
    dataIndex: "id",
    key: "id",
    align: "center",
    width: 50
  },
  {
    title: "Vendor Name",
    dataIndex: "vendor_name",
    key: "vendor_name",
    align: "center",
    width: 70
  },
  {
    title: "Contact Number",
    dataIndex: "vendor_phone",
    key: "vendor_phone",
    align: "center",
    width: 80
  },
  {
    title: "Sumbmitted",
    dataIndex: "submited",
    key: "submited",
    align: "center",
    width: 80
  },
  {
    title: "Bid Value (AED)",
    dataIndex: "bid_amount",
    key: "bid_amount",
    align: "center",
    width: 44
  }
];

export { columns, inviteColumn, UnitRatecolumn, UnitRatedataSource, bidColumn };
