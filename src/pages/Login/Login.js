// LoginPage
import React, { useEffect } from "react";
import { Input } from "reactstrap";
import { Button } from "reactstrap";
import PropTypes from "prop-types";
import { Image } from "../../components/Image";
import isEqual from "react-fast-compare";

// Main Component
const LoginPage = (props) => {
  const { name } = props;

  return (
    <div className="Loginpage">
      <div className="loginimage">
        <Image src="/images/loginimage.jpg" alt="image"></Image>
      </div>
      <div className="loginForm1">
        <div className="border">
          <div className="header1">e-Tender</div>
          <div className="header2">for amtrex</div>
          <div className="header3">Supplier Login</div>
          <div className="text">Email/Username</div>
          <div className="inputbox">
            <Input placeholder="email" />
          </div>
          <div className="text1">
            Password
            <div className="link1">
              <Button color="link">Forgot password?</Button>
            </div>
          </div>
          <div className="inputbox">
            <Input placeholder="password" />
          </div>
          <div className="text2">
            New User ?
            <div className="link">
              <Button color="link">Register</Button>
            </div>
          </div>
          <div className="loginbutton">
            <Button color="primary" size="sm">
              Login
            </Button>
          </div>
          <div className="check">
            <input name="example_1" value={true} type="checkbox" />
            &ensp; Keep me signed in
          </div>
        </div>
      </div>
    </div>
  );
};

LoginPage.propTypes = {
  name: PropTypes.string
};

LoginPage.defaultProps = {
  name: "Insert"
};

export default LoginPage;
