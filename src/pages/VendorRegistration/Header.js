import { Component } from "react";
import { Row, Col, Spinner } from "reactstrap";
import { InputField } from "../../components/InputField";
import userIcon from "../../images/user_icon.jpg";
import { Avatar } from "../../components/Avatar";
import { InputGroupField } from "../../components/InputGroupField/InputGroupField";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";

export class Header extends Component {
  render() {
    return (
      <div>
        <Row className="mainHeader">
          <Col
            md="3"
            lg="4"
            className="mainHeaderFirstCol d-none d-sm-none d-lg-block d-md-block"
          >
            <span className="mainHeaderFirstColText">e-Tender</span>
            {/* <br /> */}
            <div class="mainHeaderFirstColCompanyText">by Hexolabs</div>
          </Col>
          <Col xs="2" className="d-block d-md-none mainHeaderFirstCol">
            <MenuIcon />
          </Col>
          <Col xs="8" md="6" lg="4" className="inputgroupCol">
            <InputGroupField addOnType="prepend" iconName={<SearchIcon />} />
          </Col>
          <Col xs="2" md="3" lg="4">
            <Row className="userIconRow">
              <Col md="8" lg="10" className="usernameCol d-none d-md-block">
                test user
              </Col>
              <Col md="4" lg="2">
                <Avatar userIcon={userIcon} />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}
