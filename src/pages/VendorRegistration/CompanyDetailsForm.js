import { Component } from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { LabelField } from "../../components/Label/LabelField";
import { InputField } from "../../components/InputField/Inputfield";
import { CheckBox } from "../../components/Checkbox";
import { RadioButton } from "../../components/RadioButton/RadioButton";
import { Button } from "../../components/Button";
import { Tag } from "../../components/Tag/Tag";
import CloseIcon from "@material-ui/icons/Close";
import AttachFileIcon from "@material-ui/icons/AttachFile";

export class CompanyDetailsForm extends Component {
  state = {
    countries: ["country 1", "country 2", "country 3"],
    companyTypes: ["company1", "company 2", "company 3"]
  };
  render() {
    return (
      <div className="formDetailsMainDiv">
        <Row className="rowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">General Company Information</div>
            <div>
              For good results with current and future buyers, complete as much
              information as possible.
            </div>
          </Col>
          <Col md="6">
            <div>
              <LabelField label="* Company Name  (as per trade license)" />
            </div>
            <div>
              <InputField
                type="text"
                name="companyname"
                id="companyname"
                placeholder="Value"
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6">
            <Row>
              <Col md="7">
                <div>
                  <LabelField label="* Country of Registration" />
                </div>
                <div>
                  <InputField
                    type="select"
                    name="countryname"
                    id="countryname"
                    placeholder="Select"
                    classname="inputStyle"
                    option={this.state.countries}
                  />
                </div>
              </Col>
              <Col md="5">
                <div>
                  <LabelField label="*Year Established" />
                </div>
                <div>
                  <InputField
                    type="text"
                    name="year"
                    id="year"
                    placeholder="Year"
                    classname="inputStyle"
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>

          <Col md="6">
            <div>
              <LabelField label="* Company type" />
            </div>
            <div>
              <InputField
                type="select"
                name="countryname"
                id="countryname"
                placeholder="Select"
                classname="inputStyle"
                option={this.state.companyTypes}
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle-chk">
          <Col md="6"></Col>
          <Col md="6">
            <div>
              <LabelField label="* Business Sector" />
            </div>
            <div className="checkboxDiv">
              <CheckBox title="Manufacturing" variant="checkstyle" />
              <CheckBox title="Trading" variant="checkstyle" />
              <CheckBox title="Services" variant="checkstyle" />
              <CheckBox title="Other" variant="checkstyle" />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>

          <Col md="6">
            <div>
              <LabelField label="What does your company do?" />
            </div>
            <div>
              <InputField
                type="textarea"
                name="company"
                id="company"
                placeholder="Airmaster Technical Services is into... ."
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        
        <Row className="rowStyle">
          <Col md="6"></Col>

          <Col md="6">
            <div>
              <LabelField label="Company profile" />
            </div>
            <div>
              <InputField
                type="file"
                name="profile"
                id="profile"
                //placeholder="choose file"
                classname="inputFileStyle"
                fileIcon={<AttachFileIcon />}
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>

          <Col md="6">
            <div>
              <LabelField label="Company Logo" />
            </div>
            <div>
              <InputField
                type="file"
                name="logo"
                id="logo"
                //placeholder="choose file"
                classname="inputFileStyle"
                fileIcon={<AttachFileIcon />}
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>

          <Col md="6">
            <div>
              <LabelField label="Company Website" />
            </div>
            <div>
              <InputField
                type="text"
                name="companywebsite"
                id="companywebsite"
                placeholder="Enter a company website"
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">Parent Company</div>
          </Col>
          <Col md="6">
            <div>
              <LabelField label="Do you have a Parent  Company?" />
            </div>
            <div>
              <FormGroup>
                {/* <legend>Radio Buttons</legend> */}
                <RadioButton radioDetail="Yes" />
                <RadioButton radioDetail="No" />
              </FormGroup>
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>

          <Col md="6">
            <div>
              <LabelField label="* Name of the Parent company " />
            </div>
            <div>
              <InputField
                type="text"
                name="parentcompany"
                id="parentcompany"
                placeholder="Enter parent Company Name"
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">Primary contact person</div>
            <div>
              Please be sure to give accurate contact details of the primary
              contact responsible for the registration.
              {/* All notifications of this registration will be sent to the email id given here.  */}
            </div>
          </Col>

          <Col md="6">
            <div>
              <LabelField label="* Full Name " />
            </div>
            <div>
              <InputField
                type="text"
                name="fullname"
                id="fullname"
                placeholder="Enter Full Name"
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6">
            <div>
              All notifications of this registration will be sent to the email
              id given here.
            </div>
          </Col>

          <Col md="6">
            <div>
              <LabelField label="* Job Title " />
            </div>
            <div>
              <InputField
                type="text"
                name="jobtitle"
                id="jobtitle"
                placeholder="Enter Job Title"
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6">
            <div>
              <LabelField label="* Email ID" />
            </div>
            <div>
              <InputField
                type="email"
                name="email"
                id="email"
                placeholder="Enter Job Email ID"
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6">
            <div>
              <LabelField label="* Phone Number" />
            </div>
            <div>
              <InputField
                type="number"
                name="phonenumber"
                id="phonenumber"
                placeholder="Enter Phone Number"
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6">
            <div>
              <LabelField label="Fax Number" />
            </div>
            <div>
              <InputField
                type="text"
                name="fax"
                id="fax"
                placeholder="Enter fax"
                classname="inputStyle"
              />
            </div>
          </Col>
        </Row>
        <Row className="buttonRowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">Product and Service Categories</div>
            <div>Add products and Services you offer</div>
          </Col>
          <Col md="6">
            <div className="col-4 pl-0">
              <Button label="Add" classname="addButton"></Button>
            </div>
          </Col>
        </Row>
        <Row className="buttonRowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">Ship-to or Service Location</div>
            <div>Add locations that you serve to</div>
          </Col>
          <Col md="6">
            <div className="col-4 pl-0">
              <Button label="Add" classname="addButton"></Button>
            </div>
            <Tag tagContent="United Arab Emirates" icon={<CloseIcon />} />
          </Col>
        </Row>
        <Row className="buttonRowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">Industries you serve to</div>
            <div>
              Add industries that you serve to. This will help buyers to quickly
              decide.
            </div>
          </Col>
          <Col md="6">
            <div className="col-4 pl-0">
              <Button label="Add" classname="addButton"></Button>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
