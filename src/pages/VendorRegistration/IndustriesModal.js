import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal } from "../../components/Modal";
import { Button } from "../../components/Button";
import { Input } from "../../components/Input";
import { IconComponent } from "../../components/Icons";
import { InputGroupField } from "../../components/InputGroupField";
import { Row, Col, ListGroup, ListGroupItem } from "reactstrap";
import API from "../../api/api";
import { Popover } from "antd";
import { CheckBox } from "../../components/Checkbox";

class IndustriesModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secondaryCategory: "",
      id: "",
      secondaryCategoryId: "",
      selectedSubCategory: [],
      searchData: [],
      visible: true,
      showSearchItems: false,
      isSelectAll: false,
      industriesList: [
        {
          id: 1,
          name: "Aerospace"
        },
        {
          id: 2,
          name: "Hospitality"
        },
        {
          id: 3,
          name: "Agriculture"
        },
        {
          id: 4,
          name: "Industrial Machinery"
        },
        {
          id: 5,
          name: "Automotive"
        },
        {
          id: 6,
          name: "Metal Products"
        },
        {
          id: 7,
          name: "Building Material, Clay & Glass"
        },
        {
          id: 8,
          name: "Oil & Gas"
        },
        {
          id: 9,
          name: "Chemicals"
        },
        {
          id: 10,
          name: "Telecommunications"
        },
        {
          id: 11,
          name: "Consumables"
        },
        {
          id: 12,
          name: "Textiles"
        },
        {
          id: 13,
          name: "Construciton"
        },
        {
          id: 14,
          name: "Transportation"
        },
        {
          id: 15,
          name: "Engineering"
        },
        {
          id: 16,
          name: "Utilities"
        },
        {
          id: 17,
          name: "Forest Products"
        },
        {
          id: 18,
          name: "Wholesale Distribution"
        },
        {
          id: 19,
          name: "Furniture"
        },
        {
          id: 20,
          name: "Hightech & Electronics"
        }
      ],
      isSelectAllChecked: false,
      selectedList: [],
      selectedNameList: [],
    };
  }
  hideModal = () => {
    this.props && this.props.hide && this.props.hide();
  };

  componentDidMount = () => {
    console.log("inside.industry..");
    // document.addEventListener("mousedown", this.handleClickOutside);
    this.setState({ selectedList: this.props.selectedIndustryIdList , selectedNameList: this.props.selectedIndustryItems })
  };
  handleChange = (value, id,name) => {
    console.log("inside..." + value);
    console.log("id.." + id);

    var selectedList = this.state.selectedList;
    var selectedNameList = this.state.selectedNameList;

    if (value) {
      selectedList.push(id);
      selectedNameList.push(name)
    } else {
      selectedList = this.state.selectedList.filter((val) => val !== id);
      selectedNameList = this.state.selectedNameList.filter((val) => val !== name);
      this.setState({ isSelectAllChecked: false })

    }
    this.setState({
      selectedList: selectedList, selectedNameList: selectedNameList
    });

    // }
  };

  checkedIndustries = (id) => {
    var flag = false;
    for (var i in this.state.selectedList) {
      // console.log("this.state.selectedList[i]" + this.state.selectedList[i])
      if (Number(this.state.selectedList[i]) === Number(id)) {
        flag = true;
      }
    }
    return flag;
  };
  onSelectAllChange = (value) => {
    console.log("value...." + value)
    var selectAll = [];
    var selectedNameList = [];
    console.log("this.state.industriesList" + JSON.stringify(this.state.industriesList))
    if (value) {
      for (var i in this.state.industriesList) {
        console.log("i.id..." + this.state.industriesList[i].id)
        selectAll.push(this.state.industriesList[i].id);
        selectedNameList.push(this.state.industriesList[i].name)
      }
    }
    console.log("selectAll..." + JSON.stringify(selectAll))
    this.setState({
      selectedList: selectAll,
      selectedNameList: selectedNameList,
      isSelectAllChecked: value
    });
  };
  onCloseIconClick = (index) => {
    let temp = [...this.state.selectedList];
    temp.splice(index, 1);
    let tmp = [...this.state.selectedNameList];
    tmp.splice(index,1)
    this.setState({ selectedList: temp , selectedNameList: tmp, isSelectAllChecked: false });
  };
  onAddSelectionToProfileButtonClick = () => {
    this.props.addIndustriesSelectionToProfileClick &&
      this.props.addIndustriesSelectionToProfileClick(this.state.selectedNameList, this.state.selectedList);
    this.hideModal();
  };

  render() {
    let { isIndustriesOpen } = this.props;
    let { selectedSubCategory, isSelectAll, industriesList, isSelectAllChecked, selectedList, selectedNameList } = this.state;
    console.log("selectedList...." + JSON.stringify(selectedList))
    return (
      <Modal size="big" isOpen={isIndustriesOpen} hide={this.hideModal}>
        <Modal.Content>
          <div className="modalMainDiv">
            <div className="modalHeader">
              <div className="d-flex">
                <div className="addDocument heading pb-2">Add Industries </div>

                <div className="closeIcon" onClick={this.hideModal}>
                  <IconComponent name="close" />
                </div>
              </div>
            </div>

            <div className=" mt-2 productDiv">
              <div className="ml-4 mr-5 productText">
                Select all the industries that your company serves to.
              </div>
              <div className="ml-4 mr-5">
                <div>Your Selection </div>
                <div className="mt-2 mb-3">
                  <div className="">
                    {selectedNameList &&
                      selectedNameList.length > 0 &&
                      selectedNameList.map((data, index) => {
                        return (
                          <div className="tagDiv d-inline-block mr-2">
                           
                            <span>{data}</span>
                            <span
                              className="pl-4"
                              onClick={() => this.onCloseIconClick(index)}
                            >
                              <IconComponent name="close" />
                            </span>
                          </div>
                        );
                      })}
                  </div>
                </div>
              </div>
              <div>
                <div className="selectAllMainDiv">
                <div className="pt-3 pb-3 d-flex ml-4">
                 <div> <CheckBox
                    variant="defaultCheckbox"
                    isChecked={isSelectAllChecked}
                    // isDisabled={true}
                    // checked={}
                    onCheckboxChange={this.onSelectAllChange}
                  />
                  </div>
                   <div>Select All</div>                 

                </div>
                </div>
                <div className="d-inline-block w-50 ml-4 mr-5">
                  {industriesList &&
                    industriesList.length > 0 &&
                    industriesList.map((industryData, index) => {
                      return (
                        <div className="d-inline-block w-50 checkboxMainDiv">
                          <div className="industriesCheckbox">
                            <CheckBox
                              variant="defaultCheckbox"
                              isChecked={selectedList.includes(industryData.id)}
                              //isChecked={isSelectAll}
                              // isChecked={this.checkedIndustries(
                              //   industryData.id
                              // )}
                              // isDisabled={true}
                              // checked={}
                              onCheckboxChange={(value) =>
                                this.handleChange(value, industryData.id, industryData.name)
                              }
                            />
                          </div>
                          <div className="industriesCheckboxLabel">
                            {industryData.name}
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
            </div>
          </div>
          <div className="d-flex saveButton addToProfile">
            <div className="saveButtonSubDiv col-3">
              <div className="saveButtonDiv col-12">
                {" "}
                <Button
                  variant="primary"
                  label="Add selection to profile"
                  onClick={this.onAddSelectionToProfileButtonClick}
                ></Button>
              </div>
            </div>
          </div>
        </Modal.Content>
      </Modal>
    );
  }
}

export default IndustriesModal;
