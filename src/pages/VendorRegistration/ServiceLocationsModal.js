// Primary Category Modal Component
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal } from "../../components/Modal";
import { Button } from "../../components/Button";
import { Input } from "../../components/Input";
import { IconComponent } from "../../components/Icons";
import { InputGroupField } from "../../components/InputGroupField";
import { Row, Col, ListGroup, ListGroupItem } from "reactstrap";
import API from "../../api/api";

class ServiceLocationsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secondaryCategory: "",
      id: "",
      secondaryCategoryId: "",
      selectedSubCategory: [],
      searchData: [],
      showSearchItems: false
    };
    //this.handleClickOutside = this.handleClickOutside.bind(this);
    //this.setSearchDataRef = this.setSearchDataRef.bind(this);
  }
  hideModal = () => {
    this.props && this.props.hide && this.props.hide();
  };

  componentDidMount = () => {
    console.log("inside...");
    //document.addEventListener("mousedown", this.handleClickOutside);
  };
  setSearchDataRef(node) {
    this.setSearchRef = node;
  }
  // handleClickOutside(event) {
  //   //  alert('You clicked inside of me!' + this.wrapperRef);

  //   if (
  //     this.setSearchRef &&
  //     this.setSearchRef.contains(event.target)
  //   ) {
  //   } else {
  //     this.setState({ showSearchItems: false });
  //   }
  // }
  onPrimaryCategoryClick = (index, id) => {
    this.setState({ id: id, secondaryCategoryId: "" });
  };
  onSecondaryCategoryClick = (index, id) => {
    this.setState({ secondaryCategoryId: id });
  };
  onAddIconClick = (secondarySubCategory) => {
    let temp = this.state.selectedSubCategory;
    temp.push(secondarySubCategory);
    this.setState({ selectedSubCategory: temp });
  };
  onCloseIconClick = (index) => {
    let temp = [...this.state.selectedSubCategory];
    temp.splice(index, 1);
    this.setState({ selectedSubCategory: temp });
  };

  onChangeInput = (value) => {
    this.setState({ searchValue: value });
  };
  onSearchIconClick = () => {
    // this.setState({isLocationOpenen: true });
  };
  onAddSelectionToProfileButtonClick = () => {
    this.props.addLocationSelectionToProfileClick &&
      this.props.addLocationSelectionToProfileClick(this.state.selectedSubCategory);
    this.hideModal();
  };
  render() {
    let { isLocationOpen, locationList } = this.props;
    let {
      selectedSubCategory,
      searchValue,
      searchData,
      showSearchItems
    } = this.state;
    var count = 0;
    return (
      <Modal size="big" isOpen={isLocationOpen} hide={this.hideModal}>
        <Modal.Content>
          <div className="modalMainDiv">
            <div className="modalHeader">
              <div className="d-flex">
                <div className="addDocument heading pb-2">
                  Add Ship-to or Service Locations{" "}
                </div>

                <div className="closeIcon" onClick={this.hideModal}>
                  <IconComponent name="close" />
                </div>
              </div>
            </div>

            <div className="ml-4 mr-5 mt-2 productDiv">
              <div className="productText">
                Select the locations that your company ships to or provides
                service.
              </div>
              <div>
                <div>Your Selection </div>
                <div className="mt-2 mb-3">
                  <div className="">
                    {/* {console.log(
                      "selectedSubCategory..." +
                        JSON.stringify(selectedSubCategory)
                    )} */}
                    {selectedSubCategory &&
                      selectedSubCategory.length > 0 &&
                      selectedSubCategory.map((subcategory, index) => {
                        return (
                          <div className="tagDiv d-inline-block mr-2">
                            <span>{subcategory}</span>
                            <span
                              className="pl-4"
                              onClick={() => this.onCloseIconClick(index)}
                            >
                              <IconComponent name="close" />
                            </span>
                          </div>
                        );
                      })}
                  </div>
                </div>
              </div>

              <Row className="ml-0 mb-2 pl-0">
                <Col md="8" className="pr-0 pl-0">
                  <div className="">
                    <InputGroupField
                      addOnType="prepend"
                      iconName="search"
                      type="text"
                      name="search"
                      id="searchField"
                      placeholder=""
                      handleInputChange={this.onChangeInput}
                      value={searchValue}
                      classNames="searchInput"
                      onSearchIconClick={this.onSearchIconClick}
                    />
                  </div>
                </Col>
              </Row>

              <div>
                <Row className="listGroupRow">
                  <Col md="4">
                    <ListGroup>
                      {locationList &&
                        locationList.length > 0 &&
                        locationList.map((categoryData, index) => {
                          return (
                            <div className="productCategoryMainDiv">
                              {categoryData.name && (
                                <div
                                  className=""
                                  onClick={() =>
                                    this.onPrimaryCategoryClick(
                                      index,
                                      categoryData.id
                                    )
                                  }
                                >
                                  <span className="productCategoryDiv">
                                    <ListGroupItem>
                                      {categoryData.name}
                                    </ListGroupItem>
                                  </span>
                                  <span className="categoryIconDiv">
                                    <IconComponent name="lefttag" />
                                  </span>
                                </div>
                              )}
                            </div>
                            // categoryData && cate
                          );
                        })}
                    </ListGroup>
                  </Col>
                  {this.state.id ? (
                    <div className="rightArrow">
                      <IconComponent name="rightarrow" />
                    </div>
                  ) : (
                    ""
                  )}

                  <Col md="4">
                    <ListGroup>
                      {locationList &&
                        locationList.length > 0 &&
                        locationList.map((categoryData, index) => {
                          return (
                            <>
                              <div className="productCategoryMainDiv">
                                {categoryData.name &&
                                  categoryData &&
                                  categoryData.sub_categories &&
                                  categoryData.sub_categories.length > 0 &&
                                  categoryData.sub_categories.map(
                                    (secondaryCategoryData) => {
                                      // console.log(
                                      //   "this.state.secondaryCategory.." +
                                      //     this.state.secondaryCategory
                                      // );
                                      return (
                                        this.state.id ==
                                          secondaryCategoryData.service_locationid && (
                                          <div
                                            className=""
                                            onClick={() =>
                                              this.onSecondaryCategoryClick(
                                                index,
                                                secondaryCategoryData.id
                                              )
                                            }
                                          >
                                            <span className="productCategoryDiv">
                                              <ListGroupItem>
                                                {secondaryCategoryData.name}
                                              </ListGroupItem>
                                            </span>
                                            <span className="categoryIconDiv">
                                              <IconComponent name="lefttag" />
                                            </span>
                                          </div>
                                        )
                                      );
                                    }
                                  )}
                              </div>
                            </>
                            // categoryData && cate
                          );
                        })}
                    </ListGroup>
                  </Col>
                  {/* {console.log("this.state.secondaryCategoryId.." + this.state.secondaryCategoryId)} */}
                  {this.state.secondaryCategoryId ? (
                    <div className="rightArrowSubCat">
                      <IconComponent name="rightarrow" />
                    </div>
                  ) : (
                    ""
                  )}

                  <Col md="4" className="subcategoryCol">
                    <ListGroup>
                      {locationList &&
                        locationList.length > 0 &&
                        locationList.map((categoryData, index) => {
                          return (
                            <div>
                              {categoryData &&
                                categoryData.sub_categories &&
                                categoryData.sub_categories.length > 0 &&
                                categoryData.sub_categories.map(
                                  (secondaryCategoryData) => {
                                    return (
                                      // this.state.id ==
                                      //   secondaryCategoryData.categoryid && (
                                      secondaryCategoryData &&
                                      secondaryCategoryData.sub_categories
                                        .length > 0 &&
                                      secondaryCategoryData.sub_categories.map(
                                        (subCategory) => {
                                          return (
                                            this.state.secondaryCategoryId ==
                                              subCategory.service_locationid && (
                                              <div
                                                className=""
                                                // onClick={() =>
                                                //   this.onSecondaryCategoryClick(
                                                //     index,
                                                //     secondaryCategoryData.secondary_category
                                                //   )
                                                // }
                                              >
                                                <span className="productCategoryDiv">
                                                  <ListGroupItem>
                                                    {subCategory.name}
                                                  </ListGroupItem>
                                                </span>
                                                {selectedSubCategory &&
                                                selectedSubCategory.length >
                                                  0 ? (
                                                  selectedSubCategory.includes(
                                                    subCategory.name
                                                  ) ? (
                                                    <span className="subCategorySelectedIconDiv">
                                                      <IconComponent name="circletick" />
                                                    </span>
                                                  ) : (
                                                    <span
                                                      className="subCategoryIconDiv"
                                                      onClick={() =>
                                                        this.onAddIconClick(
                                                          subCategory.name
                                                        )
                                                      }
                                                    >
                                                      <IconComponent name="circleplus" />
                                                    </span>
                                                  )
                                                ) : (
                                                  <span
                                                    className="subCategoryIconDiv"
                                                    onClick={() =>
                                                      this.onAddIconClick(
                                                        subCategory.name
                                                      )
                                                    }
                                                  >
                                                    <IconComponent name="circleplus" />
                                                  </span>
                                                )}
                                              </div>
                                            )
                                          );
                                        }
                                      )

                                      // )
                                    );
                                  }
                                )}
                            </div>
                          );
                        })}
                    </ListGroup>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
          <div className="d-flex saveButton addToProfile">
            <div className="saveButtonSubDiv col-3">
              <div className="saveButtonDiv col-12">
                {" "}
                <Button
                  variant="primary"
                  label="Add selection to profile"
                  onClick={this.onAddSelectionToProfileButtonClick}
                ></Button>
              </div>
            </div>
          </div>
        </Modal.Content>
      </Modal>
    );
  }
}

export default ServiceLocationsModal;
