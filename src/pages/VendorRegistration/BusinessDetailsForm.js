import { Component } from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { LabelField } from "../../components/Label/LabelField";
import { InputField } from "../../components/InputField/Inputfield";
import { CheckBox } from "../../components/Checkbox";
import { RadioButton } from "../../components/RadioButton/RadioButton";
import { Button } from "../../components/Button";
import { Tag } from "../../components/Tag/Tag";
import CloseIcon from "@material-ui/icons/Close";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import addIcon from "../../images/addIcon.svg";
import { Image } from "../../components/Image";
import fileUploadIcon from "../../images/fileUploadIcon.svg";

export class BusinessDetailsForm extends Component {
  state = {
    documentation: [
      "ISO certification 1",
      "ISO certification 2",
      "ISO certification 3"
    ],
    turnOverData: ["100-120 Millions", "100-120 Millions", "120-140 Millions"],
    employees: ["0-100 employees", "100-200 employees", "300-400 employees"]
  };
  render() {
    return (
      <div className="formDetailsMainDiv">
        <Row className="rowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">Business Documents </div>
          </Col>

          <Col md="6">
            <Row>
              <Col md="7">
                <div>
                  <LabelField label="* Trade license" />
                </div>
                <div>
                  <InputField
                    type="file"
                    name="profile"
                    id="profile"
                    //placeholder="choose file"
                    classname="inputFileStyle inputFieldStyle"
                    fileIcon={<AttachFileIcon />}
                  />
                </div>
              </Col>
              <Col md="5">
                <div>
                  <LabelField label="* Expiry date" />
                </div>
                <div>
                  <InputField
                    type="date"
                    name="expirydate"
                    id="expirydate"
                    //placeholder="choose file"
                    classname="inputFileStyle inputFieldStyle"
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>

          <Col md="6">
            <Row>
              <Col md="7">
                <div>
                  <LabelField label="* Tax ID for GCC" />
                </div>
                <div>
                  <InputField
                    type="file"
                    name="taxid"
                    id="taxid"
                    //placeholder="choose file"
                    classname="inputFileStyle inputFieldStyle"
                    fileIcon={<AttachFileIcon />}
                  />
                </div>
                <div> (i.e. Gulf Cooperation Council) companies </div>
              </Col>
              <Col md="5">
                <div>
                  <LabelField label="Expiry date" />
                </div>
                <div>
                  <InputField
                    type="date"
                    name="expirydate1"
                    id="expirydate1"
                    //placeholder="choose file"
                    classname="inputFileStyle inputFieldStyle"
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6">
            <div>
              <LabelField label="* TRN  (Enter the 15 digit ID )" />
            </div>
            <div>
              <InputField
                type="text"
                name="trn"
                id="trn"
                placeholder="100341059200003"
                classname="inputStyle"
                option={this.state.countries}
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6">
            <div>
              <LabelField label="* VAT Exemption letter " />
            </div>
            <div>
              <InputField
                type="file"
                name="vat"
                id="vat"
                placeholder="85877676"
                classname="inputStyle"
                option={this.state.countries}
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6" className="d-flex">
            <div>
              <Button
                outline="secondary"
                icon="icon"
                iconName={<Image src={addIcon} />}
                label="Add another document"
              ></Button>
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">Certification </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6">
            Buying organizations in UAE are keen on employing certified vendors,
            so we recommend you to upload as many as you have and increase your
            visibility.
          </Col>
          <Col md="6" className="d-flex">
            <div>
              <Button
                outline="secondary"
                icon="icon"
                iconName={<Image src={addIcon} />}
                label="Add a document"
              ></Button>
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6">
            Having no certification imply that your company does not have any
            certification at all.
          </Col>
          <Col md="6">
            <div>
              <LabelField label="* Select document type" />
            </div>
            <div>
              <InputField
                type="select"
                name="trn"
                id="trn"
                classname="inputStyle"
                option={this.state.documentation}
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6">
            <div>
              <LabelField label="* Certification Title" />
            </div>
            <div>
              <InputField
                type="text"
                name="certificationtitle"
                id="certificationtitle"
                placeholder="ISO 26000 Certified (Social Responsibility)"
                classname="inputStyle"
                option={this.state.countries}
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"> </Col>
          <Col md="6">
            <Row>
              <Col md="6">
                <div>
                  <LabelField label="* Certification Number" />
                </div>
                <div>
                  <InputField
                    type="text"
                    name="certificationnumber"
                    id="certificationnumber"
                    classname="inlineColStyle"
                    placeholder="47575775ABC"
                  />
                </div>
              </Col>
              <Col md="6">
                <div>
                  <LabelField label="Year of Publication" />
                </div>
                <div>
                  <InputField
                    type="number"
                    name="yearofpublication"
                    id="yearofpublication"
                    classname="inlineColStyle"
                    placeholder="2014"
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"> </Col>
          <Col md="6">
            <Row>
              <Col md="6">
                <div>
                  <LabelField label="* Certificate Issued by" />
                </div>
                <div>
                  <InputField
                    type="text"
                    name="issuedby"
                    id="issuedby"
                    classname="inlineColStyle"
                    placeholder="Authority"
                  />
                </div>
              </Col>
              <Col md="6">
                <div>
                  <LabelField label="Certificate Issued at" />
                </div>
                <div>
                  <InputField
                    type="text"
                    name="issuedat"
                    id="issuedat"
                    classname="inlineColStyle"
                    placeholder="location"
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"> </Col>
          <Col md="6">
            <Row>
              <Col md="6">
                <div>
                  <LabelField label="* Effective Date" />
                </div>
                <div>
                  <InputField
                    type="date"
                    name="effectivedate"
                    id="effectivedate"
                    classname="inlineColStyle"
                  />
                </div>
              </Col>
              <Col md="6">
                <div>
                  <LabelField label="Expiry Date" />
                </div>
                <div>
                  <InputField
                    type="date"
                    name="expirydate"
                    id="expirydate"
                    classname="inlineColStyle"
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"> </Col>
          <Col md="6">
            <Row>
              <Col md="4">
                <div className="buttonStyle">
                  <Button variant="secondary" label="Save"></Button>
                </div>
              </Col>
              <Col md="6" className="d-flex">
                <div>
                  <Button variant="link" label="Cancel"></Button>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        
        <Row className="rowStyle">
          <Col md="6">
            <div className="rowCol1TextDiv">Company Size</div>
          </Col>
          <Col md="6">
            <div>
              <LabelField label="*Annual TurnOver (AED)" />
            </div>
            <div className="d-flex">
              <InputField
                type="select"
                name="turnover"
                id="turnover"
                classname="selectOption"
                option={this.state.turnOverData}
              />
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6"></Col>
          <Col md="6">
            <div>
              <LabelField label="* Size" />
            </div>
            <div className="d-flex">
              <InputField
                type="select"
                name="size"
                id="size"
                classname="selectOption"
                option={this.state.employees}
              />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
