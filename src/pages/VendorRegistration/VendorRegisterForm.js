import React, { Component, createRef } from "react";
import { Row, Col, FormGroup, Table } from "reactstrap";
import { Button } from "../../components/Button";
import { Header } from "./Header";
import { TabComponent } from "../../components/Tab/TabComponent";
import { CompanyDetailsForm } from "./CompanyDetailsForm";
import { BusinessDetailsForm } from "./BusinessDetailsForm";
import { InputField } from "../../components/InputField/Inputfield";
import { LabelField } from "../../components/Label/LabelField";
import { CheckBox } from "../../components/Checkbox";
import { RadioButton } from "../../components/RadioButton/RadioButton";
import { Tag } from "../../components/Tag/Tag";
import CloseIcon from "@material-ui/icons/Close";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import { DropDownComponent as DropDown } from "../../components/DropDownComponent";
import { IconComponent } from "../../components/Icons";
import { Form } from "react-bootstrap";
import { DatepickerComponent } from "../../components/DatePicker";
import Select from "react-select";
import { Modal } from "../../components/Modal";
import { FileUpload } from "../../components/FileUpload";
import { Popover } from "antd";
import moment from "moment";
import { MultiSelectComponent } from "../../components/MultiSelect";
import { Formik } from "formik";
import * as Yup from "yup";
import { Avatar } from "../../components/Avatar";
import ProductAndServiceModal from "./ProductAndServiceModal";
import ServiceLocationsModal from "./ServiceLocationsModal";
import IndustriesModal from "./IndustriesModal";
// import API from "../../api/api";
import API from "../../api/api";
// const stringifyObject = require('stringify-object');

export class VendorRegisterForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      tabs: [
        {
          id: 1,
          label: "Company Details"
        },
        { id: 2, label: "Business Credentials" },
        {
          id: 3,
          label: "Address and Contacts"
        },
        {
          id: 4,
          label: "Experience"
        }
      ],
      options: [
        { key: "India", cat: "" },
        { key: "Dubai", cat: "" }
      ],
      companyName: "",
      year: "",
      activeTab: 1,
      documentation: [
        "ISO certification 1",
        "ISO certification 2",
        "ISO certification 3"
      ],
      // turnOverData: ["100-120 Millions", "100-120 Millions", "120-140 Millions"],
      // employees: ["0-100 employees", "100-200 employees", "300-400 employees"],
      //Limited Liability Company Civil company Joint venture company Private shareholding company Public shareholding company
      companyType: [
        { key: "Limited Liability Company" },
        { key: "Civil company Joint venture company" },
        { key: "Private shareholding company" },
        { key: "Public shareholding company" }
      ],
      selectedDate: "",
      documentType: [
        {
          value: "iso",
          label: "ISO"
        },
        {
          value: "cmm",
          label: "CMM"
        }
      ],
      turnOverData: [
        { value: "100-120 Millions", label: "100-120 Millions" },
        { value: "100-120 Millions", label: "120-140 Millions" },
        { value: "100-120 Millions", label: "140-160 Millions" }
      ],
      companySize: [
        { value: "0-100 employees", label: "0-100 employees" },
        { value: "100-200 employees", label: "100-200 employees" },
        { value: "200-300 employees", label: "200-300 employees" }
      ],
      checked: "yes",
      companyProfile: "",
      companyLogo: "",
      selectedDateForTradeLicense: "",
      tradeLicense: "",
      taxIdForGCC: "",
      vatExemption: "",
      isOpenBusinessDetails: false,
      selectedEffectiveDate: "",
      selectedExpiryDate: "",
      fileLists: [],
      isAddToProfileOpen: false,
      documentTitle: "",
      certificationNumber: "",
      yearOfPublication: "",
      certificateIssuedAt: "",
      certificateIssuedBy: "",
      selectedDocumentType: "",
      isOpenDocumentLabel: false,
      documentLabel: "",
      documentFile: "",
      popOverOpen: false,
      visible: false,
      selectedDataIndex: "",
      selectedModalDataIndex: "",
      isOpenDetailsForAddToProfile: false,
      selectedModalDataIndexForAddToProfile: "",
      isOpenAddressDetailsModal: false,
      countryData: [
        { value: "india", label: "India" },

        { value: "dubai", label: "Dubai" }
      ],
      selectedCountry: "",
      addressName: "",
      streetLine1: "",
      streetLine2: "",
      stateOrProvince: "",
      city: "",
      postBoxNumber: "",
      addressDetails: [],
      isOpenContactDetailsModal: false,
      phoneNumberForContact: "",
      emailForContact: "",
      jobTitleForContact: "",
      personName: "",
      // selectedAddressIndex: '',
      contactDetails: [],
      selectedContactIndex: null,
      testImage: "",
      profileImage: "",
      consultant: [
        { value: "Arif & Bintoak", label: "Arif & Bintoak" },
        { value: "Consultant 1", label: "Consultant 1" },
        { value: "Consultant 3", label: "Consultant 3" }
      ],
      projectValue: [
        { value: "<  1 million", label: "<  1 million" },
        { value: "<  2 million", label: "<  2 million" },
        { value: "<  3 million", label: "<  3 million" }
      ],
      projectDuration: [
        { value: "<  1 million", label: "<  1 million" },
        { value: "<  2 ", label: "<  2" },
        { value: "<  3", label: "<  3" }
      ],
      selectedConsultant: "",
      selectedProjectDuration: "",
      selectedProjectValue: "",
      consultants: [
        { key: "Arif & Bintoak", cat: "" },
        { key: "NEB", cat: "" },
        { key: "Consultant 3", cat: "" },
        { key: "Consultant 4", cat: "" },
        { key: "Consultant 5", cat: "" },
        { key: "Consultant 6", cat: "" }
      ],
      isOpenProjectModal: false,
      isProjectAddToProfileOpen: false,
      isOpenDetailsForProject: false,
      projectName: "",
      projectDescription: "",
      client: "",
      dropdownVal: "",
      dropdownValForCompanyType: "",
      isProductsOpen: false,
      categoryList: [],
      selectedProductItems: [],
      selectedLocationItems: [],
      industriesList: [
        {
          id: 1,
          name: "Aerospace"
        },
        {
          id: 2,
          name: "Agriculture"
        },
        {
          id: 2,
          name: "Building Material, Clay & Glass"
        }
      ],
      selectedIndustryItems: [],
      selectedIndustryIdList: [],
      id: 25
    };
    this.style = {
      chips: {
        //background: "#fff",
        color: "#212B36",
        background: "#E9EEF2",
        borderRadius: "3px",
        paddingTop: 4,
        paddingBottom: 4
      }
    };
    this.fileUploadTradeLicense = React.createRef();
    this.fileUploadTaxGCC = React.createRef();
    this.fileUploadVat = React.createRef();
    this.fileUploadDocumentFile = React.createRef();
    this.fileUploadCompanyLogo = React.createRef();
    this.fileUpload = React.createRef();
    this.form1 = React.createRef();
    this.form2 = React.createRef();
  }

  onSaveButtonClick = (e, values) => {
    e.preventDefault();
    // async (values) => {
    // await new Promise((resolve) => setTimeout(resolve, 500));
    // alert(JSON.stringify(values, null, 2));
    // }

    // console.log("name..." + console.log(this.state.companyName));
    // console.log("year..." + console.log(this.state.year));

    var data = {
      companyname: this.state.companyName,
      country_Registration: "text",
      year_established: this.state.year,
      company_type: "text",
      businesstype: "text",
      what_does_your_company_do: "text",
      company_profile: "text",
      company_logo: "text",
      company_website: "text",
      is_parent_company: "text",
      parent_company_name: "text",
      fullname: "text",
      job_title: "text",
      emailid: "text",
      phoneno: "text",
      faxno: "text",
      product_and_service_categories: "text1,text2",
      ship_or_servicelocation: "text1,text2",
      industries_you_serve_to: "text1,text2",
      business_document: [],

      address: [],
      contacts: [],
      projects: [],
      consultants: "text1,text2",

      isOpen: false,
      dropdownOpen: false,
      dropdownVal: "",
      dropdownValForCompanyType: "",
      dropdownOpenForCompanyType: false,
      modalDetails: []
    };
  };

  handleInputChange = (value, name) => {
    this.setState({ [name]: value });
  };
  toggle = (name) => {
    let { dropdownOpen } = this.state;
    if (name == "countryRegistration") {
      this.setState({ dropdownOpen: !this.state.dropdownOpen });
    }
    if (name == "companyTypeValue") {
      this.setState({
        dropdownOpenForCompanyType: !this.state.dropdownOpenForCompanyType
      });
    }
  };
  toggleItem = (value, name) => {
    if (name == "countryRegistration") {
      // console.log("inside status.." + value);
      this.setState({
        dropdownOpen: !this.state.dropdownOpen,
        dropdownVal: value,
        errorMessageForCountryReg: ""
      });
    }
    if (name == "companyTypeValue") {
      this.setState({
        dropdownOpenForCompanyType: !this.state.dropdownOpenForCompanyType,
        dropdownValForCompanyType: value,
        errorMessageForCompanyType: ""
      });
    }
  };
  onTabChange = (activeTab) => {
    //this.setState({ activeTab: activeTab });
  };
  onChangeDate = (selected) => {
    this.setState({ selectedDate: selected });
  };
  onChangeTradeLicenseDate = (selected) => {
    this.setState({ selectedDateForTradeLicense: selected });
  };
  handleChange = (event) => {
    this.setState({ checked: event.target.value });
  };
  onCompanyProfileChange = (event) => {
    this.setState({ companyProfile: event.target.files[0].name });
  };

  onCompanyLogoChange = (event) => {
    this.setState({ companyLogo: event.target.files[0].name });
  };
  onTradeLicenseChange = (event) => {
    this.setState({ tradeLicense: event.target.files[0].name });
  };
  onTaxIdChange = (event) => {
    this.setState({ taxIdForGCC: event.target.files[0].name });
  };
  onVatExemptionChange = (event) => {
    this.setState({ vatExemption: event.target.files[0].name });
  };
  onUploadIconClick = () => {
    this.fileUpload.click();
    //this.setState({ companyProfile: event.target.files[0].name})
  };
  onUploadIconClickForCompanyLogo = () => {
    this.fileUploadCompanyLogo.click();
  };
  onUploadIconClickTradeLicense = () => {
    this.fileUploadTradeLicense.click();
  };
  onUploadIconForTaxId = () => {
    this.fileUploadTaxGCC.click();
  };
  onUploadIconVatExemption = () => {
    // console.log("inside upload...");
    this.fileUploadVat.click();
  };
  hideModal = () => {
    this.setState({
      isOpenBusinessDetails: false,
      selectedDocumentType: "",
      documentTitle: "",
      certificationNumber: "",
      certificateIssuedAt: "",
      certificateIssuedBy: "",
      yearOfPublication: "",
      selectedEffectiveDate: "",
      selectedExpiryDate: "",
      fileLists: [],
      modalDetails: []
    });
  };
  onAddAnotherDocumentButtonClick = () => {
    this.setState({ isOpenBusinessDetails: true });
  };
  onChangeEffectiveDate = (selected) => {
    this.setState({ selectedEffectiveDate: selected });
  };
  onChangeExpiryDate = (selected) => {
    this.setState({ selectedExpiryDate: selected });
  };
  handleFileChange = (files) => {
    let { fileLists } = this.state;
    this.setState(fileLists);
    for (let i = 0; i < files.length; i++) {
      fileLists.push(files[i]);
    }
  };
  hideModalForProfile = () => {
    this.setState({ isAddToProfileOpen: false });
  };

  onDoneButtonClick = () => {
    var isAdded = false;

    let {
      selectedDocumentType,
      documentTitle,
      certificationNumber,
      certificateIssuedAt,
      certificateIssuedBy,
      yearOfPublication,
      selectedEffectiveDate,
      selectedExpiryDate,
      fileLists,
      selectedIndex,
      selectedCertificateId
    } = this.state;

    let temp = [];
    console.log(
      "selectedDocumentType..." + JSON.stringify(selectedDocumentType)
    );
    // if (this.state.selectedIndex || this.state.selectedIndex == 0) {
    if (this.state.selectedCertificateId) {
      let updateCert = {
        //"annual_trun_over": "text",
        certificate_issued_at: certificateIssuedAt,
        certificate_issued_by: certificateIssuedBy,
        document_type:
          selectedDocumentType &&
          selectedDocumentType.value &&
          selectedDocumentType.value.toString(),
        //effective_date: selectedEffectiveDate,
        //expiry_date: selectedExpiryDate,
        //"file_url": fileLists,
        title_of_document: documentTitle,
        vendorid: this.state.id,
        certification_number: certificationNumber,
        year_of_publication: yearOfPublication
      };
      if (selectedEffectiveDate) {
        updateCert["effective_date"] = selectedEffectiveDate;
      }
      if (selectedExpiryDate) {
        updateCert["expiry_date"] = selectedExpiryDate;
      }
      API.fetchData(API.Query.UPDATE_VENDOR_CERTIFICATE, {
        id: selectedCertificateId,
        data: updateCert
      }).then((response) => {
        console.log(
          "update response for certification",
          JSON.stringify(response)
        );
        if (response.data) {
          this.setState({ selectedCertificateId: "" });
          this.fetchVendorCertification();
        }
      });

      // let temp = [...this.state.modalDetails];
      // temp[selectedIndex].selectedDocumentType = selectedDocumentType;
      // temp[selectedIndex].documentTitle = documentTitle;
      // temp[selectedIndex].certificationNumber = certificationNumber;
      // temp[selectedIndex].yearOfPublication = yearOfPublication;
      // temp[selectedIndex].certificateIssuedAt = certificateIssuedAt;
      // temp[selectedIndex].certificateIssuedBy = certificateIssuedBy;

      // temp[selectedIndex].selectedEffectiveDate = selectedEffectiveDate;
      // temp[selectedIndex].selectedExpiryDate = selectedExpiryDate;
      // temp[selectedIndex].fileLists = fileLists;

      // this.setState({ modalDetails: temp });
    } else {
      // let tempObj = {
      //   selectedDocumentType: selectedDocumentType,
      //   documentTitle: documentTitle,
      //   certificationNumber: certificationNumber,
      //   certificateIssuedBy: certificateIssuedBy,
      //   certificateIssuedAt: certificateIssuedAt,
      //   yearOfPublication: yearOfPublication,
      //   selectedEffectiveDate: selectedEffectiveDate,
      //   selectedExpiryDate: selectedExpiryDate,
      //   fileLists: fileLists
      // };

      // temp.push(tempObj);

      let cert = [];

      let certObj = {
        vendorid: this.state.id,
        document_type:
          selectedDocumentType &&
          selectedDocumentType.value &&
          selectedDocumentType.value.toString(),
        title_of_document: documentTitle,
        certificate_issued_by: certificateIssuedBy,
        certificate_issued_at: certificateIssuedAt,
        //effective_date: selectedEffectiveDate,
        //expiry_date: selectedExpiryDate,
        //"file_url": fileLists
        certification_number: certificationNumber,
        year_of_publication: yearOfPublication
      };
      if (selectedEffectiveDate) {
        certObj["effective_date"] = selectedEffectiveDate;
      }
      if (selectedExpiryDate) {
        certObj["expiry_date"] = selectedExpiryDate;
      }

      cert.push(certObj);

      // if (this.state.modalDetails && this.state.modalDetails.length > 0) {
      //   this.setState({
      //     modalDetails: [...this.state.modalDetails, tempObj],
      //     selectedDocumentType: "",
      //     documentTitle: "",
      //     certificationNumber: "",
      //     certificateIssuedAt: "",
      //     certificateIssuedBy: "",
      //     yearOfPublication: "",
      //     selectedEffectiveDate: "",
      //     selectedExpiryDate: "",
      //     fileLists: []
      //   });
      // } else {
      API.fetchData(API.Query.CREATE_VENDOR_CERTIFICATION, {
        data: cert
      }).then((response) => {
        console.log("response for certification", JSON.stringify(response));
        if (response.data) {
          //this.setState({ activeTab: activeTab+1, id: response.data.id })
          this.fetchVendorCertification();
        }
      });

      this.setState({
        // modalDetails: temp,
        selectedDocumentType: "",
        documentTitle: "",
        certificationNumber: "",
        certificateIssuedAt: "",
        certificateIssuedBy: "",
        yearOfPublication: "",
        selectedEffectiveDate: "",
        selectedExpiryDate: "",
        fileLists: []
      });
      // }
    }
  };
  // API to GET Certificate
  fetchVendorCertification = () => {
    API.fetchData(API.Query.GET_VENDOR_CERTIFICATION, {
      vendorid: this.state.id
    }).then((response) => {
      console.log("response for get certification", JSON.stringify(response));
      if (response.vendor_certification) {
        // console.log("inside..if")
        this.setState({
          modalDetails: response.vendor_certification,
          isOpenBusinessDetails: false,
          isAddToProfileOpen: true
        });
      } else {
        // console.log("inside else")
        this.setState({
          isOpenBusinessDetails: false,
          isAddToProfileOpen: true
        });
      }
    });
  };

  onEditIconClick = (data, index, id) => {
    console.log("date.." + moment(data.effective_date));
    console.log("date2.." + new Date(data.effective_date));

    this.setState({ isOpenBusinessDetails: true });
    let temp = [...this.state.modalDetails];

    this.setState({
      // modalDetails: temp,
      selectedIndex: index,
      //selectedDocumentType: data.document_type,
      selectedDocumentType: {
        label: data.document_type,
        value: data.document_type
      },
      documentTitle: data.title_of_document,
      certificationNumber: data.certification_number,
      certificateIssuedAt: data.certificate_issued_at,
      certificateIssuedBy: data.certificate_issued_by,
      yearOfPublication: data.year_of_publication,
      selectedEffectiveDate: data.effective_date
        ? new Date(data.effective_date)
        : null,
      selectedExpiryDate: data.expiry_date ? new Date(data.expiry_date) : null,
      selectedCertificateId: id
      //fileLists: data.fileLists
    });
  };

  onDeleteIconClick = (index, id) => {
    // let temp = [...this.state.modalDetails];
    // temp.splice(index, 1);
    // this.setState({ modalDetails: temp });
    API.fetchData(API.Query.DELETE_VENDOR_CERTIFICATION, {
      id: id
    }).then((response) => {
      console.log("response for certification", JSON.stringify(response));
      if (response && response.data) {
        //this.setState({ activeTab: activeTab+1, id: response.data.id })
        this.fetchVendorCertification();
      }
    });
  };
  onAddToProfileButtonClick = () => {
    this.setState({
      isOpenBusinessDetails: false,
      isAddToProfileOpen: false,
      modalData: this.state.modalDetails
    });
  };
  // form = createRef(null);

  onDocumentTypeChange = (selectedOption) => {
    this.setState({ selectedDocumentType: selectedOption });
  };
  onChangeModalFieldValue = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  onAddDocumentButtonClick = () => {
    this.setState({ isOpenDocumentLabel: true });
  };
  hideModalForDocumentLabel = () => {
    this.setState({ isOpenDocumentLabel: false });
  };
  onLabelSaveButtonClick = () => {
    this.setState({
      documentLabel: this.state.documentLabel,
      isOpenDocumentLabel: false
    });
  };
  onUploadDocumentFile = () => {
    this.fileUploadDocumentFile.click();
  };
  onDocumentLableFileChange = (event) => {
    this.setState({ documentFile: event.target.files[0].name });
  };
  togglePopOver = () => {
    this.setState({ popOverOpen: !this.state.popOverOpen });
  };
  viewDetailsClick = () => {
    this.setState({ popOverOpen: !this.state.popOverOpen });
  };
  hide = () => {
    this.setState({
      visible: false
    });
  };

  handleVisibleChange = (visible) => {
    this.setState({ visible });
  };
  onDetailsClick = (index) => {
    this.setState({ selectedDataIndex: index });
  };
  onDetailsClickInAddDocument = (index) => {
    this.setState({
      selectedModalDataIndex: index,
      isOpenDetails: !this.state.isOpenDetails
    });
  };
  onDetailsClickInAddToProfile = (index) => {
    this.setState({
      selectedModalDataIndexForAddToProfile: index,
      isOpenDetailsForAddToProfile: !this.state.isOpenDetailsForAddToProfile
    });
  };
  onModalClick = (isModal) => {
    this.setState({ isModal: isModal });
  };
  onContactModalClick = (isContactModal) => {
    this.setState({ isContactModal: isContactModal });
  };
  onAddAnAddressButtonClick = (e) => {
    e.preventDefault();
    this.setState({ isOpenAddressDetailsModal: true });
  };
  hideAddressDetails = () => {
    this.setState({ isOpenAddressDetailsModal: false });
  };

  onCountryChange = (selected) => {
    this.setState({ selectedCountry: selected });
  };

  onAddressSaveButtonClick = () => {
    // this.setState({
    //   //isOpenAddressDetailsModal: false,
    //   selectedAddressIndex: null
    // });
    let {
      addressName,
      selectedCountry,
      streetLine1,
      streetLine2,
      stateOrProvince,
      city,
      postBoxNumber,
      selectedAddressIndex,
      selectedAddressId
    } = this.state;
    //let temp = [];

    // if (
    //   this.state.selectedAddressIndex ||
    //   this.state.selectedAddressIndex == 0
    // )
    if (selectedAddressId) {
      // "vendorid": "546",
      let updateAddress = {
        address_line_one: streetLine1,
        address_line_two: streetLine2,
        address_name: addressName,
        city: city,
        country: selectedCountry && selectedCountry.value,
        postbox: postBoxNumber,
        //"updated_at": "text",
        vendorid: this.state.id,
        state: stateOrProvince
      };
      API.fetchData(API.Query.UPDATE_VENDOR_ADDRESS, {
        id: selectedAddressId,
        data: updateAddress
      }).then((response) => {
        console.log("update response for address", JSON.stringify(response));
        if (response.data) {
          this.fetchVendorAddress();
        }
      });

      // temp = [...this.state.addressDetails];
      // temp[selectedAddressIndex].addressName = addressName;
      // temp[selectedAddressIndex].selectedCountry = selectedCountry;
      // temp[selectedAddressIndex].streetLine1 = streetLine1;
      // temp[selectedAddressIndex].streetLine2 = streetLine2;
      // temp[selectedAddressIndex].stateOrProvince = stateOrProvince;
      // temp[selectedAddressIndex].city = city;
      // temp[selectedAddressIndex].postBoxNumber = postBoxNumber;

      //this.setState({ addressDetails: temp });
    } else {
      // let tempObj = {
      //   addressName: addressName,
      //   selectedCountry: selectedCountry,
      //   streetLine1: streetLine1,
      //   streetLine2: streetLine2,
      //   stateOrProvince: stateOrProvince,
      //   city: city,
      //   postBoxNumber: postBoxNumber
      // };
      // temp.push(tempObj);

      // if (this.state.addressDetails && this.state.addressDetails.length > 0) {
      // this.setState({
      //   addressDetails: [...this.state.addressDetails, tempObj],
      //   //selectedDocumentType: "",
      //   addressName: "",
      //   selectedCountry: "",
      //   streetLine1: "",
      //   streetLine2: "",
      //   stateOrProvince: "",
      //   city: "",
      //   postBoxNumber: "",
      //   selectedAddressIndex: ""
      // });
      // } else {
      let address = [];
      let addressObj = {
        vendorid: this.state.id,
        address_name: addressName,
        country: selectedCountry && selectedCountry.value.toString(),
        address_line_one: streetLine1,
        address_line_two: streetLine2,
        city: city,
        state: stateOrProvince,
        postbox: postBoxNumber
      };
      address.push(addressObj);
      API.fetchData(API.Query.CREATE_VENDOR_ADDRESS, { data: address }).then(
        (response) => {
          console.log("response for address", JSON.stringify(response));
          if (response.data) {
            this.fetchVendorAddress();
          }
        }
      );
      this.setState({
        //addressDetails: temp,
        addressName: "",
        selectedCountry: "",
        streetLine1: "",
        streetLine2: "",
        stateOrProvince: "",
        city: "",
        postBoxNumber: ""
      });
      // }
    }
  };
  // API to GET Address
  fetchVendorAddress = () => {
    API.fetchData(API.Query.GET_VENDOR_ADDRESS, {
      vendorid: this.state.id
    }).then((response) => {
      console.log("response for get address", JSON.stringify(response));
      if (response.vendor_address) {
        // console.log("inside..if")
        this.setState({
          addressDetails: response.vendor_address,
          isOpenAddressDetailsModal: false
        });
      } else {
        // console.log("inside else")
        this.setState({ isOpenAddressDetailsModal: false });
      }
    });
  };

  onRemoveAddressIconClick = (index, id) => {
    // let temp = [...this.state.addressDetails];
    // temp.splice(index, 1);
    // this.setState({ addressDetails: temp });
    API.fetchData(API.Query.DELETE_VENDOR_ADDRESS, {
      id: id
    }).then((response) => {
      console.log("response for delete address", JSON.stringify(response));
      if (response && response.data) {
        //this.setState({ activeTab: activeTab+1, id: response.data.id })
        this.fetchVendorAddress();
      }
    });
  };
  onEditAddressIconClick = (data, index, id) => {
    this.setState({ isOpenAddressDetailsModal: true });
    //let temp = [...this.state.addressDetails];
    this.setState({
      selectedAddressId: id,
      //addressDetails: temp,
      selectedAddressIndex: index,
      addressName: data.address_name,
      selectedCountry: { label: data.country, value: data.country },
      streetLine1: data.address_line_one,
      streetLine2: data.address_line_two,
      stateOrProvince: data.state,
      city: data.city,
      postBoxNumber: data.postbox
    });
  };
  onContactButtonClick = (e) => {
    e.preventDefault();
    this.setState({ isOpenContactDetailsModal: true });
  };
  hideContactDetails = () => {
    this.setState({ isOpenContactDetailsModal: false });
  };
  onContactSaveButtonClick = (values, errors) => {
    // console.log("this.state.profileImage.." + this.state.profileImage);
    // e.preventDefault();
    // console.log("values..." + JSON.stringify(values));
    //console.log("errors..." + JSON.stringify(errors));
    if (Object.keys(errors).length) {
      return;
    } else {
      let {
        personName,
        jobTitleForContact,
        emailForContact,
        phoneNumberForContact,
        selectedContactIndex,
        selectedContactsId
      } = this.state;
      if (
        personName &&
        jobTitleForContact &&
        emailForContact &&
        phoneNumberForContact
      ) {
        this.setState({
          isOpenContactDetailsModal: false,
          selectedContactIndex: null
          // selectedContactsId: ''
        });
        //let temp = [];
        // if (
        //   this.state.selectedContactIndex ||
        //   this.state.selectedContactIndex == 0
        // )
        if (selectedContactsId) {
          // "vendorid": "546",
          let updateConatcts = {
            contact_img: this.state.profileImage,
            emailid: emailForContact,
            job_title: jobTitleForContact,
            name: personName,
            phone: phoneNumberForContact
          };
          API.fetchData(API.Query.UPDATE_VENDOR_CONTACTS, {
            id: selectedContactsId,
            data: updateConatcts
          }).then((response) => {
            console.log("update res for contact", JSON.stringify(response));
            if (response.data) {
              this.fetchVendorContacts();
            }
          });

          // temp = [...this.state.contactDetails];
          // temp[selectedContactIndex].personName = personName;
          // temp[selectedContactIndex].jobTitleForContact = jobTitleForContact;
          // temp[selectedContactIndex].emailForContact = emailForContact;
          // temp[
          //   selectedContactIndex
          // ].phoneNumberForContact = phoneNumberForContact;

          // this.setState({ contactDetails: temp });
        } else {
          // let tempObj = {
          //   personName: personName,
          //   jobTitleForContact: jobTitleForContact,
          //   emailForContact: emailForContact,
          //   phoneNumberForContact: phoneNumberForContact
          // };
          // temp.push(tempObj);

          // if (
          //   this.state.contactDetails &&
          //   this.state.contactDetails.length > 0
          // ) {
          //   this.setState({
          //     contactDetails: [...this.state.contactDetails, tempObj],
          //     //selectedDocumentType: "",
          //     personName: "",
          //     jobTitleForContact: "",
          //     emailForContact: "",
          //     phoneNumberForContact: ""
          //   });
          // } else {
          let contact = [];
          let contactObj = {
            vendorid: this.state.id,
            contact_img: this.state.profileImage,
            name: personName,
            job_title: jobTitleForContact,
            emailid: emailForContact,
            phone: phoneNumberForContact
          };
          contact.push(contactObj);
          API.fetchData(API.Query.CREATE_VENDOR_CONTACT, {
            data: contact
          }).then((response) => {
            console.log("response for contact", JSON.stringify(response));
            if (response.data) {
              this.fetchVendorContacts();
            }
          });
          // this.setState({
          //   //contactDetails: temp,
          //   personName: "",
          //   jobTitleForContact: "",
          //   emailForContact: "",
          //   phoneNumberForContact: ""
          // });
          // }
        }
      }
    }
  };
  onRemoveContactIconClick = (index, id) => {
    // let temp = [...this.state.contactDetails];
    // temp.splice(index, 1);
    // this.setState({
    //   contactDetails: temp,
    //   personName: "",
    //   jobTitleForContact: "",
    //   emailForContact: "",
    //   phoneNumberForContact: ""
    // });

    API.fetchData(API.Query.DELETE_VENDOR_CONTACTS, {
      id: id
    }).then((response) => {
      console.log("response for delete contacts", JSON.stringify(response));
      if (response && response.data) {
        this.fetchVendorContacts();
      }
    });
  };
  onEditContactIconClick = (data, index, id) => {
    this.setState({ isOpenContactDetailsModal: true });
    let temp = [...this.state.contactDetails];
    this.setState({
      //contactDetails: temp,
      selectedContactsId: id,
      selectedContactIndex: index,
      personName: data.name,
      jobTitleForContact: data.job_title,
      emailForContact: data.emailid,
      phoneNumberForContact: data.phone
      //contact_img: ''
    });
  };

  //get contacts
  fetchVendorContacts = () => {
    API.fetchData(API.Query.GET_VENDOR_CONTACTS, {
      vendorid: this.state.id
    }).then((response) => {
      console.log("response for get contacts", JSON.stringify(response));
      if (response.vendor_contacts) {
        // console.log("inside..if")
        this.setState({
          contactDetails: response.vendor_contacts,
          isOpenContactDetailsModal: false,
          //contactDetails: temp,
          personName: "",
          jobTitleForContact: "",
          emailForContact: "",
          phoneNumberForContact: ""
        });
      } else {
        // console.log("inside else")
        this.setState({
          isOpenContactDetailsModal: false,
          personName: "",
          jobTitleForContact: "",
          emailForContact: "",
          phoneNumberForContact: ""
        });
      }
    });
  };

  onUserProfileImageChange = (event) => {
    let reader = new FileReader();
    reader.onload = (e) => {
      // console.log("e.target.result.." + e.target.result);
      this.setState({ profileImage: e.target.result });
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  //Experience
  onProjectValueChange = (selected) => {
    this.setState({ selectedProjectValue: selected });
  };
  onConsultantChange = (selected) => {
    // console.log("selected consultant.." + selected);
    this.setState({ selectedConsultant: selected });
  };
  onProjectDurationChange = (selected) => {
    this.setState({ selectedProjectDuration: selected });
  };

  onSelect = (value, name) => {
    // console.log("value.." + JSON.stringify(value));
    if (name == "consultants") {
      this.setState({ selectedConsultantValue: value });
    }
  };
  onRemove = (value, name) => {
    if (name == "consultants") {
      this.setState({ selectedConsultantValue: value });
    }
  };
  onChangeFromDate = (selected) => {
    this.setState({ selectedDateForFromDate: selected });
  };
  onChangeToDate = (selected) => {
    //this.setState({ selectedDateForToDate: selected });
  };

  onAddProjectuttonClick = (e) => {
    e.preventDefault();
    this.setState({ isOpenProjectModal: true });
  };
  hideExperienceProjectDetails = () => {
    this.setState({ isOpenProjectModal: false });
  };

  onProjectDoneButtonClick = (values, errors) => {
    console.log("values..." + JSON.stringify(values));
    console.log("errors..." + JSON.stringify(errors));

    if (Object.keys(errors).length) {
      console.log("inside if..");
      return;
    } else if (values.selectedDateForFromDate) {
      console.log("inside else..");

      this.setState({
        isOpenProjectModal: false,
        isProjectAddToProfileOpen: true
      });

      let {
        projectName,
        client,
        selectedConsultant,
        projectDescription,
        selectedProjectValue,
        selectedProjectDuration,
        selectedDateForFromDate,
        selectedDateForToDate,
        selectedProjectId,
        selectedIndexForProject
      } = this.state;

      let temp = [];
      // if (
      //   this.state.selectedIndexForProject ||
      //   this.state.selectedIndexForProject == 0
      // ) {
      //   let temp = [...this.state.projectDetails];
      //   temp[selectedIndexForProject].projectName = projectName;
      //   temp[selectedIndexForProject].client = client;
      //   temp[selectedIndexForProject].selectedConsultant = selectedConsultant;
      //   temp[selectedIndexForProject].projectDescription = projectDescription;
      //   temp[
      //     selectedIndexForProject
      //   ].selectedProjectValue = selectedProjectValue;

      //   temp[
      //     selectedIndexForProject
      //   ].selectedProjectDuration = selectedProjectDuration;
      //   temp[
      //     selectedIndexForProject
      //   ].selectedDateForFromDate = selectedDateForFromDate;
      //   temp[
      //     selectedIndexForProject
      //   ].selectedDateForToDate = selectedDateForToDate;

      //   this.setState({ projectDetails: temp });
      if (selectedProjectId) {
        let updateProjects = {
          vendorid: this.state.id,
          project_name: projectName,
          client: client,
          consultant: selectedConsultant && selectedConsultant.value.toString(),
          projectdetail: projectDescription,
          // "phone": "text",
          project_value:
            selectedProjectValue && selectedProjectValue.value.toString(),
          projectduration:
            selectedProjectDuration && selectedProjectDuration.value.toString(),
          from_date: selectedDateForFromDate,
          to_date: selectedDateForToDate
        };
        API.fetchData(API.Query.UPDATE_VENDOR_PROJECTS, {
          id: selectedProjectId,
          data: updateProjects
        }).then((response) => {
          console.log("update res for projects", JSON.stringify(response));
          if (response.data) {
            this.setState({ selectedProjectId: "" });
            this.fetchVendorProjects();
          }
        });
      } else {
        // let tempObj = {
        //   projectName: projectName,
        //   client: client,
        //   selectedConsultant: selectedConsultant,
        //   projectDescription: projectDescription,
        //   selectedProjectValue: selectedProjectValue,
        //   selectedProjectDuration: selectedProjectDuration,
        //   selectedDateForFromDate: selectedDateForFromDate,
        //   selectedDateForToDate: selectedDateForToDate
        // };
        // temp.push(tempObj);

        // if (this.state.projectDetails && this.state.projectDetails.length > 0) {
        //   this.setState({
        //     projectDetails: [...this.state.projectDetails, tempObj],
        //     projectName: "",
        //     client: "",
        //     selectedConsultant: "",
        //     projectDescription: "",
        //     selectedProjectValue: "",
        //     selectedProjectDuration: "",
        //     selectedDateForFromDate: "",
        //     selectedDateForToDate: ""
        //   });
        // } else {
        let projects = [];
        let projectObj = {
          vendorid: this.state.id,
          project_name: projectName,
          client: client,
          consultant: values.consultant.toString(),
          projectdetail: projectDescription,
          // "phone": "text",
          project_value: values.projectValue.toString(),
          projectduration: values.projectDuration.toString(),
          from_date: selectedDateForFromDate,
          to_date: selectedDateForToDate
        };
        projects.push(projectObj);
        API.fetchData(API.Query.CREATE_VENDOR_PROJECTS, {
          data: projects
        }).then((response) => {
          console.log("response for projects", JSON.stringify(response));
          if (response.data) {
            this.fetchVendorProjects();
          }
        });

        this.setState({
          //projectDetails: projects,
          projectName: "",
          client: "",
          selectedConsultant: "",
          projectDescription: "",
          selectedProjectValue: "",
          selectedProjectDuration: "",
          selectedDateForFromDate: "",
          selectedDateForToDate: ""
        });
        // }
      }
    }
  };

  // API to GET projects
  fetchVendorProjects = () => {
    API.fetchData(API.Query.GET_VENDOR_PROJECTS, {
      vendorid: this.state.id
    }).then((response) => {
      console.log("response for get projects", JSON.stringify(response));
      if (response.vendor_projects) {
        // console.log("inside..if")
        this.setState({
          projectDetails: response.vendor_projects,
          isOpenProjectModal: false
        });
      } else {
        // console.log("inside else")
        this.setState({ isOpenProjectModal: false });
      }
    });
  };

  onEditIconClickForProject = (data, index, id) => {
    this.setState({ isOpenProjectModal: true });
    let temp = [...this.state.projectDetails];
    this.setState({
      //projectDetails: temp,
      selectedProjectId: id,
      selectedIndexForProject: index,
      projectName: data.project_name,
      client: data.client,
      selectedConsultant: data.consultant && {
        label: data.consultant,
        value: data.consultant
      },
      projectDescription: data.projectdetail,
      selectedProjectValue: data.project_value && {
        label: data.project_value,
        value: data.project_value
      },
      selectedProjectDuration: data.projectduration && {
        label: data.projectduration,
        value: data.projectduration
      },
      selectedDateForFromDate: data.from_date && new Date(data.from_date),
      selectedDateForToDate: data.to_date && new Date(data.to_date)
    });
  };
  hideModalForProjectProfile = () => {
    this.setState({ isProjectAddToProfileOpen: false });
  };

  onDetailsClickInProjectAddToProfile = (index) => {
    this.setState({
      selectedModalDataIndexForProject: index,
      isOpenDetailsForProject: !this.state.isOpenDetailsForProject
    });
  };

  onDeleteIconClickForProject = (index) => {
    let temp = [...this.state.projectDetails];
    temp.splice(index, 1);
    this.setState({
      projectDetails: temp,
      projectName: "",
      client: "",
      selectedConsultant: "",
      projectDescription: "",
      selectedProjectValue: "",
      selectedProjectDuration: "",
      selectedDateForFromDate: "",
      selectedDateForToDate: ""
    });
  };
  onProjectAddToProfileButtonClick = () => {
    this.setState({
      isOpenProjectModal: false,
      isProjectAddToProfileOpen: false,
      projectData: this.state.projectDetails
    });
  };
  hideProjectDetails = () => {
    this.setState({
      isVisibleProject: false
    });
  };

  handleProjectVisibleChange = (visible) => {
    this.setState({ isVisibleProject: visible });
  };
  onProjectDetailsClick = (index) => {
    this.setState({ selectedProjectDataIndex: index });
  };
  onDeleteIconClickForProjectData = (index) => {
    let temp = [...this.state.projectData];
    temp.splice(index, 1);
    let projectDetails = [...this.state.projectDetails];
    projectDetails.splice(index, 1);

    this.setState({
      projectData: temp,
      projectDetails: projectDetails,
      projectName: "",
      client: "",
      selectedConsultant: "",
      projectDescription: "",
      selectedProjectValue: "",
      selectedProjectDuration: "",
      selectedDateForFromDate: "",
      selectedDateForToDate: ""
    });
  };

  onNextButtonClick = (values, errors) => {
    // e.preventDefault();
    console.log("errors.." + JSON.stringify(errors));
    console.log("values..." + JSON.stringify(values));
    console.log("this.state.dropdownVal.." + this.state.dropdownVal);
    // if (this.state.dropdownVal == "") {
    //   this.setState({ errorMessageForCountryReg: "Country name is required" });
    // }
    // if (this.state.dropdownValForCompanyType == "") {
    //   this.setState({ errorMessageForCompanyType: "Company type is required" });
    // }
    let { activeTab } = this.state;
    // if(activeTab) {
    //this.setState({ activeTab: this.state.activeTab + 1 });
    // }
    // this.form1.submit()
    if (Object.keys(errors).length) {
      return;
    } else if (values && values.companyName) {
      if (values.companyName) {
        let {
          companyName,
          registrationCountry,
          companyTypeValue,
          businessType,
          companyDescription,
          companyProfile,
          companyLogo,
          companyWebsite,
          parentCompanyName,
          fullName,
          jobTitle,
          email,
          phoneNumber,
          fax
        } = values;
        //console.log("registrationCountry... " + JSON.stringify(registrationCountry))
        console.log("registrationCountry..." + typeof registrationCountry);
        console.log("what_does_your_company_do..." + typeof companyDescription);
        //console.log("company_profile..." + typeof (companyDescription.toString()))
        console.log("companyProfile..." + typeof companyProfile);
        console.log("companyWebsite..." + typeof companyWebsite);
        console.log("parentCompanyName..." + typeof parentCompanyName);
        console.log("jobTitle..." + typeof jobTitle);
        console.log("email..." + typeof email);
        console.log("faxno..." + typeof fax);
        console.log("phoneNumber..." + typeof phoneNumber);
        console.log("companyLogo..." + typeof companyLogo);
        console.log("businessType..." + typeof JSON.stringify(businessType));
        var obj = { ...this.state.selectedProductItems };
        // obj=JSON.stringify(obj)
        var obj1 = Object.values(obj);
        console.log("obj..." + JSON.stringify({ ...obj1 }));

        let products = "";
        for (var key in obj) {
          products += obj[key] + ",";
          console.log("value..." + products);
          console.log("products..." + typeof products);
        }

        if (
          this.state.selectedProductItems &&
          this.state.selectedProductItems.length > 0
        ) {
          this.state.selectedProductItems;
        }
        let params = {
          company_name: companyName,
          country_Registration: registrationCountry,
          year_established: year.toString(),
          company_type: companyTypeValue,
          //businesstype: businessType.join(),
          //businesstype: "",

          what_does_your_company_do: companyDescription,
          company_profile: companyProfile,
          company_logo: companyLogo,
          company_website: companyWebsite,
          is_parent_company: this.state.checked == "yes" ? true : false,
          //is_parent_company: this.state.checked,
          parent_company_name: parentCompanyName,
          fullname: fullName,
          job_title: jobTitle,
          email: email,
          phone: Number(phoneNumber),
          faxno: fax.toString()
          //product_and_service_categories: products ? products : '',
          //ship_or_servicelocation: this.state.selectedLocationItems,
          //ship_or_servicelocation: "",
          //industries_you_serve_to: "",

          //industries_you_serve_to: this.state.selectedIndustryItems
        };

        // const pretty = stringifyObject(params, {
        //   indent: '  ',
        //   singleQuotes: false
        // });

        //console.log("pretty .." +pretty);
        console.log("API.Query.CREATE_VENDOR,..." + API.Query.CREATE_VENDOR);
        // console.log("params.data..." + JSON.stringify(params.data.company_name));
        // console.log("params.... " + toString(params));

        API.fetchData(API.Query.CREATE_VENDOR, { data: params }).then(
          (response) => {
            console.log(
              "create vendor company details.",
              JSON.stringify(response)
            );
            console.log("response.data.id .." + response.data.id);
            if (response.data) {
              this.setState({ activeTab: activeTab + 1, id: response.data.id });
            }
          }
        );
      }
    }
  };

  //  toString(o) {
  //   Object.keys(o).forEach(k => {
  //     if (typeof o[k] === 'object') {
  //       return toString(o[k]);
  //     }

  //     o[k] = '' + o[k];
  //   });

  //   return o;
  //}
  //}

  onNextBusinessButtonClick = (values, errors) => {
    //e.preventDefault();
    console.log("values. business.." + JSON.stringify(values));
    console.log("errors..." + JSON.stringify(errors));
    if (Object.keys(errors).length) {
      return;
    } else if (values && values.expiryDateForTradeLicense) {
      let {
        tradeOrCommercialLicense,
        expiryDateForTradeLicense,
        trn,
        taxIdForGCC,
        expiryDateForTax,
        vatExemption,
        companySize,
        turnOverData
      } = values;
      let params = {
        trade_license: tradeOrCommercialLicense,
        trade_license_expirydate: expiryDateForTradeLicense,
        trn: trn,
        taxid_for_gcc: taxIdForGCC,
        taxid_for_gcc_expirydate: expiryDateForTax,
        vat_excemption_letter: vatExemption,
        annual_trun_over: turnOverData,
        no_of_employeed: companySize
      };

      API.fetchData(API.Query.UPDATE_VENDOR, {
        vendorid: this.state.id,
        data: params
      }).then((response) => {
        console.log(
          "create vendor business details.",
          JSON.stringify(response)
        );
        if (response.data) {
          this.setState({
            activeTab: this.state.activeTab + 1
          });
        }
      });
    }
    //this.form2.submit()
  };
  onSaveButtonClickForContact = (e, values) => {
    e.preventDefault();
    // console.log("values. business.." + JSON.stringify(values));
  };
  onPreviousButtonClick = (e) => {
    e.preventDefault();
    this.setState({ activeTab: this.state.activeTab - 1 });
  };

  // products and service
  onProductAndServiceClick = () => {
    // this.setState({isProductsOpen: true });
    let params = {
      category: "%%"
    };

    API.fetchData(API.Query.GET_PRODUCT_AND_SERVICE, params).then(
      (response) => {
        console.log(
          "product and service response..",
          JSON.stringify(response.data)
        );

        this.setState({
          categoryList: response.data,
          isProductsOpen: true
        });
      }
    );
    // this.setState({isProductsOpen: true ,  })
  };
  hideProductsModal = () => {
    this.setState({ isProductsOpen: false });
  };
  addSelectionToProfileClick = (selected) => {
    this.setState({ selectedProductItems: selected });
  };
  onProductCloseIconClick = (index) => {
    let temp = [...this.state.selectedProductItems];
    temp.splice(index, 1);
    this.setState({ selectedProductItems: temp });
  };

  // locations

  onLocationClick = () => {
    // this.setState({isProductsOpen: true });
    let params = {
      category: "%%"
    };

    API.fetchData(API.Query.GET_SERVICE_LOCATION, params).then((response) => {
      console.log("location response..", JSON.stringify(response.data));

      this.setState({
        locationList: response.data,
        isLocationOpen: true
      });
    });
    // this.setState({isProductsOpen: true ,  })
  };
  hideLocationModal = () => {
    this.setState({ isLocationOpen: false });
  };
  addLocationSelectionToProfileClick = (selected) => {
    this.setState({ selectedLocationItems: selected });
  };
  onLocationCloseIconClick = (index) => {
    let temp = [...this.state.selectedLocationItems];
    temp.splice(index, 1);
    this.setState({ selectedLocationItems: temp });
  };
  // Industries

  onIndustriesClick = () => {
    this.setState({
      isIndustriesOpen: true
    });
  };
  hideIndustriesModal = () => {
    this.setState({ isIndustriesOpen: false });
  };
  addIndustriesSelectionToProfileClick = (selected, selectedIndustryIdList) => {
    this.setState({
      selectedIndustryItems: selected,
      selectedIndustryIdList: selectedIndustryIdList
    });
  };
  onIndustryCloseIconClick = (index) => {
    let temp = [...this.state.selectedIndustryItems];
    temp.splice(index, 1);
    this.setState({ selectedIndustryItems: temp });
  };

  onCompanySizeChange = (selected) => {
    this.setState({ selectedCompanySize: selected });
  };
  onTurnOverChange = (selected) => {
    this.setState({ selectedTurnOverData: selected });
  };
  onNextAddressButtonClick = () => {
    this.setState({ activeTab: this.state.activeTab + 1 });
  };

  render() {
    console.log("id in state.." + this.state.id);
    // console.log("state companyName.." + this.state.companyName);
    // console.log("state year.." + this.state.year);
    // console.log("documentTitle..." + this.state.documentTitle);
    // console.log(
    //   "this.state.modalDetails. render." +
    //     JSON.stringify(this.state.modalDetails)
    // );
    //console.log("contactDetails." + this.state.contactDetails);
    //console.log("selectedModalDataIndex.." + this.state.selectedModalDataIndex);

    // const handleSubmit = (event) => {
    //   event.preventDefault();
    //   event.stopPropagation();
    //   const form = event.currentTarget;
    //   // console.log("event..." + event.currentTarget.elements.companyName.value);
    //   //console.log(
    //   //   "event..." + event.currentTarget.elements.formBasicCheckbox.checked
    //   // );
    //   // console.log(
    //   //   "event..." + event.currentTarget.elements.formCheckbox.checked
    //   // );
    //   // console.log(
    //   //   "countryRegistration.." +
    //   //     event.currentTarget.elements.countryRegistration.value
    //   // );
    //   // console.log("year...." + event.currentTarget.elements.year.value);
    //   // console.log(
    //   //   "description..." + event.currentTarget.elements.companyDescription.value
    //   // );

    //   console.log("form data", form); // form data should print here to send to api
    //   if (form.checkValidity() === false) {
    //   }
    //   this.setState({
    //     validated: true
    //   });
    // };

    var {
      validated,
      isOpen,
      options,
      activeTab,
      companyProfile,
      companyLogo,
      taxIdForGCC,
      vatExemption,
      tradeLicense,
      documentType,
      documentTitle,
      certificateIssuedAt,
      certificateIssuedBy,
      certificationNumber,
      selectedEffectiveDate,
      selectedExpiryDate,
      yearOfPublication,
      selectedDocumentType,
      documenFile,
      isOpenDetails,
      isOpenDetailsForAddToProfile,
      selectedModalDataIndexForAddToProfile,

      selectedCountry,
      addressDetails,
      addressName,
      selectedCountry,
      streetLine1,
      streetLine2,
      stateOrProvince,
      city,
      postBoxNumber,
      phoneNumberForContact,
      emailForContact,
      jobTitleForContact,
      personName,
      isProjectAddToProfileOpen,
      projectDetails,
      client,
      projectName,
      projectDescription,
      isOpenDetailsForProject,
      selectedModalDataIndexForProject,
      selectedProductItems,
      selectedLocationItems,
      selectedIndustryItems,
      selectedIndustryIdList,
      selectedCompanySize,
      selectedTurnOverData
    } = this.state;
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    const companyNameRegExp = /^[a-zA-Z ]*$/;

    return (
      <>
        {/* <div> */}
        {/* <Form
          ref={this.form}
          noValidate
          validated={validated}
          onSubmit={handleSubmit}
        > */}
        {activeTab == 1 ? (
          <Formik
            initialValues={{
              companyName: "",
              countryRegistration: "",
              year: "",
              parentCompanyName: "",
              fullName: "",
              jobTitle: "",
              email: "",
              phoneNumber: "",
              companyDescription: "",
              companyType: "",
              businessType: [],
              companyDescription: "",
              companyProfile: "",
              companyLogo: "",
              companyWebsite: "",
              fax: "",
              registrationCountry: "",
              companyTypeValue: ""
              // manufacturing: "",
            }}
            // onSubmit={async (values) => {
            //   //await new Promise((resolve) => setTimeout(resolve, 500));
            //   console.log("values........." + JSON.stringify(values));
            //   //alert(JSON.stringify(values, null, 2));
            //   //handleSubmit;
            //   //this.onNextButtonClick(values);
            // }}
            // onSubmit={}

            validationSchema={Yup.object().shape({
              companyName: Yup.string()
                .required("Company name is required")
                .min(1, "Company name is required")
                .trim()
                .matches(companyNameRegExp, "Company name is not valid"),
              // countryRegistration: Yup.string().required(
              //   "Country of Registration is required"
              // ),
              year: Yup.string().required("Year is Required"),
              parentCompanyName: Yup.string()
                .required("Parent company is required")
                .matches(companyNameRegExp, "Parent company name is not valid")
                .trim(),
              fullName: Yup.string().required("Full name is required").trim(),
              jobTitle: Yup.string().required("Job Title is required").trim(),
              email: Yup.string().email().required("Email is required!").trim(),
              phoneNumber: Yup.string()
                .required("Phone number is required")
                .matches(/^\d{10}$/, "Phone number is not valid"),
              registrationCountry: Yup.string().required(
                "Country of registration is required"
              ),
              companyTypeValue: Yup.string().required(
                "Company type is required"
              )
              //.matches(phoneRegExp, "Phone number is not valid")

              // phoneNumber: Yup.string().required("phoneNumber is required"),
            })}
          >
            {(props) => {
              const {
                values,
                touched,
                errors,
                dirty,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
                handleReset,
                setFieldValue,
                validateOnChange
              } = props;
              return (
                <Form
                  // ref={this.form}
                  noValidate
                  //validated={validated}
                  onSubmit={handleSubmit}
                  ref={(i) => (this.form1 = i)}
                >
                  <div>
                    <Row className="vendorRegFormRow">
                      <Col md="6" lg="5" className="vendorRegFormText heading">
                        Vendor Registeration Form
                      </Col>
                      <Col
                        md="6"
                        lg="7"
                        className="vendorRegFormRowSecCol d-flex"
                      >
                        <div className="mr-2">
                          <Button label="Prev" isDisabled={true}></Button>
                        </div>
                        <div className="mr-2">
                          <Button
                            label="Next"
                            className=""
                            variant="primary"
                            //isDisabled={isSubmitting}
                            type="submit"
                            onClick={() => {
                              // handleSubmit(e);
                              this.onNextButtonClick(props.values, errors);
                            }}
                            type="submit"
                          ></Button>
                        </div>
                        <div className="mr-2">
                          <Button
                            label="Save For Later"
                            classname=""
                            variant="primary"
                            onClick={this.onSaveButtonClick}
                          ></Button>
                        </div>
                        <div className="mr-2">
                          <Button
                            label="Publish"
                            classname=""
                            isDisabled={true}
                          ></Button>
                        </div>
                        <div className="mr-2">
                          <Button
                            label="Cancel"
                            classname=""
                            variant="link"
                            onClick={handleReset}
                          ></Button>
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <div>
                    <TabComponent
                      tabData={this.state.tabs}
                      // tabContent={<CompanyDetailsForm />}
                      // tabBusinessDetailsContent={<BusinessDetailsForm />}
                      // tabContentAddressDetails={<AddressAndContactsForm />}
                      onTabChange={this.onTabChange}
                      activeTab={activeTab}
                      tabContent={
                        // activeTab == 1 ? (

                        <div className="formDetailsMainDiv">
                          <Row className="rowStyle">
                            <Col md="6">
                              <div className="rowCol1TextDiv heading">
                                General Company Information
                              </div>
                              <div>
                                For good results with current and future buyers,
                                complete as much information as possible.
                              </div>
                            </Col>
                            <Col md="6">
                              <Form.Group controlId="companyName">
                                <Form.Label>
                                  * Company Name (as per trade license)
                                </Form.Label>
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="Enter Company name"
                                  value={values.companyName}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  name="companyName"
                                  // id="companyName"
                                  isInvalid={
                                    !!errors.companyName &&
                                    !!touched.companyName
                                  }
                                  // validateOnChange={false}
                                />

                                {errors.companyName && touched.companyName && (
                                  <Form.Control.Feedback type="invalid">
                                    {errors.companyName}
                                  </Form.Control.Feedback>
                                )}
                                {/* )} */}
                              </Form.Group>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6"></Col>
                            <Col md="6">
                              <Row>
                                <Col md="7">
                                  <div>
                                    <Form.Group
                                      // as={Col}
                                      controlId="countryRegistration"
                                    >
                                      <Form.Label>
                                        * Country of Registration
                                      </Form.Label>
                                      {/* <Form.Control
                                        as={() => ( */}
                                      <DropDown
                                        options={this.state.options}
                                        isOpen={this.state.dropdownOpen}
                                        toggleItem={(value, name) => {
                                          this.toggleItem(value, name);
                                          handleChange(value);
                                          setFieldValue(
                                            "registrationCountry",
                                            value,
                                            true
                                          );
                                        }}
                                        toggle={this.toggle}
                                        dropdownVal={
                                          this.state.dropdownVal
                                            ? this.state.dropdownVal
                                            : "Select"
                                        }
                                        direction="down"
                                        size="lg"
                                        name="countryRegistration"
                                        fromAddMore={false}
                                        icon={
                                          <IconComponent name="upanddownarrow" />
                                        }
                                        classNames={
                                          this.state.dropdownVal
                                            ? {
                                                dropdownStyle: "dropdownDiv",
                                                dropDownMenu: "dropDownMenu",
                                                dropDownItem: "dropDownItem"
                                              }
                                            : {
                                                dropdownStyle:
                                                  "defaultDropdownDiv",
                                                dropDownMenu: "dropDownMenu",
                                                dropDownItem: "dropDownItem"
                                              }
                                        }
                                        name="countryRegistration"
                                        type="text"
                                        // onChange={(event)=>{
                                        //   console.log(event);
                                        //   setFieldValue('countryRegistration', event, false);
                                        // }}                                            required
                                        // value={values.countryRegistration}
                                      />
                                      {/* )}
                                         // defaultValue="Select"
                                        // name="countryRegistration"
                                        // type="text"
                                        // onChange={props.handleChange}
                                        // required
                                      ></Form.Control> */}

                                      {touched.registrationCountry && (
                                        <div className="errorMessage">
                                          {errors.registrationCountry}
                                        </div>
                                      )}
                                      {console.log(
                                        "registrationCountry.." +
                                          touched.registrationCountry
                                      )}
                                      {/* {this.state.errorMessageForCountryReg && (
                                        <div className="errorMessage">
                                          {this.state.errorMessageForCountryReg}
                                        </div>
                                      )} */}
                                    </Form.Group>
                                  </div>
                                </Col>
                                <Col md="5">
                                  <Form.Group controlId="year">
                                    <Form.Label>* Year Established</Form.Label>
                                    <Form.Control
                                      required
                                      type="number"
                                      placeholder="yyyy"
                                      onChange={handleChange}
                                      name="year"
                                      isInvalid={
                                        !!errors.year && !!touched.year
                                      }

                                      //  value={values.year}
                                    />
                                    {errors.year && touched.year && (
                                      <Form.Control.Feedback type="invalid">
                                        {errors.year}
                                      </Form.Control.Feedback>
                                    )}
                                  </Form.Group>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6"></Col>

                            <Col md="6">
                              <div>
                                <Form.Group controlId="companyType">
                                  <Form.Label>* Company type</Form.Label>

                                  {/* <Form.Control
                                    as={() => ( */}
                                  <DropDown
                                    options={this.state.companyType}
                                    isOpen={
                                      this.state.dropdownOpenForCompanyType
                                    }
                                    // toggleItem={(value, name) => {this.toggleItem(value,name);
                                    //   setFieldValue(
                                    //     "companyTypeValue",
                                    //     value,
                                    //     true
                                    //   ); }}
                                    toggleItem={(value, name) => {
                                      this.toggleItem(value, name);
                                      handleChange(value);
                                      setFieldValue(
                                        "companyTypeValue",
                                        value,
                                        true
                                      );
                                    }}
                                    toggle={this.toggle}
                                    dropdownVal={
                                      this.state.dropdownValForCompanyType
                                        ? this.state.dropdownValForCompanyType
                                        : "Select"
                                    }
                                    direction="down"
                                    size="lg"
                                    name="companyTypeValue"
                                    fromAddMore={false}
                                    type="text"
                                    icon={
                                      <IconComponent name="upanddownarrow" />
                                    }
                                    classNames={
                                      this.state.dropdownValForCompanyType
                                        ? {
                                            dropdownStyle: "dropdownDiv",
                                            dropDownMenu: "dropDownMenu",
                                            dropDownItem: "dropDownItem"
                                          }
                                        : {
                                            dropdownStyle: "defaultDropdownDiv",
                                            dropDownMenu: "dropDownMenu",
                                            dropDownItem: "dropDownItem"
                                          }
                                    }
                                  />
                                  {/* )}
                                  /> */}
                                  {/* {this.state.errorMessageForCompanyType && (
                                    <div className="errorMessage">
                                      {this.state.errorMessageForCompanyType}
                                    </div>
                                  )} */}
                                  {touched.companyTypeValue && (
                                    <div className="errorMessage">
                                      {errors.companyTypeValue}
                                    </div>
                                  )}
                                </Form.Group>
                              </div>
                            </Col>
                          </Row>
                          <Row className="rowStyle-chk">
                            <Col md="6"></Col>
                            <Col md="6">
                              <Form.Group controlId="businessType">
                                {/* <Form.Label>* Business Type</Form.Label> */}
                                <Form.Label
                                  as="legend"
                                  column
                                  sm={12}
                                  className="mb-0 pl-0"
                                >
                                  Business Type
                                </Form.Label>

                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label="Manufacturing"
                                  name="businessType"
                                  value="Manufacturing"
                                  onChange={handleChange}
                                />
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label="Trading"
                                  name="businessType"
                                  value="Trading"
                                  onChange={handleChange}
                                />
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label="Services"
                                  name="businessType"
                                  value="Services"
                                  onChange={handleChange}
                                />
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label="Others"
                                  name="businessType"
                                  value="Others"
                                  onChange={handleChange}
                                />
                                {/* <Form.Check type="checkbox" label="Manufacturing" /> */}
                              </Form.Group>
                              <Form.Group controlId="formCheckbox"></Form.Group>
                            </Col>
                          </Row>
                          <Row className="rowStyle mt-3">
                            <Col md="6"></Col>

                            <Col md="6">
                              <div className="companyDescriptionDiv">
                                <Form.Group controlId="companyDescription">
                                  <Form.Label>
                                    What does your company do?
                                  </Form.Label>
                                  <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Airmaster Technical Services is into... ."
                                    name="companyDescription"
                                    onChange={handleChange}
                                  />
                                </Form.Group>
                              </div>
                            </Col>
                          </Row>

                          <Row className="rowStyle">
                            <Col md="6"></Col>

                            <Col md="6">
                              <Form.Group controlId="companyProfile">
                                <Form.Label>Company profile</Form.Label>
                                <Form.File
                                  id="custom-file"
                                  label={companyProfile}
                                  custom
                                  //value={companyProfile}
                                  //onChange={this.onCompanyProfileChange}
                                  onChange={(e) => {
                                    this.onCompanyProfileChange(e);
                                    setFieldValue(
                                      "companyProfile",
                                      event.target.files[0].name,
                                      false
                                    );
                                  }}
                                  //ref="fileUpload"
                                  ref={(input) => (this.fileUpload = input)}
                                />
                              </Form.Group>
                              <div
                                onClick={this.onUploadIconClick}
                                className="fileUploadIcon"
                              >
                                <IconComponent name="file_upload" />
                              </div>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6"></Col>

                            <Col md="6">
                              <Form.Group controlId="companyLogo">
                                <Form.Label>Company Logo</Form.Label>
                                <Form.File
                                  id="custom-file_2"
                                  label={companyLogo}
                                  custom
                                  // value={companyLogo}
                                  onChange={(e) => {
                                    this.onCompanyLogoChange(e);
                                    setFieldValue(
                                      "companyLogo",
                                      event.target.files[0].name,
                                      false
                                    );
                                  }}
                                  ref={(input) =>
                                    (this.fileUploadCompanyLogo = input)
                                  }
                                />
                              </Form.Group>
                              <div
                                onClick={this.onUploadIconClickForCompanyLogo}
                                className="fileUploadIcon"
                              >
                                <IconComponent name="file_upload" />
                              </div>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6"></Col>

                            <Col md="6">
                              <Form.Group controlId="companyWebsite">
                                <Form.Label>Company Website</Form.Label>
                                <Form.Control
                                  type="text"
                                  placeholder="www.airmaster-uae.ae"
                                  name="companyWebsite"
                                  onChange={(e) =>
                                    setFieldValue(
                                      "companyWebsite",
                                      e.target.value,
                                      false
                                    )
                                  }
                                  // id="companyName"
                                />
                              </Form.Group>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6">
                              <div className="rowCol1TextDiv heading">
                                Parent Company
                              </div>
                            </Col>
                            <Col md="6">
                              <Form.Group controlId="parentCompany">
                                <Form.Label
                                  as="legend"
                                  column
                                  sm={12}
                                  className="mb-0 pl-0"
                                >
                                  Do you have a Parent Company?
                                </Form.Label>

                                <Form.Check
                                  inline
                                  type="radio"
                                  label="Yes"
                                  name="options"
                                  value="yes"
                                  defaultChecked
                                  checked={this.state.checked == "yes"}
                                  onChange={this.handleChange}
                                />
                                <Form.Check
                                  inline
                                  type="radio"
                                  label="No"
                                  name="options"
                                  value="no"
                                  checked={this.state.checked == "no"}
                                  onChange={this.handleChange}
                                />
                              </Form.Group>
                            </Col>
                          </Row>
                          {this.state.checked == "yes" ? (
                            <Row className="rowStyle">
                              <Col md="6"></Col>

                              <Col md="6">
                                <Form.Group controlId="parentCompanyName">
                                  <Form.Label>
                                    * Name of the Parent company
                                  </Form.Label>
                                  <Form.Control
                                    required
                                    type="text"
                                    placeholder="Value"
                                    onChange={handleChange}
                                    name="parentCompanyName"
                                    isInvalid={
                                      !!errors.parentCompanyName &&
                                      !!touched.parentCompanyName
                                    }
                                  />
                                  {errors.parentCompanyName &&
                                    touched.parentCompanyName && (
                                      <Form.Control.Feedback type="invalid">
                                        {errors.parentCompanyName}
                                      </Form.Control.Feedback>
                                    )}
                                </Form.Group>
                              </Col>
                            </Row>
                          ) : (
                            ""
                          )}
                          <Row className="rowStyle">
                            <Col md="6">
                              <div className="rowCol1TextDiv heading">
                                Primary contact person
                              </div>
                              <div>
                                Please be sure to give accurate contact details
                                of the primary contact responsible for the
                                registration.
                              </div>
                            </Col>

                            <Col md="6">
                              <Form.Group controlId="fullName">
                                <Form.Label>* Full Name</Form.Label>
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="Value"
                                  onChange={handleChange}
                                  isInvalid={
                                    !!errors.fullName && !!touched.fullName
                                  }
                                />
                                {errors.fullName && touched.fullName && (
                                  <Form.Control.Feedback type="invalid">
                                    {errors.fullName}
                                  </Form.Control.Feedback>
                                )}
                              </Form.Group>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6">
                              <div>
                                All notifications of this registration will be
                                sent to the email id given here.
                              </div>
                            </Col>

                            <Col md="6">
                              <Form.Group controlId="jobTitle">
                                <Form.Label>* Job Title</Form.Label>
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="Value"
                                  name="jobTitle"
                                  onChange={handleChange}
                                  isInvalid={
                                    !!errors.jobTitle && !!touched.jobTitle
                                  }
                                />
                                {errors.jobTitle && touched.jobTitle && (
                                  <Form.Control.Feedback type="invalid">
                                    {errors.jobTitle}
                                  </Form.Control.Feedback>
                                )}
                              </Form.Group>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6"></Col>
                            <Col md="6">
                              <Form.Group controlId="email">
                                <Form.Label>* Email ID</Form.Label>
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="Value"
                                  name="email"
                                  onChange={handleChange}
                                  isInvalid={!!errors.email && !!touched.email}
                                />
                                {errors.email && touched.email && (
                                  <Form.Control.Feedback type="invalid">
                                    {errors.email}
                                  </Form.Control.Feedback>
                                )}
                              </Form.Group>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6"></Col>
                            <Col md="6">
                              <Form.Group controlId="phoneNumber">
                                <Form.Label>* Phone Number</Form.Label>
                                <Form.Control
                                  required
                                  type="number"
                                  placeholder="Value"
                                  name="phoneNumber"
                                  onChange={handleChange}
                                  isInvalid={
                                    !!errors.phoneNumber &&
                                    !!touched.phoneNumber
                                  }
                                />
                                {errors.phoneNumber && touched.phoneNumber && (
                                  <Form.Control.Feedback>
                                    {errors.phoneNumber}
                                  </Form.Control.Feedback>
                                )}
                              </Form.Group>
                            </Col>
                          </Row>
                          <Row className="rowStyle">
                            <Col md="6"></Col>
                            <Col md="6">
                              <Form.Group controlId="fax">
                                <Form.Label>Fax Number</Form.Label>
                                <Form.Control
                                  type="number"
                                  placeholder="Value"
                                  name="fax"
                                  onChange={handleChange}
                                />
                              </Form.Group>
                            </Col>
                          </Row>
                          <Row className="buttonRowStyle">
                            <Col md="6">
                              <div className="rowCol1TextDiv heading">
                                Product and Service Categories
                              </div>
                              <div>Add products and Services you offer</div>
                            </Col>
                            <Col md="6">
                              <div className="col-4 pl-0 addProductButton">
                                <div className="addProductButtonSubdiv">
                                  <Button
                                    label="Add"
                                    variant="add"
                                    onClick={this.onProductAndServiceClick}
                                  ></Button>
                                </div>
                              </div>
                            </Col>
                          </Row>
                          <Row>
                            <Col md="6"></Col>
                            <Col md="6">
                              <div className="d-flex">
                                {selectedProductItems &&
                                  selectedProductItems.length > 0 &&
                                  selectedProductItems.map((data, index) => {
                                    return (
                                      <div className="tagDiv d-inline-block mr-2">
                                        <span>{data}</span>
                                        <span
                                          className="pl-1"
                                          onClick={() =>
                                            this.onProductCloseIconClick(index)
                                          }
                                        >
                                          <IconComponent name="close" />
                                        </span>
                                      </div>
                                    );
                                  })}
                              </div>
                            </Col>
                          </Row>
                          {this.state.isProductsOpen == true && (
                            <ProductAndServiceModal
                              isProductsOpen={true}
                              hide={this.hideProductsModal}
                              categoryList={this.state.categoryList}
                              addSelectionToProfileClick={
                                this.addSelectionToProfileClick
                              }
                            />
                          )}
                          <Row className="buttonRowStyle">
                            <Col md="6">
                              <div className="rowCol1TextDiv heading">
                                Ship-to or Service Location
                              </div>
                              <div>Add locations that you serve to</div>
                            </Col>
                            <Col md="6">
                              <div className="col-4 pl-0">
                                <Button
                                  label="Add"
                                  variant="add"
                                  onClick={this.onLocationClick}
                                ></Button>
                              </div>
                            </Col>
                          </Row>
                          <Row>
                            <Col md="6"></Col>
                            <Col md="6">
                              <div className="d-flex">
                                {selectedLocationItems &&
                                  selectedLocationItems.length > 0 &&
                                  selectedLocationItems.map((data, index) => {
                                    return (
                                      <div className="tagDiv d-inline-block mr-2">
                                        <span>{data}</span>
                                        <span
                                          className="pl-1"
                                          onClick={() =>
                                            this.onLocationCloseIconClick(index)
                                          }
                                        >
                                          <IconComponent name="close" />
                                        </span>
                                      </div>
                                    );
                                  })}
                              </div>
                            </Col>
                          </Row>
                          {this.state.isLocationOpen == true && (
                            <ServiceLocationsModal
                              isLocationOpen={true}
                              hide={this.hideLocationModal}
                              locationList={this.state.locationList}
                              addLocationSelectionToProfileClick={
                                this.addLocationSelectionToProfileClick
                              }
                            />
                          )}
                          <Row className="buttonRowStyle industriesDiv">
                            <Col md="6">
                              <div className="rowCol1TextDiv heading">
                                Industries you serve to
                              </div>
                              <div>
                                Add industries that you serve to. This will help
                                buyers to quickly decide.
                              </div>
                            </Col>
                            <Col md="6">
                              <div className="col-4 pl-0">
                                <Button
                                  label="Add"
                                  variant="add"
                                  onClick={this.onIndustriesClick}
                                ></Button>
                              </div>
                            </Col>
                          </Row>
                          {this.state.isIndustriesOpen == true && (
                            <IndustriesModal
                              isIndustriesOpen={true}
                              hide={this.hideIndustriesModal}
                              addIndustriesSelectionToProfileClick={
                                this.addIndustriesSelectionToProfileClick
                              }
                              selectedIndustryItems={selectedIndustryItems}
                              selectedIndustryIdList={selectedIndustryIdList}
                            />
                          )}
                          <Row className="pb-4">
                            <Col md="6"></Col>
                            <Col md="6">
                              <div className="d-flex">
                                {selectedIndustryItems &&
                                  selectedIndustryItems.length > 0 &&
                                  selectedIndustryItems.map((data, index) => {
                                    return (
                                      <div className="tagDiv d-inline-block mr-2">
                                        <span>{data}</span>
                                        <span
                                          className="pl-1"
                                          onClick={() =>
                                            this.onIndustryCloseIconClick(index)
                                          }
                                        >
                                          <IconComponent name="close" />
                                        </span>
                                      </div>
                                    );
                                  })}
                              </div>
                            </Col>
                          </Row>
                        </div>
                      }
                    />
                  </div>
                  <div className="mb-3">
                    <Row className="vendorRegFormRow">
                      <Col
                        md="12"
                        lg="12"
                        className="vendorRegFormRowSecCol d-flex"
                      >
                        <div className="mr-2">
                          <Button label="Prev" isDisabled={true}></Button>
                        </div>
                        <div className="mr-2">
                          <Button
                            label="Next"
                            className=""
                            variant="primary"
                            type="submit"
                            onClick={() => {
                              //handleSubmit(e);
                              this.onNextButtonClick(props.values, errors);
                            }}
                          ></Button>
                        </div>
                        <div className="mr-2">
                          <Button
                            label="Save For Later"
                            classname=""
                            variant="primary"
                            onClick={this.onSaveButtonClick}
                          ></Button>
                        </div>
                        <div className="mr-2">
                          <Button
                            label="Publish"
                            classname=""
                            isDisabled={true}
                          ></Button>
                        </div>
                        <div className="mr-2">
                          <Button
                            label="Cancel"
                            classname=""
                            variant="link"
                          ></Button>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Form>
              );
            }}
          </Formik>
        ) : activeTab == 2 ? (
          <div>
            <Formik
              initialValues={{
                // tradeLicense: "",
                trn: "",
                expiryDateForTradeLicense: "",
                expiryDateForTax: "",
                turnOverData: "",
                companySize: "",
                tradeOrCommercialLicense: "",
                vatExemption: "",
                taxIdForGCC: ""
              }}
              // onSubmit={async (values) => {
              //   //await new Promise((resolve) => setTimeout(resolve, 500));
              //  alert("values........." + JSON.stringify(values));
              //   //alert(JSON.stringify(values, null, 2));
              // //this.onNextBusinessButtonClick(values, errors);
              // }}

              validationSchema={Yup.object().shape({
                // tradeLicense: Yup.string().required(
                //   "Trade License is required"
                // ),
                trn: Yup.string()
                  .required("TRN is required")
                  .matches(/^[a-zA-Z0-9]*$/, "TRN is not valid")
                  .min(15, "TRN is not valid")
                  .max(15, "TRN is not valid"),
                expiryDateForTradeLicense: Yup.date().required(
                  "Expiry date is required"
                ),
                expiryDateForTax: Yup.date().required(
                  "Expiry date is required"
                ),
                // turnOverData: Yup.object().shape({
                //   label: Yup.string().required(),
                //   value: Yup.string().required(),
                // })
                // turnOverData:  Yup.object().shape({
                //   label: Yup.string().required(),Yup.object("Turn data is required"),
                //companySize: Yup.object("Company size is rquired"),
                // turnOverData: Yup.object().shape({
                //   label: Yup.string().required(),
                //   value: Yup.string().required("Annual turnover is required")
                // }),
                turnOverData: Yup.string().required(
                  "Annual turnover is required"
                ),
                companySize: Yup.string().required("Size is required")
                // companySize: Yup.object().shape({
                //   label: Yup.string().required(),
                //   value: Yup.string().required("Size is required")
                // })
                //turnOverData: Yup.string().required("Annual turnover is required ")
              })}
              // mapPropsToValues= ({ initialValues }) => ({
              //   ...initialValues
              // })
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  dirty,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  handleReset,
                  setFieldValue,
                  isValid,
                  setFieldTouched
                } = props;
                return (
                  <Form
                    //ref={this.form}
                    noValidate
                    // validated={validated}
                    onSubmit={handleSubmit}
                    ref={(i) => (this.form2 = i)}
                  >
                    <div>
                      <Row className="vendorRegFormRow">
                        <Col md="6" lg="5" className="vendorRegFormText">
                          Vendor Registeration Form
                        </Col>
                        <Col
                          md="6"
                          lg="7"
                          className="vendorRegFormRowSecCol d-flex"
                        >
                          <div className="mr-2">
                            <Button
                              label="Prev"
                              variant="primary"
                              onClick={this.onPreviousButtonClick}
                            ></Button>
                          </div>
                          <div className="mr-2">
                            <Button
                              label="Next"
                              className=""
                              variant="primary"
                              // isDisabled={isSubmitting}
                              type="submit"
                              onClick={(e) => {
                                handleSubmit(e);
                                this.onNextBusinessButtonClick(values, errors);
                              }}
                            ></Button>
                          </div>
                          <div className="mr-2">
                            <Button
                              label="Save For Later"
                              classname=""
                              variant="primary"
                              onClick={this.onSaveButtonClick}
                            ></Button>
                          </div>
                          <div className="mr-2">
                            <Button
                              label="Publish"
                              classname=""
                              isDisabled={true}
                            ></Button>
                          </div>
                          <div className="mr-2">
                            <Button
                              label="Cancel"
                              classname=""
                              variant="link"
                            ></Button>
                          </div>
                        </Col>
                      </Row>
                    </div>
                    <div>
                      <TabComponent
                        tabData={this.state.tabs}
                        // tabContent={<CompanyDetailsForm />}
                        // tabBusinessDetailsContent={<BusinessDetailsForm />}
                        // tabContentAddressDetails={<AddressAndContactsForm />}
                        onTabChange={this.onTabChange}
                        activeTab={activeTab}
                        tabContent={
                          <div className="formDetailsMainDiv businessDocumentsStyle">
                            <Row className="rowStyle">
                              <Col md="6">
                                <div className="rowCol1TextDiv">
                                  Business Documents{" "}
                                </div>
                                <div>
                                  Buying organizations in UAE are keen on
                                  employing certified vendors, so we recommend
                                  you to upload as many as you have and increase
                                  your visibility.
                                </div>
                              </Col>

                              <Col md="6">
                                <Row>
                                  <Col md="7">
                                    <Form.Group
                                      controlId="tradeLicense"
                                      className="mr-2"
                                    >
                                      <Form.Label>
                                        * Trade / Commercial License
                                      </Form.Label>
                                      <Form.File
                                        id="custom-file-3"
                                        label={tradeLicense}
                                        name="tradeLicense"
                                        custom
                                        //  value={tradeLicense}
                                        onChange={(e) => {
                                          this.onTradeLicenseChange(e);
                                          handleChange(e.target.files[0].name);
                                          props.setFieldValue(
                                            "tradeOrCommercialLicense",
                                            e.target.files[0].name
                                          );
                                        }}
                                        placeholder="tradelicense.pdf"
                                        //ref={this.fileUploadTradeLicense}
                                        ref={(input) =>
                                          (this.fileUploadTradeLicense = input)
                                        }
                                      />
                                      {console.log(
                                        "tradeLicense error.." +
                                          errors.tradeLicense
                                      )}
                                      {/* {(!values.tradeLicense && touched.tradeLicense) && (
                                      <Form.Control.Feedback>
                                        {errors.tradeLicense}
                                      </Form.Control.Feedback>
                                    )} */}
                                      {/* {values &&
                                    values.file &&
                                    !values.file.name ? (
                                      <Form.Control.Feedback>
                                        Trade License is required
                                      </Form.Control.Feedback>
                                    ) : (
                                      touched.tradeLicense && (
                                        <Form.Control.Feedback>
                                          Trade License is required
                                        </Form.Control.Feedback>
                                      )
                                    )} */}
                                    </Form.Group>
                                    <div
                                      onClick={
                                        this.onUploadIconClickTradeLicense
                                      }
                                      className="fileUploadIcon"
                                    >
                                      <IconComponent name="file_upload" />
                                    </div>
                                  </Col>
                                  <Col md="5">
                                    <Form.Group
                                      controlId="expiryDateForTradeLicense"
                                      className="ml-2"
                                    >
                                      {/*  <Form.Label>* Expiry date</Form.Label>
                                    <Form.Control
                                      // type="date"
                                      placeholder="Value"
                                      required
                                      name="expiryDateForTradeLicense"
                                      as={(props) => ( */}
                                      <Form.Label>* Expiry date</Form.Label>
                                      <DatepickerComponent
                                        selected={
                                          this.state.selectedDateForTradeLicense
                                        }
                                        // onBlur={() =>
                                        //   setFieldTouched(
                                        //     "expiryDateForTradeLicense",
                                        //     true
                                        //   )
                                        // }
                                        onChange={(date) => {
                                          this.onChangeTradeLicenseDate(date);
                                          //handleChange(date);
                                          setFieldValue(
                                            "expiryDateForTradeLicense",
                                            date,
                                            true
                                          );
                                        }}
                                        isFromVendors={true}
                                        //name="expiryDateForTradeLicense"
                                        // onBlur={handleBlur}
                                        required
                                        // isValid={ this.state
                                        //   .selectedDateForTradeLicense ? true : false}
                                        // error={errors.expiryDateForTradeLicense}
                                        // touched={touched.expiryDateForTradeLicense}
                                        // isInvalid={!!errors.expiryDateForTradeLicense && touched.expiryDateForTradeLicense}
                                      />

                                      {touched.expiryDateForTradeLicense && (
                                        <div className="errorMessage">
                                          {errors.expiryDateForTradeLicense}
                                        </div>
                                      )}

                                      {/* {values.expiryDateForTradeLicense ? (
                                      ""
                                    ) : (
                                      <Form.Control.Feedback>
                                        {errors.expiryDateForTradeLicense}
                                      </Form.Control.Feedback>
                                    )} */}
                                    </Form.Group>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                            <Row className="rowStyle">
                              <Col md="6" className="mt-2">
                                Having no certification imply that your company
                                does not have any certification at all.
                              </Col>
                              <Col md="6">
                                <Form.Group controlId="trn">
                                  <Form.Label>
                                    * TRN{" "}
                                    <span style={{ color: "#8BA4BA" }}>
                                      (Enter the 15 digit ID )
                                    </span>
                                  </Form.Label>
                                  <Form.Control
                                    required
                                    type="text"
                                    placeholder="100341059200003"
                                    name="trn"
                                    onChange={handleChange}
                                    //value={values.trn}
                                    // id="companyName"
                                    isInvalid={!!errors.trn && !!touched.trn}
                                  />
                                  {console.log("trn..." + errors.trn)}
                                  {errors.trn && touched.trn && (
                                    <Form.Control.Feedback type="invalid">
                                      {errors.trn}
                                    </Form.Control.Feedback>
                                  )}
                                </Form.Group>
                              </Col>
                            </Row>
                            <Row className="rowStyle">
                              <Col md="6"></Col>

                              <Col md="5">
                                <Form.Group controlId="vatExemption">
                                  <Form.Label>* VAT Certificate</Form.Label>
                                  <Form.File
                                    id="custom-file-4"
                                    label={vatExemption}
                                    custom
                                    //  value={taxIdForGCC}
                                    onChange={(event) => {
                                      this.onVatExemptionChange(event);
                                      handleChange(event.target.files[0].name);
                                      setFieldValue(
                                        "vatExemption",
                                        event.target.files[0].name,
                                        true
                                      );
                                    }}
                                    // ref={this.fileUploadVat}
                                    ref={(input) =>
                                      (this.fileUploadVat = input)
                                    }
                                  />
                                </Form.Group>
                                <div
                                  onClick={this.onUploadIconVatExemption}
                                  className="fileUploadIcon"
                                >
                                  <IconComponent name="file_upload" />
                                </div>
                              </Col>
                            </Row>
                            <Row className="rowStyle">
                              <Col md="6"></Col>

                              <Col md="6">
                                <Row>
                                  <Col md="7">
                                    <Form.Group
                                      controlId="taxId"
                                      className="mr-2"
                                    >
                                      <Form.Label>Tax ID for GCC</Form.Label>
                                      <Form.File
                                        id="custom-file-7"
                                        label={taxIdForGCC}
                                        custom
                                        //  value={taxIdForGCC}
                                        //onChange={this.onTaxIdChange}
                                        onChange={(event) => {
                                          this.onTaxIdChange(event);
                                          handleChange(
                                            event.target.files[0].name
                                          );
                                          setFieldValue(
                                            "taxIdForGCC",
                                            event.target.files[0].name,
                                            false
                                          );
                                        }}
                                        ref={(input) =>
                                          (this.fileUploadTaxGCC = input)
                                        }

                                        //ref={this.fileUploadTaxGCC}
                                      />
                                    </Form.Group>
                                    <div
                                      onClick={this.onUploadIconForTaxId}
                                      className="fileUploadIconTax"
                                    >
                                      <IconComponent name="file_upload" />
                                    </div>

                                    <div
                                      style={{ color: "#8BA4BA" }}
                                      className="mb-2"
                                    >
                                      {" "}
                                      (i.e. Gulf Cooperation Council) companies{" "}
                                    </div>
                                  </Col>
                                  <Col md="5">
                                    {/* <Form.Group
                                    controlId="expiryDateForTax"
                                    className="ml-2"
                                  > */}
                                    <Form.Label>* Expiry date</Form.Label>
                                    {/* <Form.Control
                                      required
                                      // type="date"
                                      placeholder="Value"
                                      as={() => ( */}
                                    <DatepickerComponent
                                      selected={this.state.selectedDate}
                                      // onChange={this.onChangeDate}
                                      onChange={(date, e) => {
                                        this.onChangeDate(date);
                                        //handleChange(date);
                                        setFieldValue(
                                          "expiryDateForTax",
                                          date,
                                          true
                                        );
                                      }}
                                      isFromVendors={true}
                                    />
                                    {/* )}
                                    /> */}
                                    {/* {errors.expiryDateForTax && errors.expiryDateForTax && (
                                      <div className="errorMessage">
                                        {errors.expiryDateForTax}
                                      </div>
                                    )} */}
                                    {/* </Form.Group> */}
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                            {this.state.documentLabel ? (
                              <Row className="rowStyle">
                                <Col md="6"></Col>
                                <Col md="5">
                                  <Form.Group controlId="fileUploadDocument">
                                    <Form.Label>
                                      {this.state.documentLabel}
                                    </Form.Label>
                                    <Form.File
                                      id="custom-file"
                                      label={this.state.documentFile}
                                      custom
                                      //  value={taxIdForGCC}
                                      onChange={this.onDocumentLableFileChange}
                                      ref={this.fileUploadDocumentFile}
                                    />
                                  </Form.Group>
                                  <div
                                    onClick={this.onUploadDocumentFile}
                                    className="fileUploadIcon"
                                  >
                                    <IconComponent name="file_upload" />
                                  </div>
                                </Col>
                              </Row>
                            ) : (
                              ""
                            )}
                            {/* <Row className="rowStyle mt-5">
                              <Col md="6"></Col>
                              <Col md="6" className="d-flex">
                                <div>
                                  <Button
                                    // outline="secondary"
                                    icon="icon"
                                    iconName={
                                      <IconComponent name="circleplusbackground" />
                                    }
                                    label="Add another document"
                                    variant="upload"
                                    onClick={this.onAddDocumentButtonClick}
                                  ></Button>
                                </div>
                              </Col>
                            </Row> */}
                            {/* <Modal
                              size="med"
                              isOpen={this.state.isOpenDocumentLabel}
                              hide={this.hideModalForDocumentLabel}
                            >
                              <Modal.Content>
                                <div className="modalMainDiv">
                                  <div className="modalHeader">
                                    <div className="d-flex">
                                      <div className="addDocument">
                                        Add Label
                                      </div>

                                      <div
                                        className="closeIcon"
                                        onClick={this.hideModalForDocumentLabel}
                                      >
                                        <IconComponent name="close" />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="mt-5 ml-3 mr-3">
                                    <Form.Group controlId="documentLabel">
                                      <Form.Label>Enter Label</Form.Label>
                                      <Form.Control
                                        required
                                        type="text"
                                        placeholder="label"
                                        name="documentLabel"
                                        onChange={this.onChangeModalFieldValue}
                                      />
                                    </Form.Group>
                                  </div>
                                  <div className="mt-1 d-flex saveButton">
                                    <div className="saveButtonSubDiv col-2">
                                      <div className="saveButtonDiv">
                                        {" "}
                                        <Button
                                          variant="primary"
                                          label="Save"
                                          onClick={this.onLabelSaveButtonClick}
                                        ></Button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </Modal.Content>
                            </Modal> */}
                            <Row className="rowStyle mt-5">
                              <Col md="6">
                                <div className="rowCol1TextDiv">
                                  Certification{" "}
                                </div>
                              </Col>
                            </Row>
                            <Row className="rowStyle">
                              <Col md="6">
                                Buying organizations in UAE are keen on
                                employing certified vendors, so we recommend you
                                to upload as many as you have and increase your
                                visibility.
                              </Col>
                              <Col md="6" className="">
                                {this.state.modalData &&
                                  this.state.modalData.length > 0 &&
                                  this.state.modalData.map((data, index) => {
                                    return (
                                      <div>
                                        <div>{data.title_of_document}</div>
                                        <div>
                                          <Popover
                                            content={
                                              <div className="pl-3 pr-3 pb-5">
                                                <div
                                                  className="closeIconInView"
                                                  onClick={this.hide}
                                                >
                                                  <IconComponent name="close" />
                                                </div>

                                                <div className="font-weight-bold documentTitleInView">
                                                  {data.title_of_document}
                                                </div>
                                                <Row className="rowStyle">
                                                  <Col md="6">
                                                    Certification Number
                                                  </Col>
                                                  <Col md="6">
                                                    Year of Publication
                                                  </Col>
                                                </Row>
                                                <Row className="rowStyle mb-3">
                                                  <Col
                                                    md="6"
                                                    className="font-weight-bold"
                                                  >
                                                    {data.certification_number
                                                      ? data.certification_number
                                                      : ""}
                                                  </Col>
                                                  <Col
                                                    md="6"
                                                    className="font-weight-bold"
                                                  >
                                                    {data.year_of_publication
                                                      ? data.year_of_publication
                                                      : ""}
                                                  </Col>
                                                </Row>
                                                <Row className="rowStyle ">
                                                  <Col md="6">Issued by</Col>
                                                  <Col md="6">Issued at</Col>
                                                </Row>
                                                <Row className="rowStyle mb-3">
                                                  <Col
                                                    md="6"
                                                    className="font-weight-bold"
                                                  >
                                                    {data.certificate_issued_at
                                                      ? data.certificate_issued_at
                                                      : ""}
                                                  </Col>
                                                  <Col
                                                    md="6"
                                                    className="font-weight-bold"
                                                  >
                                                    {data.certificate_issued_by
                                                      ? data.certificate_issued_by
                                                      : ""}
                                                  </Col>
                                                </Row>
                                                <Row className="rowStyle ">
                                                  <Col md="6">
                                                    Effective date
                                                  </Col>
                                                  <Col md="6">Expiry date</Col>
                                                </Row>
                                                <Row className="rowStyle mb-3">
                                                  <Col
                                                    md="6"
                                                    className="font-weight-bold"
                                                  >
                                                    {data.effective_date
                                                      ? moment(
                                                          data.effective_date
                                                        ).format("DD/MM/YYYY")
                                                      : ""}
                                                  </Col>
                                                  <Col
                                                    md="6"
                                                    className="font-weight-bold"
                                                  >
                                                    {data.expiry_date
                                                      ? moment(
                                                          data.expiry_date
                                                        ).format("DD/MM/YYYY")
                                                      : ""}
                                                  </Col>
                                                </Row>
                                              </div>
                                            }
                                            // title="Title"
                                            trigger="click"
                                            visible={
                                              index ==
                                              this.state.selectedDataIndex
                                                ? this.state.visible
                                                : ""
                                            }
                                            placement="bottomLeft"
                                            onVisibleChange={
                                              this.handleVisibleChange
                                            }
                                          >
                                            <div className="d-flex">
                                              <div
                                                className="detailsText"
                                                onClick={() =>
                                                  this.onDetailsClick(index)
                                                }
                                              >
                                                Details
                                              </div>{" "}
                                              <div
                                                className="ml-2"
                                                onClick={() =>
                                                  this.onDetailsClick(index)
                                                }
                                              >
                                                <IconComponent name="downarrow" />
                                              </div>
                                            </div>
                                          </Popover>
                                        </div>
                                      </div>
                                    );
                                  })}
                              </Col>
                            </Row>
                            <Row className="rowStyle">
                              <Col md="6">
                                <Modal
                                  size="med"
                                  isOpen={this.state.isOpenBusinessDetails}
                                  hide={this.hideModal}
                                >
                                  <Modal.Content>
                                    <div className="modalMainDiv">
                                      <div className="modalHeader">
                                        <div className="d-flex">
                                          <div className="addDocument">
                                            Add Document
                                          </div>

                                          <div
                                            className="closeIcon"
                                            onClick={this.hideModal}
                                          >
                                            <IconComponent name="close" />
                                          </div>
                                        </div>
                                      </div>

                                      <div className="ml-4 mr-5 mt-5 businessDetailsModal">
                                        <Row className="rowStyle">
                                          <Col md="12">
                                            {this.state.modalDetails &&
                                              this.state.modalDetails.length >
                                                0 &&
                                              this.state.modalDetails.map(
                                                (data, index) => {
                                                  return (
                                                    <div className="editDeleteModalDiv">
                                                      {console.log(
                                                        "data.title_of_document.." +
                                                          JSON.stringify(
                                                            data.title_of_document
                                                          )
                                                      )}{" "}
                                                      <div className="d-flex">
                                                        <div className="pr-3">
                                                          {
                                                            data.title_of_document
                                                          }
                                                        </div>
                                                        <div className="d-flex w-100 justify-content-end">
                                                          <div
                                                            className="detailsText"
                                                            onClick={() =>
                                                              this.onDetailsClickInAddDocument(
                                                                index
                                                              )
                                                            }
                                                          >
                                                            Details
                                                          </div>{" "}
                                                          <div
                                                            className="ml-2"
                                                            onClick={() =>
                                                              this.onDetailsClickInAddDocument(
                                                                index
                                                              )
                                                            }
                                                          >
                                                            <IconComponent name="downarrow" />
                                                          </div>
                                                          <div
                                                            className=" pl-4 pr-4"
                                                            onClick={() =>
                                                              this.onEditIconClick(
                                                                data,
                                                                index,
                                                                data.id
                                                              )
                                                            }
                                                          >
                                                            <IconComponent name="edit" />
                                                          </div>{" "}
                                                          <div
                                                            className="pr-4"
                                                            onClick={() =>
                                                              this.onDeleteIconClick(
                                                                index,
                                                                data.id
                                                              )
                                                            }
                                                          >
                                                            <IconComponent name="remove" />
                                                          </div>
                                                        </div>
                                                      </div>
                                                      {isOpenDetails &&
                                                      index ==
                                                        this.state
                                                          .selectedModalDataIndex ? (
                                                        <div className="mt-2 ml-3">
                                                          <Row className="rowStyle">
                                                            <Col md="6">
                                                              Certification
                                                              Number
                                                            </Col>
                                                            <Col md="6">
                                                              Year of
                                                              Publication
                                                            </Col>
                                                          </Row>
                                                          <Row className="rowStyle mb-3">
                                                            <Col
                                                              md="6"
                                                              className="font-weight-bold"
                                                            >
                                                              {data.certification_number
                                                                ? data.certification_number
                                                                : ""}
                                                            </Col>
                                                            <Col
                                                              md="6"
                                                              className="font-weight-bold"
                                                            >
                                                              {data.year_of_publication
                                                                ? data.year_of_publication
                                                                : ""}
                                                            </Col>
                                                          </Row>
                                                          <Row className="rowStyle ">
                                                            <Col md="6">
                                                              Issued by
                                                            </Col>
                                                            <Col md="6">
                                                              Issued at
                                                            </Col>
                                                          </Row>
                                                          <Row className="rowStyle mb-3">
                                                            <Col
                                                              md="6"
                                                              className="font-weight-bold"
                                                            >
                                                              {data.certificate_issued_at
                                                                ? data.certificate_issued_at
                                                                : ""}
                                                            </Col>
                                                            <Col
                                                              md="6"
                                                              className="font-weight-bold"
                                                            >
                                                              {data.certificate_issued_by
                                                                ? data.certificate_issued_by
                                                                : ""}
                                                            </Col>
                                                          </Row>
                                                          <Row className="rowStyle ">
                                                            <Col md="6">
                                                              Effective date
                                                            </Col>
                                                            <Col md="6">
                                                              Expiry date
                                                            </Col>
                                                          </Row>
                                                          <Row className="rowStyle mb-3">
                                                            <Col
                                                              md="6"
                                                              className="font-weight-bold"
                                                            >
                                                              {data.effective_date
                                                                ? moment(
                                                                    data.effective_date
                                                                  ).format(
                                                                    "DD/MM/YYYY"
                                                                  )
                                                                : ""}
                                                            </Col>
                                                            <Col
                                                              md="6"
                                                              className="font-weight-bold"
                                                            >
                                                              {data.expiry_date
                                                                ? moment(
                                                                    data.expiry_date
                                                                  ).format(
                                                                    "DD/MM/YYYY"
                                                                  )
                                                                : ""}
                                                            </Col>
                                                          </Row>
                                                        </div>
                                                      ) : (
                                                        ""
                                                      )}
                                                    </div>
                                                  );
                                                }
                                              )}
                                          </Col>
                                        </Row>

                                        <Form.Group controlId="documentType">
                                          <Form.Label>
                                            Select document type
                                          </Form.Label>
                                          <Form.Control
                                            as={() => (
                                              <Select
                                                options={
                                                  this.state.documentType
                                                }
                                                placeholder="Select"
                                                name="documentType"
                                                onChange={
                                                  this.onDocumentTypeChange
                                                }
                                                value={selectedDocumentType}
                                              />
                                            )}
                                          />
                                        </Form.Group>
                                        <Form.Group controlId="documentTitle">
                                          <Form.Label>
                                            Title of Document
                                          </Form.Label>
                                          <Form.Control
                                            required
                                            type="text"
                                            placeholder="ISO 26000 Certified (Social Responsibility)"
                                            name="documentTitle"
                                            onChange={
                                              this.onChangeModalFieldValue
                                            }
                                            value={documentTitle}
                                          />
                                        </Form.Group>

                                        <Row className="rowStyle">
                                          <Col md="6">
                                            <Form.Group controlId="certificationNumber">
                                              <Form.Label>
                                                Certification Number
                                              </Form.Label>
                                              <Form.Control
                                                type="text"
                                                name="certificationNumber"
                                                placeholder="47575775ABC"
                                                onChange={
                                                  this.onChangeModalFieldValue
                                                }
                                                value={certificationNumber}
                                              />
                                            </Form.Group>
                                          </Col>
                                          <Col md="6">
                                            <Form.Group controlId="yearOfPublication">
                                              <Form.Label>
                                                Year of Publication
                                              </Form.Label>
                                              <Form.Control
                                                type="number"
                                                placeholder="2014"
                                                name="yearOfPublication"
                                                onChange={
                                                  this.onChangeModalFieldValue
                                                }
                                                value={yearOfPublication}
                                              />
                                            </Form.Group>
                                          </Col>
                                        </Row>
                                        <Row className="rowStyle">
                                          <Col md="6">
                                            <Form.Group controlId="certificateIssuedBy">
                                              <Form.Label>
                                                Certificate Issued by
                                              </Form.Label>
                                              <Form.Control
                                                type="text"
                                                placeholder="Authority"
                                                name="certificateIssuedBy"
                                                onChange={
                                                  this.onChangeModalFieldValue
                                                }
                                                value={certificateIssuedBy}
                                              />
                                            </Form.Group>
                                          </Col>
                                          <Col md="6">
                                            <Form.Group controlId="certificateIssuedAt">
                                              <Form.Label>
                                                Certificate Issued at
                                              </Form.Label>
                                              <Form.Control
                                                type="text"
                                                placeholder="location"
                                                name="certificateIssuedAt"
                                                onChange={
                                                  this.onChangeModalFieldValue
                                                }
                                                value={certificateIssuedAt}
                                              />
                                            </Form.Group>
                                          </Col>
                                        </Row>
                                        <Row className="rowStyle">
                                          <Col md="6">
                                            <Form.Group controlId="effectiveDate">
                                              <Form.Label>
                                                Effective Date
                                              </Form.Label>
                                              <Form.Control
                                                required
                                                // type="date"
                                                placeholder="Value"
                                                as={() => (
                                                  <DatepickerComponent
                                                    selected={
                                                      this.state
                                                        .selectedEffectiveDate
                                                    }
                                                    onChange={
                                                      this.onChangeEffectiveDate
                                                    }
                                                    isFromVendors={true}
                                                  />
                                                )}
                                              />
                                            </Form.Group>
                                          </Col>
                                          <Col md="6">
                                            <Form.Group controlId="expiryDateForBusinessDetail">
                                              <Form.Label>
                                                Expiry Date
                                              </Form.Label>
                                              <Form.Control
                                                // type="date"
                                                placeholder="Value"
                                                as={() => (
                                                  <DatepickerComponent
                                                    selected={
                                                      this.state
                                                        .selectedExpiryDate
                                                    }
                                                    onChange={
                                                      this.onChangeExpiryDate
                                                    }
                                                    isFromVendors={true}
                                                  />
                                                )}
                                              />
                                            </Form.Group>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col md="12">
                                            <div className="ten-doc-lst-cnt">
                                              <div>
                                                <FileUpload
                                                  onChange={
                                                    this.handleFileChange
                                                  }
                                                  fromVendors={true}
                                                />
                                              </div>
                                            </div>
                                          </Col>
                                        </Row>
                                      </div>
                                    </div>

                                    <div className="mt-1 d-flex saveButton">
                                      <div className="saveButtonSubDiv col-2">
                                        <div className="saveButtonDiv">
                                          {" "}
                                          <Button
                                            variant="primary"
                                            label="Done"
                                            onClick={
                                              documentTitle
                                                ? this.onDoneButtonClick
                                                : ""
                                            }
                                          ></Button>
                                        </div>
                                      </div>
                                    </div>
                                  </Modal.Content>
                                </Modal>
                              </Col>
                            </Row>
                            <Modal
                              size="med"
                              isOpen={this.state.isAddToProfileOpen}
                              hide={this.hideModalForProfile}
                            >
                              <Modal.Content>
                                <div className="modalMainDiv">
                                  <div className="modalHeader">
                                    <div className="d-flex">
                                      <div className="addDocument">
                                        Add Document
                                      </div>

                                      <div
                                        className="closeIcon"
                                        onClick={this.hideModalForProfile}
                                      >
                                        <IconComponent name="close" />
                                      </div>
                                    </div>
                                  </div>

                                  <div className="ml-4 mr-5 mt-5 businessDetailsModal">
                                    <Row className="rowStyle">
                                      <Col md="12">
                                        {this.state.modalDetails &&
                                          this.state.modalDetails.length > 0 &&
                                          this.state.modalDetails.map(
                                            (data, index) => {
                                              return (
                                                <div className="editDeleteModalDiv">
                                                  <div className="d-flex">
                                                    <div className="pr-3">
                                                      {data.title_of_document}{" "}
                                                    </div>
                                                    <div className="d-flex w-100 justify-content-end">
                                                      <div
                                                        className="detailsText"
                                                        onClick={() =>
                                                          this.onDetailsClickInAddToProfile(
                                                            index
                                                          )
                                                        }
                                                      >
                                                        Details
                                                      </div>
                                                      <div
                                                        className="ml-2"
                                                        onClick={() =>
                                                          this.onDetailsClickInAddToProfile(
                                                            index
                                                          )
                                                        }
                                                      >
                                                        <IconComponent name="downarrow" />
                                                      </div>
                                                      <div
                                                        className=" pl-4 pr-4"
                                                        onClick={() =>
                                                          this.onEditIconClick(
                                                            data,
                                                            index,
                                                            data.id
                                                          )
                                                        }
                                                      >
                                                        <IconComponent name="edit" />
                                                      </div>{" "}
                                                      <div
                                                        className="pr-4"
                                                        onClick={() =>
                                                          this.onDeleteIconClick(
                                                            index,
                                                            data.id
                                                          )
                                                        }
                                                      >
                                                        <IconComponent name="remove" />
                                                      </div>
                                                    </div>
                                                  </div>
                                                  {isOpenDetailsForAddToProfile &&
                                                  index ==
                                                    this.state
                                                      .selectedModalDataIndexForAddToProfile ? (
                                                    <div className="mt-2 ml-3">
                                                      <Row className="rowStyle">
                                                        <Col md="6">
                                                          Certification Number
                                                        </Col>
                                                        <Col md="6">
                                                          Year of Publication
                                                        </Col>
                                                      </Row>
                                                      <Row className="rowStyle mb-3">
                                                        <Col
                                                          md="6"
                                                          className="font-weight-bold"
                                                        >
                                                          {data.certification_number
                                                            ? data.certification_number
                                                            : ""}
                                                        </Col>
                                                        <Col
                                                          md="6"
                                                          className="font-weight-bold"
                                                        >
                                                          {data.year_of_publication
                                                            ? data.year_of_publication
                                                            : ""}
                                                        </Col>
                                                      </Row>
                                                      <Row className="rowStyle ">
                                                        <Col md="6">
                                                          Issued by
                                                        </Col>
                                                        <Col md="6">
                                                          Issued at
                                                        </Col>
                                                      </Row>
                                                      <Row className="rowStyle mb-3">
                                                        <Col
                                                          md="6"
                                                          className="font-weight-bold"
                                                        >
                                                          {data.certificate_issued_at
                                                            ? data.certificate_issued_at
                                                            : ""}
                                                        </Col>
                                                        <Col
                                                          md="6"
                                                          className="font-weight-bold"
                                                        >
                                                          {data.certificate_issued_by
                                                            ? data.certificate_issued_by
                                                            : ""}
                                                        </Col>
                                                      </Row>
                                                      <Row className="rowStyle ">
                                                        <Col md="6">
                                                          Effective date
                                                        </Col>
                                                        <Col md="6">
                                                          Expiry date
                                                        </Col>
                                                      </Row>
                                                      <Row className="rowStyle mb-3">
                                                        <Col
                                                          md="6"
                                                          className="font-weight-bold"
                                                        >
                                                          {data.effective_date
                                                            ? moment(
                                                                data.effective_date
                                                              ).format(
                                                                "DD/MM/YYYY"
                                                              )
                                                            : ""}
                                                        </Col>
                                                        <Col
                                                          md="6"
                                                          className="font-weight-bold"
                                                        >
                                                          {data.expiry_date
                                                            ? moment(
                                                                data.expiry_date
                                                              ).format(
                                                                "DD/MM/YYYY"
                                                              )
                                                            : ""}
                                                        </Col>
                                                      </Row>
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )}
                                                </div>
                                              );
                                            }
                                          )}
                                      </Col>
                                    </Row>

                                    <Row className="rowStyle">
                                      <Col md="12" className="d-flex">
                                        <div>
                                          <Button
                                            outline="secondary"
                                            icon="icon"
                                            iconName={
                                              <IconComponent name="circleplusbackground" />
                                            }
                                            label="Add another document"
                                            variant="upload"
                                            onClick={
                                              this
                                                .onAddAnotherDocumentButtonClick
                                            }
                                          ></Button>
                                        </div>
                                      </Col>
                                    </Row>
                                  </div>
                                </div>
                                <div className="d-flex saveButton addToProfile">
                                  <div className="saveButtonSubDiv col-4">
                                    <div className="saveButtonDiv col-10">
                                      {" "}
                                      <Button
                                        variant="primary"
                                        label="Add To Profile"
                                        onClick={this.onAddToProfileButtonClick}
                                      ></Button>
                                    </div>
                                  </div>
                                </div>
                              </Modal.Content>
                            </Modal>

                            <Row className="rowStyle mt-4">
                              <Col md="6">
                                Having no certification imply that your company
                                does not have any certification at all.
                              </Col>
                              <Col md="6" className="d-flex">
                                <div>
                                  <Button
                                    outline="secondary"
                                    icon="icon"
                                    iconName={
                                      <IconComponent name="circleplusbackground" />
                                    }
                                    label="Add a document"
                                    variant="upload"
                                    onClick={
                                      this.onAddAnotherDocumentButtonClick
                                    }
                                  ></Button>
                                </div>
                              </Col>
                            </Row>

                            <Row className="rowStyle companySize">
                              <Col md="6">
                                <div className="rowCol1TextDiv">
                                  Company Size
                                </div>
                              </Col>
                              <Col md="4">
                                {/* <Form.Group controlId="turnOverData">
                                <Form.Label>*Annual TurnOver (AED)</Form.Label>
                                <Form.Control
                                type="text"
                                  as={() => ( */}
                                <Form.Label>*Annual TurnOver (AED)</Form.Label>

                                <Select
                                  options={this.state.turnOverData}
                                  placeholder="value"
                                  name="turnOverData"
                                  // name="state"
                                  // id="state"
                                  // onBlur={() =>
                                  //   setFieldTouched("turnOverData", true)
                                  // }
                                  onBlur={() =>
                                    setFieldTouched("turnOverData", true)
                                  }
                                  value={selectedTurnOverData}
                                  //value={values.turnOverData}
                                  onChange={(opt) => {
                                    this.onTurnOverChange(opt);
                                    handleChange(opt.value);
                                    // handleChange("turnOverData")(opt.value);
                                    setFieldValue(
                                      "turnOverData",
                                      opt.value,
                                      true
                                    );
                                  }}
                                />
                                {/* //options={newStateList}
                                // // error={errors.turnOverData}
                                // // touched={touched.turnOverData} */}
                                {console.log(
                                  "touched.turnOverData.." +
                                    JSON.stringify(touched.turnOverData)
                                )}
                                {touched.turnOverData && (
                                  <div className="errorMessage">
                                    {errors.turnOverData && errors.turnOverData}
                                  </div>
                                )}
                                {/* )}
                                /> */}

                                {/* {console.log(
                                "touched.turnOverData.." +
                                  JSON.stringify(touched.turnOverData)
                              )} */}

                                {/* </Form.Group> */}
                              </Col>
                            </Row>
                            <Row className="rowStyle sizeStyle">
                              <Col md="6"></Col>
                              <Col md="4">
                                {/* <Form.Group controlId="companySize"> */}
                                <Form.Label>* Size</Form.Label>
                                {/* <Form.Control
                                  as={() => ( */}
                                <Select
                                  options={this.state.companySize}
                                  placeholder="value"
                                  name="companySize"
                                  onBlur={() =>
                                    setFieldTouched("companySize", true)
                                  }
                                  value={selectedCompanySize}
                                  onChange={(option, e) => {
                                    this.onCompanySizeChange(option);
                                    handleChange(option.value);
                                    setFieldValue(
                                      "companySize",
                                      option.value,
                                      true
                                    );
                                  }}
                                />
                                {touched.companySize && (
                                  <div className="errorMessage">
                                    {errors.companySize && errors.companySize}
                                  </div>
                                )}
                                {/* //     )}
                              //   />
                              // </Form.Group> */}
                              </Col>
                            </Row>
                          </div>
                        }
                      />{" "}
                    </div>
                    <div className="mb-3">
                      <Row className="vendorRegFormRow">
                        <Col
                          md="12"
                          lg="12"
                          className="vendorRegFormRowSecCol d-flex"
                        >
                          <div className="mr-2">
                            <Button
                              label="Prev"
                              variant="primary"
                              onClick={this.onPreviousButtonClick}
                            ></Button>
                          </div>
                          <div className="mr-2">
                            <Button
                              label="Next"
                              className=""
                              variant="primary"
                              // onClick={(e) =>
                              //   this.onNextButtonClick(e, props.values)
                              // }
                              onClick={(e) => {
                                handleSubmit(e);
                                this.onNextBusinessButtonClick(values, errors);
                              }}
                            ></Button>
                          </div>
                          <div className="mr-2">
                            <Button
                              label="Save For Later"
                              classname=""
                              variant="primary"
                              onClick={this.onSaveButtonClick}
                            ></Button>
                          </div>
                          <div className="mr-2">
                            <Button
                              label="Publish"
                              classname=""
                              isDisabled={true}
                            ></Button>
                          </div>
                          <div className="mr-2">
                            <Button
                              label="Cancel"
                              classname=""
                              variant="link"
                            ></Button>
                          </div>
                        </Col>
                      </Row>
                    </div>{" "}
                  </Form>
                );
              }}
            </Formik>
          </div>
        ) : activeTab == 3 ? (
          <Form
            ref={this.form}
            noValidate
            //validated={validated}
            // onSubmit={this.onNextButtonClick}
          >
            <div>
              <Row className="vendorRegFormRow">
                <Col md="6" lg="5" className="vendorRegFormText">
                  Vendor Registeration Form
                </Col>
                <Col md="6" lg="7" className="vendorRegFormRowSecCol d-flex">
                  <div className="mr-2">
                    <Button
                      label="Prev"
                      variant="primary"
                      onClick={this.onPreviousButtonClick}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Next"
                      className=""
                      variant="primary"
                      onClick={(e) => this.onNextAddressButtonClick(e)}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Save For Later"
                      classname=""
                      variant="primary"
                      onClick={this.onSaveButtonClick}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Publish"
                      classname=""
                      isDisabled={true}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button label="Cancel" classname="" variant="link"></Button>
                  </div>
                </Col>
              </Row>
            </div>
            <div>
              <TabComponent
                tabData={this.state.tabs}
                onTabChange={this.onTabChange}
                activeTab={activeTab}
                tabContent={
                  <div className="formDetailsMainDiv">
                    <Modal
                      size="sml"
                      isOpen={this.state.isOpenAddressDetailsModal}
                      hide={this.hideAddressDetails}
                    >
                      <Modal.Content>
                        <div className="modalMainDiv">
                          <div className="modalHeader">
                            <div className="d-flex pb-2">
                              <div className="addDocument heading">
                                Add an address
                              </div>

                              <div
                                className="closeIcon"
                                onClick={this.hideAddressDetails}
                              >
                                <IconComponent name="close" />
                              </div>
                            </div>
                          </div>
                          <div className="addressMainDiv">
                            <Form.Group controlId="addressName">
                              <Form.Label>Address name</Form.Label>
                              <Form.Control
                                required
                                type="text"
                                name="addressName"
                                onChange={this.onChangeModalFieldValue}
                                value={addressName}
                              />
                            </Form.Group>{" "}
                            <div className="exampleText">
                              Example : Head office, Branch office, etc.,{" "}
                            </div>
                            <Form.Group controlId="countryName">
                              <Form.Label>Country</Form.Label>
                              <Form.Control
                                as={() => (
                                  <Select
                                    options={this.state.countryData}
                                    placeholder="Select"
                                    name="countryName"
                                    onChange={this.onCountryChange}
                                    value={selectedCountry}
                                  />
                                )}
                              />
                            </Form.Group>
                            <Form.Group controlId="streetLine1">
                              <Form.Label>Street Address Line 1</Form.Label>
                              <Form.Control
                                type="text"
                                name="streetLine1"
                                onChange={this.onChangeModalFieldValue}
                                value={streetLine1}
                              />
                            </Form.Group>
                            <Form.Group controlId="streetLine2">
                              <Form.Label>Street Address Line 2</Form.Label>
                              <Form.Control
                                type="text"
                                name="streetLine2"
                                onChange={this.onChangeModalFieldValue}
                                value={streetLine2}
                              />
                            </Form.Group>
                            <Form.Group controlId="city">
                              <Form.Label>City</Form.Label>
                              <Form.Control
                                type="text"
                                name="city"
                                onChange={this.onChangeModalFieldValue}
                                value={city}
                              />
                            </Form.Group>
                            <Form.Group controlId="stateOrProvince">
                              <Form.Label>State / Province / Region</Form.Label>
                              <Form.Control
                                type="text"
                                name="stateOrProvince"
                                onChange={this.onChangeModalFieldValue}
                                value={stateOrProvince}
                              />
                            </Form.Group>
                            <Form.Group controlId="postBoxNumber">
                              <Form.Label>Post Box Number</Form.Label>
                              <Form.Control
                                type="number"
                                //placeholder="streetLine1"
                                name="postBoxNumber"
                                onChange={this.onChangeModalFieldValue}
                                value={postBoxNumber}
                              />
                            </Form.Group>
                          </div>
                          <div className="mt-1 d-flex saveButton">
                            <div className="saveButtonSubDiv col-2">
                              <div className="saveButtonDiv">
                                {" "}
                                <Button
                                  variant="primary"
                                  label="Save"
                                  onClick={this.onAddressSaveButtonClick}
                                ></Button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Modal.Content>
                    </Modal>
                    <Row className="rowStyle">
                      <Col md="12">
                        <div className="rowCol1TextDiv heading">
                          Office address
                        </div>
                        <div>
                          Try to have all the addresses. Having the address that
                          we use for invoice is must.
                        </div>
                      </Col>
                    </Row>
                    <Row className="rowStyle">
                      <Col md="6" className="d-flex">
                        <div className="addAnAddressMainDiv">
                          <div>
                            <Button
                              // outline="secondary"
                              icon="icon"
                              iconName={
                                <IconComponent name="circleplusbackground" />
                              }
                              label="Add an address"
                              variant="upload"
                              onClick={this.onAddAnAddressButtonClick}
                            ></Button>
                          </div>
                        </div>
                      </Col>
                    </Row>
                    <Row className="rowStyle addressRowStyle">
                      {addressDetails &&
                        addressDetails.length > 0 &&
                        addressDetails.map((data, index) => {
                          let {
                            address_name,
                            country,
                            address_line_one,
                            address_line_two,
                            state,
                            city,
                            postbox
                          } = data;
                          return (
                            <Col md="4">
                              <div className="addressDetailsMainDiv">
                                <div className="headingFontStyle addressDiv">
                                  <div className="d-flex mb-3">
                                    <div>
                                      {address_name ? address_name : ""}
                                    </div>
                                    <div className="d-flex w-100 justify-content-end">
                                      <div
                                        className=" pl-3 pr-3"
                                        onClick={() =>
                                          this.onEditAddressIconClick(
                                            data,
                                            index,
                                            data.id
                                          )
                                        }
                                      >
                                        <IconComponent name="edit" />
                                      </div>
                                      <div
                                        className="closeIcon mr-3"
                                        onClick={() =>
                                          this.onRemoveAddressIconClick(
                                            index,
                                            data.id
                                          )
                                        }
                                      >
                                        <IconComponent name="close" />
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div className="addressDiv">
                                  {address_line_one ? address_line_one : ""}
                                  {address_line_two ? address_line_two : ""}
                                  {state ? state : ""}
                                </div>
                                <div className="addressDiv">
                                  {country ? country : ""}
                                </div>
                                <div className="addressDiv">
                                  {postbox ? (
                                    <>
                                      <span>postBoxNumber</span>{" "}
                                      <span>{postbox}</span>
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              </div>
                            </Col>
                          );
                        })}
                    </Row>
                    <Row className="rowStyle">
                      <Modal
                        size="sml"
                        isOpen={this.state.isOpenContactDetailsModal}
                        hide={this.hideContactDetails}
                      >
                        <Modal.Content>
                          <Formik
                            initialValues={{
                              personName: "",
                              phoneNumberForContact: "",
                              jobTitleForContact: "",
                              emailForContact: ""
                            }}
                            // onSubmit={async (values) => {
                            //   //await new Promise((resolve) => setTimeout(resolve, 500));
                            //   console.log(
                            //     "values........." + JSON.stringify(values)
                            //   );
                            //   //alert(JSON.stringify(values, null, 2));
                            //   //this.onContactSaveButtonClick(values);
                            // }}
                            // onSubmit={}

                            validationSchema={Yup.object().shape({
                              personName: Yup.string().required(
                                "Person name is required"
                              ),
                              jobTitleForContact: Yup.string().required(
                                "Job Title is required"
                              ),

                              emailForContact: Yup.string()
                                .email("Email is invalid")
                                .required("Email is required!"),
                              phoneNumberForContact: Yup.string()
                                .required("Phone number is required")
                                .matches(
                                  /^\d{10}$/,
                                  "Phone number is not valid"
                                )
                              // phoneNumberForContact: Yup.string()
                              //   .required("Phone number is required")
                              //   .matches(
                              //     phoneRegExp,
                              //     "Phone number is not valid"
                              //   )
                            })}
                          >
                            {(props) => {
                              const {
                                values,
                                touched,
                                errors,
                                dirty,
                                isSubmitting,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                handleReset
                              } = props;
                              return (
                                <Form
                                  ref={this.form}
                                  noValidate
                                  // validated={validated}
                                  onSubmit={handleSubmit}
                                >
                                  <div className="modalMainDiv">
                                    <div className="modalHeader">
                                      <div className="d-flex pb-2">
                                        <div className="addDocument">
                                          Add a Contact
                                        </div>

                                        <div
                                          className="closeIcon"
                                          onClick={this.hideContactDetails}
                                        >
                                          <IconComponent name="close" />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="addressMainDiv">
                                      <Row className="rowStyle mt-4">
                                        <Col md="3" className="userProfile">
                                          <Form.Group controlId="userProfileImage">
                                            <Form.File
                                              id="custom-file"
                                              label={
                                                this.state.profileImage ? (
                                                  <img
                                                    className="userProfileImage"
                                                    src={
                                                      this.state.profileImage
                                                    }
                                                  />
                                                ) : (
                                                  <div className="placeHolderImageMainDiv">
                                                    <div className="placeHolderImageDiv">
                                                      <IconComponent name="userprofile" />
                                                    </div>
                                                  </div>
                                                )
                                              }
                                              custom
                                              onChange={(event) => {
                                                this.onUserProfileImageChange(
                                                  event
                                                );
                                                //setFieldValue("userImage", )
                                              }}
                                            />
                                          </Form.Group>
                                        </Col>
                                        <Col md="9">
                                          <Form.Group controlId="personName">
                                            <Form.Label>
                                              * Name of the person
                                            </Form.Label>
                                            <Form.Control
                                              required
                                              type="text"
                                              placeholder="Rengarajan"
                                              name="personName"
                                              onChange={(e) => {
                                                this.onChangeModalFieldValue(e);
                                                handleChange(e);
                                              }}
                                              value={personName}
                                              isInvalid={
                                                !!errors.personName &&
                                                !!errors.personName
                                              }
                                            />
                                            {errors.personName &&
                                              touched.personName && (
                                                <Form.Control.Feedback type="invalid">
                                                  {errors.personName}
                                                </Form.Control.Feedback>
                                              )}
                                          </Form.Group>
                                          <Form.Group controlId="jobTitleForContact">
                                            <Form.Label>* Job Title</Form.Label>
                                            <Form.Control
                                              required
                                              type="text"
                                              placeholder="Procurement Executive"
                                              name="jobTitleForContact"
                                              // onChange={
                                              //   this.onChangeModalFieldValue
                                              // }
                                              onChange={(e) => {
                                                this.onChangeModalFieldValue(e);
                                                handleChange(e);
                                              }}
                                              value={jobTitleForContact}
                                              isInvalid={
                                                !!errors.jobTitleForContact &&
                                                !!touched.jobTitleForContact
                                              }
                                            />
                                            {errors.jobTitleForContact &&
                                              touched.jobTitleForContact && (
                                                <Form.Control.Feedback type="invalid">
                                                  {errors.jobTitleForContact}
                                                </Form.Control.Feedback>
                                              )}
                                          </Form.Group>
                                          <Form.Group controlId="emailForContact">
                                            <Form.Label>* Email ID</Form.Label>
                                            <Form.Control
                                              required
                                              type="text"
                                              placeholder="renga@amtrex-uae.com"
                                              name="emailForContact"
                                              // onChange={
                                              //   this.onChangeModalFieldValue
                                              // }
                                              onChange={(e) => {
                                                this.onChangeModalFieldValue(e);
                                                handleChange(e);
                                              }}
                                              value={emailForContact}
                                              isInvalid={
                                                !!errors.emailForContact &&
                                                !!touched.emailForContact
                                              }
                                            />
                                            {errors.emailForContact &&
                                              touched.emailForContact && (
                                                <Form.Control.Feedback type="invalid">
                                                  {errors.emailForContact}
                                                </Form.Control.Feedback>
                                              )}
                                          </Form.Group>
                                          <Form.Group controlId="phoneNumberForContact">
                                            <Form.Label>*Phone</Form.Label>
                                            <Form.Control
                                              required
                                              type="text"
                                              placeholder="+971 50 7645678"
                                              name="phoneNumberForContact"
                                              onChange={(e) => {
                                                this.onChangeModalFieldValue(e);
                                                handleChange(e);
                                              }}
                                              // onChange={
                                              //   this.onChangeModalFieldValue
                                              // }
                                              value={phoneNumberForContact}
                                              isInvalid={
                                                !!errors.phoneNumberForContact &&
                                                !!touched.phoneNumberForContact
                                              }
                                            />
                                            {errors.phoneNumberForContact &&
                                              touched.phoneNumberForContact && (
                                                <Form.Control.Feedback type="invalid">
                                                  {errors.phoneNumberForContact}
                                                </Form.Control.Feedback>
                                              )}
                                          </Form.Group>
                                        </Col>
                                      </Row>
                                    </div>
                                    <div className="mt-1 d-flex saveButton">
                                      <div className="saveButtonSubDiv col-2">
                                        <div className="saveButtonDiv">
                                          {" "}
                                          <Button
                                            variant="primary"
                                            label="Save"
                                            type="submit"
                                            onClick={() =>
                                              this.onContactSaveButtonClick(
                                                values,
                                                errors
                                              )
                                            }
                                          ></Button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </Form>
                              );
                            }}
                          </Formik>
                        </Modal.Content>
                      </Modal>

                      <Col md="12">
                        <div className="rowCol1TextDiv heading">Contacts</div>
                      </Col>
                    </Row>
                    <Row className="rowStyle">
                      <Col md="6" className="d-flex">
                        <div className="contactMainDiv">
                          <div>
                            <Button
                              // outline="secondary"
                              icon="icon"
                              iconName={
                                <IconComponent name="circleplusbackground" />
                              }
                              label="Add a Contact"
                              variant="upload"
                              onClick={this.onContactButtonClick}
                            ></Button>
                          </div>
                        </div>
                      </Col>
                    </Row>

                    <Row className="tableStyle">
                      {this.state.contactDetails &&
                      this.state.contactDetails.length > 0 ? (
                        <Table borderless>
                          <thead>
                            <tr>
                              <th className="tableHeading">Name</th>
                              <th className="tableHeading">Job Title</th>
                              <th className="tableHeading">Email ID</th>
                              <th className="phoneNumberStyle tableHeading">
                                Phone Number
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.contactDetails &&
                              this.state.contactDetails.length > 0 &&
                              this.state.contactDetails.map((data, index) => {
                                let { name, emailid, phone, job_title } = data;
                                return (
                                  <tr>
                                    <td>
                                      <span className="d-inline-block pr-2">
                                        <Avatar image="" size="normal" />
                                      </span>
                                      <span>{name ? name : ""}</span>
                                    </td>
                                    <td>{job_title ? job_title : ""}</td>
                                    <td>{emailid ? emailid : ""}</td>
                                    <td className="phoneNumberStyle">
                                      {phone ? phone : ""}
                                    </td>

                                    <td
                                      className="pl-3 pr-3 editContactStyle"
                                      onClick={() =>
                                        this.onEditContactIconClick(
                                          data,
                                          index,
                                          data.id
                                        )
                                      }
                                    >
                                      <IconComponent name="editiconforcontact" />
                                    </td>
                                    <td
                                      className="closeIcon mr-3"
                                      onClick={() =>
                                        this.onRemoveContactIconClick(
                                          index,
                                          data.id
                                        )
                                      }
                                    >
                                      <IconComponent name="close" />
                                    </td>
                                  </tr>
                                );
                              })}
                          </tbody>
                        </Table>
                      ) : (
                        ""
                      )}
                    </Row>
                  </div>
                }
              />{" "}
            </div>
            <div className="mb-3">
              <Row className="vendorRegFormRow">
                <Col md="12" lg="12" className="vendorRegFormRowSecCol d-flex">
                  <div className="mr-2">
                    <Button
                      label="Prev"
                      variant="primary"
                      onClick={this.onPreviousButtonClick}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Next"
                      className=""
                      variant="primary"
                      onClick={(e) => this.onNextAddressButtonClick(e)}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Save For Later"
                      classname=""
                      variant="primary"
                      onClick={this.onSaveButtonClick}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Publish"
                      classname=""
                      isDisabled={true}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button label="Cancel" classname="" variant="link"></Button>
                  </div>
                </Col>
              </Row>
            </div>{" "}
          </Form>
        ) : activeTab == 4 ? (
          <Form
            ref={this.form}
            noValidate
            //validated={validated}
            //onSubmit={this.onNextButtonClick}
          >
            <div>
              <Row className="vendorRegFormRow">
                <Col md="6" lg="5" className="vendorRegFormText">
                  Vendor Registeration Form
                </Col>
                <Col md="6" lg="7" className="vendorRegFormRowSecCol d-flex">
                  <div className="mr-2">
                    <Button
                      label="Prev"
                      variant="primary"
                      onClick={this.onPreviousButtonClick}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Next"
                      className=""
                      variant="primary"
                      // onClick={(e) => this.onNextButtonClick(e,props.values)}
                      isDisabled={true}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Save For Later"
                      classname=""
                      variant="primary"
                      onClick={this.onSaveButtonClick}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Publish"
                      classname=""
                      variant="primary"
                      isDisabled={true}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button label="Cancel" classname="" variant="link"></Button>
                  </div>
                </Col>
              </Row>
            </div>
            <div>
              <TabComponent
                tabData={this.state.tabs}
                onTabChange={this.onTabChange}
                activeTab={activeTab}
                tabContent={
                  <div className="formDetailsMainDiv">
                    <div className="">
                      <Row className="rowStyle">
                        <Col md="6">
                          <div className="rowCol1TextDiv heading">
                            Key Projects{" "}
                          </div>
                        </Col>
                        <Col md="6" className="d-flex">
                          <div className="d-flex">
                            <Button
                              outline="secondary"
                              icon="icon"
                              iconName={<IconComponent name="circleplus" />}
                              label="Add a Project"
                              variant="upload"
                              onClick={this.onAddProjectuttonClick}
                            ></Button>
                          </div>
                        </Col>
                      </Row>
                      <Row className="rowStyle mt-3">
                        <Col md="6">
                          For good results with current and future buyers,
                          complete as much information as possible.
                        </Col>

                        <Col md="6">
                          {this.state.projectData &&
                            this.state.projectData.length > 0 &&
                            this.state.projectData.map((data, index) => {
                              return (
                                <div>
                                  <div className="d-flex">
                                    <div>{data.projectName}</div>
                                    <div className="d-flex w-100 justify-content-end">
                                      <div
                                        className=" pl-4 pr-4"
                                        onClick={() =>
                                          this.onEditIconClickForProject(
                                            data,
                                            index,
                                            data.id
                                          )
                                        }
                                      >
                                        <IconComponent name="edit" />
                                      </div>{" "}
                                      <div
                                        className="pr-4"
                                        onClick={() =>
                                          this.onDeleteIconClickForProjectData(
                                            index
                                          )
                                        }
                                      >
                                        <IconComponent name="close" />
                                      </div>
                                    </div>
                                  </div>

                                  <div>
                                    <Popover
                                      content={
                                        <div className="pl-3 pr-3 pb-5">
                                          <div
                                            className="closeIconInView"
                                            onClick={this.hideProjectDetails}
                                          >
                                            <IconComponent name="close" />
                                          </div>

                                          <div className="font-weight-bold documentTitleInView">
                                            {data.projectName}
                                          </div>
                                          <Row className="rowStyle">
                                            <Col md="6">
                                              Certification Number
                                            </Col>
                                            <Col md="6">
                                              Year of Publication
                                            </Col>
                                          </Row>
                                          <Row className="rowStyle mb-3">
                                            <Col
                                              md="6"
                                              className="font-weight-bold"
                                            >
                                              {data.certificationNumber
                                                ? data.certificationNumber
                                                : ""}
                                            </Col>
                                            <Col
                                              md="6"
                                              className="font-weight-bold"
                                            >
                                              {data.yearOfPublication
                                                ? data.yearOfPublication
                                                : ""}
                                            </Col>
                                          </Row>
                                        </div>
                                      }
                                      // title="Title"
                                      trigger="click"
                                      visible={
                                        index ==
                                        this.state.selectedProjectDataIndex
                                          ? this.state.isVisibleProject
                                          : ""
                                      }
                                      placement="bottomLeft"
                                      onVisibleChange={
                                        this.handleProjectVisibleChange
                                      }
                                    >
                                      <div className="d-flex">
                                        <div
                                          className="detailsText"
                                          onClick={() =>
                                            this.onProjectDetailsClick(index)
                                          }
                                        >
                                          Details
                                        </div>{" "}
                                        <div
                                          className="ml-2"
                                          onClick={() =>
                                            this.onProjectDetailsClick(index)
                                          }
                                        >
                                          <IconComponent name="downarrow" />
                                        </div>
                                      </div>
                                    </Popover>
                                  </div>
                                </div>
                              );
                            })}
                        </Col>
                      </Row>
                      <Modal
                        size="sml"
                        isOpen={this.state.isOpenProjectModal}
                        hide={this.hideExperienceProjectDetails}
                      >
                        <Modal.Content>
                          <Formik
                            initialValues={{
                              consultant: "",
                              projectValue: "",
                              projectDuration: "",
                              selectedDateForFromDate: "",
                              selectedDateForToDate: ""
                            }}
                            // onSubmit={async (values) => {
                            //   //await new Promise((resolve) => setTimeout(resolve, 500));
                            //   console.log(
                            //     "values........." + JSON.stringify(values)
                            //   );
                            //   //alert(JSON.stringify(values, null, 2));
                            //   //this.onContactSaveButtonClick(values);
                            // }}
                            // onSubmit={}

                            validationSchema={Yup.object().shape({
                              consultant: Yup.string().required(
                                "Consultant is required"
                              ),
                              projectValue: Yup.string().required(
                                "Job Title is required"
                              ),

                              projectDuration: Yup.string().required(
                                "Project duration is required"
                              ),
                              selectedDateForFromDate: Yup.date().required(
                                "From date is required"
                              ),
                              selectedDateForToDate: Yup.date().required(
                                "To date is required"
                              )
                            })}
                          >
                            {(props) => {
                              const {
                                values,
                                touched,
                                errors,
                                dirty,
                                isSubmitting,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                handleReset,
                                setFieldValue
                              } = props;
                              return (
                                <Form
                                  ref={this.form}
                                  noValidate
                                  // validated={validated}
                                  onSubmit={handleSubmit}
                                >
                                  <div className="modalMainDiv">
                                    <div className="modalHeader">
                                      <div className="d-flex pb-2">
                                        <div className="addDocument heading">
                                          Add a Project
                                        </div>

                                        <div
                                          className="closeIcon"
                                          onClick={
                                            this.hideExperienceProjectDetails
                                          }
                                        >
                                          <IconComponent name="close" />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="ml-4 mr-5 mt-5 experienceModal">
                                      <Row className="rowStyle">
                                        <Col md="12">
                                          <Form.Group controlId="projectName mt-2">
                                            <Form.Label>
                                              Name of the project{" "}
                                            </Form.Label>
                                            <span className="exampleText pl-1">
                                              (eg., Head office, warehouse..)
                                            </span>
                                            <Form.Control
                                              required
                                              type="text"
                                              placeholder="Aluminium glazing for XYZ Mall"
                                              name="projectName"
                                              onChange={
                                                this.onChangeModalFieldValue
                                              }
                                              value={projectName}
                                            />
                                          </Form.Group>
                                        </Col>
                                      </Row>
                                      <Row className="rowStyle">
                                        <Col md="6">
                                          <Form.Group controlId="client">
                                            <Form.Label>Client </Form.Label>
                                            <Form.Control
                                              type="text"
                                              placeholder="Soylent Corporation"
                                              name="client"
                                              onChange={
                                                this.onChangeModalFieldValue
                                              }
                                              value={client}
                                            />
                                          </Form.Group>
                                        </Col>
                                        <Col md="6">
                                          <Form.Group controlId="consultant">
                                            <Form.Label>Consultant</Form.Label>
                                            {/* <Form.Control
                                      as={() => ( */}
                                            <Select
                                              options={this.state.consultant}
                                              placeholder="Select"
                                              name="consultant"
                                              onChange={(option) => {
                                                this.onConsultantChange();
                                                setFieldValue(
                                                  "consultant",
                                                  option.value,
                                                  true
                                                );
                                                handleChange(option.value);
                                              }}
                                              value={
                                                this.state.selectedConsultant
                                              }
                                            />
                                            {/* //   )}
                                    // /> */}
                                          </Form.Group>
                                          {touched.consultant && (
                                            <div className="errorMessage">
                                              {errors.consultant &&
                                                errors.consultant}
                                            </div>
                                          )}
                                        </Col>
                                      </Row>

                                      <Row className="rowStyle">
                                        <Col md="12">
                                          <div className="companyDescriptionDiv">
                                            <Form.Group controlId="projectDescription">
                                              <Form.Label>
                                                Project details in short
                                              </Form.Label>
                                              <Form.Control
                                                as="textarea"
                                                rows={6}
                                                placeholder="Value"
                                                name="projectDescription"
                                                value={projectDescription}
                                                onChange={
                                                  this.onChangeModalFieldValue
                                                }
                                              />
                                            </Form.Group>
                                          </div>
                                        </Col>
                                      </Row>
                                      <Row className="rowStyle">
                                        <Col md="12">
                                          <Form.Group controlId="projectValue">
                                            <Form.Label>
                                              * Project Value
                                            </Form.Label>
                                            {/* <Form.Control
                                      as={() => ( */}
                                            <Select
                                              options={this.state.projectValue}
                                              placeholder="Select"
                                              name="selectedProjectValue"
                                              onChange={(option) => {
                                                this.onProjectValueChange(
                                                  option
                                                );
                                                setFieldValue(
                                                  "projectValue",
                                                  option.value,
                                                  true
                                                );
                                                handleChange(option.value);
                                              }}
                                              value={
                                                this.state.selectedProjectValue
                                              }
                                            />
                                            {/* )}
                                    /> */}
                                            {touched.projectValue && (
                                              <div className="errorMessage">
                                                {errors.projectValue &&
                                                  errors.projectValue}
                                              </div>
                                            )}
                                          </Form.Group>
                                        </Col>
                                      </Row>
                                      <Row className="rowStyle">
                                        <Col md="4" className="pr-0">
                                          <Form.Group controlId="projectDuration">
                                            <Form.Label>
                                              * Project Duration
                                            </Form.Label>
                                            {/* <Form.Control
                                      as={() => ( */}
                                            <Select
                                              options={
                                                this.state.projectDuration
                                              }
                                              placeholder="Select"
                                              name="projectDuration"
                                              onChange={(option) => {
                                                this.onProjectDurationChange(
                                                  option
                                                );
                                                handleChange(option.value);
                                                setFieldValue(
                                                  "projectDuration",
                                                  option.value,
                                                  true
                                                );
                                              }}
                                              value={
                                                this.state
                                                  .selectedProjectDuration
                                              }
                                            />
                                            {/* )}
                                    /> */}
                                            {touched.projectDuration && (
                                              <div className="errorMessage">
                                                {errors.projectDuration &&
                                                  errors.projectDuration}{" "}
                                              </div>
                                            )}
                                          </Form.Group>
                                        </Col>
                                        <Col md="4" className="pr-0">
                                          <Form.Group
                                            controlId="fromDate"
                                            className="ml-2"
                                          >
                                            <Form.Label>* From Date</Form.Label>
                                            {/* <Form.Control
                                      placeholder="Value"
                                      as={() => ( */}
                                            <DatepickerComponent
                                              selected={
                                                this.state
                                                  .selectedDateForFromDate
                                              }
                                              onChange={(date) => {
                                                this.onChangeFromDate(date);
                                                setFieldValue(
                                                  "selectedDateForFromDate",
                                                  date,
                                                  true
                                                );
                                                //handleChange(date)
                                              }}
                                              isFromVendorExperience={true}
                                            />
                                            {/* )}
                                    /> */}
                                            {touched.selectedDateForFromDate && (
                                              <div className="errorMessage">
                                                {errors.selectedDateForFromDate &&
                                                  errors.selectedDateForFromDate}{" "}
                                              </div>
                                            )}
                                          </Form.Group>
                                        </Col>
                                        <Col md="4" className="pr-0">
                                          <Form.Group
                                            controlId="toDate"
                                            className="ml-2"
                                          >
                                            <Form.Label>* To Date</Form.Label>
                                            {/* <Form.Control
                                      placeholder="Value"
                                      as={() => ( */}
                                            <DatepickerComponent
                                              selected={
                                                this.state.selectedDateForToDate
                                              }
                                              // onChange={this.onChangeToDate}
                                              onChange={(date) => {
                                                this.onChangeToDate(date);
                                                setFieldValue(
                                                  "selectedDateForToDate",
                                                  date,
                                                  true
                                                );
                                                //handleChange(date);
                                              }}
                                              isFromVendorExperience={true}
                                            />
                                            {/* )}
                                    /> */}
                                            {touched.selectedDateForToDate && (
                                              <div className="errorMessage">
                                                {errors.selectedDateForToDate &&
                                                  errors.selectedDateForToDate}{" "}
                                              </div>
                                            )}
                                          </Form.Group>
                                        </Col>
                                      </Row>
                                    </div>
                                  </div>
                                  <div className="mt-1 d-flex saveButton">
                                    <div className="saveButtonSubDiv col-2">
                                      <div className="saveButtonDiv">
                                        {" "}
                                        <Button
                                          variant="primary"
                                          label="Done"
                                          type="submit"
                                          onClick={() =>
                                            this.onProjectDoneButtonClick(
                                              values,
                                              errors
                                            )
                                          }
                                        ></Button>
                                      </div>
                                    </div>
                                  </div>
                                </Form>
                              );
                            }}
                          </Formik>
                        </Modal.Content>
                      </Modal>
                      <Row className="rowStyle mt-4">
                        <Col md="6">
                          <div className="rowCol1TextDiv heading">
                            Consultants
                          </div>
                        </Col>
                        <Col md="6"></Col>
                      </Row>
                      <Row className="rowStyle mt-2 consultantsStyle">
                        <Col md="6">
                          Provide details of the consultants you have dealt
                          with. This will help you get a better visibility and
                          better chances of winning a project.{" "}
                        </Col>
                        <Col md="6">
                          <div className="pb-1">Name of the Consultant</div>
                          <div className="pr-0">
                            <MultiSelectComponent
                              options={this.state.consultants}
                              displayValue="key"
                              showCheckbox={true}
                              onSelect={this.onSelect}
                              onRemove={this.onRemove}
                              styles={this.style}
                              closeIcon=""
                              ref={this.consultsRef}
                              //selectedValues={selectedValueForProjectLocation}
                              name="consultants"
                              singleSelect={false}
                            />
                          </div>
                        </Col>
                      </Row>

                      <Modal
                        size="med"
                        isOpen={this.state.isProjectAddToProfileOpen}
                        hide={this.hideModalForProjectProfile}
                      >
                        <Modal.Content>
                          <div className="modalMainDiv">
                            <div className="modalHeader">
                              <div className="d-flex">
                                <div className="addDocument heading">
                                  Add a Project
                                </div>

                                <div
                                  className="closeIcon"
                                  onClick={this.hideModalForProjectProfile}
                                >
                                  <IconComponent name="close" />
                                </div>
                              </div>
                            </div>

                            <div className="ml-4 mr-5 mt-5 businessDetailsModal">
                              <Row className="rowStyle">
                                <Col md="12">
                                  {this.state.projectDetails &&
                                    this.state.projectDetails.length > 0 &&
                                    this.state.projectDetails.map(
                                      (data, index) => {
                                        return (
                                          <div className="editDeleteModalDiv">
                                            <div className="d-flex">
                                              <div className="pr-3">
                                                {data.project_name}
                                              </div>
                                              <div className="d-flex w-100 justify-content-end">
                                                <div
                                                  className="detailsText"
                                                  onClick={() =>
                                                    this.onDetailsClickInProjectAddToProfile(
                                                      index
                                                    )
                                                  }
                                                >
                                                  Details
                                                </div>
                                                <div
                                                  className="ml-2"
                                                  onClick={() =>
                                                    this.onDetailsClickInProjectAddToProfile(
                                                      index
                                                    )
                                                  }
                                                >
                                                  <IconComponent name="downarrow" />
                                                </div>
                                                <div
                                                  className=" pl-4 pr-4"
                                                  onClick={() =>
                                                    this.onEditIconClickForProject(
                                                      data,
                                                      index,
                                                      data.id
                                                    )
                                                  }
                                                >
                                                  <IconComponent name="edit" />
                                                </div>{" "}
                                                <div
                                                  className="pr-4"
                                                  onClick={() =>
                                                    this.onDeleteIconClickForProject(
                                                      index
                                                    )
                                                  }
                                                >
                                                  <IconComponent name="close" />
                                                </div>
                                              </div>
                                            </div>
                                            {isOpenDetailsForProject &&
                                            index ==
                                              this.state
                                                .selectedModalDataIndexForProject ? (
                                              <div className="mt-2 ml-3">
                                                <Row className="rowStyle">
                                                  <Col md="6">
                                                    Certification Number
                                                  </Col>
                                                  <Col md="6">
                                                    Year of Publication
                                                  </Col>
                                                </Row>
                                              </div>
                                            ) : (
                                              ""
                                            )}
                                          </div>
                                        );
                                      }
                                    )}
                                </Col>
                              </Row>

                              <Row className="rowStyle">
                                <Col md="12" className="d-flex">
                                  <div>
                                    <Button
                                      outline="secondary"
                                      icon="icon"
                                      iconName={
                                        <IconComponent name="circleplusbackground" />
                                      }
                                      label="Add another project"
                                      variant="upload"
                                      onClick={this.onAddProjectuttonClick}
                                    ></Button>
                                  </div>
                                </Col>
                              </Row>
                            </div>
                          </div>
                          <div className="d-flex saveButton addToProfile">
                            <div className="saveButtonSubDiv col-4">
                              <div className="saveButtonDiv col-10">
                                {" "}
                                <Button
                                  variant="primary"
                                  label="Add To Profile"
                                  onClick={
                                    this.onProjectAddToProfileButtonClick
                                  }
                                ></Button>
                              </div>
                            </div>
                          </div>
                        </Modal.Content>
                      </Modal>
                    </div>
                  </div>
                }
              />{" "}
            </div>
            <div className="mb-3">
              <Row className="vendorRegFormRow">
                <Col md="12" lg="12" className="vendorRegFormRowSecCol d-flex">
                  <div className="mr-2">
                    <Button
                      label="Prev"
                      variant="primary"
                      onClick={this.onPreviousButtonClick}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Next"
                      className=""
                      variant="primary"
                      isDisabled={true}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Save For Later"
                      classname=""
                      variant="primary"
                      onClick={this.onSaveButtonClick}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button
                      label="Publish"
                      variant="primary"
                      classname=""
                      isDisabled={true}
                    ></Button>
                  </div>
                  <div className="mr-2">
                    <Button label="Cancel" classname="" variant="link"></Button>
                  </div>
                </Col>
              </Row>
            </div>
          </Form>
        ) : (
          ""
        )}
      </>
    );
  }
}
