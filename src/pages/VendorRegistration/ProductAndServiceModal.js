// Primary Category Modal Component
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal } from "../../components/Modal";
import { Button } from "../../components/Button";
import { Input } from "../../components/Input";
import { IconComponent } from "../../components/Icons";
import { InputGroupField } from "../../components/InputGroupField";
import { Row, Col, ListGroup, ListGroupItem } from "reactstrap";
import API from "../../api/api";
import { Popover } from "antd";

class ProductAndServiceModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secondaryCategory: "",
      id: "",
      secondaryCategoryId: "",
      selectedSubCategory: [],
      searchData: [
        // {
        //   id: 2048,
        //   name: "pebble stone mini",
        //   type: "secondary_category",
        //   primary_category: {
        //     id: 2043,
        //     name: "Aggregates and Earthworks",
        //     type: "primary_category"
        //   },
        //   secondary_category: {
        //     id: 2044,
        //     name: "Boulder",
        //     type: "secondary_category"
        //   }
        // },
        // {
        //   id: 2048,
        //   name: "pebble stone mini",
        //   type: "secondary_category",
        //   primary_category: {
        //     id: 2043,
        //     name: "Aggregates and Earthworks",
        //     type: "primary_category"
        //   },
        //   secondary_category: {
        //     id: 2044,
        //     name: "Boulder",
        //     type: "secondary_category"
        //   }
        // },
        // {
        //   id: 2048,
        //   name: "pebble stone mini",
        //   type: "secondary_category",
        //   primary_category: {
        //     id: 2043,
        //     name: "Aggregates and Earthworks",
        //     type: "primary_category"
        //   },
        //   secondary_category: {
        //     id: 2044,
        //     name: "Boulder",
        //     type: "secondary_category"
        //   }
        // },
        // {
        //   id: 2048,
        //   name: "pebble stone mini",
        //   type: "secondary_category",
        //   primary_category: {
        //     id: 2043,
        //     name: "Aggregates and Earthworks",
        //     type: "primary_category"
        //   },
        //   secondary_category: {
        //     id: 2044,
        //     name: "Boulder",
        //     type: "secondary_category"
        //   }
        // }
      ],
      visible: true,
      showSearchItems: false
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.setSearchDataRef = this.setSearchDataRef.bind(this);
  }
  hideModal = () => {
    this.props && this.props.hide && this.props.hide();
  };

  componentDidMount = () => {
    console.log("inside...");
    document.addEventListener("mousedown", this.handleClickOutside);
  };
  setSearchDataRef(node) {
    this.setSearchRef = node;
  }
  handleClickOutside(event) {
    //  alert('You clicked inside of me!' + this.wrapperRef);

    if (this.setSearchRef && this.setSearchRef.contains(event.target)) {
      // alert('outside click!');
    } else {
      this.setState({ showSearchItems: false });
    }
  }
  onPrimaryCategoryClick = (index, id) => {
    this.setState({ id: id, secondaryCategoryId: "" });
  };
  onSecondaryCategoryClick = (index, id) => {

    this.setState({ secondaryCategoryId: id });
  };
  onAddIconClick = (secondarySubCategory) => {
    //this.setState({ secondarySubCategory: secondarySubCategory })
    let temp = this.state.selectedSubCategory;
    temp.push(secondarySubCategory);
    this.setState({ selectedSubCategory: temp });
  };
  onCloseIconClick = (index) => {
    let temp = [...this.state.selectedSubCategory];
    temp.splice(index, 1);
    this.setState({ selectedSubCategory: temp });
  };
  handleVisibleChange = (visible) => {
    this.setState({ visible });
    // this.setState({ visible: true })
  };
  onChangeInput = (value) => {
    this.setState({ searchValue: value });
  };
  onSearchIconClick = () => {
    // this.setState({isProductsOpen: true });
    let params = {
      category: `%${this.state.searchValue}%`
    };

    API.fetchData(API.Query.SEARCH_PRODUCT_AND_SERVICE, params).then(
      (response) => {
        console.log(
          "search product and service response..",
          JSON.stringify(response.data)
        );
        this.setState({
          searchData: response.data,
          showSearchItems: true
        });
      }
    );
    // this.setState({isProductsOpen: true ,  })
  };
  onAddSelectionToProfileButtonClick = () => {
    this.props.addSelectionToProfileClick &&
      this.props.addSelectionToProfileClick(this.state.selectedSubCategory);
    this.hideModal();
  };
  render() {
    let { isProductsOpen, categoryList } = this.props;
    let {
      selectedSubCategory,
      searchValue,
      searchData,
      showSearchItems
    } = this.state;
    return (
      <Modal size="big" isOpen={isProductsOpen} hide={this.hideModal}>
        <Modal.Content>
          <div className="modalMainDiv">
            <div className="modalHeader">
              <div className="d-flex">
                <div className="addDocument heading pb-2">
                  Add Products and Services
                </div>

                <div className="closeIcon" onClick={this.hideModal}>
                  <IconComponent name="close" />
                </div>
              </div>
            </div>

            <div className="ml-4 mr-5 mt-2 productDiv">
              <div className="productText">
                Your services will be displayed at the top of your company
                profile and will be made visible to the buyers for immediate
                attention.
              </div>
              <div>
                <div>Your Selection </div>
                <div className="mt-2 mb-3">
                  <div className="">
                    {/* {console.log(
                      "selectedSubCategory..." +
                        JSON.stringify(selectedSubCategory)
                    )} */}
                    {selectedSubCategory &&
                      selectedSubCategory.length > 0 &&
                      selectedSubCategory.map((subcategory, index) => {
                        return (
                          <div className="tagDiv d-inline-block mr-2">
                            <span>{subcategory}</span>
                            <span
                              className="pl-4"
                              onClick={() => this.onCloseIconClick(index)}
                            >
                              <IconComponent name="close" />
                            </span>
                          </div>
                        );
                      })}
                  </div>
                </div>
              </div>

              <Row className="ml-0 mb-2 pl-0">
                <Col md="8" className="pr-0 pl-0">
                  <div className="">
                    <InputGroupField
                      addOnType="prepend"
                      iconName="search"
                      type="text"
                      name="search"
                      id="searchField"
                      placeholder="Construction"
                      handleInputChange={this.onChangeInput}
                      value={searchValue}
                      classNames="searchInput"
                      onSearchIconClick={this.onSearchIconClick}
                    />
                  </div>
                </Col>
              </Row>

              <div className="searchItemsDiv" ref={this.setSearchDataRef}>
                {searchData &&
                  showSearchItems &&
                  searchData.length > 0 &&
                  searchData.map((data) => {
                    return (
                      <div className="">
                        <div>
                          <div className="d-flex pt-2 pb-2 serachListItem">
                            {data.type == "primary_category" ? (
                              <div className="pl-3 pr-3">
                                {data.name ? data.name : ""}
                                {selectedSubCategory &&
                                selectedSubCategory.length > 0 ? (
                                  selectedSubCategory.includes(data.name) ? (
                                    <span className="pl-2">
                                      <IconComponent name="circletick" />
                                    </span>
                                  ) : (
                                    <span
                                      className="pl-2"
                                      onClick={() =>
                                        this.onAddIconClick(data.name)
                                      }
                                    >
                                      <IconComponent name="circleplus" />
                                    </span>
                                  )
                                ) : (
                                  <span
                                    className="pl-2"
                                    onClick={() =>
                                      this.onAddIconClick(data.name)
                                    }
                                  >
                                    <IconComponent name="circleplus" />
                                  </span>
                                )}
                              </div>
                            ) : data.type == "secondary_category" ? (
                              <>
                                <div className="pl-3 pr-3">
                                  {data.primary_category &&
                                    data.primary_category.name &&
                                    data.primary_category.name}
                                </div>
                                <div>
                                  <IconComponent name="lefttag" />
                                </div>
                                {data.secondary_category &&
                                data.secondary_category.name ? (
                                  <>
                                    <div className="pl-3 pr-3">
                                      {data.secondary_category.name}
                                    </div>
                                    <div>
                                      <IconComponent name="lefttag" />{" "}
                                    </div>
                                    <div className="pl-3 pr-3">
                                      {data.name}
                                      {selectedSubCategory &&
                                      selectedSubCategory.length > 0 ? (
                                        selectedSubCategory.includes(
                                          data.name
                                        ) ? (
                                          <span className="pl-2">
                                            <IconComponent name="circletick" />
                                          </span>
                                        ) : (
                                          <span
                                            className="pl-2"
                                            onClick={() =>
                                              this.onAddIconClick(data.name)
                                            }
                                          >
                                            <IconComponent name="circleplus" />
                                          </span>
                                        )
                                      ) : (
                                        <span
                                          className="pl-2"
                                          onClick={() =>
                                            this.onAddIconClick(data.name)
                                          }
                                        >
                                          <IconComponent name="circleplus" />
                                        </span>
                                      )}
                                    </div>
                                  </>
                                ) : (
                                  <div className="pl-3 pr-3">
                                    {data.name}
                                    {selectedSubCategory &&
                                    selectedSubCategory.length > 0 ? (
                                      selectedSubCategory.includes(
                                        data.name
                                      ) ? (
                                        <span className="pl-2">
                                          <IconComponent name="circletick" />
                                        </span>
                                      ) : (
                                        <span
                                          className="pl-2"
                                          onClick={() =>
                                            this.onAddIconClick(data.name)
                                          }
                                        >
                                          <IconComponent name="circleplus" />
                                        </span>
                                      )
                                    ) : (
                                      <span
                                        className="pl-2"
                                        onClick={() =>
                                          this.onAddIconClick(data.name)
                                        }
                                      >
                                        <IconComponent name="circleplus" />
                                      </span>
                                    )}
                                  </div>
                                )}
                              </>
                            ) : (
                              ""
                            )}

                            {/* // <div>
                            //   <IconComponent name="lefttag" />{" "}
                            // </div>
                            // <div className="pl-3 pr-3 ">
                            //   {data.name ? data.name : ""}
                            // </div> */}
                          </div>
                          {/* <div className="d-flex pt-2 pb-2 serachListItem">
                            <div className="pl-3 pr-3">
                              {data.primary_category &&
                                data.primary_category.name &&
                                data.primary_category.name}{" "}
                            </div>
                            <div>
                              <IconComponent name="lefttag" />{" "}
                            </div>
                            <div className="pl-3 pr-3">
                              {data.secondary_category &&
                                data.secondary_category.name &&
                                data.secondary_category.name}
                            </div>
                            <div>
                              <IconComponent name="lefttag" />{" "}
                            </div>
                            <div className="pl-3 pr-3 ">
                              {data.name ? data.name : ""}
                            </div>
                          </div> */}
                        </div>
                      </div>
                    );
                  })}

                {/* <Popover
                content={<div className="pl-3 pr-3 pb-5">
                  test
                  {searchData && searchData.length > 0 && searchData.map((data) => {
                    return <div className="d-inline-block">
                      <div>

                        <div>
                          {data.name} > {data.primary_category.name} > {data.secondary_category.name}
                          </div>
                        </div>

                    </div>

                  })}
                  
                  </div>}
                // title="Title"
                trigger="click"
                visible={this.state.visible}
                placement="bottomLeft"
                //onVisibleChange={this.handleVisibleChange}
              ></Popover> */}
              </div>

              <div>
                {console.log("categoryList..." + JSON.stringify(categoryList))}
                <Row className="listGroupRow">
                  <Col md="4">
                    <ListGroup>
                      {categoryList &&
                        categoryList.length > 0 &&
                        categoryList.map((categoryData, index) => {
                          return (
                            <div className="productCategoryMainDiv">
                              {categoryData.name && (
                                <div
                                  className=""
                                  onClick={() =>
                                    this.onPrimaryCategoryClick(
                                      index,
                                      categoryData.id
                                    )
                                  }
                                >
                                  <span className="productCategoryDiv">
                                    <ListGroupItem>
                                      {categoryData.name}
                                    </ListGroupItem>
                                  </span>
                                  <span className="categoryIconDiv">
                                    <IconComponent name="lefttag" />
                                  </span>
                                </div>
                              )}
                            </div>
                            // categoryData && cate
                          );
                        })}
                    </ListGroup>
                  </Col>
                  {this.state.id ? (
                    <div className="rightArrow">
                      <IconComponent name="rightarrow" />
                    </div>
                  ) : (
                    ""
                  )}

                  <Col md="4">
                    <ListGroup>
                      {categoryList &&
                        categoryList.length > 0 &&
                        categoryList.map((categoryData, index) => {
                          return (
                            <>
                              <div className="productCategoryMainDiv">
                                {categoryData.name &&
                                  categoryData &&
                                  categoryData.sub_categories &&
                                  categoryData.sub_categories.length > 0 &&
                                  categoryData.sub_categories.map(
                                    (secondaryCategoryData) => {
                                      // console.log(
                                      //   "this.state.secondaryCategory.." +
                                      //     this.state.secondaryCategory
                                      // );
                                      return (
                                        this.state.id ==
                                          secondaryCategoryData.categoryid && (
                                          <div
                                            className=""
                                            onClick={() =>
                                              this.onSecondaryCategoryClick(
                                                index,
                                                secondaryCategoryData.id
                                              )
                                            }
                                          >
                                            <span className="productCategoryDiv">
                                              <ListGroupItem>
                                                {secondaryCategoryData.name}
                                              </ListGroupItem>
                                            </span>
                                            <span className="categoryIconDiv">
                                              <IconComponent name="lefttag" />
                                            </span>
                                          </div>
                                        )
                                      );
                                    }
                                  )}
                              </div>
                            </>
                            // categoryData && cate
                          );
                        })}
                    </ListGroup>
                  </Col>
                  {console.log(
                    "this.state.secondaryCategoryId.." +
                      this.state.secondaryCategoryId
                  )}
                  {this.state.secondaryCategoryId ? (
                    <div className="rightArrowSubCat">
                      <IconComponent name="rightarrow" />
                    </div>
                  ) : (
                    ""
                  )}

                  <Col md="4" className="subcategoryCol">
                    <ListGroup>
                      {categoryList &&
                        categoryList.length > 0 &&
                        categoryList.map((categoryData, index) => {
                          return (
                            <div>
                              {categoryData &&
                                categoryData.sub_categories &&
                                categoryData.sub_categories.length > 0 &&
                                categoryData.sub_categories.map(
                                  (secondaryCategoryData) => {
                                    return (
                                      // this.state.id ==
                                      //   secondaryCategoryData.categoryid && (
                                      secondaryCategoryData &&
                                      secondaryCategoryData.sub_categories
                                        .length > 0 &&
                                      secondaryCategoryData.sub_categories.map(
                                        (subCategory) => {
                                          return (
                                            this.state.secondaryCategoryId ==
                                              subCategory.categoryid && (
                                              <div
                                                className=""
                                                // onClick={() =>
                                                //   this.onSecondaryCategoryClick(
                                                //     index,
                                                //     secondaryCategoryData.secondary_category
                                                //   )
                                                // }
                                              >
                                                <span className="productCategoryDiv">
                                                  <ListGroupItem>
                                                    {subCategory.name}
                                                  </ListGroupItem>
                                                </span>
                                                {selectedSubCategory &&
                                                selectedSubCategory.length >
                                                  0 ? (
                                                  selectedSubCategory.includes(
                                                    subCategory.name
                                                  ) ? (
                                                    <span className="subCategoryIconDiv">
                                                      <IconComponent name="circletick" />
                                                    </span>
                                                  ) : (
                                                    <span
                                                      className="subCategoryIconDiv"
                                                      onClick={() =>
                                                        this.onAddIconClick(
                                                          subCategory.name
                                                        )
                                                      }
                                                    >
                                                      <IconComponent name="circleplus" />
                                                    </span>
                                                  )
                                                ) : (
                                                  <span
                                                    className="subCategoryIconDiv"
                                                    onClick={() =>
                                                      this.onAddIconClick(
                                                        subCategory.name
                                                      )
                                                    }
                                                  >
                                                    <IconComponent name="circleplus" />
                                                  </span>
                                                )}
                                                {/* {selectedSubCategory && selectedSubCategory.length > 0 ? selectedSubCategory.map((subCategory) => {
                                                  return subCategory == subCategory.name &&  <span
                                                  className="categoryIconDiv"
                                                 
                                                >
                                                  <IconComponent name="circletick" />
                                                </span>  
                                                }) :
                                                <span
                                                  className="categoryIconDiv"
                                                  onClick={() =>
                                                    this.onAddIconClick(
                                                      subCategory.name
                                                    )
                                                  }
                                                >
                                                  <IconComponent name="circleplus" />
                                                </span>
                                              } */}
                                              </div>
                                            )
                                          );
                                        }
                                      )

                                      // )
                                    );
                                  }
                                )}
                            </div>
                            // categoryData && cate
                          );
                        })}
                    </ListGroup>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
          <div className="d-flex saveButton addToProfile">
            <div className="saveButtonSubDiv col-3">
              <div className="saveButtonDiv col-12">
                {" "}
                <Button
                  variant="primary"
                  label="Add selection to profile"
                  onClick={this.onAddSelectionToProfileButtonClick}
                ></Button>
              </div>
            </div>
          </div>
        </Modal.Content>
      </Modal>
    );
  }
}

export default ProductAndServiceModal;
