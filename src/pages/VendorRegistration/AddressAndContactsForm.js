import { Component } from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { LabelField } from "../../components/Label/LabelField";
import { InputField } from "../../components/InputField/Inputfield";
import { CheckBox } from "../../components/Checkbox";
import { RadioButton } from "../../components/RadioButton/RadioButton";
import { Button } from "../../components/Button";
import { Tag } from "../../components/Tag/Tag";
import CloseIcon from "@material-ui/icons/Close";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import addIcon from "../../images/addIcon.svg";
import { Image } from "../../components/Image";
import { Table, Rate } from "antd";
import edit_icon from "../../images/edit_icon.svg";
import { ModalComponent } from "../../components/Modal";

export class AddressAndContactsForm extends Component {
  state = {
    dataSource: [
      {
        id: "1",
        name: "Jane Cooper",
        jobTitle: "AManager  - IT",
        emailId: "rera@gmail.com",
        phoneNumber: "+971 00 64747575"
      },
      {
        id: "2",
        name: "Brooklyn Simmons",
        jobTitle: "Admin Operations",
        emailId: "rera@gmail.com",
        phoneNumber: "+971 00 64747575"
      }
    ],
    columns: [
      {
        title: "Name",
        dataIndex: "name",
        key: "name"
      },
      {
        title: "Job Title",
        dataIndex: "jobTitle",
        key: "jobTitle"
      },
      {
        title: "Email Id",
        dataIndex: "emailId",
        key: "emailId"
      },
      {
        title: "Phone Number",
        dataIndex: "phoneNumber",
        key: "phoneNumber"
      },
      {
        title: "",
        dataIndex: "edit",
        key: "edit",
        width: 50,
        render: () => <Image src={edit_icon} imageStyle={true} />
      }
    ],
    isModal: false,
    isContactModal: false,
    countries: ["United Arab Emirates", "country 2", "country 3"]
  };

  onButtonClick = (label) => {
    console.log("inside button..." + label);
    if (label == "Add an address") {
      this.setState({ isModal: true });
    } else if (label == "Add a Contact") {
      this.setState({ isContactModal: true });
    }
  };

  onModalClick = (isModal) => {
    this.setState({ isModal: isModal });
  };
  onContactModalClick = (isContactModal) => {
    this.setState({ isContactModal: isContactModal });
  };

  render() {
    const {
      dataSource,
      columns,
      isModal,
      countries,
      isContactModal
    } = this.state;
    console.log("is modal..." + this.state.isModal);
    return (
      <div className="formDetailsMainDiv">
        <ModalComponent
          isOpen={isModal}
          onModalClick={this.onModalClick}
          heading="Add an address"
          headerStyle="modalHeader"
          modalBody={
            <div className="">
              <div>
                <LabelField label="Address name" />
              </div>
              <div>
                <InputField
                  type="text"
                  name="address"
                  id="address"
                  placeholder=""
                  classname="inputStyle"
                />
              </div>
              <div className="exampleText">
                Example : Head office, Branch office, etc.,{" "}
              </div>
              <div className="modalDiv">
                <LabelField label="Country" />
              </div>
              <div>
                <InputField
                  type="select"
                  name="countryname"
                  id="countryname"
                  placeholder="Select"
                  classname="inputStyle"
                  option={countries}
                />
              </div>
              <div className="modalDiv">
                <LabelField label="Street Address Line 1" />
              </div>
              <div>
                <InputField
                  type="text"
                  name="street1"
                  id="street1"
                  placeholder=""
                  classname="inputStyle"
                />
              </div>
              <div className="modalDiv">
                <LabelField label="Street Address Line 2" />
              </div>
              <div>
                <InputField
                  type="text"
                  name="street2"
                  id="street2"
                  placeholder=""
                  classname="inputStyle"
                />
              </div>
              <div className="modalDiv">
                <LabelField label="City" />
              </div>
              <div>
                <InputField
                  type="text"
                  name="city"
                  id="city"
                  placeholder=""
                  classname="inputStyle"
                />
              </div>
              <div className="modalDiv">
                <LabelField label="State / Province  / Region" />
              </div>
              <div>
                <InputField
                  type="text"
                  name="state"
                  id="state"
                  placeholder=""
                  classname="inputStyle"
                />
              </div>
              <div className="modalDiv">
                <LabelField label="State / Province  / Region" />
              </div>
              <div>
                <InputField
                  type="text"
                  name="state"
                  id="state"
                  placeholder=""
                  classname="inputStyle"
                />
              </div>
            </div>
          }
        />
        <Row className="rowStyle">
          <Col md="12">
            <div className="rowCol1TextDiv">Office address</div>
            <div>
              Try to have all the addresses. Having the address that we use for
              invoice is must.
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6" className="d-flex">
            <div>
              <Button
                variant="default"
                icon="icon"
                iconName={<Image src={addIcon} imageStyle={true} />}
                label="Add an address"
                onButtonClick={this.onButtonClick}
              ></Button>
            </div>
          </Col>
        </Row>
        <Row className="rowStyle addressRowStyle">
          <Col md="4">
            <div className="headingFontStyle addressDiv">Head Office</div>
            <div className="addressDiv">Amtrex Technical Services,</div>
            <div className="addressDiv">128783,AL KARAMA,DUBAI </div>
            <div className="addressDiv">United Arab Emirates </div>
            <div className="addressDiv">Post Box No., 5747</div>
            <div className="linkButtonDiv">
              <div className="d-inline-block">
                <Button variant="linkbtn" label="Edit"></Button>
              </div>
              <span className="d-inline-block borderStyle"></span>
              <div className="d-inline-block">
                <Button variant="linkbtn" label="Remove"></Button>
              </div>
            </div>
          </Col>
          <Col md="4">
            <div className="headingFontStyle addressDiv">Branch Office</div>
            <div className="addressDiv">Amtrex Technical Services,</div>
            <div className="addressDiv">Al Batha Tower, Office 2001,</div>
            <div className="addressDiv">Sharjah</div>
            <div className="addressDiv">Post Box No., 4657</div>
            <div className="linkButtonDiv">
              <div className="d-inline-block">
                <Button variant="linkbtn" label="Edit"></Button>
              </div>
              <span className="d-inline-block borderStyle"></span>
              <div className="d-inline-block">
                <Button variant="linkbtn" label="Remove"></Button>
              </div>
            </div>
          </Col>
          <Col md="4">
            <div className="headingFontStyle addressDiv">Warehouse</div>
            <div className="addressDiv">Amtrex Technical Services,</div>
            <div className="addressDiv">Al Batha Tower, Office 2001,</div>
            <div className="addressDiv">Sharjah</div>
            <div className="addressDiv">Post Box No., 9586</div>
            <div className="linkButtonDiv">
              <div className="d-inline-block">
                <Button variant="linkbtn" label="Edit"></Button>
              </div>
              <span className="d-inline-block borderStyle"></span>
              <div className="d-inline-block">
                <Button variant="linkbtn" label="Remove"></Button>
              </div>
            </div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <ModalComponent
            isOpen={isContactModal}
            onModalClick={this.onContactModalClick}
            heading="Add a Contact"
            headerStyle="modalHeader"
            modalBody={
              <div className="">
                <div className="modalDiv">
                  <LabelField label="* Name of the person" />
                </div>
                <div>
                  <InputField
                    type="text"
                    name="name"
                    id="name"
                    placeholder="Rengarajan"
                    classname="inputStyle"
                  />
                </div>
                <div className="modalDiv">
                  <LabelField label="* Job Title" />
                </div>
                <div>
                  <InputField
                    type="text"
                    name="job"
                    id="job"
                    placeholder="Procurement Executive"
                    classname="inputStyle"
                  />
                </div>
                <div className="modalDiv">
                  <LabelField label="* Email ID" />
                </div>
                <div>
                  <InputField
                    type="email"
                    name="email"
                    id="email"
                    placeholder="renga@amtrex-uae.com"
                    classname="inputStyle"
                  />
                </div>
                <div className="modalDiv">
                  <LabelField label="* Phone" />
                </div>
                <div>
                  <InputField
                    type="text"
                    name="phone"
                    id="phone"
                    placeholder="+971 50 7645678"
                    classname="inputStyle"
                  />
                </div>
              </div>
            }
          />
          <Col md="12">
            <div className="rowCol1TextDiv">Contacts</div>
          </Col>
        </Row>
        <Row className="rowStyle">
          <Col md="6" className="d-flex">
            <div>
              <Button
                variant="default"
                icon="icon"
                iconName={<Image src={addIcon} imageStyle={true} />}
                label="Add a Contact"
                onButtonClick={this.onButtonClick}
              ></Button>
            </div>
          </Col>
        </Row>
        <Row className="tableStyle">
          <Table
            dataSource={dataSource}
            columns={columns}
            size="small"
            style={{ width: "100%" }}
            pagination={false}
          />
        </Row>
      </div>
    );
  }
}
