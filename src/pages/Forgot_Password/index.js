// ForgotPassword Page
import React, { PureComponent } from "react";
import { notification } from "antd";
import UserApi from "../../api/user";
import { withFormik } from "formik";
import * as Yup from "yup";
import { FormGroup, Form, FormFeedback, Input, Button } from "reactstrap";
import { Link } from "react-router-dom";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required!")
  }),

  mapPropsToValues: ({ user }) => ({
    ...user
  }),

  handleSubmit: (payload, { setSubmitting }) => {
    setSubmitting(false);
    UserApi.forgotPassword(payload.email)
      .then((res) => {
        notification.success({
          message: "Check Your Mail"
          // description: res.message,
        });
        console.log(res);
      })
      .catch((err) => {
        notification.warning({
          message: "EMAIL NOT FOUND",
          description: err.message
        });
        return Promise.resolve(err);
      });
  },
});

const TextInput = ({
  type,
  id,
  label,
  error,
  value,
  onChange,
  className,
  ...props
}) => {
  return (
    <div>
      <FormGroup>
        <Input
          id={id}
          type={type}
          type={type}
          value={value}
          error={error}
          onChange={onChange}
          {...props}
          invalid={!!error}
        />
        <FormFeedback invalid>{error}</FormFeedback>
      </FormGroup>
    </div>
  );
};

const MyForm = (props) => {
  const {
    values,
    errors,
    handleChange,
    handleSubmit,
    isSubmitting
  } = props;

  // console.log("form value", values);
  return (
    <Form onSubmit={handleSubmit} className="m-3 p-3">
      <div className="fgt">
        <div className="fgt-wrp">
          <div className="fgt-cnt">
            <div className="fgt-lgo heading">e-Tender</div>
            <div className="fgt-tit heading">for amtrex</div>
            <div className="fgt-nme heading">Forgot Password ?</div>
            <div className="fgt-dta">
              <div className="fgt-txt">
                Enter the email address that you used during registration. You
                will receive instructions on this email to reset your password.{" "}
              </div>
              <div className="fgt-mil">Email address</div>
              <div className="fgt-int">
                <TextInput
                  id="email"
                  type="email"
                  placeholder="email"
                  error={errors.email}
                  size="small"
                  value={values.email}
                  onChange={handleChange}
                />
              </div>
              <div className="fgt-btn">
                <Button
                  type="submit"
                  disabled={isSubmitting}
                  color="primary"
                >Submit</Button>
              </div>
              <div className="fgt-lnk">
                <Link to="/login">
                  <Button
                    color="link"
                    size="md"
                    label=""
                  >{"<<"} Back to Login</Button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Form>
  );
};

const ForgotpasswordForm = formikEnhancer(MyForm);

// Main Component
export default class ForgotPassword extends PureComponent {
  render() {
    return (
      <div>
        <ForgotpasswordForm user={{ email: "" }} />
      </div>
    );
  }
}
