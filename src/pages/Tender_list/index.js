// Tendor List
import React, { PureComponent } from "react";
import { Button } from "../../components/Button";
import { Sidebar } from "../../components/Sidebar";
import { Grid } from "../../components/Grid";
import { Card, CardTitle, CardText } from "reactstrap";
import { tenderData } from "../helpers/mock_data";
import API from "../../api/api";

// Main Component
export default class TenderList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tenderData : tenderData
    };
  }

  componentDidMount() {
    // Api to get the data
    API.fetchData(API.Query.TENDER_BY_ID, {tenderId:22}).then((data) => {
      console.log("tenders list", data);
      /* this.setState({
        tenderData: tenderData
      }) */
      this.setState({
        tenderData: data.tender
      })
    });

    // Passes id using the useparams()
    // const id = this.props.match.params.id;
    // console.log(id);
  }

  render() {
    let { tenderData } = this.state;
    return (
      <div className="ten-lst">
        <Grid>
          <Grid.Cell span={20}>
            <Sidebar />
          </Grid.Cell>
          <Grid.Cell span={80}>
            <div className="ten-hed">
              <div className="ten-hed-hdr">
                <div className="ten-hed-tit">
                  <div className="ten-hed-nme">Tenders List</div>
                </div>
                <div className="ten-hed-btn">
                  <div>
                    <Button label="Go Back" variant="link" size="big" />
                  </div>
                  <div>
                    <Button
                      label="Create Tender"
                      variant="primary"
                      size="big"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="ten-crd">
              <Card>
                <CardTitle tag="h1">5</CardTitle>
                <CardText>
                  Issued Tendors
                </CardText>
              </Card>
              <Card>
                <CardTitle tag="h1">8</CardTitle>
                <CardText>
                  Awarded Packages
                </CardText>
              </Card>
              <Card>
                <CardTitle tag="h1">1</CardTitle>
                <CardText>
                    Approved
                </CardText>
              </Card>
            </div>
            <div className="lst-vds">
                List of Vendors
            </div>
          </Grid.Cell>
        </Grid>
      </div>
    );
  }
}
