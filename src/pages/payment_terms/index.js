// Payment Terms

import React, { PureComponent } from "react";
import { Grid } from "../../components/Grid";
import { Label, Input, Button } from "reactstrap";
import { IconComponent } from "../../components/Icons";
import {
  FormBuilder,
  FieldGroup,
  FieldControl,
  Validators
} from "react-reactive-form";

const TextInput = ({ handler, touched, hasError, meta }) => (
  <div>
    <Input type="textarea" placeholder={`Enter ${meta.label}`} {...handler()} />
    <div className="val-req">
      {touched && hasError("required") && `${meta.label} is required`}
    </div>
  </div>
);

class PaymentTerm extends PureComponent {
  paymentTerms = FormBuilder.group({
    scopeWork: ["", Validators.required],
    exclution: ["", Validators.required],
    validity: ["", Validators.required],
    duration: ["", Validators.required],
    pricebasis: ["", Validators.required],
    paymentterms: ["", Validators.required],
    warranty: ["", Validators.required],
    remarks: ["", Validators.required],
    customlabel: ["", Validators.required]
  });

  handleSubmit = (e) => {
    e.preventDefault();
    console.log("Form values", this.paymentTerms.value);
  };

  render() {
    return (
      <div className="pay-trm">
        <Grid>
          <Grid.Cell span={20}>
            <div className="pay-trm-tit">Tender Terms</div>
            <div className="pay-trm-des">
              Instructions to buyers. Will also have tips and tricks about
              filling out this section with legible and good enough information.
            </div>
            <div className="pay-trm-tip">
              Tips and tricks follow..bla bla bla
            </div>
          </Grid.Cell>
          <Grid.Cell span={80}>
            <FieldGroup
              control={this.paymentTerms}
              render={({ get, invalid }) => (
                <form onSubmit={this.handleSubmit}>
                  <div className="pay-trm-scp">
                    <Label for="scope">Scope of Work</Label>
                    <FieldControl
                      name="scopeWork"
                      render={TextInput}
                      meta={{ label: "Scope of Work" }}
                    />
                  </div>
                  <div className="pay-trm-exc">
                    <Label for="exclution">Exclutions</Label>
                    <FieldControl
                      name="exclution"
                      render={TextInput}
                      meta={{ label: "exclution" }}
                    />
                  </div>
                  <div className="pay-trm-exc">
                    <Grid>
                      <Grid.Cell span={10}>
                        <Label for="validity">Validity</Label>
                        <FieldControl
                          name="validity"
                          render={TextInput}
                          meta={{ label: "Validity" }}
                        />
                      </Grid.Cell>
                      <Grid.Cell span={10}>
                        <Label for="duration">Duration</Label>
                        <FieldControl
                          name="duration"
                          render={TextInput}
                          meta={{ label: "Duration" }}
                        />
                      </Grid.Cell>
                    </Grid>
                  </div>
                  <div className="pay-trm-exc">
                    <Label for="price">Price Basis</Label>
                    <FieldControl
                      name="pricebasis"
                      render={TextInput}
                      meta={{ label: "Price Basis" }}
                    />
                  </div>
                  <div className="pay-trm-exc">
                    <Label for="payment">Payment Terms</Label>
                    <FieldControl
                      name="paymentterms"
                      render={TextInput}
                      meta={{ label: "Payment Terms" }}
                    />
                  </div>
                  <div className="pay-trm-exc">
                    <Grid>
                      <Grid.Cell span={10}>
                        <Label for="warranty">Warranty</Label>
                        <FieldControl
                          name="warranty"
                          render={TextInput}
                          meta={{ label: "Warranty" }}
                        />
                      </Grid.Cell>
                      <Grid.Cell span={10}>
                        <Label for="remarks">Remarks</Label>
                        <FieldControl
                          name="remarks"
                          render={TextInput}
                          meta={{ label: "Remarks" }}
                        />{" "}
                      </Grid.Cell>
                    </Grid>
                  </div>
                  <div className="pay-trm-exc">
                    <Label for="custom">Custom Label</Label>
                    <FieldControl
                      name="customlabel"
                      render={TextInput}
                      meta={{ label: "Custom Label" }}
                    />
                  </div>
                  <div className="pay-trm-tem">
                    <Button outline color="secondary">
                      &ensp;
                      <IconComponent
                        name="report"
                        fill="#fff"
                        width="20"
                        height="20"
                      />
                      Add another Team
                    </Button>
                  </div>
                </form>
              )}
            />
          </Grid.Cell>
        </Grid>
      </div>
    );
  }
}

export default PaymentTerm;
