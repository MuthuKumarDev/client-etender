import React, { PureComponent } from "react";
import { withFormik } from "formik";
import * as Yup from "yup";
import { FormFeedback, FormGroup, Form, Input, Label, Button } from "reactstrap";
import { AttachFile } from "../../components/AttachFile";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape({
    firstName: Yup.string()
      .min(2, "C'mon, your name is longer than that")
      .required("First name is required."),
    lastName: Yup.string()
      .min(2, "C'mon, your name is longer than that")
      .required("Last name is required."),
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required!")
  }),

  mapPropsToValues: ({ user }) => ({
    ...user
  }),
  handleSubmit: (payload, { setSubmitting }) => {
    
    // handling data on submit
    console.log('submited value',payload);
    setSubmitting(false);

    //For handling file upload api request
    var form_data = new FormData();
    for ( var key in payload ) {
      form_data.append(key, payload[key]);
    }
    console.log('form data ',form_data);
    alert(payload.email);
  },
  displayName: "MyForm"
});

const InputFeedback = ({ error }) =>
  error ? <div className="input-feedback">{error}</div> : null;

/* const Label = ({ error, className, children, ...props }) => {
  return (
    <label className="label" {...props}>
      {children}
    </label>
  );
}; */

const TextInput = ({
  type,
  id,
  label,
  error,
  value,
  onChange,
  className,
  ...props
}) => {
  /* const classes = classnames(
    "input-group",
    {
      "animated shake error": !!error
    },
    className
  ); */
  return (
    <div className={className}>
      <FormGroup>
        <Label htmlFor={id} error={error}>{label}</Label>
        <Input 
          id={id}
          className="text-input"
          type={type}
          value={value}
          onChange={onChange}
          {...props}
          invalid={!!error}
        />
        <FormFeedback invalid>{error}</FormFeedback>
      </FormGroup>
      {/* <Label htmlFor={id} error={error}>
        {label}
      </Label>
      <Input
        id={id}
        className="text-input"
        type={type}
        value={value}
        onChange={onChange}
        {...props}
      />
      <InputFeedback error={error} /> */}
    </div>
  );
};
const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
    setFieldValue
  } = props;
  return (
    <Form onSubmit={handleSubmit} className="m-3 p-3">
      <TextInput
        id="firstName"
        type="text"
        label="First Name"
        placeholder="John"
        error={touched.firstName && errors.firstName}
        value={values.firstName}
        onChange={handleChange}
        onBlur={handleBlur}
      />
      <TextInput
        id="lastName"
        type="text"
        label="Last Name"
        placeholder="Doe"
        error={touched.lastName && errors.lastName}
        value={values.lastName}
        onChange={handleChange}
        onBlur={handleBlur}
      />
      <TextInput
        id="email"
        type="email"
        label="Email"
        placeholder="Enter your email"
        error={touched.email && errors.email}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
      />
      <div>
        <Label>Profile Picture</Label>
        <AttachFile 
          type="file"
          label="Files"
          onChange={(event)=>{
            console.log(event);
            setFieldValue('attachedFiles', event, false);
          }}
          {...props}
        />
      </div>
      <Button type="submit" disabled={isSubmitting} color="primary">
        Submit
      </Button>
      <Button
        onClick={handleReset}
        disabled={!dirty || isSubmitting}
        color="secondary"
      >
        Reset
      </Button>
    </Form>
  );
};

const MyEnhancedForm = formikEnhancer(MyForm);


// Main Component
export default class FormExample extends PureComponent {
  render() {
    return <div>
      <div>User Registraion</div>
      <MyEnhancedForm user={{ email: "", firstName: "", lastName: "" }} />
    </div>;
  }
}
