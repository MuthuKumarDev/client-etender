import React, { PureComponent } from "react";
import VerifiedVendors from '../VerifiedVendors';
import UnVerifiedVendors from '../UnverifiedVendors';


// Main Component
export default class Vendors extends PureComponent {
  render() {
    return (
      <div>Vendor Screen
        {this.props.match.params.type == "verified" ? <VerifiedVendors /> : this.props.match.params.type == "un-verified" ? <UnVerifiedVendors /> : ''}
      </div>
    )
  }
}
