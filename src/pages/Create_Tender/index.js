// Create New Tender

import React, { PureComponent } from "react";
import { withFormik, Field } from "formik";
import * as Yup from "yup";
import { FormFeedback, FormGroup, Form } from "reactstrap";
import { Grid } from "../../components/Grid";
// import { Input } from "../../components/Input";
//import { Button } from "../../components/Button";
import { Label } from "reactstrap";
import { Button, Input } from "reactstrap";
import { IconComponent } from "../../components/Icons";
import Select from "react-select";
import { DatepickerComponent } from "../../components/DatePicker";
import { TextEditer } from "../../components/TextEditer";
import API from "../../api/api";
import { withRouter } from "react-router";
import UnitTender from "../Unit_tender";
import { notification } from "antd";
import { Link } from "react-router-dom";
import { FileUpload } from "../../components/FileUpload";

const clonedeep = require("lodash.clonedeep");

import {
  projectStatusLists,
  projectLocationLists,
  tenderTypeLists,
  businessTypeLists,
  serviceCategoryLists,
  SingleContactList,
  projectValueLists
} from "../helpers/mock_data";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape({
    project_name: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Project Name is required."),
    tender_title: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Tender Title is required."),
    validity: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("validity is required."),
    tender_value: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .nullable(),
    scope_work: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Scope of Work is required."),
    exclution: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Exclusions is required."),
    delivery: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Delivery is required."),
    price_basis: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Price Basis is required."),
    payment_terms: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Payment Terms is required."),
    warranty: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Warranty is required."),
    remarks: Yup.string()
      .min(1, "Trailing spaces not allowed")
      .trim("The contact name cannot include leading and trailing spaces")
      .required("Remarks is required."),
    project_location: Yup.object()
      .required("Project location is required.")
      .nullable(),
    project_value: Yup.object().required("Project location is required.")
  }),

  mapPropsToValues: ({ user }) => ({
    ...user
  }),
  displayName: "MyForm"
});

const TextInput = ({
  type,
  id,
  label,
  error,
  size,
  value,
  onChange,
  className,
  ...props
}) => {
  return (
    <div className={className}>
      <FormGroup>
        <Input
          id={id}
          type={type}
          size={size}
          value={value}
          onChange={onChange}
          {...props}
          invalid={!!error}
        />
        <FormFeedback invalid>{error}</FormFeedback>
      </FormGroup>
    </div>
  );
};

const InputFile = ({ accept, onFiles, files, getFilesFromEvent }) => {
  let text = "";
  if (files.length > 0) {
    text = "Add more files";
  } else {
    text = "Choose files";
  }
  // let  text = files.length > 0 ? 'Add more files' : 'Choose files';​
  return (
    <label
      style={{
        backgroundColor: "#007bff",
        color: "#fff",
        cursor: "pointer",
        padding: 15,
        borderRadius: 3
      }}
    >
      {text}
      <input
        style={{ display: "none" }}
        type="file"
        accept={accept}
        multiple
        onChange={(e) => {
          getFilesFromEvent(e).then((chosenFiles) => {
            onFiles(chosenFiles);
          });
        }}
        onClick={handleClickInput}
      />
    </label>
  );
};

const TextEdit = ({ id, label, value, onChange, className, ...props }) => {
  return (
    <div>
      <FormGroup>
        <TextEditer id={id} value={value} onChange={onChange} />
      </FormGroup>
    </div>
  );
};

const SelectText = ({
  id,
  options,
  placeholder,
  label,
  error,
  value,
  onChange,
  className,
  ...props
}) => {
  return (
    <div>
      <FormGroup>
        <Select
          id={id}
          options={options}
          value={value}
          onChange={onChange}
          placeholder={placeholder}
          {...props}
        />
        <FormFeedback invalid>{error}</FormFeedback>
      </FormGroup>
    </div>
  );
};

class MyForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tender: null,
      tenderId: 0,
      selected: new Date(),
      textValue: "",
      isOpen: false,
      value: "",
      tenderData: null,
      fileLists: [],
      ComponentData: null,
      Idtender: "",
      uploadS3: [],
      selectedFile: []
    };
  }

  InputFile = ({ accept, onFiles, files, getFilesFromEvent }) => {
    let text = "";
    if (files.length > 0) {
      text = "Add more files";
    } else {
      text = "Choose files";
    }
    // let  text = files.length > 0 ? 'Add more files' : 'Choose files';​
    return (
      <label
        style={{
          backgroundColor: "#007bff",
          color: "#fff",
          cursor: "pointer",
          padding: 15,
          borderRadius: 3
        }}
      >
        {text}
        <input
          style={{ display: "none" }}
          type="file"
          accept={accept}
          multiple
          onChange={(e) => {
            getFilesFromEvent(e).then((chosenFiles) => {
              onFiles(chosenFiles);
            });
          }}
          onClick={this.handleClickInput}
        />
      </label>
    );
  };

  handleClickInput = () => {
    console.log("file Upload", this.state.Idtender);
    console.log("save values", this.props.values);
    let saveData = this.props.values;
    saveData.tender_id = parseInt("007");
    saveData.tender_value = parseInt(saveData.tender_value);
    saveData.project_id = parseInt(saveData.project_id);
    saveData.project_value =
      saveData.project_value && saveData.project_value.label;
    saveData.project_status =
      saveData.project_status && saveData.project_status.label;
    saveData.project_location =
      saveData.project_location && saveData.project_location.label;
    saveData.tender_type = saveData.tender_type && saveData.tender_type.label;
    saveData.service_category =
      saveData.service_category && saveData.service_category.label;
    saveData.singleContact =
      saveData.singleContact && saveData.singleContact.label;
    saveData.business_type =
      saveData.business_type && saveData.business_type.label;
    saveData.description = saveData.description;
    saveData.deadline = this.state.selected;
    saveData.contract_period = "contract_period";
    saveData.status = "drafts";
    this.setState({ values: saveData });
    if (
      saveData.project_name !== "" &&
      saveData.tender_title !== "" &&
      saveData.project_location !== "" &&
      this.state.Idtender === ""
    ) {
      API.fetchData(API.Query.CREATE_TENDER, { data: saveData }).then(
        ({ data }) => {
          // console.log("tender data updated", data);
          if (data) {
            let { id } = data;
            this.setState({ Idtender: id });
            console.log("id", id);
            // if (id) {
            //   this.props.history.push(`/tenders/${id}`);
            // }
          }
        }
      );
      console.log("save", this.state.Idtender);
      console.log("save Values", this.props.values);
    }
    // console.log("file Uploadafer", this.state.Idtender);
  };

  handleSave = (payload, files, allFiles) => {
    console.log("data payload", payload);
    let tenderid = this.props.match.params.tenderId;
    let { tender, fileLists, uploadS3, tenderId } = this.state;
    let data = payload;
    const cloneData = clonedeep(payload);
    console.log("data", data);
    console.log("data value", cloneData);
    console.log("inside savae file", this.state.uploadS3);
    let TempUpload = uploadS3;
    TempUpload.map((file) => {
      file.tenderid = tenderId;
    });
    this.setState({ uploadS3: TempUpload });
    console.log("save file", uploadS3);
    API.fetchData("create_technicalfile_upload", {
      data: uploadS3
    }).then(({ data }) => {
      console.log("create Worklist", data);
    });
    if (tenderid) {
      tender.tender_id = parseInt(data.tender_id);
      tender.tender_value = parseInt(data.tender_value);
      tender.project_id = parseInt(data.project_id);
      tender.project_name = data.project_name;
      tender.tender_title = data.tender_title;
      tender.scope_work = data.scope_work;
      tender.exclution = data.exclution;
      tender.validity = data.validity;
      tender.delivery = data.delivery;
      tender.price_basis = data.price_basis;
      tender.payment_terms = data.payment_terms;
      tender.warranty = data.warranty;
      tender.remarks = data.remarks;
      tender.custom_level = data.custom_level;
      tender.project_status = data.project_status && data.project_status.label;
      tender.project_value = data.project_value && data.project_value.label;
      tender.project_location =
        data.project_location && data.project_location.label;
      tender.tender_type = data.tender_type && data.tender_type.label;
      tender.service_category =
        data.service_category && data.service_category.label;
      tender.singleContact = data.singleContact && data.singleContact.label;
      tender.business_type = data.business_type && data.business_type.label;
      tender.description = this.state.textValue;
      tender.performanceBond = data.performanceBond;
      tender.lockedBox = data.lockedBox;
      tender.closeDate = data.closeDate;
      tender.addpreference = data.addpreference;
      tender.partialBinds = data.partialBinds;
      tender.consultant = "consultant";
      tender.deadline = this.state.selected;
      tender.contract_period = "contract_period";
      tender.status = "drafts";
      // tender.technicalDocuments= formData;
      (tender.contact_person = "Jack"),
        (tender.issue_date = "2021-02-02T12:42:52.469817+00:00"),
        (tender.email = "example@123.com"),
        (tender.mobile = 9944226688),
        (tender.client = "client"),
        (tender.nature = "nature"),
        (tender.sector = "sector"),
        (tender.set_payment_terms = "payment terms"),
        (tender.type_of_contract = "Type of Contract"),
        (tender.closing_date = "2021-02-02T12:42:52.469817+00:00");
    } else {
      data.tender_id = parseInt(this.state.tenderId);
      data.tender_value = parseInt(data.tender_value);
      data.project_id = parseInt(data.project_id);
      data.project_value = data.project_value && data.project_value;
      data.project_status = data.project_status && data.project_status;
      data.project_location = data.project_location && data.project_location;
      data.tender_type = data.tender_type && data.tender_type;
      data.service_category = data.service_category && data.service_category;
      data.singleContact = data.singleContact && data.singleContact;
      data.business_type = data.business_type && data.business_type;
      data.validity = data.validity;
      data.scope_work = data.scope_work;
      data.delivery = data.delivery;
      data.price_basis = data.price_basis;
      data.payment_terms = data.payment_terms;
      data.remarks = data.remarks;
      data.warranty = data.warranty;
      data.exclution = data.exclution;
      data.description = data.description;
      data.deadline = this.state.selected;
      data.contract_period = "contract_period";
      data.status = "drafts";
    }
    if (
      data.project_name === "" ||
      data.tender_title === "" ||
      data.project_location === undefined ||
      data.validity === "" ||
      data.scope_work === "" ||
      data.delivery === "" ||
      data.price_basis === "" ||
      data.payment_terms === "" ||
      data.warranty === "" ||
      data.remarks === "" ||
      data.exclution === ""
    ) {
      console.log("invalid");
      notification.error({
        message: "Requirement Needed"
      });
    } else {
      if (tenderid) {
        API.fetchData(API.Query.UPDATE_TENDER, {
          tenderid: tenderid,
          data: {
            addpreference: tender.addpreference,
            business_type: tender.business_type,
            client: tender.client,
            closeDate: tender.closeDate,
            closing_date: tender.closing_date,
            consultant: tender.consultant,
            contact_person: tender.contact_person,
            custom_level: tender.custom_level,
            contract_period: tender.contract_period,
            deadline: tender.deadline,
            delivery: tender.delivery,
            description: tender.description,
            email: tender.email,
            exclution: tender.exclution,
            issue_date: tender.issue_date,
            lockedBox: tender.lockedBox,
            mobile: tender.mobile,
            nature: tender.nature,
            partialBinds: tender.partialBinds,
            payment_terms: tender.payment_terms,
            performanceBond: tender.performanceBond,
            price_basis: tender.price_basis,
            project_id: tender.project_id,
            project_location: tender.project_location,
            project_name: tender.project_name,
            project_status: tender.project_status,
            project_value: tender.project_value,
            remarks: tender.remarks,
            scope_work: tender.scope_work,
            sector: tender.sector,
            service_category: tender.service_category,
            set_payment_terms: tender.set_payment_terms,
            singleContact: tender.singleContact,
            status: tender.status,
            tender_id: tender.tender_id,
            tender_title: tender.tender_title,
            tender_type: tender.tender_type,
            tender_value: tender.tender_value,
            type_of_contract: tender.type_of_contract,
            updated_at: tender.updated_at,
            warranty: tender.warranty,
            validity: tender.validity
          }
        })
          .then(() => {
            notification.success({
              message: "Updated Successfully"
            });
            this.props.history.push(`/tenders/${tenderId}`);
          })
          .catch((err) => {
            console.log(err.message);
          });
        console.log("update tender inside", tender);
      } else {
        cloneData.tender_id = parseInt(this.state.tenderid);
        cloneData.tender_value = parseInt(data.tender_value);
        cloneData.project_id = parseInt(data.project_id);
        cloneData.project_value =
          data.project_value && data.project_value.label;
        cloneData.project_status =
          data.project_status && data.project_status.label;
        cloneData.project_location =
          data.project_location && data.project_location.label;
        cloneData.tender_type = data.tender_type && data.tender_type.label;
        cloneData.service_category =
          data.service_category && data.service_category.label;
        cloneData.singleContact =
          data.singleContact && data.singleContact.label;
        cloneData.business_type =
          data.business_type && data.business_type.label;
        cloneData.description = data.description;
        cloneData.deadline = this.state.selected;
        cloneData.contract_period = "contract_period";
        cloneData.status = "drafts";
        (cloneData.contact_person =
          data.singleContact && data.singleContact.label),
          (cloneData.issue_date = "2021-02-02T12:42:52.469817+00:00"),
          (cloneData.email = "example@123.com"),
          (cloneData.mobile = 9944226688),
          (cloneData.client = "client"),
          (cloneData.nature = "nature"),
          (cloneData.sector = "sector"),
          (cloneData.set_payment_terms = "payment terms"),
          (cloneData.type_of_contract = "Type of Contract"),
          (cloneData.closing_date = "2021-02-02T12:42:52.469817+00:00");
        // API.fetchformData("create_technicalfile_upload", data)
        API.fetchData(API.Query.UPDATE_TENDER, {
          tenderid: tenderId,
          data: {
            addpreference: cloneData.addpreference,
            business_type: cloneData.business_type,
            client: cloneData.client,
            closeDate: cloneData.closeDate,
            closing_date: cloneData.closing_date,
            consultant: cloneData.consultant,
            contact_person: cloneData.contact_person,
            custom_level: cloneData.custom_level,
            contract_period: cloneData.contract_period,
            deadline: cloneData.deadline,
            delivery: cloneData.delivery,
            description: cloneData.description,
            email: cloneData.email,
            exclution: cloneData.exclution,
            issue_date: cloneData.issue_date,
            lockedBox: cloneData.lockedBox,
            mobile: cloneData.mobile,
            nature: cloneData.nature,
            partialBinds: cloneData.partialBinds,
            payment_terms: cloneData.payment_terms,
            performanceBond: cloneData.performanceBond,
            price_basis: cloneData.price_basis,
            project_id: cloneData.project_id,
            project_location: cloneData.project_location,
            project_name: cloneData.project_name,
            project_status: cloneData.project_status,
            project_value: cloneData.project_value,
            remarks: cloneData.remarks,
            scope_work: cloneData.scope_work,
            sector: cloneData.sector,
            service_category: cloneData.service_category,
            set_payment_terms: cloneData.set_payment_terms,
            singleContact: cloneData.singleContact,
            status: cloneData.status,
            tender_id: cloneData.tender_id,
            tender_title: cloneData.tender_title,
            tender_type: cloneData.tender_type,
            tender_value: cloneData.tender_value,
            type_of_contract: cloneData.type_of_contract,
            updated_at: cloneData.updated_at,
            warranty: cloneData.warranty,
            validity: cloneData.validity
          }
        }).then(({ data }) => {
          // console.log("tender data updated", data);
          if (data) {
            // let { id } = data;
            // console.log("id", id);
            // this.setState({ tenderId: id });
            console.log("submit", this.state.tenderId);
            // if (id) {
            this.props.history.push(`/tenders/${tenderId}`);
            // }
          }
        });
        console.log("Tender id to be submit", this.state.tenderId);
        console.log("Tender data to be submit", data);
      }
    }
    // setSubmitting(false);
  };

  componentDidMount = () => {
    let { tenderId } = this.state;
    let tenderSummaryData = this.props.location;
    let tenderData = tenderSummaryData;
    const tenderid = this.props.match.params.tenderId;
    console.log("tenderSummaryData", tenderData.tender);
    console.log("tenderSummaryData id", tenderid);

    API.fetchData(API.Query.GENERATE_TENDER_ID).then((dataId) => {
      console.log("tenderID", dataId.tenderid);
    });
    if (tenderid && tenderData.tender === undefined) {
      API.fetchData(API.Query.TENDER_BY_ID, { tenderid }).then((data) => {
        console.log("tenders list", data);
        let { tender } = data;
        this.setState(
          {
            tender,
            tender_id: tender && tender.id,
            tenderId: tender && tender.id,
            textValue: tender && tender.description,
            selected: new Date(tender && tender.deadline),
            selectedFile: tender && tender.technical_files
          },
          () => {
            this.getTenderValue();
          }
        );
        console.log("tender after tender  with id", tender);
      });
    } else if (tenderid && tenderData) {
      let { tender } = tenderData;
      console.log("data while editing", tender.tender);
      if (tender.tender.tender_id === null) {
        this.setState({ tenderId: tender.tender.id });
      } else {
        this.setState({ tenderId: tender.tender.tender_id });
      }
      let tendeSummaryvalue = tender;
      this.setState(
        {
          tender,
          textValue:
            (tendeSummaryvalue && tendeSummaryvalue.tender.description) || "",
          selected: new Date(
            tendeSummaryvalue && tendeSummaryvalue.tender.deadline
          ),
          selectedFile:
            tendeSummaryvalue && tendeSummaryvalue.tender.technical_files
          // tenderId: tendeSummaryvalue && tendeSummaryvalue.tender.id
        },
        () => {
          this.getTenderEditvalue();
        }
      );
      console.log("tender after tender summary", tender);
    } else if (tenderData) {
      let { tender } = tenderData;
      console.log("tender without id", tender && tender.tender.tender_id);
      this.setState({ tenderId: tender && tender.tender.tender_id });
      let tendeSummaryvalue = tender;
      this.setState(
        {
          tender,
          textValue:
            (tendeSummaryvalue && tendeSummaryvalue.tender.description) || "",
          selected: new Date(
            tendeSummaryvalue && tendeSummaryvalue.tender.deadline
          ),
          selectedFile:
            tendeSummaryvalue && tendeSummaryvalue.tender.technical_files
        },
        () => {
          this.getTenderEditvalue();
        }
      );
      console.log("tender after tender summary without id", tender);
    }
  };

  componentDidUpdate(prevProps, prevState) {
    console.log("did update", this.state.selectedFile);
    if (prevState.selectedFile != this.state.selectedFile) {
      this.setState({ selectedFile: this.state.selectedFile });
    }
  }

  getTenderValue() {
    let { tender } = this.state;
    let { values } = this.props;
    if (tender) {
      values.project_name = tender.project_name;
      values.project_id = tender.project_id;
      values.tender_title = tender.tender_title;
      values.tender_value = tender.tender_value;
      values.project_status = { label: tender.project_status };
      values.project_location = { label: tender.project_location };
      values.business_type = { label: tender.business_type };
      values.project_value = { label: tender.project_value };
      values.service_category = { label: tender.service_category };
      values.tender_type = { label: tender.tender_type };
      values.scope_work = tender.scope_work;
      values.exclution = tender.exclution;
      values.validity = tender.validity;
      values.delivery = tender.delivery;
      values.price_basis = tender.price_basis;
      values.payment_terms = tender.payment_terms;
      values.warranty = tender.warranty;
      values.remarks = tender.remarks;
      values.description = tender.description;
      values.deadline = tender.deadline;
      values.singleContact = { label: tender.singleContact };
      values.custom_level = tender.custom_level;
      values.performanceBond = tender.performanceBond;
      values.lockedBox = tender.lockedBox;
      values.closeDate = tender.closeDate;
      values.addpreference = tender.addpreference;
      values.partialBinds = tender.partialBinds;
      values.technical_files = tender.technical_files;
      values.tender_id = parseInt(this.props.match.params.tenderId);
      this.setState({ values });
      console.log("edit value id", values);
    }
  }

  getTenderEditvalue() {
    let { tender } = this.state;
    let { values } = this.props;
    if (tender) {
      values.project_name = tender.tender.project_name;
      values.tender_id = parseInt(this.state.tenderId);
      values.tender_title = tender.tender.tender_title;
      values.tender_id = tender.tender.tender_id;
      values.project_id = tender.tender.project_id;
      values.tender_value = tender.tender.tender_value;
      values.project_status = { label: tender.tender.project_status };
      values.project_location = { label: tender.tender.project_location };
      values.business_type = { label: tender.tender.business_type };
      values.project_value = { label: tender.tender.project_value };
      values.service_category = { label: tender.tender.service_category };
      values.tender_type = { label: tender.tender.tender_type };
      values.business_type = { label: tender.tender.business_type };
      values.singleContact = { label: tender.tender.singleContact };
      values.scope_work = tender.tender.scope_work;
      values.exclution = tender.tender.exclution;
      values.validity = tender.tender.validity;
      values.delivery = tender.tender.delivery;
      values.price_basis = tender.tender.price_basis;
      values.payment_terms = tender.tender.payment_terms;
      values.warranty = tender.tender.warranty;
      values.remarks = tender.tender.remarks;
      values.custom_level = tender.tender.custom_level;
      values.description = tender.tender.description;
      values.deadline = tender.tender.deadline;
      values.performanceBond = tender.tender.performanceBond;
      values.lockedBox = tender.tender.lockedBox;
      values.closeDate = tender.tender.closeDate;
      values.addpreference = tender.tender.addpreference;
      values.partialBinds = tender.tender.partialBinds;
      values.technical_files = tender.tender.technical_files;
      this.setState({ values });
      console.log("tender summary values", values);
    }
  }

  handleClick = (values) => {
    let { uploadS3, tenderId } = this.state;
    let TempUpload = uploadS3;
    TempUpload.map((file) => {
      file.tenderid = tenderId;
    });
    this.setState({ uploadS3: TempUpload });
    console.log("save file", uploadS3);
    API.fetchData("create_technicalfile_upload", {
      data: uploadS3
    }).then(({ data }) => {
      console.log("create Worklist", data);
    });
    let data = values;
    const cloneData = clonedeep(values);
    console.log("data", data);
    console.log("data value", cloneData);
    data.tender_id = parseInt(this.state.tenderId);
    data.tender_value = parseInt(data.tender_value);
    data.project_id = parseInt(data.project_id);
    data.project_status = data.project_status && data.project_status;
    data.project_value = data.project_value && data.project_value;
    data.project_location = data.project_location && data.project_location;
    data.tender_type = data.tender_type && data.tender_type;
    data.service_category = data.service_category && data.service_category;
    data.singleContact = data.singleContact && data.singleContact;
    data.business_type = data.business_type && data.business_type;
    data.validity = data.validity;
    data.scope_work = data.scope_work;
    data.delivery = data.delivery;
    data.price_basis = data.price_basis;
    data.payment_terms = data.payment_terms;
    data.remarks = data.remarks;
    data.warranty = data.warranty;
    data.exclution = data.exclution;
    data.description = data.description;
    data.deadline = this.state.selected;
    data.contract_period = "contract_period";
    data.status = "drafts";
    data.technical_files = this.state.uploadS3;
    if (
      data.project_name === "" ||
      data.tender_title === "" ||
      data.project_location === undefined ||
      data.validity === "" ||
      data.scope_work === "" ||
      data.delivery === "" ||
      data.price_basis === "" ||
      data.payment_terms === "" ||
      data.warranty === "" ||
      data.remarks === "" ||
      data.exclution === ""
    ) {
      console.log("invalid");
      notification.error({
        message: "Requirement Needed"
      });
    } else {
      let tenderid = this.props.match.params.tenderId;
      console.log("prams id", tenderid);
      cloneData.tender_id = parseInt(this.state.tenderId);
      cloneData.tender_value = parseInt(data.tender_value);
      cloneData.project_id = parseInt(data.project_id);
      cloneData.project_status =
        data.project_status && data.project_status.label;
      cloneData.project_value = data.project_value && data.project_value.label;
      cloneData.project_location =
        data.project_location && data.project_location.label;
      cloneData.tender_type = data.tender_type && data.tender_type.label;
      cloneData.service_category =
        data.service_category && data.service_category.label;
      cloneData.singleContact = data.singleContact && data.singleContact.label;
      cloneData.business_type = data.business_type && data.business_type.label;
      cloneData.validity = data.validity;
      cloneData.scope_work = data.scope_work;
      cloneData.delivery = data.delivery;
      cloneData.price_basis = data.price_basis;
      cloneData.payment_terms = data.payment_terms;
      cloneData.remarks = data.remarks;
      cloneData.warranty = data.warranty;
      cloneData.exclution = data.exclution;
      cloneData.description = data.description;
      cloneData.deadline = this.state.selected;
      cloneData.contract_period = "contract_period";
      cloneData.status = "drafts";
      cloneData.technical_files = this.state.uploadS3;
      console.log("view Summary data", data);
      const { history } = this.props;
      if (tenderid) {
        console.log("view Summary data", data);
        history.push({
          pathname: `/tenders/${tenderid}`,
          tender: cloneData
        });
      } else {
        history.push({
          pathname: "/tenders/view",
          tender: cloneData
        });
      }
    }
  };

  onChangeDate = (selected) => {
    this.setState({ selected });
  };

  modalHandleClick = () => {
    this.setState({ isOpen: true });
  };

  handleHide = () => {
    this.setState({ isOpen: false });
  };

  handleFileChange = (files, uploadData) => {
    let { fileLists, uploadS3 } = this.state;
    for (let i = 0; i < files.length; i++) {
      console.log("fileName inside handle change", files[i].name);
      fileLists.push(files[i]);
    }
    // uploadS3.push(uploadData);
    this.setState({ uploadS3: uploadData });
    this.setState(fileLists);
  };

  getFilesFromEvent = (e) => {
    return new Promise((resolve) => {
      getDroppedOrSelectedFiles(e).then((chosenFiles) => {
        resolve(chosenFiles.map((f) => f.fileObject));
      });
    });
  };

  getTenderId(values) {
    let data = values;
    const Savedata = clonedeep(values);
    console.log("values when click", values);
    if (
      data.project_name === "" ||
      data.tender_title === "" ||
      data.project_location === undefined ||
      data.validity === "" ||
      data.scope_work === "" ||
      data.delivery === "" ||
      data.price_basis === "" ||
      data.payment_terms === "" ||
      data.warranty === "" ||
      data.remarks === "" ||
      data.exclution === ""
    ) {
      console.log("error");
    } else if (this.state.tenderId === undefined) {
      Savedata.tender_id = parseInt(this.state.tenderid);
      Savedata.tender_value = parseInt(data.tender_value);
      Savedata.project_id = parseInt(data.project_id);
      Savedata.project_value = data.project_value && data.project_value.label;
      Savedata.project_status =
        data.project_status && data.project_status.label;
      Savedata.project_location =
        data.project_location && data.project_location.label;
      Savedata.tender_type = data.tender_type && data.tender_type.label;
      Savedata.service_category =
        data.service_category && data.service_category.label;
      Savedata.singleContact = data.singleContact && data.singleContact.label;
      Savedata.business_type = data.business_type && data.business_type.label;
      Savedata.description = data.description;
      Savedata.deadline = this.state.selected;
      Savedata.contract_period = "contract_period";
      Savedata.status = "drafts";
      API.fetchData(API.Query.CREATE_TENDER, { data: Savedata }).then(
        ({ data }) => {
          // console.log("tender data updated", data);
          if (data) {
            let { id } = data;
            console.log("id", id);
            this.setState({ tenderId: id });
            console.log("submit", this.state.tenderId);
            // if (id) {
            //   this.props.history.push(`/tenders/${id}`);
            // }
          }
        }
      );
    }
  }

  handleDelete = (id, index) => {
    console.log("Selected delete file", index);
    let deleteFile = [...this.state.selectedFile];
    deleteFile.splice(index, 1);
    console.log("select delete new variable", deleteFile);
    // let { selectedFile } = this.state;
    // selectedFile.splice(index, 1);
    this.setState({ selectedFile: deleteFile });
    API.fetchData("delete_technicalfiles", {
      id: id
    }).then(({ data }) => {
      console.log("delete api file", data);
    });
    console.log("delete", this.state.selectedFile);
  };

  render() {
    const {
      values,
      touched,
      errors,
      dirty,
      handleChange,
      handleBlur,
      handleSubmit,
      handleReset,
      isSubmitting,
      setFieldValue,
      setValue
    } = this.props;

    let { tenderId, fileLists, uploadS3, selectedFile } = this.state;
    console.log("file Uploadafer", this.state.tenderId);

    console.log("file inside render", fileLists);
    console.log("upload files", this.state.uploadS3);
    // console.log("upload save", this.state.uploadS3.length);

    let tenderValue =
      (values.tender_type && values.tender_type.label) ||
      (values.tender_type && values.tender_type);
    let tenderLocation = values.project_location && values.project_location;
    console.log("type value", tenderValue);
    console.log("type location", tenderLocation);
    // console.log("values", values);
    console.log("delete selected file", this.state.selectedFile);
    console.log("values", values);

    return (
      <Form onSubmit={handleSubmit}>
        <div>
          <div className="crt-ten">
            <Grid>
              <Grid.Cell>
                <div className="crt-ten-cnt">
                  <div className="smr-hdr p-2">
                    <div className="smr-tit">
                      <div className="crt-hdr-nme">
                        {tenderId ? "Edit " : "Create "}Tender
                      </div>
                    </div>
                    <div className="smr-btn">
                      <div>
                        <Link to="/tenders">
                          <Button color="discard" size="sm" className="mr-2">
                            Discard
                          </Button>
                        </Link>
                      </div>
                      <div>
                        <Button
                          color="primary"
                          type="sumbit"
                          size="sm"
                          className="mr-2"
                          onClick={() => {
                            this.handleSave(values);
                          }}
                        >
                          Save
                        </Button>
                      </div>
                      <div>
                        <Button
                          color="primary"
                          type="sumbit"
                          size="sm"
                          className="mr-2"
                          onClick={() => {
                            this.handleClick(values);
                          }}
                        >
                          View Summary
                        </Button>
                      </div>
                    </div>
                  </div>
                  <div className="ten-pge">
                    <div className="ten-tit">Tender details</div>
                    <Grid break="lg">
                      <Grid.Cell span={8}>
                        <div className="cell-tit">*&emsp;Project Name</div>
                        <TextInput
                          id="project_name"
                          type="text"
                          placeholder="Project 1, Project 2, Project 3"
                          error={touched.project_name && errors.project_name}
                          value={values.project_name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </Grid.Cell>
                      <Grid.Cell span={6}>
                        <div className="cell-tit">Project Status</div>
                        <SelectText
                          id="project_status"
                          label="project_status"
                          placeholder="Select Project Status"
                          value={values.project_status}
                          onChange={(event) => {
                            setFieldValue("project_status", event);
                          }}
                          options={projectStatusLists}
                          onBlur={handleBlur}
                        />
                      </Grid.Cell>
                      <Grid.Cell span={4}>
                        <div className="cell-tit">Project Number</div>
                        <TextInput
                          id="project_id"
                          type="number"
                          error={touched.project_id && errors.project_id}
                          size="medium"
                          value={values.project_id}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </Grid.Cell>
                      <Grid.Cell span={7}>
                        <div className="cell-tit">Project Value</div>
                        <SelectText
                          id="project_value"
                          label="project_value"
                          placeholder="Select Project Value"
                          value={values.project_value}
                          onChange={(event) => {
                            setFieldValue("project_value", event);
                          }}
                          onBlur={handleBlur}
                          options={projectValueLists}
                          onBlur={handleBlur}
                        />
                      </Grid.Cell>
                      <Grid.Cell span={3}>
                        <div className="cell-tit">*&emsp;Project Location</div>
                        <SelectText
                          id="project_location"
                          label="project_location"
                          placeholder="Select Project Location"
                          value={values.project_location || tenderLocation}
                          onChange={(event) => {
                            setFieldValue("project_location", event);
                          }}
                          onBlur={handleBlur}
                          options={projectLocationLists}
                          onBlur={handleBlur}
                        />
                        <span className="lcn-err">
                          {touched.project_location && errors.project_location}
                        </span>
                      </Grid.Cell>
                    </Grid>
                    <Grid break="lg">
                      <Grid.Cell span={40}>
                        <Grid break="lg">
                          <Grid.Cell span={20}>
                            <div className="cell-tit">*&emsp;Tender Title</div>
                            <TextInput
                              id="tender_title"
                              type="text"
                              placeholder="/Project/tender title"
                              error={
                                touched.tender_title && errors.tender_title
                              }
                              size="medium"
                              value={values.tender_title}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </Grid.Cell>
                          <Grid.Cell span={10}>
                            <div className="cell-tit">Tender Value</div>
                            <TextInput
                              id="tender_value"
                              type="text"
                              placeholder="tender value"
                              error={
                                touched.tender_value && errors.tender_value
                              }
                              size="medium"
                              value={values.tender_value}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </Grid.Cell>
                          {/* <Grid.Cell span={10}>
                            <div className="cell-tit">*&emsp;Tender ID</div>
                            <TextInput
                              id="tender_id"
                              type="text"
                              placeholder="747565758 AQS"
                              error={errors.tender_id}
                              size="medium"
                              value={values.tender_id}
                              onChange={(value) => {
                                setFieldValue("tender_id", value);
                              }}
                            />
                          </Grid.Cell> */}
                        </Grid>
                        <Grid>
                          <Grid.Cell span={6}>
                            <div className="cell-tit">Tender Description</div>
                            <div className="edit-box">
                              <TextEdit
                                id="textEdit"
                                label="Text Description"
                                value={values.description || ""}
                                onChange={(event) => {
                                  setFieldValue("description", event);
                                }}
                              />
                            </div>
                          </Grid.Cell>
                        </Grid>
                      </Grid.Cell>
                    </Grid>
                    <Grid break="lg">
                      <Grid.Cell span={100}>
                        <Grid break="lg">
                          <Grid.Cell span={20}>
                            <div className="cell-tit">Tender Type</div>
                            <SelectText
                              id="tender_type"
                              label="tender_type"
                              placeholder="Select Tender Type"
                              value={values.tender_type}
                              onChange={(event) => {
                                setFieldValue("tender_type", event);
                              }}
                              options={tenderTypeLists}
                            />
                          </Grid.Cell>
                          <Grid.Cell span={20}>
                            <div className="cell-tit">Business Type</div>
                            <SelectText
                              id="business_type"
                              label="business_type"
                              placeholder="Select Business Type"
                              value={values.business_type}
                              onChange={(event) => {
                                setFieldValue("business_type", event);
                              }}
                              options={businessTypeLists}
                            />
                          </Grid.Cell>
                          <Grid.Cell span={20}>
                            <div className="cell-tit">Service Category</div>
                            <SelectText
                              id="service_category"
                              label="service_category"
                              placeholder="Select Service Category"
                              value={values.service_category}
                              onChange={(event) => {
                                setFieldValue("service_category", event);
                              }}
                              options={serviceCategoryLists}
                            />
                          </Grid.Cell>
                        </Grid>
                      </Grid.Cell>
                    </Grid>
                  </div>
                  <div className="pge-brk"></div>
                  <div className="pay-trm">
                    <Grid>
                      <Grid.Cell span={30}>
                        <div className="pay-trm-tit">Tender Terms</div>
                        <div className="pay-trm-des">
                          Instructions to buyers. Will also have tips and tricks
                          about filling out this section with legible and good
                          enough information.
                        </div>
                        <div className="pay-trm-tip">
                          Tips and tricks follow..bla bla bla
                        </div>
                      </Grid.Cell>
                      <Grid.Cell span={70}>
                        <div className="pay-trm-scp">
                          <Label for="scope">Scope of Work</Label>
                          <TextInput
                            id="scope_work"
                            type="textarea"
                            placeholder="Scope of Work"
                            error={touched.scope_work && errors.scope_work}
                            size="medium"
                            value={values.scope_work}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                        <div className="pay-trm-exc">
                          <Grid>
                            <Grid.Cell span={10}>
                              <Label for="validity">Validity</Label>
                              <TextInput
                                id="validity"
                                type="textarea"
                                placeholder="Validity"
                                error={touched.validity && errors.validity}
                                size="medium"
                                value={values.validity}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </Grid.Cell>
                            <Grid.Cell span={10}>
                              <Label for="delivery">Delivery</Label>
                              <TextInput
                                id="delivery"
                                type="textarea"
                                placeholder="Delivery"
                                error={touched.delivery && errors.delivery}
                                size="medium"
                                value={values.delivery}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </Grid.Cell>
                          </Grid>
                        </div>
                        <div className="pay-trm-exc">
                          <Label for="price">Price Basis</Label>
                          <TextInput
                            id="price_basis"
                            type="textarea"
                            placeholder="Price Basis"
                            error={touched.price_basis && errors.price_basis}
                            size="medium"
                            value={values.price_basis}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                        <div className="pay-trm-exc">
                          <Label for="payment">Payment Terms</Label>
                          <TextInput
                            id="payment_terms"
                            type="textarea"
                            placeholder="Payment Terms"
                            error={
                              touched.payment_terms && errors.payment_terms
                            }
                            size="medium"
                            value={values.payment_terms}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                        <div className="pay-trm-exc">
                          <Grid>
                            <Grid.Cell span={10}>
                              <Label for="warranty">Warranty</Label>
                              <TextInput
                                id="warranty"
                                type="textarea"
                                placeholder="Warranty"
                                error={touched.warranty && errors.warranty}
                                size="medium"
                                value={values.warranty}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </Grid.Cell>
                            <Grid.Cell span={10}>
                              <Label for="remarks">Remarks</Label>
                              <TextInput
                                id="remarks"
                                type="textarea"
                                placeholder="Remarks"
                                error={touched.remarks && errors.remarks}
                                size="medium"
                                value={values.remarks}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </Grid.Cell>
                          </Grid>
                        </div>
                        <div className="pay-trm-exc">
                          <Label for="exclution">Exclutions</Label>
                          <TextInput
                            id="exclution"
                            type="textarea"
                            placeholder="Exclusions"
                            error={touched.exclution && errors.exclution}
                            size="medium"
                            value={values.exclution}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                        <div>
                          <Button
                            outline
                            color="secondary"
                            style={{ width: 195, marginTop: 15 }}
                          >
                            &ensp;
                            <IconComponent
                              name="report"
                              fill="#fff"
                              width="20"
                              height="20"
                            />
                            Add another Term
                          </Button>
                        </div>
                      </Grid.Cell>
                    </Grid>
                  </div>
                  <div className="pge-brk"></div>
                  <div className="wrp-ten-lst">
                    <div className="ten-doc">
                      {tenderValue && tenderValue !== "Lumpsum Unit Rate" && (
                        <div className="ten-doc-lst">
                          <div className="ten-doc-lst-nme">
                            Technical Documents
                          </div>
                          <div className="ten-doc-lst-cnt">
                            <div>
                              <FileUpload
                                onChange={this.handleFileChange}
                                onClick={() => this.getTenderId(values)}
                              />
                              {selectedFile && (
                                <div>
                                  <div>Uploaded Files</div>
                                  <div class="files">
                                    {selectedFile &&
                                      selectedFile.map((file, index) => {
                                        return (
                                          <div className="sel-lst">
                                            <ul>{file.name}</ul>
                                            <span
                                              onClick={() =>
                                                this.handleDelete(
                                                  file.id,
                                                  index
                                                )
                                              }
                                            >
                                              <IconComponent name="close" />
                                            </span>
                                          </div>
                                        );
                                      })}
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      )}
                      {tenderValue && tenderValue === "Lumpsum Unit Rate" && (
                        <div className="ten-wrk-lst">
                          <div className="add-wrk">
                            <Button
                              outline
                              color="secondary"
                              style={{
                                height: 30,
                                marginTop: 15,
                                fontSize: 12
                              }}
                              onClick={this.modalHandleClick}
                            >
                              <div className="btn-icn">
                                <IconComponent
                                  name="circleplus"
                                  fill="#fff"
                                  width="15"
                                  height="15"
                                />
                              </div>
                              Add Worklist
                            </Button>
                          </div>
                        </div>
                      )}
                    </div>
                    <Grid break="lg">
                      <Grid.Cell span={30}>
                        <div className="ten-info-cri">
                          <Grid>
                            <div className="ten-info-cri-hdr">
                              Key information
                            </div>
                          </Grid>
                          <Grid>
                            <div className="ten-info-cri-dte">
                              <div className="ten-dte-nme">
                                *&ensp;Response Deadline
                              </div>
                              <div className="ten-dte-pic">
                                <DatepickerComponent
                                  selected={this.state.selected}
                                  onChange={this.onChangeDate}
                                />
                              </div>
                            </div>
                          </Grid>
                          <Grid>
                            <div className="ten-info-cri-pnt">
                              <div className="ten-dte-nme">
                                Single point of Contact
                              </div>
                              <div className="ten-pnt-txt">
                                <SelectText
                                  id="singleContact"
                                  label="singleContact"
                                  placeholder="Select Single Contact"
                                  value={values.singleContact}
                                  onChange={(event) => {
                                    setFieldValue("singleContact", event);
                                  }}
                                  options={SingleContactList}
                                />
                              </div>
                            </div>
                          </Grid>
                        </div>
                      </Grid.Cell>
                      <Grid.Cell span={60}>
                        <div className="ten-info-pre">
                          <Grid>
                            <div className="ten-info-pre-hdr">Preferences</div>
                          </Grid>
                          <div className="ten-info-pre-cnt">
                            <Grid>
                              <div className="ten-info-pre-chk1">
                                <Field type="checkbox" name="performanceBond" />
                                <span className="ten-info-lbl">
                                  Performance Bond should be submitted
                                </span>
                              </div>
                            </Grid>
                            <Grid>
                              <div className="ten-info-pre-chk1">
                                <Field type="checkbox" name="lockedBox" />
                                <span className="ten-info-lbl">
                                  Locked box tender
                                </span>
                              </div>
                              <div className="ten-chk1-info">
                                No access to bid submissions for tender owners
                                until after the close date
                              </div>
                            </Grid>
                            <Grid>
                              <div className="ten-info-pre-chk1">
                                <Field type="checkbox" name="closeDate" />
                                <span className="ten-info-lbl">
                                  Enforce Close date
                                </span>
                              </div>
                              <div className="ten-chk1-info">
                                Binders cannot have access after close date.
                              </div>
                            </Grid>
                          </div>
                          <Grid>
                            <Grid.Cell span={25}>
                              <div className="ten-info-pre-chk1">
                                <Field type="checkbox" name="addpreference" />
                                <span className="ten-info-lbl">
                                  Add peference for vendor
                                </span>
                              </div>
                            </Grid.Cell>
                            <Grid.Cell span={20}>
                              <div className="ten-info-pre-chk1">
                                <Field type="checkbox" name="partialBinds" />
                                <span className="ten-info-lbl">
                                  Accept partial bids
                                </span>
                              </div>
                            </Grid.Cell>
                          </Grid>
                        </div>
                      </Grid.Cell>
                    </Grid>
                  </div>
                  <div className="crt-ten-ftr">
                    <div className="ftr-btn">
                      <div>
                        <Link to="/tenders">
                          <Button color="discard" size="sm" className="mr-2">
                            Discard
                          </Button>
                        </Link>
                      </div>
                      <div>
                        <Button
                          color="primary"
                          type="sumbit"
                          size="sm"
                          className="mr-2"
                          onClick={() => {
                            this.handleSave(values);
                          }}
                        >
                          Save
                        </Button>
                      </div>
                      <div>
                        <Button
                          color="primary"
                          className="mr-2"
                          onClick={() => {
                            this.handleClick(values);
                          }}
                          size="sm"
                        >
                          View Summary
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </Grid.Cell>
            </Grid>
          </div>
          <UnitTender isOpen={this.state.isOpen} hide={this.handleHide} />
        </div>
      </Form>
    );
  }
}

const MyEnhancedForm = formikEnhancer(withRouter(MyForm));

// Main Component
class CreateTenderNew extends PureComponent {
  render() {
    return (
      <div>
        <MyEnhancedForm
          user={{
            project_name: "",
            tender_title: "",
            tender_value: "",
            project_id: "",
            tender_id: "",
            validity: "",
            scope_work: "",
            delivery: "",
            price_basis: "",
            payment_terms: "",
            warranty: "",
            remarks: "",
            exclution: ""
          }}
        />
      </div>
    );
  }
}

export default withRouter(CreateTenderNew);
