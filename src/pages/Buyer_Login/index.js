// BuyerLoginPage
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Input } from "../../components/Input";
import { Button } from "reactstrap";
import { Image } from "../../components/Image";
import { CheckBox } from "../../components/Checkbox";
import { notification } from "antd";
import API from "../../api/api";
import { Formik } from "formik";
import * as Yup from "yup";
import { Form, FormFeedback } from "reactstrap";
import cookie from 'react-cookies';

// Main Component
export default class BuyerLoginPage extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value
    };

    console.log("props", props);
  }
  handleKeyPress = (charCode) => {
    if (charCode == 13) {
      // this.onNextValue();
      const { history } = this.props;
      let params = {
        email: this.state.email,
        password: this.state.password
      };
      if (this.state.email && this.state.password) {
        API.fetchData(API.Query.LOGIN, params).then((responseData) => {
          if (
            responseData &&
            responseData.data &&
            responseData.data.user &&
            responseData.data.user.stsTokenManager &&
            responseData.data.user.stsTokenManager.accessToken
          ) {
            let tokenValue = responseData.data.user.stsTokenManager.accessToken;
            if(this.state.keepSign){
              localStorage.setItem("token", tokenValue);
            }else{
              cookie.save('token', tokenValue);
            }

            localStorage.setItem(
              "emailId",
              responseData &&
                responseData.data.user &&
                responseData.data.user.email
            );
            // history.push("/tenders");
          }
          if (
            responseData &&
            responseData.userdata &&
            responseData.userdata.length > 0
          ) {
            var userRole = responseData.userdata.map(
              (userData) => userData && userData.role
            );
            if (userRole) {
              // console.log("user role in login.." + userRole);
              localStorage.setItem("userRole", userRole);
              var userId = responseData.data.user.uid;

              if (userRole == "vendor") {
                localStorage.setItem("userIdForVendor", userId);
              }
              if (userRole == "buyer") {
                localStorage.setItem("userIdForBuyer", userId);
              }
              history.push("/tenders");
            }
          }
          if (responseData && responseData.status == 1) {
            notification.warning({
              message: "Login Error",
              description: responseData.error.message
            });
          }
        });
      }
    }
  };
  render() {
    const { email, password } = this.state;

    const onChangeEmail = (value) => {
      console.log("value.." + value);
      this.setState({ email: value });
    };

    const onChangePassword = (value) => {
      this.setState({ password: value });
    };

    const onNextValue = (e, values) => {
      const { history } = this.props;
      // console.log("params.." + JSON.stringify(params));
      let params = {
        email: email,
        password: password
      };
      if (email && password) {
        API.fetchData(API.Query.LOGIN, params).then((responseData) => {
          console.log("login response..", JSON.stringify(responseData));
          if (
            responseData &&
            responseData.data &&
            responseData.data.user &&
            responseData.data.user.stsTokenManager &&
            responseData.data.user.stsTokenManager.accessToken
          ) {
            let tokenValue = responseData.data.user.stsTokenManager.accessToken;
            if(this.state.keepSign){
              localStorage.setItem("token", tokenValue);
            }else{
              cookie.save('token', tokenValue);
            }

            localStorage.setItem(
              "emailId",
              responseData &&
                responseData.data.user &&
                responseData.data.user.email
            );
            // history.push("/tenders");
          }
          if (
            responseData &&
            responseData.userdata &&
            responseData.userdata.length > 0
          ) {
            var userRole = responseData.userdata.map(
              (userData) => userData && userData.role
            );
            if (userRole) {
              // console.log("user role in login.." + userRole);
              localStorage.setItem("userRole", userRole);
              var userId = responseData.data.user.uid;

              if (userRole == "vendor") {
                localStorage.setItem("userIdForVendor", userId);
              }
              if (userRole == "buyer") {
                localStorage.setItem("userIdForBuyer", userId);
              }
              history.push("/tenders");
            }
          }
          if (responseData && responseData.status == 1) {
            notification.warning({
              message: "Login Error",
              description: responseData.error.message
            });
          }
        });
      }
    };

    return (
      <div className="wrp-lgn">
        <div className="lgn-img">
          <div className="img-lgn">
            <Image
              w="90%"
              h="100%"
              src="/images/login_img.png"
              alt="image"
            ></Image>
          </div>
          <div className="lgn-hdr-cnt heading">
            Optimize resources
            <br />
            <span className="hed-prt"> Reduce Costs</span>
            <br /> Improve time to market <br />
            Increase efficiency <br />
            Seamless communication
          </div>
        </div>
        <div className="lgn-cnt">
          <div className="lgn-frm">
            <div className="lgn-hdr">
              <div className="hdr1 heading">e-Tender</div>
              <div className="hdr2 heading">for amtrex</div>
            </div>

            <div className="lgn-frm-cnt">
              <Formik
                initialValues={{
                  email: "",
                  password: "",
                  keepSign: false
                }}
                // onSubmit={async (values) => {
                //   //alert(JSON.stringify(values, null, 2));
                //   //handleSubmit;
                // }}

                validationSchema={Yup.object().shape({
                  email: Yup.string()
                    .email("Email must be a valid email")
                    .required("Email is required")
                    .trim(),
                  password: Yup.string().required("Password is required")
                })}
              >
                {(props) => {
                  const {
                    values,
                    touched,
                    errors,
                    dirty,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    handleReset,
                    setFieldValue
                  } = props;
                  return (
                    <Form
                      ref={this.form}
                      noValidate
                      //validated={validated}
                      onSubmit={handleSubmit}
                    >
                      <>
                        <div className="lgn-mil">
                          <div className="txt-mil">Email/Username</div>
                          <div className="int-box">
                            <Input
                              placeholder="email"
                              size="small"
                              onChange={(val, name, e) => {
                                onChangeEmail(val);
                                handleChange(e);
                              }}
                              name="email"
                              value={email}
                              onKeyPress={this.handleKeyPress}
                              error={!!errors.email && !!touched.email}
                            />
                          </div>
                          {errors.email && touched.email && (
                            <div className="errorText">{errors.email}</div>
                          )}
                        </div>
                        <div className="lgn-pwd">
                          <div className="cnt-pwd">
                            <span className="txt-pwd">Password</span>
                            <span className="fgt-pwd">
                              <a
                                href="/forgotpassword"
                                style={{ color: "#4A42ED" }}
                              >
                                Forgot Password?
                              </a>
                            </span>
                          </div>
                          <div className="int-box">
                            <Input
                              placeholder="password"
                              size="small"
                              onChange={(val, name, e) => {
                                onChangePassword(val);
                                handleChange(e);
                              }}
                              //onChange={onChangePassword}
                              type="password"
                              name="password"
                              value={password}
                              error={!!errors.password && !!touched.password}
                              onKeyPress={this.handleKeyPress}
                            />
                          </div>
                          {errors.password && touched.password && (
                            <div className="errorText">{errors.password}</div>
                          )}
                        </div>
                        <div className="text-center mt-2">
                          <Button
                            color="primary"
                            size="md"
                            onClick={(e) => {
                              handleSubmit(e);
                              onNextValue(e, values);
                            }}
                          >Login</Button>
                        </div>
                        <div className="lgn-chk">
                          <CheckBox
                            variant="checkstyle"
                            title="Keep me signed in."
                            onCheckboxChange={(val) => {
                              console.log('test data ', val);
                              this.setState({
                                keepSign: val
                              });
                              setFieldValue('keepSign', val);
                            }}
                            name="keepSign"
                            value={values.keepSign}
                          />
                        </div>
                      </>
                    </Form>
                  );
                }}
              </Formik>
              <div className="lgn-adm">
                <div className="lgn-hlp">
                  <span className="lgn-txt">Need help? Contact &ensp;</span>
                  <span>
                    <a class="adm-lnk" href="url">
                      System Admin
                    </a>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BuyerLoginPage.propTypes = {
  name: PropTypes.string
};

BuyerLoginPage.defaultProps = {
  name: "Insert"
};
