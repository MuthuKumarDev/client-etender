// Invite Vendor Register
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Input } from "../../components/Input";
import { Grid } from "../../components/Grid";
import { Button } from "reactstrap";
import JoditEditor from "jodit-react";
import { ReactMultiEmail } from "react-multi-email";
import "react-multi-email/style.css";
import API from "../../api/api";
import { notification } from "antd";
import { Link } from "react-router-dom";
import { SERVER_URL } from "../../config";

//Main Component
export default class InviteVendorRegister extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      emails: [],
      textAreaValue: "",
      textValue: `
      <div style='display: flex;
      align-items: center;
      justify-content: center;
      width: 98%;
      height: 74px;
      margin: auto;
      border-bottom:  1px solid #d7b47c;'>
      <Image
        w={690}
        src="${SERVER_URL}/images/image.png"
        alt="hrd-image"
      />
    </div>
    <div style='display: flex; flex-direction: row;'>
        <div style=" width: 100%;
min-height: 100%;
margin: 5%;">
          <div style='
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 17px;
color: #212B36;'>
            Here is a tender from Al-Sahel for material,
            services or whatever.
          </div>
          <div style='padding-top: 5%;'>
            <span> Learn more about us</span>
            <span>
              <a href='http://alsahelcon.com/about-us/introduction/'>
                http://alsahelcon.com/about-us/introduction/
              </a>
            </span>
          </div>
          <div style='padding-top: 5%;'>
            <div style='
font-style: normal;
font-weight: 700;
font-size: 14px;
line-height: 17px;
color: #212B36;'>Details</div>
            <div style='
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 17px;
color: #212B36;'>
              Project value : <strong>100 million AED</strong>
            </div>
            <div style='
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 17px;
color: #212B36;'>
              Project status : <strong>Job in Hand </strong>
            </div>
          </div>
          <div style='padding-top: 5%;'>
            <div style='
font-style: normal;
font-weight: 700;
font-size: 16px;
line-height: 17px;
color: #212B36;'>
              Only limited number of bidders.
            </div>
            <div style='
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 17px;
color: #212B36;'>
              View tenders at
            </div>
            <div>
              <a href='url'>
                View all the tenders by org name
              </a>
            </div>
          </div>
          <div style='
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 17px;
color: #212B36;'>
            To increase your chances to win the contract in
            <strong>just 3 easy steps</strong>
          </div>
          <div style='
font-style: normal;
font-weight: 700;
font-size: 18px;
line-height: 22px;
color: #212B36;'>
            Why to register with us?
          </div>
          <div style='padding-top: 5%;'>
            - Receive new business opportunities
            <strong>first hand</strong> <br />
            - Gain more visibility to potential buyers <br />
            <strong>- Reach more buyers,</strong> grow sales
            <br />- <strong>Go global</strong> truly in this
            globalized era <br />- <strong>Stand tal</strong>
            amongst your competition <br />
            - You don’t have to build a website for yourself,
            start selling on etender. <br />
          </div>
          <div style='padding-top: 5%;'>
            Best regards, <br />
            Procurement team, Al-Sahel <br />
            <strong>Raj (+971 04 48576767)</strong> <br />
            <strong>Landline (+971 04 48576767)</strong>
            <br />
          </div>
          <div style='width: 90%;
          margin-top: 8%;'>
                        <Button style='height: 36px;
                        border-radius: 3px;
                        background: linear-gradient(
                          180deg,
                          #a5b85f 0%,
                          #cfea88 45.83%,
                          #cfe988 93.75%,
                          #a8bb66 100%
                        );
                        box-shadow: 0px 1px 2px rgba(22, 29, 37, 0.1),
                          inset 0px 1px 0px rgba(255, 255, 255, 0.06);
                        color: #ffffff;
                        font-style: normal;
                        font-weight: bold;
                        font-size: 14px;
                        line-height: 20px;
                        text-align: center;
                        color: #397e2d;
                    '>Sign up now & Get the Benefits</Button>
                      </div>
        </div>
        <div style='width: 98%;
min-height: 100%;
border-top: 2px solid #aabe64;
margin-top: 5%;
margin-right: 5%;'>
          <div style='width: 100%;
height: 70%;
background-color: #040849;
box-shadow: 0px 0px 0px rgba(63, 63, 68, 0.05), 0px 1px 3px rgba(63, 63, 68, 0.15);
padding: 2% 0 20% 0;'>
            <div style='display: flex; flex-direction: row;'>
              <div style='padding-top: 1%;
margin-left: 5%;'>
                <Image
                  w={15}
                  src='${SERVER_URL}/images/radio.png'
                  alt='rdo-image'
                />
              </div>
              <div style='
font-style: normal;
font-weight: normal;
font-size: 12px;
line-height: 15px;
color: #FFFFFF;
padding-left: 10%;
padding-top: 2%;
width: 80%;'>
                Based and headquartered in Dubai
              </div>
            </div>
            <div style='display: flex; flex-direction: row; padding-top: 5%'>
              <div style='padding-top: 1%;
margin-left: 5%;'>
                <Image
                  w={15}
                  src='${SERVER_URL}/images/radio.png'
                  alt='rdo-image'
                />
              </div>
              <div style='
font-style: normal;
font-weight: normal;
font-size: 12px;
line-height: 15px;
color: #FFFFFF;
padding-left: 10%;
padding-top: 2%;
width: 80%;'>50000 employees</div>
            </div>
            <div style='display: flex; flex-direction: row; padding-top: 5%'>
              <div style='padding-top: 1%;
margin-left: 5%;'>
                <Image
                  w={15}
                  src='${SERVER_URL}/images/radio.png'
                  alt='rdo-image'
                />
              </div>
              <div style='
font-style: normal;
font-weight: normal;
font-size: 12px;
line-height: 15px;
color: #FFFFFF;
padding-left: 10%;
padding-top: 2%;
width: 80%;'>
                250+ projects Residential/Commercial
                Buildings, Luxury Villas, High Rise Buildings,
                Shopping Malls, Warehouses, Schools, Hotels.
              </div>
            </div>
            <div style='display: flex; flex-direction: row; padding-top: 5%'>
              <div style='padding-top: 1%;
margin-left: 5%;'>
                <Image
                  w={15}
                  src='${SERVER_URL}/images/radio.png'
                  alt='rdo-image'
                />
              </div>
              <div style='
font-style: normal;
font-weight: normal;
font-size: 12px;
line-height: 15px;
color: #FFFFFF;
padding-left: 10%;
padding-top: 2%;
width: 80%;'>
                The first Contracting Company in Dubai (Main
                Contractor) to be certified by LRQA to ISO
                9002:1994 Standard.
              </div>
            </div>
          </div>
        </div>
        </div>
                      `
    };
  }

  onChangeTextvalue = (textValue) => {
    this.setState({ textValue });
  };

  handleChange = (textAreaValue) => {
    this.setState({ textAreaValue });
  };

  handleMail = () => {
    let mailValidation = this.state.emails;
    if (mailValidation.length > 0 && this.state.textAreaValue !== "") {
      API.fetchData(API.Query.SEND_MAIl, {
        toemails: [this.state.emails],
        subject: this.state.textAreaValue,
        content: this.state.textValue
      }).then(() => {
        notification.success({
          message: "Message Sent"
        }); 
      });
      this.props.history.push("/vendors");
    } else {
      notification.error({
        message: "Mail and Subject Required"
      });
    }
  };
  
  render() {
    const { emails, textValue, textAreaValue } = this.state;
    console.log("email", emails);
    console.log("textvalue", textValue);
    console.log("textAreaValue", textAreaValue);
    return (
      <div className="ivt-reg-wrp">
        <Grid>
          <Grid.Cell>
            <div className="ivt-reg">
              <div className="ivt-hdr">
                <div className="ivt-hdr-nme">
                  <span className="hdr-nme-fnt header">Invite Vendor to</span>
                  <span className="hdr-nme-bck header">REGISTER</span>
                </div>
              </div>
              <div className="ivt-cnt">
                <Grid>
                  <div className="ivt-snd">
                    <div className="ivt-snd-nme">To</div>
                    <div className="ivt-snd-lst">
                      <ReactMultiEmail
                        placeholder="Input your Email Address"
                        emails={emails}
                        onChange={(_emails) => {
                          this.setState({ emails: _emails });
                        }}
                        getLabel={(email, index, removeEmail) => {
                          return (
                            <div data-tag key={index}>
                              {email}
                              <span
                                data-tag-handle
                                onClick={() => removeEmail(index)}
                              >
                                ×
                              </span>
                            </div>
                          );
                        }}
                      />
                    </div>
                  </div>
                </Grid>
                <Grid>
                  <div className="ivt-sbj">
                    <div className="ivt-sbj-nme">Subject</div>
                    <div className="ivt-sbj-lst">
                      <Input
                        type="textarea"
                        onChange={this.handleChange}
                        value={textAreaValue}
                        placeholder="Subject"
                      />
                    </div>
                  </div>
                </Grid>
                <Grid>
                  <div className="ivt-ten-reg">
                    <div className="ten-reg-txt">
                      Register with Amtrex Technical Services as a Vendor.
                    </div>
                    <div className="ten-reg-tbl">
                      <JoditEditor
                        value={this.state.textValue}
                        onChange={this.onChangeTextvalue}
                      />
                    </div>
                  </div>
                </Grid>
              </div>
              <div className="ivt-ven-ftr">
                <div className="ven-ftr-btn">
                  <div>
                    <Button
                      color="primary"
                      size="md"
                      onClick={this.handleMail}
                    >Send</Button>
                  </div>
                  <div className="ven-smr-lnk">
                  <Link to="/vendors">
                    <Button 
                      color="link"
                      size="md"
                    >Discard</Button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </Grid.Cell>
        </Grid>
      </div>
    );
  }
}

InviteVendorRegister.propTypes = {
  name: PropTypes.string
};

InviteVendorRegister.defaultProps = {
  name: "Invite Vendor to Register"
};
