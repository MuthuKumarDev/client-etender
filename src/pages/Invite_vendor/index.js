// InviteVendor Page Component
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import { Table, Pagination } from "antd";
import { CheckBox } from "../../components/Checkbox";
import { Input } from "../../components/Input";
import { Sidebar } from "../../components/Sidebar";
import { Grid } from "../../components/Grid";
import API from "../../api/api";
import _ from "lodash";
import { MultiSelectComponent } from "../../components/MultiSelect";
import { IconComponent } from "../../components/Icons";
import { DropDownComponent as DropDown } from "../../components/DropDownComponent";

import { inviteColumn } from "../helpers/tabledata";

// Main Component
export default class InviteVendor extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      vendors: [],
      searchText: "",
      location: [
        { key: "UAE", cat: "" },
        { key: "INDIA", cat: "" },
        { key: "ENGLAND", cat: "" }
      ],
      sector: [
        { key: "10- 12 million AED", cat: "" },
        { key: "13- 15 million AED", cat: "" },
        { key: "17- 20 million AED", cat: "" }
      ],
      turnover: [
        { key: "1 day", cat: "" },
        { key: "1 week", cat: "" }
      ],
      Rating: [
        { key: "Rating", cat: "" },
        { key: "Annual Turnover", cat: "" },
        { key: "Certificate", cat: "" },
        { key: "Consultant", cat: "" },
        { key: "Last Logged in", cat: "" },
        { key: "Ship to location", cat: "" }
      ],
      AddFilters: [
        { key: "1 day", cat: "" },
        { key: "1 week", cat: "" }
      ],
      sortingOptions: [
        { key: "Latest Addition", cat: "" },
        { key: "Closing Date", cat: "" }
      ],
      isOpenForSorting: false,
      sortValue: "Latest Addition",
      fromAddMore: false,
      isOpen: false,
      dropdownOpen: false,
      dropdownVal: "Select",
      isOpenForPublished: false,
      customizedDate: "",
      publishedWithinValue: "Select",
      customizedClosingDate: "",
      closingDateValue: "Select",
      isOpenForServiceType: false,
      serviceTypeValue: "Select"
    };

    this.style = {
      chips: {
        background: "#fff",
        color: "#000"
      },
      searchBox: {},
      multiselectContainer: {
        // width: 170
      }
    };
  }

  componentDidMount() {
    API.fetchData(API.Query.GET_VENDORS).then((data) => {
      console.log("vendor data", data.vendors);

      this.setState({
        vendors: data.vendors
      });
    });
  }

  onSelect = (value, name) => {
    if (name == "location") {
      this.setState({ selectedValue: value });
    }
    if (name == "sector") {
      this.setState({ selectedValueForProjectValue: value });
    }
    if (name == "turnover") {
      this.setState({ selectedValueForSector: value });
    }
    if (name == "Rating") {
      this.setState({ selectedValueForMoreFilters: value });
    }
    if (name == "AddFilters") {
      this.setState({ selectedValueForMoreFilters: value });
    }
  };

  onChangeSearchText = (event) => {
    // console.log("Onchange text", searchText);
    this.setState({ searchText: event.target.value });
  };

  handleMove = (props) => {
    this.props.history.push(`/tenders`);
  };

  toggleItem = (value, name) => {
    // console.log("inside toggle item value..." + value);
    let {
      dropdownOpen,
      isOpenForPublished,
      isOpenForClosingDate,
      dropdownVal,
      publishedWithinValue,
      closingDateValue,
      isOpenForServiceType,
      isOpenForNumberOfBids
    } = this.state;
    if (name == "status") {
      // console.log("inside status.." + value);
      this.setState({
        dropdownOpen: !this.state.dropdownOpen,
        dropdownVal: value
      });
    }
    if (name == "publishedWithin") {
      this.setState({
        isOpenForPublished: !this.state.isOpenForPublished,
        publishedWithinValue: value
      });
      this.setState({ customizedDate: "" });
    }
    if (name == "closingDate") {
      this.setState({
        isOpenForClosingDate: !isOpenForClosingDate,
        closingDateValue: value
      });
      this.setState({ customizedClosingDate: "" });
    }
    if (name == "servicetype") {
      this.setState({
        isOpenForServiceType: !isOpenForServiceType,
        serviceTypeValue: value
      });
    }
    if (name == "noofbids") {
      this.setState({
        isOpenForNumberOfBids: !isOpenForNumberOfBids,
        noOfBidsValue: value
      });
    }
    if (name == "sortby") {
      this.setState({
        isOpenForSorting: !this.state.isOpenForSorting,
        sortValue: value
      });
      if (value == "Closing Date") {
        let params = {
          closing_date: "desc"
        };
      }
      if (value == "Latest Addition") {
        let params = {
          latest_addition: "desc"
        };
      }
    }
  };

  toggle = (name) => {
    let { dropdownOpen } = this.state;
    if (name == "status") {
      this.setState({ dropdownOpen: !this.state.dropdownOpen });
    }
    if (name == "publishedWithin") {
      this.setState({ isOpenForPublished: !this.state.isOpenForPublished });
    }
    if (name == "closingDate") {
      this.setState({ isOpenForClosingDate: !this.state.isOpenForClosingDate });
    }
    if (name == "servicetype") {
      this.setState({ isOpenForServiceType: !this.state.isOpenForServiceType });
    }
    if (name == "noofbids") {
      this.setState({
        isOpenForNumberOfBids: !this.state.isOpenForNumberOfBids
      });
    }
    if (name == "sortby") {
      this.setState({
        isOpenForSorting: !this.state.isOpenForSorting
      });
    }
  };

  render() {
    const {
      location,
      turnover,
      sector,
      Rating,
      AddFilters,
      sortingOptions,
      sortValue,
      fromAddMore,
      isOpenForSorting,
      isOpenForPublished,
      customizedDate,
      publishedWithinValue,
      isOpenForClosingDate,
      customizedClosingDate,
      closingDateValue,
      isOpenForServiceType,
      serviceTypeValue
    } = this.state;

    const results =
      this.state.vendors &&
      this.state.vendors.length > 0 &&
      this.state.vendors.filter(
        (vendor) =>
          (vendor.company_name &&
            vendor.company_name
              .toLowerCase()
              .includes(this.state.searchText)) ||
          (vendor.company_name &&
            vendor.company_name.includes(this.state.searchText)) ||
          (vendor.company_name &&
            vendor.company_name.toUpperCase().includes(this.state.searchText))
      );

    const dataSource =
      results &&
      results.length > 0 &&
      results.map((vendor) => ({
        key: vendor.id,
        vId: vendor.vendor_id,
        company: vendor.company_name,
        location: vendor.location,
        phNumber: vendor.phone,
        email: vendor.email,
        businesstype: vendor.business_sector
      }));

    // const places = this.state.location.map((place) => ({
    //   key: place.id

    // }))

    return (
      <div className="inv-ven">
        <Grid>
          <Grid.Cell>
            <div className="inv-ven-top">
              <div className="inv-ven-top-hdr">
                <div className="hdr-tit">
                  <div className="smr-hdr-nme heading">
                    Invite Vendor to Register
                  </div>
                </div>
                <div className="smr-btn">
                  <div>
                    <Button color="link" onClick={this.handleMove}>
                      Go Back to Tenders
                    </Button>
                  </div>
                  <div>
                    <Button color="primary" size="md">
                      Post
                    </Button>
                  </div>
                </div>
              </div>
              <div className="inv-ven-pge">
                <div className="inv-ven-srh">
                  <div class="main">
                    <div class="form-group has-search">
                      <div className="icn-main">
                        <IconComponent name="search" />
                      </div>
                      <input
                        type="text"
                        class="form-control"
                        name="search"
                        placeholder="Search Vendors"
                        onChange={this.onChangeSearchText}
                      ></input>
                    </div>
                  </div>
                  <div className="ven-lst-sel">
                    <DropDown
                      options={location}
                      isOpen={
                        this.state.isOpen == false
                          ? this.state.dropdownOpen
                          : ""
                      }
                      toggleItem={this.toggleItem}
                      toggle={this.toggle}
                      dropdownVal={this.state.dropdownVal}
                      direction="down"
                      size="lg"
                      fromAddMore={fromAddMore}
                      icon={<IconComponent name="downarrow" />}
                      name="status"
                      classNames={
                        this.state.dropdownVal == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                  <div className="ven-lst-sel">
                    <DropDown
                      options={sector}
                      isOpen={
                        this.state.isOpen == false ? isOpenForClosingDate : ""
                      }
                      toggleItem={this.toggleItem}
                      toggle={this.toggle}
                      dropdownVal={
                        customizedClosingDate
                          ? customizedClosingDate
                          : closingDateValue
                      }
                      direction="down"
                      size="lg"
                      name="closingDate"
                      fromAddMore={fromAddMore}
                      icon={<IconComponent name="downarrow" />}
                      classNames={
                        closingDateValue == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : customizedClosingDate
                          ? {
                              dropdownStyle: "dropdownDivForCustomRange",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>
                  <div className="ven-lst-sel">
                    <DropDown
                      options={turnover}
                      isOpen={
                        this.state.isOpen == false ? isOpenForPublished : ""
                      }
                      toggleItem={this.toggleItem}
                      toggle={this.toggle}
                      dropdownVal={
                        customizedDate ? customizedDate : publishedWithinValue
                      }
                      direction="down"
                      size="lg"
                      fromAddMore={fromAddMore}
                      icon={<IconComponent name="downarrow" />}
                      name="publishedWithin"
                      classNames={
                        publishedWithinValue == "Select"
                          ? {
                              dropdownStyle: "defaultDropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : customizedDate
                          ? {
                              dropdownStyle: "dropdownDivForCustomRange",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                          : {
                              dropdownStyle: "dropdownDiv",
                              dropDownMenu: "dropDownMenu",
                              dropDownItem: "dropDownItem"
                            }
                      }
                    />
                  </div>

                  <div className="ven-lst-sel">
                    <MultiSelectComponent
                      options={Rating}
                      displayValue="key"
                      showCheckbox={true}
                      onSelect={this.onSelect}
                      styles={this.style}
                      singleSelect={false}
                      selectedValues={this.selectedValue}
                      name="Rating"
                    />
                  </div>
                  <div className="ven-lst-sel">
                    <MultiSelectComponent
                      options={AddFilters}
                      displayValue="key"
                      showCheckbox={true}
                      onSelect={this.onSelect}
                      styles={this.style}
                      singleSelect={false}
                      selectedValues={this.selectedValue}
                      name="AddFilters"
                    />
                  </div>
                </div>
                <div className="inv-ven-shw">
                  <div className="inv-ven-shw-txt">
                    Showing {this.state.vendors.length} Vendors
                  </div>
                  <div className="inv-ven-shw-box">
                    <div className="d-flex sortByMainDiv">
                      <div className="sortBy">Sort By</div>
                      <div className="sortByDropdownDiv">
                        <DropDown
                          options={sortingOptions}
                          isOpen={isOpenForSorting}
                          toggleItem={this.toggleItem}
                          toggle={this.toggle}
                          dropdownVal={sortValue}
                          direction="down"
                          size="lg"
                          name="sortby"
                          icon={<IconComponent name="downarrow" />}
                          fromAddMore={fromAddMore}
                          classNames={
                            sortValue == "Select"
                              ? {
                                  dropdownStyle: "defaultDropdownDivForSort",
                                  dropDownMenu: "dropDownMenu",
                                  dropDownItem: "dropDownItem"
                                }
                              : {
                                  dropdownStyle: "dropdownDivForSort",
                                  dropDownMenu: "dropDownMenu",
                                  dropDownItem: "dropDownItem"
                                }
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="inv-ven-tab">
                  <Table
                    dataSource={dataSource}
                    columns={inviteColumn}
                    size="small"
                    bordered
                    pagination={false}
                  />
                </div>
                <div className="inv-ven-pag">
                  <Pagination simple defaultCurrent={1} total={100} />
                </div>
              </div>
            </div>
          </Grid.Cell>
        </Grid>
      </div>
    );
  }
}

InviteVendor.propTypes = {
  name: PropTypes.string
};

InviteVendor.defaultProps = {
  name: "Invite Tender"
};
