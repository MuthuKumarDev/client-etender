import React, { PureComponent } from "react";
import { CardComponent } from "../../components/Card";
import { Row, Col } from "reactstrap";
import "../styles/tenders.scss";
import { Button } from "../../components/Button";
import { InputGroupField } from "../../components/InputGroupField";
import { InputField } from "../../components/InputField/Inputfield";
import { Table , Tooltip} from "antd";
import { CheckBox } from "../../components/Checkbox";
import { ReactComponent as SeenIcon } from "../../svgs/seen_icon.svg";
import { ReactComponent as ChatIcon } from "../../svgs/chat_icon.svg";
import { ReactComponent as LikeIcon } from "../../svgs/like_icon.svg";
import { BadgeComponent } from "../../components/Badge";
// import history from '../../history';
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import API from "../../api/api";

// Main Component
class AllTenders extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        tendersList: [
          {
            key: "1",
            tenderId: "1",
            date: "12-02-2021",
            noOfVendors: "2",
            endsIn: "-",
            status: "issued",
            seen: "2",
            question: "0",
            like: "3",
            bids: (
              <BadgeComponent value="5 bids" badgeStyles="badgeStyle" />
            ),
            addenda: "-"
          },
          {
            key: "2",
            tenderId: "2",
            date: "12-02-2021",
            noOfVendors: "2",
            endsIn: "-",
            status: "issued",
            seen: "2",
            question: "0",
            like: "3",
            bids: (
              // <BadgeComponent value="5 bids" badgeStyles="issuedBadgeStyle" />
              <BadgeComponent value="5 bids" badgeStyles="badgeStyle" />
            ),
            addenda: "-"
          },
          {
            key: "3",
            tenderId: "3",
            date: "12-02-2021",
            noOfVendors: "2",
            endsIn: "-",
            status: "closed",
            seen: "2",
            question: "0",
            like: "5",
            bids: (
              // <BadgeComponent value="5 bids" badgeStyles="approvedBadgeStyle" />
              <BadgeComponent value="5 bids" badgeStyles="badgeStyle" />
            ),
            addenda: "-"
          },
          {
            key: "4",
            tenderId: "4",
            date: "12-02-2021",
            noOfVendors: "2",
            endsIn: "-",
            status: "closed",
            addenda: "-",
            seen: "2",
            question: "0",
            like: "3",
            bids: (
              // <BadgeComponent value="5 bids" badgeStyles="issuedBadgeStyle" />
              <BadgeComponent value="5 bids" badgeStyles="badgeStyle" />
            ),
            addenda: "-"
          },
          {
            key: "5",
            tenderId: "5",
            date: "12-02-2021",
            noOfVendors: "2",
            endsIn: "-",
            status: "drafts",
            seen: "2",
            question: "0",
            like: "5",
            bids: (
              // <BadgeComponent value="5 bids" badgeStyles="approvedBadgeStyle" />
              <BadgeComponent value="5 bids" badgeStyles="badgeStyle" />
            ),
            addenda: "-"
          },
          {
            key: "6",
            tenderId: "6",
            date: "12-02-2021",
            noOfVendors: "2",
            endsIn: "-",
            status: "issued",
            addenda: "-",
            seen: "2",
            question: "0",
            like: "3",
            bids: (
              // <BadgeComponent value="5 bids" badgeStyles="issuedBadgeStyle" />
              <BadgeComponent value="5 bids" badgeStyles="badgeStyle" />
            ),
            addenda: "-"
          }
        ]
      },

      columns: [
        {
          title: "",
          dataIndex: "checkbox",
          key: "checkbox",
          // render: (_text) => (
          //   <div>
          //     <CheckBox variant="normal" />
          //   </div>
          // )
        },

        {
          title: "Tender ID",
          dataIndex: "tenderId",
          key: "tenderId"
        },
        {
          title: "Date",
          dataIndex: "date",
          key: "date"
        },
        {
          title: "No of Vendors",
          dataIndex: "noOfVendors",
          key: "noOfVenders"
        },
        {
          title: "Ends in",
          dataIndex: "endsIn",
          key: "noOfVendors"
        },
        {
          title: "Status",
          dataIndex: "status",
          key: "status"
        },
        {
          title: <SeenIcon />,
          dataIndex: "seen",
          key: "seen",
                    // render: seen => (
          //   <Tooltip placement="topLeft" title={seen}>
          //     {seen}
          //   </Tooltip>
          // )
        },
        {
          title: <ChatIcon />,
          dataIndex: "question",
          key: "question"
        },
        {
          title: <LikeIcon />,
          dataIndex: "like",
          key: "like"
        },
        {
          title: "",
          dataIndex: "bids",
          key: "bids"
        },
        {
          title: "Addenda",
          dataIndex: "addenda",
          key: "addenda"
        }
      ],
      sort: ["Newest Update", "option1", "option2"],
      open: ["Open", "option1", "option2"],
      timePeriod: ["Within a month", "option1", "option2"],
      moreFilters: ["More filters", "option1", "option 2"],
      filteredTendersList: [],
      selectedRowKeys: "",
      selectedTimePeriodValue: '',
      selectedFieldValue: '',
      selectedMoreFilterValue: '',
      selectedOpenValue: '',
    };
    this.tableRef = React.createRef();
  }

  OnRowClick = (rowData, index) => {
    //console.log("value and val.." + JSON.stringify(rowData) + ".." + index);
    this.props.history.push(`/tenders/${rowData.tenderId}`);
  };
  componentDidMount = () => {
    //console.log("status..." + this.props.status);
    //var tenderListData = this.state.data;
    // tenderListData.tendersList =  this.state.data.tendersList;
    this.setState({ filteredTendersList: this.state.data.tendersList });
    this.updateData(this.props.status);

   
    API.fetchData(API.Query.All_TENDERS).then((data) => {
      //console.log("API.Query.All_TENDERS.." + API.Query.All_TENDERS)
      console.log("tenders list new..", JSON.stringify(data));
        let { tender } = data;
        this.setState({
          tenderList: data,
      })
    })

  }
  componentDidUpdate(prevProps) {
    console.log("props..." + JSON.stringify(prevProps));
    console.log("this.props.status.." + this.props.status);
    if (prevProps.status != this.props.status) {
      //this.setState({ status: this.props.status });
      this.updateData(this.props.status);
    }
  }
  updateData = (status) => {
    // let { data } = this.state;
    //   let { tendersList } = data;

    // var temp= this.state.data;
    // var tempData = [...temp.tendersList];
    //console.log("list befr.." + JSON.stringify(tempData));
    // console.log("status... + " + status);
    // console.log("this.props..." + JSON.stringify(this.props))
    // console.log("status props.." + this.props.match.params.status);
    if (
      this.props.match.params.status == "all-tenders" ||
      this.props.location.pathname == "/tenders"
    ) {
      this.setState({ filteredTendersList: this.state.data.tendersList });
    } else {
      var filterData = this.state.data.tendersList.filter(
        (filteredData) => filteredData.status == status
      );

      // var temp= this.state.data;
      // temp.tendersList = filterData;
      if (filterData) {
        this.setState({
          filteredTendersList: filterData
        });
        console.log("filterData..." + JSON.stringify(filterData));
      }
    }
    // console.log("filter list.." + JSON.stringify(filterData));
    // //onsole.log("list... + " + JSON.stringify(tempData))
    // console.log("temp..." + JSON.stringify(temp))
  };
  onSelectChange = (selectedRowKeys) => {
    console.log("selectedRowKeys changed: ", selectedRowKeys);
    this.setState({ selectedRowKeys });
  };
  handleSortChange = (selected) => {
    console.log("selected value.." + selected)
    this.setState({ selectedFieldValue: selected });
  };
  handleTimePeriodChange = (selected) => {
    console.log("selected value.." + selected)
    this.setState({ selectedTimePeriodValue: selected });
  }
  handleOpenFilterChange = (selected) => {
    console.log("selected value.." + selected)
    this.setState({ selectedOpenValue: selected });
  }
  handleMoreFilterChange = (selected) => {
    console.log("selected value.." + selected)
    this.setState({ selectedMoreFilterValue: selected });
  }

  render() {
    //console.log("inside render page....tender" + JSON.stringify(this.props))

    let {
      data,
      columns,
      sort,
      open,
      timePeriod,
      moreFilters,
      filteredTendersList,
      selectedRowKeys
    } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    };
 
    return (
      <div>
        <div className="ml-3 mr-3">
          <div>
            <Row>
              <Col xs="12" sm="6" lg="8" className="tenderHeading">
                Tenders List
              </Col>
              <Col sm="3" md="2" lg="2">
                <Link to="/Dashboard"><Button variant="link" label="Go Back"></Button></Link>
              </Col>
              <Col sm="3" md="3" lg="2">
                <Link to="/tenders/create">
                  <Button variant="primary" label="Create Tender"></Button>
                </Link>
              </Col>
            </Row>
          </div>
          <div className="cardDiv">
            <Row>
              <Col md="4" lg="3" sm="8" className="mb-3">
                <CardComponent
                  classNames={{
                    cardStyle: "cardStyle",
                    cardTitleStyle: "cardTitleStyle",
                    cardSubTitleStyle: "cardSubTitleStyle"
                  }}
                  title="0"
                  subTitle="Issued Tenders"
                />
              </Col>
              <Col md="4" lg="3" sm="8" className="mb-3">
                <CardComponent
                  classNames={{
                    cardStyle: "cardStyle",
                    cardTitleStyle: "cardTitleStyle",
                    cardSubTitleStyle: "cardSubTitleStyle"
                  }}
                  title="0"
                  subTitle="Awarded Packages"
                />
              </Col>
              <Col md="4" lg="3" sm="8" className="mb-3">
                <CardComponent
                  classNames={{
                    cardStyle: "cardStyle",
                    cardTitleStyle: "cardTitleStyle",
                    cardSubTitleStyle: "cardSubTitleStyle"
                  }}
                  title="1"
                  subTitle="Approved"
                />
              </Col>
            </Row>
          </div>
          <div>
            <div className="tenderListTableHeading">List of Tenders</div>
          </div>
          <Row className="d-flex" className="filterStyle">
            <Col md="6" className="pr-0">
              <InputGroupField
                addOnType="prepend"
                iconName="search"
                type="text"
                name="search"
                id="searchField"
                placeholder="Filter Tender"
              />
            </Col>
            <Col md="2" className="pr-0 pl-0">
              <InputField
                type="select"
                name="open"
                id="open"
                placeholder="Open"
                classname="inputStyle"
                option={open}
                handleInputChange={this.handleOpenFilterChange}

              />
            </Col>
            <Col md="2" className="pr-0 pl-0">
              <InputField
                type="select"
                name="open"
                id="open"
                placeholder="Open"
                classname="inputStyle"
                option={timePeriod}
                handleInputChange={this.handleTimePeriodChange}

              />
            </Col>
            <Col md="2" className="pl-0">
              <InputField
                type="select"
                name="open"
                id="open"
                placeholder="Open"
                classname="inputStyle"
                option={moreFilters}
                handleInputChange={this.handleMoreFiltersChange}

              />
            </Col>
          </Row>
          <div className="mt-5 numbersOfTenderDiv">
            <Row>
              <Col md="9">Showing 50 vendors</Col>
              <Col md="3">
                <InputField
                  type="select"
                  name="open"
                  id="open"
                  placeholder="Sort"
                  classname="inputStyle"
                  option={sort}
                  size="1"
                  handleInputChange={this.handleSortChange}
                />
              </Col>
            </Row>
          </div>
        </div>

        <div className="mt-4 tableDiv">
          <Table
            dataSource={filteredTendersList}
            columns={columns}
            size="small"
            pagination={false}
            ref={this.tableRef}
            rowSelection={rowSelection}
            onRow={(record, rowIndex) => {
              return {
                onClick: (event) => {
                  this.OnRowClick(record, rowIndex);
                }
              };
            }}
            rowClassName={(record) => {
               console.log("record.." +JSON.stringify(record))  
               return (record ? "tableRow" : "")}}
          />
        </div>
      </div>
    );
  }
}
export default withRouter(AllTenders);
