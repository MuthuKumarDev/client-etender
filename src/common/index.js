import cookie from "react-cookies";

export const isBuyer = () => {
  //let userRole = localStorage.getItem("emailId");
  let userRole = localStorage.getItem("userRole");
  //console.log("userRole..." + userRole);
  var isLoggedInFromBuyer = false;
  if (userRole && userRole == "buyer") {
    isLoggedInFromBuyer = true;
  } else if (userRole && userRole == "vendor") {
    isLoggedInFromBuyer = false;
  }
  return isLoggedInFromBuyer;
};

export const isLoggedIn = () => {
  //let userRole = localStorage.getItem("emailId");
  let isLoggedIn = !!localStorage.getItem("token") || !!cookie.load('token');
  console.log('is user logged in ', isLoggedIn);
  return isLoggedIn;
};
