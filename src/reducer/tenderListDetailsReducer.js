const initialState = {
  tenderListCount: '',
};
export const tenderListDetailsReducer = (state = initialState, action) => {
  // console.log("inside tender list..." + action.payload)
  if (action.type === "SET_TENDER_LIST_COUNT") {
    return { ...state, ...{ tenderListCount: action.payload } };
  }
  return state;
};
