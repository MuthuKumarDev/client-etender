import { tenderListDetailsReducer } from "./tenderListDetailsReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  tenderListDetails: tenderListDetailsReducer,
});

export default rootReducer;
