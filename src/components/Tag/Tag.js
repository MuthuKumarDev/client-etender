import React, { Component } from "react";
import { Label } from "reactstrap";

export class Tag extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { tagContent, icon } = this.props;
    return (
      <div>
        <div className="tagMainDiv">
          {tagContent} {icon}
        </div>
      </div>
    );
  }
}
