// Core - SideBar Component
import React, { PureComponent } from "react";
import isEqual from "react-fast-compare";
import { IconComponent } from "../Icons";
import { Link } from "react-router-dom";
import history from "../../history";
import API from "../../api/api";
import { BadgeComponent } from "../Badge";
import { Collapse } from "reactstrap";
import { connect } from "react-redux";
import { isBuyer } from "../../common";

const SidebarCompany = () => {
  return (
    <div className="sid-cpy">
      <div className="sid-cpy-icn">
        {<IconComponent name="company" />}
        {/* <Image className="sid-cpy-img" src="/images/e-tender-buyer-logo.png" alt="logo"/> */}
      </div>
      <div className="sid-cpy-nme">Company</div>
    </div>
  );
};

const SideBarUser = () => {
  return (
    <div className="sid-usr">
      <div className="sid-usr-icn">{<IconComponent name="settings" />}</div>
      <div className="sid-itm-tit">Settings</div>
    </div>
  );
};

// Main Component
class Sidebar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isClicked: false,
      menuTitle: "",
      selectedSubMenuIndex: "",
      noOfTenders: "",
      isOpen: true
    };
  }

  onMenuClick = (title) => {
    // console.log("inside click" + title);
    this.setState({ selectedSubMenuIndex: "" });
    if (title == "Tender") {
      this.setState({ isClicked: true, menuTitle: title });
    } else if (title == "Vendors") {
      this.setState({ isClicked: true, menuTitle: title });
    } else {
      this.setState({ isClicked: false, menuTitle: "" });
    }
    this.setState({ menuTitle: title });
  };

  onSubMenuClick = (selectedSubMenuIndex) => {
    //console.log("submenuPath..." + submenuPath);
    this.setState({ selectedSubMenuIndex: selectedSubMenuIndex });
  };

  SidebarItems = (items) => {
    let {
      isClicked,
      menuTitle,
      subMenu,
      selectedSubMenuIndex,
      noOfTenders
    } = this.state;
    return (
      <>
        <div className="sid-lit">
          {items.map((item) => (
              item && item.showOnlyBuyer && !isBuyer()?"":(
                <div>
                  <div>
                    <Link
                      to={`/${item.path}`}
                      onClick={() => this.onMenuClick(item.title)}
                    >
                      <div
                        className={menuTitle == item.title ? "sid-slc" : "sid-itm"}
                      >
                        <div
                          className={
                            this.state.isOpen
                              ? "sid-itm-icn"
                              : "sid-itm-icn siditmicn"
                          }
                        >
                          {<IconComponent name={item.title} />}
                        </div>
                        {this.state.isOpen ? (
                          <div className="sid-itm-tit">
                            {item.title == "Tenders" ? (
                              <>
                                <span>{item.title}</span>{" "}
                                {this.state.isOpen && noOfTenders && (
                                  <span className="noOfTendersStyle">
                                    <BadgeComponent
                                      value={
                                        <span className="noOftendersText">
                                          {/* {noOfTenders ? noOfTenders : ""} */}
                                          {this.props &&
                                          this.props.tenderListDetails &&
                                          this.props.tenderListDetails.tenderListCount
                                            ? this.props.tenderListDetails.tenderListCount
                                            : noOfTenders}
                                        </span>
                                      }
                                      badgeStyles="noOfTextBadge"
                                    />
                                  </span>
                                )}
                              </>
                            ) : (
                              item.title
                            )}{" "}
                          </div>
                        ) : (
                          ""
                        )}
                        {/* <div>{item.submenuTitle}</div> */}
                      </div>
                    </Link>{" "}
                  </div>
                  <div>
                    <div>
                      {menuTitle == item.title && (
                        <div>
                          {item.submenu &&
                            item.submenu.map((submenuItem, index) => (
                              <Link
                                to={`/${item.path}/${submenuItem.path}`}
                                style={{ color: "#fff" }}
                                className={
                                  index == selectedSubMenuIndex
                                    ? "sid-sub-slc"
                                    : "sid-sub-itm"
                                }
                                onClick={() => this.onSubMenuClick(index)}
                              >
                                {isClicked == true &&
                                  item.title == menuTitle &&
                                  submenuItem.submenuTitle}
                              </Link>
                            ))}
                        </div>
                      )}
                    </div>
                    {/* ))} */}
                  </div>
                </div>
              ) 
          ))}
          <div className={this.state.isOpen ? "sid-usr" : "sid-usr sidusr"}>
            <div
              className={
                this.state.isOpen ? "sid-usr-icn" : "sid-usr-icn sid-usr-icon"
              }
            >
              {<IconComponent name="settings" />}
            </div>
            {this.state.isOpen ? (
              <div className="sid-itm-tit">Settings</div>
            ) : (
              ""
            )}
          </div>
        </div>
      </>
    );
  };
  componentDidMount = () => {
    // console.log("params.." + JSON.stringify(params));
    let params = {
      latest_addition: "desc"
    };
    API.fetchData(API.Query.All_TENDERS_FILTERS, params).then((data) => {
      // console.log(
      //   "tenders list new in sidebar..",
      //   JSON.stringify(data.tender.length)
      // );
      if (data && data.tender && data.tender.length > 0) {
        this.setState({ noOfTenders: data.tender.length });
      }
    });
  };

  onExapandAndCollapseClick = () => {
    this.props &&
      this.props.onCollapseClick &&
      this.props.onCollapseClick(!this.state.isOpen);
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    console.log("this.props." + JSON.stringify(this.props));
    if (this.props && this.props.tenderListDetails) {
      console.log(
        "tenderListDetails.. " + JSON.stringify(this.props.tenderListDetails)
      );
    }

    const items = [
      {
        id: "item-1",
        title: "Home",
        path: "dashboard"
      },
      {
        id: "item-2",
        title: "Tenders",
        path: "tenders"
        // submenu: [
        //   {
        //     title: "Tender",
        //     submenuTitle: "All Tenders",
        //     path: "all-tenders"
        //   },
        //   {
        //     title: "Tender",
        //     submenuTitle: "Issued",
        //     path: "issued"
        //   },
        //   {
        //     title: "Tender",
        //     submenuTitle: "Drafts",
        //     path: "drafts"
        //   },
        //   {
        //     title: "Tender",
        //     submenuTitle: "Closed",
        //     path: "closed"
        //   }
        // ]
      },
      {
        id: "item-3",
        title: "Vendors",
        path: "vendors",
        showOnlyBuyer: true,
        submenu: [
          {
            title: "Vendors",
            submenuTitle: "Verified",
            path: "verified"
          },
          {
            title: "Vendors",
            submenuTitle: "Un-Verified",
            path: "un-verified"
          }
        ]
      },
      {
        id: "item-4",
        title: "Tasks",
        path: "Tasks"
      },
      {
        id: "item-5",
        showOnlyBuyer: true,
        title: "Analytics",
        path: "analytics"
      },
      {
        id: "item-6",
        title: "Auctions",
        path: "auctions"
      },
      {
        id: "item-7",
        title: "Resource Library",
        path: "resourcelibrary"
      }
    ];

    return (
      <div>
        <div>
          <div
            className={
              this.state.isOpen ? "sidebarExpandCollapse" : "sidebarCollapse"
            }
            onClick={this.onExapandAndCollapseClick}
          >
            <div className="sidebarIconMainDiv">
              <IconComponent name="left_arrow_with_background" />
            </div>
          </div>
        </div>
        {/* <Collapse isOpen={this.state.isOpen}> */}
        {this.state.isOpen ? (
          <div className="sid">
            <div className="sid-cnt">
              <div style={{ height: "100%" }}>
                <SidebarCompany />
                {/* <SidebarItems items={items} /> */}{" "}
                {this.SidebarItems(items)}
                {/* <SideBarUser /> */}
              </div>
            </div>
          </div>
        ) : (
          <div className="sid sidemenu">
            <div className="sid-cnt">
              <div style={{ height: "100%" }}>
                <SidebarCompany isOpen={this.state.isOpen} />
                {/* <SidebarItems items={items} /> */}{" "}
                {this.SidebarItems(items)}
                {/* <SideBarUser /> */}
              </div>
            </div>
          </div>
        )}
        {/* </Collapse> */}
      </div>
    );
  }
}

Sidebar.propTypes = {};

Sidebar.defaultProps = {};

// export default React.memo(Sidebar, isEqual);

const mapStateToProps = (globalState) => {
  return {
    tenderListDetails: globalState.tenderListDetails
  };
};

export default connect(mapStateToProps, null)(Sidebar);
