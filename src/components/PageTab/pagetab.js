// PageTab Component
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import clsx from "clsx"; 

export default class PageTab extends PureComponent {
  static get propTypes() {
    return {
      activeMenu: PropTypes.string,
      onChangeTab: PropTypes.func
    };
  }

  static get defaultProps() {
    return {
      activeMenu: "",
      onChangeTab: null
    };
  }

  /* OnClick Function */
  _onChangeTab = (id) => {
    const { onChangeTab } = this.props;
    if (onChangeTab) {
      onChangeTab(id);
    }
  };

  _renderTitle(title, id) {
    const { activeMenu } = this.props;
    const cls = {
      pge: true,
      "pge-act": id === activeMenu
    };

    return (
      <div className={`${clsx(cls)}`} onClick={() => this._onChangeTab(id)}>
        {title}
      </div>
    );
  }

  /* Main Render Functions */
  render() {
    return (
      <div className="pge-tab">
        {this._renderTitle("Tender Overview", "tenderOverview")}&emsp;
        {this._renderTitle("Tender Terms", "tenderTerms")}&emsp;
        {this._renderTitle("Technical", "technical")}&emsp;
        {this._renderTitle("Addendum", "tddendum")}&emsp;
      </div>
    );
  }
}
