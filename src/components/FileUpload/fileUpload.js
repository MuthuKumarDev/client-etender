// File Upload Component
import React from "react";
import { IconComponent } from "../Icons";
import S3 from "react-aws-s3";

export default class FileUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      fileName: [],
      uploadData: []
    };
  }
  handleFileChange = (e) => {
    let { fileName, uploadData } = this.state;
    console.log(e.target.files[0]);
    for (let i = 0; i < e.target.files.length; i++) {
      console.log("file Name", e.target.files[i].name);
      let fileType = e.target.files[i].name;
      var allowedExtensions =  
/(\.exe)$/i;
      if(!allowedExtensions.exec(fileType)){
      const config = {
        bucketName: process.env.REACT_APP_BUCKET_NAME,
        region: process.env.REACT_APP_REGION,
        accessKeyId: process.env.REACT_APP_ACCESS_ID,
        secretAccessKey: process.env.REACT_APP_ACCESS_KEY,
      };
      let newFileName = e.target.files[i].name.replace(/\..+$/, "");
      const ReactS3Client = new S3(config);
      ReactS3Client.uploadFile(e.target.files[i], newFileName).then((data) => {
        if (data.status === 204) {
          console.log(data);
          let fileobj={
            key:data.key,
            name: data.key,
            url:data.location
          }
          uploadData.push(fileobj);
          this.setState({uploadData});
        } else {
          console.log("fail");
        }
      });
      fileName.push(e.target.files[i]);
    } else (
      alert("invalid type File exe")
    )
  }
    this.setState({ fileName });
    this.props.onChange(e.target.files, uploadData);
    console.log("props onChange", this.props.handleFileChange);
  };
  handleUpload = (file) => {
    const config = {
      bucketName: process.env.REACT_APP_BUCKET_NAME,
      dirName: process.env.REACT_APP_DIR_NAME /* optional */,
      region: process.env.REACT_APP_REGION,
      accessKeyId: process.env.REACT_APP_ACCESS_ID,
      secretAccessKey: process.env.REACT_APP_ACCESS_KEY,
    };
    let newFileName = file.name.replace(/\..+$/, "");
    const ReactS3Client = new S3(config);
    ReactS3Client.uploadFile(file, newFileName).then((data) => {
      if (data.status === 204) {
        // console.log("success");
        console.log(data);
      } else {
        // console.log("fail");
      }
    });
  };

  handleClick = (event) => {
    if(this.props.onClick){
      this.props.onClick(event);
    }
    
  };

  handleDelete = (index) => {
    console.log("file index", index);
    let { fileName } = this.state;
    fileName.splice(index, 1);
    this.setState({ fileName });
  };

  render() {
    let { fileName } = this.state;
    // let { fileLists } = this.props;
    console.log("file", fileName);
    // console.log("file from props", fileLists);
    // const fileInput = React.useRef();
    let { fromVendors } = this.props;
    return (
      <div>
        <div class="container">
          <form action="">
            <div>
              <label class="lbl" for="upload">
                <input
                  type="file"
                  id="upload"
                  multiple
                  onChange={this.handleFileChange}
                  onClick={this.handleClick}
                />
                <label className="drp-fil" for="upload">
                   {/* {fromVendors ? <div className="addFile">Add file</div> : <div>Add files(s)</div>}  */}
                &ensp;Add files(s)<br></br>
                <br></br> &emsp;&emsp;or<br></br>
                  Drop files here
                </label>
                <label class="" for="upload">
                </label>
              </label>
            </div>
            <div class="files">
              {/* <div class="fle-hed">Files Selected</div> */}
              {fileName.map((item, index) => {
                return (
                  <div class="fle-hed-lst">
                    <ul>{item.name}</ul>
                    <span onClick={() => this.handleDelete(index)}>
                      <IconComponent name="close" />
                    </span>
                  </div>
                );
              })}
            </div>
          </form>
        </div>
      </div>
    );
  }
}