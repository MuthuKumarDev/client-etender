import React, { Component } from "react";
import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";

export class TabComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 1
    };
  }

  toggle = (tab) => {
    this.setState({ activeTab: tab });
    this.props && this.props.onTabChange && this.props.onTabChange(tab);
  };

  render() {
    const { activeTab } = this.state;
    const {
      tabData,
      tabContent,
      tabBusinessDetailsContent,
      tabContentAddressDetails
    } = this.props;
    // const { tabData, tabContent,  } = this.props;
    return (
      <div className="tabMainDiv">
        <Nav tabs className="navTab">
          {tabData.map((tab) => {
            return (
              <NavItem>
                <NavLink
                  active={this.props.activeTab == tab.id}
                  onClick={() => {
                    this.toggle(tab.id);
                  }}
                  className={this.props.activeTab == tab.id ? "activeTab heading" : "tab heading"}
                >
                  {tab.label}
                </NavLink>
              </NavItem>
            );
          })}
        </Nav>
        <TabContent activeTab={this.props.activeTab}>
          {/* {this.state.activeTab == "1" && (
            <TabPane tabId={this.state.activeTab}>{tabContent}</TabPane>
          )}
          {this.state.activeTab == "2" && ( */}
          <TabPane tabId={this.props.activeTab}>{tabContent}</TabPane>
          {/* )} */}
          {/* {this.state.activeTab == "3" && (
            <TabPane tabId={this.state.activeTab}>
              {tabContentAddressDetails}
            </TabPane>
          )} */}
        </TabContent>
      </div>
    );
  }
}
