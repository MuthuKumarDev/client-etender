import React, { Component } from "react";
import { Input, InputGroup, InputGroupText, InputGroupAddon } from "reactstrap";
import { InputField } from "../InputField";
//import SearchIcon from "@material-ui/icons/Search";
import search_icon from "../../svgs/search_icon.svg";
import { ReactComponent as Search } from "../../svgs/search_icon.svg";

export class InputGroupField extends Component {
  constructor(props) {
    super(props);
  }
  handleInputChange = (value) => {
    this.props&& this.props.handleInputChange&& this.props.handleInputChange(value);
  }
  onSearchIconClick = () => {
    //this.
    this.props && this.props.onSearchIconClick && this.props.onSearchIconClick()
  }

  render() {
    const { addOnType, iconName, type, name, id, placeholder, value, classNames } = this.props;
    return addOnType && addOnType == "prepend" ? (
      <InputGroup>
        <InputGroupAddon addonType={addOnType}>
          <InputGroupText>
            {/* <SearchIcon /> */}
            {iconName == "search" ? <div onClick={this.onSearchIconClick}><Search /></div> : ""}
          </InputGroupText>
        </InputGroupAddon>
        <InputField
          type={type}
          name={name}
          id={id}
          placeholder={placeholder}
          classname=""
          handleInputChange={this.handleInputChange}
          value={value? value: ''}
        />
      </InputGroup>
    ) : (
      <InputGroup>
        <InputField
          type={type}
          name={name}
          id={id}
          placeholder={placeholder}
          classname={classNames? classNames : ''}
          handleInputChange={this.handleInputChange}
          value={value? value: ''}
        />
        <InputGroupAddon addonType={addOnType}>
          <InputGroupText>
            {/* <SearchIcon /> */}
            {iconName == "search" ? <div onClick={this.onSearchIconClick}><Search /></div> : ""}
          </InputGroupText>
        </InputGroupAddon>
      </InputGroup>
    );
  }
}
