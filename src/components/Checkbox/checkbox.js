// Core - CheckBox Component
import React, { useState } from "react";
import PropTypes from "prop-types";
import { Checkbox } from "reakit/Checkbox";
import isEqual from "react-fast-compare";
import clsx from "clsx";

import { addToClass } from "../../helper/styles";

// Main Component
const CheckBox = (props) => {
  const {
    variant,
    name,
    title,
    value,
    isInvalid,
    isDisabled,
    isRequired,
    ariaLabel,
    ariaBy,
    onChange,
    isChecked,
    onCheckboxChange
  } = props;

  const cls = {
    chk: true
  };

  addToClass(
    cls,
    {
      checkstyle: "checkboxStyle",
      normal: "nrm-chk",
      defaultCheckbox: "defaultCheckbox"
    },
    variant
  );

  const [checked, setChecked] = useState(isChecked ? isChecked : false);
  // const toggle = () => setChecked(!checked);
  const toggle= () =>{
    onCheckboxChange&& onCheckboxChange(!checked);
    setChecked(!checked);
  }

  return (
    <div className="labelStyle">
      <Checkbox className={clsx(cls)} checked={checked} onChange={toggle} disabled={isDisabled? isDisabled: false} />
      <span className="chk-box">&nbsp;</span>
      {title && <span className="chk-tit">{title}</span>}
      <span className="chk-box">&nbsp;</span>
    </div>
  );
};

CheckBox.propTypes = {
  variant: PropTypes.oneOf(["normal", "checkstyle", "defaultCheckbox"]),
  name: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.bool,
  ariaLabel: PropTypes.string,
  ariaBy: PropTypes.string,
  onChange: PropTypes.func
};

CheckBox.defaultProps = {
  variant: "normal",
  name: "",
  title: "",
  value: false,
  ariaLabel: "",
  ariaBy: "",
  onChange: null
};

export default React.memo(CheckBox, isEqual);
