// Header Component

import React, { useState } from "react";
import PropsTypes from "prop-types";
import isEqual from "react-fast-compare";
import { Input } from "../Input";
import { Avatar } from "../Avatar";
import { Image } from "../Image";
import { InputGroupField } from "../../components/InputGroupField";
import { Breadcrumb } from "../../components/Breadcrumb";

import { Popover, UncontrolledPopover, PopoverHeader, ListGroup, ListGroupItem , Button} from "reactstrap";
import UserApi from "../../api/user";
import { IconComponent } from "../Icons";
import cookie from "react-cookies";
import { isLoggedIn, isBuyer } from "../../common";
import './style.scss';

// Main Component
const HeaderComponent = (props) => {
  const [popoverOpen, setPopoverOpen] = useState(false);
  const [filter, setFilter] = useState("");
  const toggle = () => setPopoverOpen(!popoverOpen);
  const isLogged = isLoggedIn();
  const user = ()=>localStorage.getItem('emailId');
  console.log("props", props);

  const onLogout = () => {
    const { history } = props;

    history.push("/login");
    localStorage.clear();
    cookie.remove('token', { path: '/' })

    // UserApi.logout()
    //   .then(() => {
    //     localStorage.clear();
    //     history.push("/login");
    //   })
    //   .catch((err) => {
    //     // notification.err('Err - Logout Info', err.message);
    //     console.log(err.message);
    //   });
  };

  const onChangeInput = (value) => {
    setFilter(value);
  };

  return (
    <div className="hed-wrp">
      <div className="hed-cnt">
        <div className="hdr-tlt">
          <div className="hdr-lay">
            <Breadcrumb  />
          </div>
        </div>
        <div className="hdr-int">
          <InputGroupField
            addOnType="append"
            iconName="search"
            type="text"
            name="search"
            id="searchField"
            placeholder="Search"
            handleInputChange={onChangeInput}
            value={filter}
            classNames="searchInput"
          />
        </div>
        <div className="hdr-det">
          {  isLogged && ( <div className="d-flex">
                <div className="hdr-usr">
                  <div className="hdr-prf"> {isBuyer()?`Buyer Procuerment`:`Vendor`}</div>
                  <div className="hdr-prf"> {user}</div>
                  <div className="hdr-edt">Edit Profile</div>
                </div>
                <div className="hdr-atr user-profile-avatar" >
                  <Avatar image="/images/user_icon.jpg" size="normal"/>
                </div>
                <Button className="mt-2" type="button" style={{width:'auto'}} id="userMenu" size="xs" color="white">
                  <img
                    width={20}
                    height={15}
                    src="/images/dropdown_image.png"
                    alt="dropdownimage"
                  />
                </Button>
                <UncontrolledPopover
                    placement="bottom"
                    trigger="legacy"
                    target="userMenu"
                  >
                    <PopoverHeader>
                      <ListGroup>
                        <ListGroupItem tag="button" action className="lst-grp1">
                          <IconComponent
                            name="profile"
                            fill="#B7BCC0"
                            width="20"
                            height="20"
                          />
                          &ensp; Edit Profile
                        </ListGroupItem>
                        <ListGroupItem tag="button" action className="lst-grp1">
                          <IconComponent
                            name="menusettings"
                            fill="#B7BCC0"
                            width="20"
                            height="20"
                          />
                          &ensp; Settings
                        </ListGroupItem>
                        <ListGroupItem tag="button" action onClick={onLogout} className="lst-grp1">
                          <div>
                            <IconComponent
                              name="logout"
                              fill="#B7BCC0"
                              width="20"
                              height="20"
                            />
                            &ensp;SignOut
                          </div>
                        </ListGroupItem>
                      </ListGroup>
                    </PopoverHeader>
                  </UncontrolledPopover>
              </div>)}
          </div>
      </div>
    </div>
  );
};

HeaderComponent.PropsTypes = {
  name: PropsTypes.string
};

HeaderComponent.defaultProps = {
  name: ""
};

export default React.memo(HeaderComponent, isEqual);
