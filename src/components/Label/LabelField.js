import React, { Component } from "react";
import { Label } from "reactstrap";

export class LabelField extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { label } = this.props;
    return <Label>{label}</Label>;
  }
}
