// eading Component
import React from "react";
import PropTypes from "prop-types";
import isEqual from "react-fast-compare";
import clsx from "clsx";

import { addToClass } from "../../helper/styles";

// Main Component
const Heading = (props) => {
  const { variant, title, isI18n, hasNoGap, color } = props;

  const cls = {
    hed: true,
    clr: true,
    "hed-ngp": hasNoGap
  };

  addToClass(
    cls,
    {
      h1: "hed-hd1",
      h2: "hed-hd2",
      h3: "hed-hd3",
      h4: "hed-hd4",
      h5: "hed-hd5",
      h6: "hed-hd6",
      h7: "hed-hd7",
      h8: "hed-hd8"
    },
    variant,
    ""
  );

  addToClass(
    cls,
    {
      normal: "clr-normalfont",
      style: "clr-stylefont"
    },
    color,
    ""
  );

  return React.createElement(variant, { className: clsx(cls) }, title);
};

Heading.propTypes = {
  variant: PropTypes.oneOf(["h1", "h2", "h3", "h4", "h5", "h6", "h7", "h8"]),
  color: PropTypes.oneOf(["normal", "style"]),
  title: PropTypes.string.isRequired,
  hasNoGap: PropTypes.bool
};

Heading.defaultProps = {
  variant: "h1",
  color: "normal",
  title: "",
  hasNoGap: false
};

export default React.memo(Heading, isEqual);
