// Modal Component
import React, { useRef, useState, useCallback } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { Dialog, DialogBackdrop } from "reakit/Dialog";
import { addToClass } from "../../helper/styles";

const ModalBlock = ({ children }) => {
  return (
    <div className="mdl-blk" data-testid="mdl-blk">
      {children}
    </div>
  );
};

ModalBlock.propTypes = {
  children: PropTypes.node
};

ModalBlock.defaultProps = {
  children: null
};

const ModalContent = ({ children, gapsSize }) => {
  const cls = {
    "mdl-cnt": true
  };

  addToClass(
    cls,
    {
      small: "mdl-cnt-gsm",
      none: "mdl-cnt-gno"
    },
    gapsSize,
    "normal"
  );

  return (
    <div className={clsx(cls)} data-testid="mdl-cnt">
      {children}
    </div>
  );
};

ModalContent.propTypes = {
  gapsSize: PropTypes.oneOf(["normal", "small", "none"]),
  children: PropTypes.node
};

ModalContent.defaultProps = {
  gapsSize: "normal",
  children: null
};

const useModal = (defaultState = {}) => {
  const [state, setState] = useState({ isOpen: false, ...defaultState });
  const show = useCallback(
    (valueObj = {}) => {
      setState((prevState) => ({
        ...prevState,
        ...valueObj,
        isOpen: !state.isOpen
      }));
    },
    [setState]
  );

  const hide = useCallback(() => {
    setState((prevState) => ({ ...prevState, isOpen: false }));
  }, [setState]);
  return {
    isOpen: state.isOpen,
    hide,
    show
  };
};

// Main Component
const Modal = React.forwardRef((props, ref) => {
  const { children, isOpen, size, hide, ariaLabel } = props;
  const dialogRef = useRef();
  const cls = {
    mdl: true,
    "mdl-shw": isOpen
  };

  addToClass(
    cls,
    {
      small: "mdl-sml",
      medium: "mdl-med",
      big: "mdl-big",
      full: "mdl-full"
    },
    size,
    "normal"
  );

  const dialogState = {
    visible: isOpen,
    animated: true,
    modal: true,
    baseId: "base"
  };

  const childrenWithProps = React.Children.map(children, (child) =>
    React.cloneElement(child, { hide })
  );

  return (
    <DialogBackdrop className="mdl-bck" data-testid="mdl-bck" {...dialogState}>
      <Dialog
        data-testid="mdl"
        tabIndex={0}
        ref={dialogRef}
        aria-label={ariaLabel}
        className={clsx(cls)}
        preventBodyScroll
        unstable_finalFocusRef
        hideOnEsc={false}
        hideOnClickOutside={true}
        hide={hide}
        {...dialogState}
      >
        {childrenWithProps}
      </Dialog>
    </DialogBackdrop>
  );
});

Modal.propTypes = {
  size: PropTypes.oneOf(["normal", "small", "medium", "big", "full"]),
  isOpen: PropTypes.bool,
  ariaLabel: PropTypes.string,
  hide: PropTypes.func,
  children: PropTypes.node
};

Modal.defaultProps = {
  size: "normal",
  isOpen: false,
  ariaLabel: "Dialog",
  hide: null,
  children: null
};

Modal.Content = ModalContent;
Modal.Block = ModalBlock;

export { useModal, Modal };
export default Modal;
