import React, { Component } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Button } from "../../components/Button";

export class ModalComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //isModel: true
    };
  }
  toggle = () => {
    //this.useState({ isModel: !this.state.isModel });
    this.props &&
      this.props.onModalClick &&
      this.props.onModalClick(!this.props.isOpen);
  };

  render() {
    //console.log("this.props.isOpen,," + this.props.isOpen);
    const { isOpen, heading, modalBody, headerStyle } = this.props;
    return (
      <div>
        <Modal isOpen={isOpen} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle} className={headerStyle}>
            {heading}
          </ModalHeader>
          <ModalBody>{modalBody}</ModalBody>
          <ModalFooter>
            <div className="d-flex">
              <Button
                variant="primary"
                onButtonClick={this.toggle}
                label="Save"
              ></Button>{" "}
            </div>
            <div className="d-flex">
              <Button
                variant="linkbtn"
                onButtonClick={this.toggle}
                label="Cancel"
              ></Button>
            </div>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
