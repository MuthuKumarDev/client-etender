// Text Editer Component
import React, { useState } from "react";
import PropTypes from "prop-types";
import isEqual from "react-fast-compare";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

// Main Component
const TextEditer = (props) => {

  return (
    <div>
      <ReactQuill value={props.value} onChange={props.onChange} />
    </div>
  );
};

TextEditer.PropTypes = {};

TextEditer.defaultProps = {};

export default React.memo(TextEditer, isEqual);
