import React, { Component } from "react";
import { Badge } from "reactstrap";
import isEqual from "react-fast-compare";
import { Multiselect } from "multiselect-react-dropdown";

class MultiSelectComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.childRef = React.createRef();
  }
  onSelect = (value, name) => {
    console.log("onSelectValue..." + value);
    this.props && this.props.onSelect(value, name);
  };
  onRemove = (value, name) => {
    //console.log("onSelectValue..." + value);
    this.props && this.props.onRemove(value, name);
  };

  render() {
    const {
      options,
      issingleSelect,
      displayValue,
      showCheckbox,
      styles,
      singleSelect,
      name,
      ref,
      selectedValues
    } = this.props;
    //console.log("options.." + JSON.stringify(options));
    return (
      <Multiselect
        options={options}
        displayValue={displayValue}
        showCheckbox={showCheckbox}
        // placeholder="Select"
        onSelect={(e) => this.onSelect(e, name)}
        style={styles}
        //closeIcon=""
        singleSelect={singleSelect}
        hidePlaceholder={true}
        closeOnSelect={false}
        ref={this.childRef}
        avoidHighlightFirstOption={true}
        selectedValues={selectedValues && selectedValues}
        onRemove={(e) => this.onRemove(e, name)}
        // emptyRecordMsg=""
        // onSearch={(e) => this.onSearch(e)}
      />
    );
  }
}

export default React.memo(MultiSelectComponent, isEqual);
