// Core - ButtonGroup Component
import React from "react";
import PropTypes from "prop-types";
import { Button } from "../Button";
import { ButtonGroup } from "reactstrap";

// Main Component
const Buttongroup = (props) => {
  const { ariaLabel, role, size, vertical, variant, children } = props;

  return (
    <ButtonGroup size={size} vertical={vertical}>
      {children}
    </ButtonGroup>
  );
};

Buttongroup.propTypes = {
  ariaLabel: PropTypes.string,
  className: PropTypes.string,
  role: PropTypes.string,
  size: PropTypes.oneOf("lg", "sm", "md"),
  vertical: PropTypes.bool,
  children: PropTypes.node
};

Buttongroup.defaultProps = {
  ariaLabel: "",
  className: "",
  role: "",
  size: "lg",
  vertical: false,
  children: null
};

export default Buttongroup;
