import React, { Component } from "react";
import isEqual from "react-fast-compare";
import {
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem
} from "reactstrap";
import { IconComponent } from "../Icons";

class DropDownComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  toggleItem = (e, value, name) => {
    this.props && this.props.toggleItem && this.props.toggleItem(value, name);
  };
  toggleItemForMoreFilter = (e, value, name) => {
    this.props &&
      this.props.toggleItemForMoreFilter &&
      this.props.toggleItemForMoreFilter(value, name);
  };
  toggle = () => {
    this.props && this.props.toggle && this.props.toggle(this.props.name);
  };
  toggleForMoreFilter = () => {
    //console.log("inside toggle add more..")
    this.props &&
      this.props.toggleForMoreFilter &&
      this.props.toggleForMoreFilter(this.props.name);
  };
  render() {
    const {
      options,
      isOpen,
      dropdownVal,
      direction,
      size,
      classNames,
      name,
      fromAddMore,
      icon
    } = this.props;
    //console.log("fromAddMore.." + fromAddMore);

    return (
      <Dropdown
        isOpen={isOpen}
        toggle={fromAddMore ? this.toggleForMoreFilter : this.toggle}
        size={size}
      >
        <DropdownToggle
          className={
            classNames && classNames.dropdownStyle && classNames.dropdownStyle
          }
        >
          {" "}
          {icon && <span className="iconStyle">{icon}</span>}
          {dropdownVal && dropdownVal ? dropdownVal : ""}
        </DropdownToggle>
        <DropdownMenu
          className={
            classNames && classNames.dropDownMenu && classNames.dropDownMenu
          }
        >
          {options &&
            options.length > 0 &&
            options.map((option) => {
              return (
                <DropdownItem
                  className={
                    classNames &&
                    classNames.dropDownItem &&
                    classNames.dropDownItem
                  }
                  onClick={
                    fromAddMore
                      ? (e) => this.toggleItemForMoreFilter(e, option.key, name)
                      : (e) => this.toggleItem(e, option.key, name)
                  }
                >
                  {option.key}
                </DropdownItem>
              );
            })}
        </DropdownMenu>
      </Dropdown>
    );
  }
}

export default React.memo(DropDownComponent, isEqual);
