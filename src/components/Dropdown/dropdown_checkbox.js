// DropDown Menu Component
import React from "react";
import PropTypes from "prop-types";
import isEqual from "react-fast-compare";
import DropdownMenu, {
  DropdownItemCheckbox,
  DropdownItemGroupCheckbox
} from "@atlaskit/dropdown-menu";

// Main Component
const DropdownCheckbox = (props) => {
  const { defaultOpen, isSelected, id, onClick } = props;

  const cls = {};

  return (
    <div className={cls}>
      <DropdownMenu trigger="Filter cities" triggerType="button">
        <DropdownItemGroupCheckbox id="cities">
          <DropdownItemCheckbox id="adelaide">Adelaide</DropdownItemCheckbox>
          <DropdownItemCheckbox id="sydney">Sydney</DropdownItemCheckbox>
        </DropdownItemGroupCheckbox>
      </DropdownMenu>
    </div>
  );
};

DropdownCheckbox.propTypes = {
  defaultSelected: PropTypes.bool,
  id: PropTypes.string,
  isSelected: PropTypes.bool,
  onClick: PropTypes.func
};

DropdownCheckbox.defaultProps = {
  defaultOpen: false,
  isSelected: false,
  id: "",
  onClick: null
};

export default React.memo(DropdownCheckbox, isEqual);
