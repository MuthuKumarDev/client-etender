// DropDown Menu Component
import React from "react";
import PropTypes from "prop-types";
import isEqual from "react-fast-compare";
import DropdownMenu, { DropdownItem } from "@atlaskit/dropdown-menu";
// Main Component
const Dropdownitem = (props) => {
  const { isCompact, isDisabled, isHidden, description, onClick } = props;

  const cls = {};

  return (
    <div className={cls}>
      <DropdownItem isCompact>Edit</DropdownItem>
    </div>
  );
};

Dropdownitem.propTypes = {
  isCompact: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isHidden: PropTypes.bool,
  description: PropTypes.string,
  onClick: PropTypes.func
};

Dropdownitem.defaultProps = {
  isCompact: false,
  isDisabled: false,
  isHidden: false,
  description: "",
  onClick: null
};

export default React.memo(Dropdownitem, isEqual);
