// DropDown Menu Component
import React from "react";
import PropTypes from "prop-types";
import isEqual from "react-fast-compare";
import DropdownMenu from "@atlaskit/dropdown-menu";

// Main Component
const Dropdownmenu = (props) => {
  const {
    defaultOpen,
    position,
    onOpenChange,
    appearance,
    boundariesElement,
    children,
    triggerType,
    trigger
  } = props;

  const cls = {};

  return (
    <div data-testid="img-dsn" className={cls}>
      <DropdownMenu trigger={trigger} triggerType="button" />
    </div>
  );
};

Dropdownmenu.propTypes = {
  defaultOpen: PropTypes.bool,
  position: PropTypes.string,
  trigger: PropTypes.string,
  onOpenChange: PropTypes.func,
  appearance: PropTypes.oneOf(["default", "tall"]),
  boundariesElement: PropTypes.oneOf(["viewport", "window", "scrollParent"]),
  triggerType: PropTypes.oneOf(["default", "button"])
};

Dropdownmenu.defaultProps = {
  defaultOpen: false,
  position: "",
  trigger: "",
  onOpenChange: null,
  appearance: "default",
  boundariesElement: "viewport",
  triggerType: "default"
};

export default React.memo(Dropdownmenu, isEqual);
