// Attach File Component
import React from "react";
import { IconComponent } from "../Icons";
import S3 from "react-aws-s3";

export default class AttachFile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      fileName: []
    };
  }

  handleFileChange = (e) => {
    let { fileName } = this.state;
    console.log(e.target.files[0]);
    for (let i = 0; i < e.target.files.length; i++) {
      console.log("file Name", e.target.files[i].name);
      this.handleUpload(e.target.files[i]);
      fileName.push(e.target.files[i]);
  }
    this.setState({ fileName });
    this.props.onChange(e.target.files);
    console.log("props onChange", this.props.handleFileChange);
  };

  handleUpload = (file) => {
    const config = {
      bucketName: process.env.REACT_APP_BUCKET_NAME,
      dirName: process.env.REACT_APP_DIR_NAME /* optional */,
      region: process.env.REACT_APP_REGION,
      accessKeyId: process.env.REACT_APP_ACCESS_ID,
      secretAccessKey: process.env.REACT_APP_ACCESS_KEY,
    };
    let newFileName = file.name.replace(/\..+$/, "");
    const ReactS3Client = new S3(config);
    ReactS3Client.uploadFile(file, newFileName).then((data) => {
      if (data.status === 204) {
        // console.log("success");
        console.log(data);
      } else {
        // console.log("fail");
      }
    });
  };

  handleDelete = (index) => {
     let { fileName } = this.state;
    fileName.splice(index, 1);
    this.setState({ fileName });
  };

  render() {
    let { fileName } = this.state;
    return (
      <div className="fil-att-main">
        <div class="m-1">
          <form action="" className="frm-fil-att">
            <div className="fil-inp-nme">
              <label class="fil-att-lbl" for="attach">
                <input
                  type="file"
                  id="attach"
                  multiple
                  onChange={this.handleFileChange}
                />
                <IconComponent name="attach" />
                &ensp; Attach
              </label>
            </div>
            <div class="attachfiles">
              {fileName.map((item, index) => {
                return (
                  <div class="atc-hed-lst">
                    <ul>{item.name}</ul>
                    <span onClick={() => this.handleDelete(index)}>
                      <IconComponent name="close" />
                    </span>
                  </div>
                );
              })}
            </div>
          </form>
        </div>
      </div>
    );
  }
}
