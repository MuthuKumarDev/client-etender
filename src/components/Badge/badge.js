import React, { Component } from "react";
import { Badge } from "reactstrap";
import isEqual from "react-fast-compare";

class BadgeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { value, badgeStyles, color } = this.props;
    return (
      <Badge className={badgeStyles && badgeStyles} color={color}>
        {value && value}
      </Badge>
    );
  }
}

export default React.memo(BadgeComponent, isEqual);
