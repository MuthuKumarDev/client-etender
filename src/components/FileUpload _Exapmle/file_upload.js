import React, { Component, Fragment } from "react";

//Old Package
import S3Client, { deleteFile, uploadFile } from "react-s3";
import Dropzone from "react-dropzone-uploader";
import { getDroppedOrSelectedFiles } from "html5-file-selector";
import { Button } from "../Button";

//New Package
// import S3Client, { deleteFile, uploadFile } from "aws-s3";
// import S3Client from "aws-s3";

class File extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: []
    };
  }

  Input = ({ accept, onFiles, files, getFilesFromEvent }) => {
    let text = "";
    if (files.length > 0) {
      text = "Add more files";
    } else {
      text = "Choose files";
    }
  
    return (
      <label
        style={{
          backgroundColor: "#007bff",
          color: "#fff",
          cursor: "pointer",
          padding: 15,
          borderRadius: 3
        }}
      >
        {text}
        <input
          style={{ display: "none" }}
          type="file"
          accept={accept}
          multiple
          onChange={(e) => {
            getFilesFromEvent(e).then((chosenFiles) => {
              onFiles(chosenFiles);
            });
          }}
        />
      </label>
    );
  };

  handleSubmitButton = (files) => {
    console.log(files.map(f => f.meta))
    allFiles.forEach(f => f.remove())
  }

  handleSubmit = (files, allFiles) => {
    console.log(files.map(f => f.meta))
    allFiles.forEach(f => f.remove())
  };

  getFilesFromEvent = (e) => {
    return new Promise((resolve) => {
      getDroppedOrSelectedFiles(e).then((chosenFiles) => {
        resolve(chosenFiles.map((f) => f.fileObject));
      });
    });
  };

  render = () => {
    return (
      <div>
        <Dropzone
          accept="image/*,audio/*,video/*,.pdf"
          getUploadParams={() => ({ url: "https://httpbin.org/post" })}
          onSubmit={this.handleSubmit}
          InputComponent={this.Input}
          getFilesFromEvent={this.getFilesFromEvent}
        />
        <Button onClick={this.handleSubmitButton} size="large" variant="primary" label="Sumbit" />
      </div>
    );
  };
}

export default File;
