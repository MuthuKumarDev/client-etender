import React, { Component } from "react";
import { Form, Label, FormGroup, Input, Spinner } from "reactstrap";
import AttachFileIcon from "@material-ui/icons/AttachFile";

export class InputField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileName: ""
    };
  }

  handleChange = (event) => {
    //console.log("file name.." + JSON.stringify(event.target.files[0].name));
    this.setState({ fileName: event.target.files&& event.target.files[0].name });
    this.props&& this.props.handleInputChange&& this.props.handleInputChange(event.target.value, event.target.name)
  };

  render() {
    console.log("this.props.fileIcon" + this.props.fileIcon);
    const {
      label,
      type,
      name,
      id,
      placeholder,
      classname,
      option,
      size,
      fileIcon,
      value
    } = this.props;
    return (
      <>
        <Input
          type={type}
          name={name}
          id={id}
          placeholder={placeholder}
          onChange={this.handleChange}
          className={classname ? classname : ""}
          icon="primary"
          size={size}
          value={value? value: ''}
        >
          {option && <option>Select</option>}
          {option &&
            option.map((optionData) => {
              return <option>{optionData}</option>;
            })}
        </Input>
        {this.props.fileIcon ? this.props.fileIcon : ""}
        {this.state.fileName ? this.state.fileName : ""}
      </>
    );
  }
}
