// Sidebar Stories
import React from "react";
import { withKnobs } from "@storybook/addon-knobs";

import { Sidebar } from "../Sidebar";

export default {
  title: "Components",
  component: Sidebar,
  decorators: [withKnobs]
};

export const sidebar = () => {
  return <Sidebar />;
};
