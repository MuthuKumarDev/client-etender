// Text Editor Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import  { TextEditer } from "../TextEditer";

export default {
    title: "Components",
    component: TextEditer,
    decorators: [withKnobs]
  };

export const textEditor = () => {
    return (
      <TextEditer />
    );
};
  