// Core - Grid Stories
import React from "react";
import { withKnobs, boolean, select } from "@storybook/addon-knobs";
import { Grid } from "../Grid";
import { Button } from "../Button";

export default {
  title: "Grid",
  component: Grid,
  decorators: [withKnobs]
};

const data = {
  gutterOptions: {
    Normal: "normal",
    medium: "medium",
    Medium: "medium",
    Big: "big"
  },
  mediaOptions: {
    XS: "xs",
    SM: "sm",
    MD: "md",
    LG: "lg",
    XL: "xl"
  }
};

export const simpleGrid = () => {
  const gutter = select("Gutter", data.gutterOptions, "normal");
  const breakValue = select("Breakpoint", data.mediaOptions, "md");
  const isInline = boolean("Is Inline", false);

  return (
    <Grid gutter={gutter} break={breakValue} isInline={isInline}>
      <Grid.Cell span={25} minH="100px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
      <Grid.Cell span={15} minH="100px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
      <Grid.Cell span={40} minH="100px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
      <Grid.Cell span={20} minH="100px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
    </Grid>
  );
};

export const verticalGrid = () => {
  const gutter = select("Gutter", data.gutterOptions, "medium");

  return (
    <Grid gutter={gutter} isVertical>
      <Grid.Cell minH="70px" maxH="70px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
      <Grid.Cell minH="200px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
      <Grid.Cell minH="150px" maxH="150px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
    </Grid>
  );
};

export const fixedColumnGrid = () => {
  const gutter = select("Gutter", data.gutterOptions, "normal");
  const breakValue = select("Breakpoint", data.mediaOptions, "md");

  return (
    <Grid gutter={gutter} break={breakValue}>
      <Grid.Cell span={1} maxW="100px" minW="100px" minH="100px"></Grid.Cell>
      <Grid.Cell span={20} minH="100px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
      <Grid.Cell span={30} minH="100px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
      <Grid.Cell span={20} minH="100px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
      <Grid.Cell span={1} maxW="30px" minW="30px" minH="100px">
        <Button variant="primary" size="medium" label="Click Here" />
      </Grid.Cell>
    </Grid>
  );
};
