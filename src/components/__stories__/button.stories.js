// Button Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import { Button } from "../Button";

export default {
  title: "Components",
  component: Button,
  decorators: [withKnobs]
};

export const button = () => {
  const variantOptions = {
    Normal: "normal",
    Primary: "primary",
    Secondary: "secondary",
    Link: "link",
    Error: "error",
    Login: "login",
    Signup: "signup",
    Discard: "discard"
  };

  const sizeOptions = {
    Small: "small",
    Medium: "medium",
    Big: "big",
    Large: "large"
  };

  const variant = select("Variant", variantOptions, "normal");
  const isDisabled = boolean("Disabled", false);
  const size = select("Size", sizeOptions, "medium");
  const label = text("Label", "Click Here");
  const onClick = action("On Click");

  return (
    <Button
      variant={variant}
      size={size}
      label={label}
      isDisabled={isDisabled}
      onClick={onClick}
    />
  );
};
