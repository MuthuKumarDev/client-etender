// DropdownItem Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from '@storybook/addon-actions';

import { Dropdownitem } from "../Dropdown";

export default {
  title: "Components",
  component: Dropdownitem,
  decorators: [withKnobs]
};

export const DropdownItemComponent = () => {
  const description = text("Position", "Checkbox1");
  const isCompact = boolean("isCompact", false); 
  const isDisabled = boolean("isDisabled", false); 
  const isHidden = boolean("isDisabled", false);
  return (
    <Dropdownitem
    description={description}
    isCompact={isCompact}
    isDisabled={isDisabled}
    isHidden={isHidden}
    onClick={action("clicked")}
    />
  );
};
