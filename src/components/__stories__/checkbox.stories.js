// CheckBox Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import { CheckBox } from "../Checkbox";

export default {
  title: "Components",
  component: CheckBox,
  decorators: [withKnobs]
};

export const checkBoxComponents = () => {
  const variantOptions = {
    normal: "normal",
    Checkstyle: "checkstyle"
  };

  const variant = select("Variant", variantOptions, "normal");
  const title = text("Title", "Title");
  const name = text("Name", "Checkbox1");
  const value = boolean("Value", false);
  const ariaLabel = text("AriaLabel", "Concept");
  const ariaBy = text("AriaBy", "Concept");

  return (
    <CheckBox
      variant={variant}
      name={name}
      title={title}
      value={value}
      ariaLabel={ariaLabel}
      ariaBy={ariaBy}
      onChange={action("clicked")}
    />
  );
};
