// Core - Avatar Stories
import React from "react";
import { withKnobs, text, select } from "@storybook/addon-knobs";

import { Avatar } from "../Avatar";

export default {
  title: "Components",
  component: Avatar,
  decorators: [withKnobs]
};

export const AvatarComponents = () => {
  const badgeOptions = {
    None: "none",
    Online: "online"
  };

  const sizeOptions = {
    Normal: "normal",
    Small: "small",
    Medium: "medium",
    Large: "large"
  };

  const image = text("Image", "");
  const size = select("Size", sizeOptions, "normal");
  const badge = select("Badge", badgeOptions, "none");

  return <Avatar image={image} size={size} badge={badge} />;
};
