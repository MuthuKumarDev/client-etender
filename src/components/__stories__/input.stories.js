// Input Stories
import React from "react";
import {
  withKnobs,
  text,
  select,
  number,
  boolean,
  action
} from "@storybook/addon-knobs";

import { Input } from "../Input";

export default {
  title: "Components",
  component: Input,
  decorators: [withKnobs]
};

export const InputComponents = () => {
  const sizeOptions = {
    Normal: "normal",
    Small: "small",
    Medium: "medium",
    Large: "large"
  };

  const typeOptions = {
    Text: "text",
    Password: "password",
    Number: "number",
    Email: "email"
  };

  const name = text("Name", "Input");
  const size = select("Size", sizeOptions, "normal");
  const type = select("Type", typeOptions, "text");
  const maxLength = number("Max Length", 50);
  const placeHolder = text("Placeholder", "placeholder");
  const isDisabled = boolean("IsDisabled", false);

  return (
    <Input
      name={name}
      size={size}
      type={type}
      maxLength={maxLength}
      placeholder={placeHolder}
      isDisabled={isDisabled}
    />
  );
};
