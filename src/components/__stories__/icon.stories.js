// Icon Stories
import React from "react";
import { withKnobs, text } from "@storybook/addon-knobs";

import { IconComponent } from "../Icons";

export default {
  title: "Components",
  component: IconComponent,
  decorators: [withKnobs]
};

export const icon = () => {
  const name = text("Name", "");

  return <IconComponent name={name} />;
};
