// Header Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import { HeaderComponent } from "../Header";

export default {
  title: "Components",
  component: HeaderComponent,
  decorators: [withKnobs]
};

export const headercomponent = () => {
  return <HeaderComponent />;
};
