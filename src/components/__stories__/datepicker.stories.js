// datePicker Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from '@storybook/addon-actions';

import { DatepickerComponent } from "../DatePicker";

export default {
    title: "Components",
    component: DatepickerComponent,
    decorators: [withKnobs]
  };
  
export const datepickerComponent = () => {
    return (
        <DatepickerComponent />
    );
};
