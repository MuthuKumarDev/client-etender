// DropdownMenu Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import { DropdownCheckbox } from "../Dropdown";

export default {
  title: "Components",
  component: DropdownCheckbox,
  decorators: [withKnobs]
};

export const DropdownCheckboxComponent = () => {
  const id = text("id", "cities");
  const defaultSelected = boolean("defaultSelected", false);
  const isSelected = boolean("isSelected", false);

  return (
    <DropdownCheckbox
      id={id}
      defaultSelected={defaultSelected}
      isSelected={isSelected}
      onClick={action("clicked")}
    />
  );
};
