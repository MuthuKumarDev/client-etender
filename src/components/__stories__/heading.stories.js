// Heading Stories
import React from "react";
import { withKnobs, text, select } from "@storybook/addon-knobs";

import { Heading } from "../Heading";

export default {
  title: "Components",
  component: Heading,
  decorators: [withKnobs]
};

export const headingComponent = () => {
  const variantOptions = {
    Heading1: "h1",
    Heading2: "h2",
    Heading3: "h3",
    Heading4: "h4",
    Heading5: "h5",
    Heading6: "h6",
    Heading7: "h7",
    Heading8: "h8"
  };

  const colorOptions = {
    Normal: "normal",
    Style: "style"
  };

  const title = text("Title", "E-Tender Heading");
  const variant = select("Variant", variantOptions, "h1");
  const color = select("Color", colorOptions, "normal");

  return <Heading variant={variant} title={title} color={color} />;
};
