// DropdownMenu Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from '@storybook/addon-actions';

import { Dropdownmenu } from "../Dropdown";

export default {
  title: "Components",
  component: Dropdownmenu,
  decorators: [withKnobs]
};

export const DropdownMenuComponent = () => {
  const AppearanceOptions = {
    Default: "default",
    Tall: "tall"
  };

  const boundariesElementOptions = {
    Viewport: "viewport",
    window : "window",
    scrollParent: "scrollParent",
  };

  const triggerTypeOptions = {
    Default: "default",
    button: "button",
  }

  const appearance = select("Appearance", AppearanceOptions, "default");
  const position = text("Position", "Checkbox1");
  const trigger = text("Trigger", "Checktitle");
  const defaultOpen = boolean("DefaultOpen", false); 
  const boundariesElement = select("boundariesElement", boundariesElementOptions, "viewport");
  const triggerType = select("triggerType", triggerTypeOptions, "default");

  return (
    <Dropdownmenu
      appearance={appearance}
      position={position}
      trigger={trigger}
      defaultOpen={defaultOpen}
      onOpenChange={action("clicked")}
      boundariesElement={boundariesElement}
      triggerType={triggerType}
    />
  );
};
