// ButtonGroup Stories
import React from "react";
import { withKnobs, text, select, boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import { Buttongroup } from "../ButtonGroup";

export default {
  title: "Components",
  component: Buttongroup,
  decorators: [withKnobs]
};

export const buttonGroup = () => {
  const sizeOptions = {
    Small: "sm",
    Medium: "md",
    Large: "lg"
  };

  const vertical = boolean("Vertical", false);
  const size = select("Size", sizeOptions, "medium");

  return <Buttongroup size={size} vertical={vertical} />;
};
