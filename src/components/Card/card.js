import React, { Component } from "react";
import { Card, CardBody, CardText, CardTitle, CardSubtitle } from "reactstrap";
import isEqual from "react-fast-compare";

class CardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    const { classNames, title, subTitle } = this.props;
    //console.log("classNames.." + JSON.stringify(classNames))
    return (
      <div>
        <Card className={classNames&&classNames.cardStyle ? classNames.cardStyle : ''}>
          <CardBody>
            <CardTitle className={classNames&&classNames.cardTitleStyle ? classNames.cardTitleStyle : ''}> {title}</CardTitle>
            <CardSubtitle className={classNames&&classNames.cardSubTitleStyle ? classNames.cardSubTitleStyle : ''}> {subTitle}</CardSubtitle>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default React.memo(CardComponent, isEqual);
