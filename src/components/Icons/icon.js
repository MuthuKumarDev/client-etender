// Icon Component

import React, { Component } from "react";
import PropTypes from "prop-types";

import { ReactComponent as Search } from "../../svgs/search_icon.svg";
import { ReactComponent as Home } from "../../svgs/home_icon.svg";
import { ReactComponent as Tender } from "../../svgs/tender_icon.svg";
import { ReactComponent as Vendor } from "../../svgs/vendor_icon.svg";
import { ReactComponent as Tasks } from "../../svgs/tasks_icon.svg";
import { ReactComponent as Analytics } from "../../svgs/analytics_icon.svg";
import { ReactComponent as Auctions } from "../../svgs/auction_icon.svg";
import { ReactComponent as ResourceLibrary } from "../../svgs/resource_library_icon.svg";
import { ReactComponent as Settings } from "../../svgs/settings_icon.svg";
import { ReactComponent as Company } from "../../svgs/company_icon.svg";
import { ReactComponent as Calender } from "../../svgs/calender_icon.svg";
import { ReactComponent as Profile } from "../../svgs/profile.svg";
import { ReactComponent as MenuSettings } from "../../svgs/settings.svg";
import { ReactComponent as Logout } from "../../svgs/logout.svg";
import { ReactComponent as Report } from "../../svgs/report.svg";
import { ReactComponent as Tenders } from "../../svgs/tenders_icon.svg";
import { ReactComponent as Shortlisted } from "../../svgs/shortlisted_icon.svg";
import { ReactComponent as NotShortlisted } from "../../svgs/not_shortlisted_icon.svg";
import { ReactComponent as Location } from "../../svgs/location_icon.svg";
import { ReactComponent as ClosingDate } from "../../svgs/closing_date_icon.svg";
import { ReactComponent as TenderValue } from "../../svgs/tender_value_icon.svg";
import { ReactComponent as MoreFilter } from "../../svgs/more_filter_icon.svg";
import { ReactComponent as Notes } from "../../svgs/notes.svg";
import { ReactComponent as Tips } from "../../svgs/tips.svg";
import { ReactComponent as LeftTag } from "../../svgs/lefttag.svg";
import { ReactComponent as Close } from "../../svgs/close_icon.svg";
import { ReactComponent as Edit } from "../../svgs/edit.svg";
import { ReactComponent as Infoyel } from "../../svgs/info_yellow.svg";
import { ReactComponent as ModalSuc } from "../../svgs/modal_suc.svg";
import { ReactComponent as Threedot } from "../../svgs/threedot.svg";
import { ReactComponent as CirclePlus } from "../../svgs/circle_plus.svg";
import { ReactComponent as DeleteIcon } from "../../svgs/delete_icon.svg";
import { ReactComponent as InfoIcon } from "../../svgs/info_icon.svg";
import { ReactComponent as DownArrow } from "../../svgs/downarrow_icon.svg";
import { ReactComponent as UpAndDownArrow } from "../../svgs/up_down_arrow_icon.svg";
import { ReactComponent as DropdownArrow } from "../../svgs/dropdown_arrow.svg";
import { ReactComponent as LeftArrow } from "../../svgs/left_arrow_with_background.svg";
import { ReactComponent as FileUpload } from "../../svgs/file_upload_icon.svg";
import { ReactComponent as EditIcon } from "../../svgs/edit_icon.svg";
import { ReactComponent as Remove } from "../../svgs/delete_icon.svg";
import { ReactComponent as Download } from "../../svgs/download_icon.svg";
import { ReactComponent as Attach } from "../../svgs/attach_icon.svg";
import { ReactComponent as DrawPencil } from "../../svgs/drawpencil.svg"
import { ReactComponent as CirClePlusWithBackground } from "../../svgs/circle_plus_with_background.svg";
import { ReactComponent as EditIconForContact } from "../../svgs/edit_icon_contact.svg";
import { ReactComponent as UserProfile } from "../../svgs/user_icon.svg";
import { ReactComponent as CircleTick } from "../../svgs/circle_tick_icon.svg";
import { ReactComponent as ResourceTag } from "../../svgs/resource_tag.svg"
import { ReactComponent as RightArrow } from "../../svgs/right_arrow_icon.svg";
import { ReactComponent as Ignore } from "../../svgs/ignore_icon.svg";
import { ReactComponent as Reply } from "../../svgs/reply_icon.svg";
 


export default class IconComponent extends Component {
  /* Main Render Function */
  static get propTypes() {
    return {
      fill: PropTypes.string,
      name: PropTypes.string,
      height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      width: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    };
  }

  static get defaultProps() {
    return {
      fill: "",
      name: "",
      height: 0,
      width: 0
    };
  }

  render() {
    const { name, fill, height, width } = this.props;

    if (name === "search") {
      return <Search />;
    }
    if (name === "Home") {
      return <Home />;
    }
    if (name === "Tenders") {
      return <Tender />;
    }
    if (name === "Vendors") {
      return <Vendor />;
    }
    if (name === "Tasks") {
      return <Tasks />;
    }
    if (name === "Analytics") {
      return <Analytics />;
    }
    if (name === "Auctions") {
      return <Auctions />;
    }
    if (name === "Resource Library") {
      return <ResourceLibrary />;
    }
    if (name === "settings") {
      return <Settings />;
    }
    if (name === "company") {
      return <Company />;
    }
    if (name === "calender") {
      return <Calender />;
    }
    if (name === "profile") {
      return <Profile fill={fill} width={width} height={height} />;
    }
    if (name === "menusettings") {
      return <MenuSettings fill={fill} width={width} height={height} />;
    }
    if (name === "logout") {
      return <Logout fill={fill} width={width} height={height} />;
    }
    if (name === "report") {
      return <Report />;
    }
    if (name === "tenders") {
      return <Tenders />;
    }
    if (name === "shortlisted") {
      return <Shortlisted />;
    }
    if (name === "not_shortlisted") {
      return <NotShortlisted />;
    }
    if (name === "location") {
      return <Location />;
    }
    if (name === "closing_date") {
      return <ClosingDate />;
    }
    if (name === "tender_value") {
      return <TenderValue />;
    }
    if (name === "more_filter") {
      return <MoreFilter />;
    }
    if (name === "edit") {
      return <Edit />;
    }
    if (name === "notes") {
      return <Notes />;
    }
    if (name === "tips") {
      return <Tips />;
    }
    if (name === "lefttag") {
      return <LeftTag />;
    }
    if (name === "close") {
      return <Close />;
    }
    if (name === "infoyel") {
      return <Infoyel />;
    }
    if (name === "modalsuc") {
      return <ModalSuc />;
    }
    if (name === "threedot") {
      return <Threedot fill={fill} width={width} height={height} />;
    }
    if (name === "circleplus") {
      return <CirclePlus />;
    }
    if (name === "deleteicon") {
      return <DeleteIcon />;
    }
    if (name === "infoicon") {
      return <InfoIcon />;
    }
    if (name === "downarrow") {
      return <DownArrow />;
    }
    if (name === "upanddownarrow") {
      return <UpAndDownArrow />;
    }
    if (name === "dropdownarrow") {
      return <DropdownArrow />;
    }
    if (name === "left_arrow_with_background") {
      return <LeftArrow />;
    }
    if (name === "file_upload") {
      return <FileUpload />;
    }
    if (name === "edit") {
      return <EditIcon />;
    }
    if (name === "remove") {
      return <Remove />;
    }

    if (name === "download") {
      return <Download />;
    }
    if (name === "attach") {
      return <Attach />;
    }
    if (name === "circleplusbackground") {
      return <CirClePlusWithBackground />;
    }
    if (name === "editiconforcontact") {
      return <EditIconForContact />;
    }
    if (name === "userprofile") {
      return <UserProfile />;
    }
    if(name === "drawpencil") {
      return <DrawPencil />
    }
    if(name === "circletick") {
      return <CircleTick />
    }
    if(name === "resourcetag") {
      return <ResourceTag width={width} height={height} />
    }
    if(name === "rightarrow") {
      return <RightArrow />
    }
    if(name === "ignore") {
      return <Ignore />
    }
    if(name === "reply") {
      return <Reply />
    }
    return null;
  }
}
