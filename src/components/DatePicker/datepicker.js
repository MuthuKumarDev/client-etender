// DatePicker Component
import React, { useState } from "react";
import PropTypes from "prop-types";
import isEqual from "react-fast-compare";
import DatePicker from "react-datepicker";
import { IconComponent } from "../Icons";
import { InputGroup, InputGroupAddon, InputGroupText, Input } from "reactstrap";
import "react-datepicker/dist/react-datepicker.css";

// Main Component
const DatepickerComponent = (props) => {
  let start = "";
  if (props.selected !== null) {
    start = props.selected;
  } else {
    start = new Date();
  }
  let end = "";
  if (props.selected !== null) {
    end = props.selected;
  } else {
    end = new Date();
  }
  const [startDate, setStartDate] = useState(start);
  const [endDate, setEndDate] = useState(end);

  const DatePickerCustomInput = ({ value, onClick, onChange }) => (
    <InputGroup onClick={onClick}>
      <Input
        value={value}
        onChange={(e) => {
          console.log("ondate input change");
        }}
      />
      <InputGroupAddon addonType="append">
        <InputGroupText>
          <IconComponent name="calender" />
        </InputGroupText>
      </InputGroupAddon>
    </InputGroup>
  );

  console.log("selected date ", startDate);

  return (
    <div className="dte-pic-wrp">
      <DatePicker
        selected={startDate}
        startDate={startDate ? startDate : ""}
        endDate={endDate ? endDate : ""}
        placeholderText="dd / mm / yyyy "
        onChange={(date,e) => {
          props.onChange(date,e);
          setStartDate(date);
        }}
        timeInputLabel={props.isFromTenders  || props.isFromVendors ? "" : "Time:"}
        dateFormat={props.isFromVendors ? "dd/MM/yyyy" :props.isFromVendorExperience? "MM/yyyy":  "MM/dd/yyyy h:mm aa"}
        showTimeInput={(props.isFromTenders) || (props.isFromVendors) || (props.isFromVendorExperience) ? false : true}
        customInput={<DatePickerCustomInput />}
        inline={props.isFromTenders ? true : false}
        selectsStart
        maxDate={props.maxDate ? props.maxDate: ''}
        showYearDropdown={props.showYearDropdown ? props.showYearDropdown : false}

      />
      {props.isFromTenders && (
        <DatePicker
          selected={endDate}
          placeholderText="dd / mm / yyyy "
          onChange={(date) => {
            props.onChangeEndDate(date);
            setEndDate(date);
            //setStartDate(date);
          }}
          startDate={startDate ? startDate : ""}
          endDate={endDate ? endDate : ""}
          //timeInputLabel="Time:"
          //dateFormat="MM/dd/yyyy h:mm aa"
          //showTimeInput
          customInput={<DatePickerCustomInput />}
          inline={props.isFromTenders ? true : false}
          minDate={startDate}
          maxDate={props.maxDate ? props.maxDate: ''}

          selectsEnd
          showYearDropdown={props.showYearDropdown ? props.showYearDropdown : false}

        />
      )}
    </div>
  );
};

DatepickerComponent.PropTypes = {};

DatepickerComponent.defaultProps = {};

export default React.memo(DatepickerComponent, isEqual);
