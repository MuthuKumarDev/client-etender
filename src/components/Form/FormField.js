import React, { Component } from "react";
import { Form } from "reactstrap";

export class FormField extends Component {
  render() {
    const { children } = this.props;
    return <Form>{children}</Form>;
  }
}
