import React, { Component } from "react";
import { FormGroup, Label } from "reactstrap";
import { InputField } from "../InputField/Inputfield";

export class RadioButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { radioDetail } = this.props;
    return (
      <FormGroup check className="formGroupStyle" k>
        <Label check>
          <InputField type="radio" name="radio1" /> {radioDetail}
        </Label>
      </FormGroup>
    );
  }
}
