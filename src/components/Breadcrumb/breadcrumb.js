import React from "react";
import { Link } from "react-router-dom";
import withBreadcrumbs from "react-router-breadcrumbs-hoc";

const Breadcrumb = ({ breadcrumbs }) => (
  <div className="brd">
    {breadcrumbs.map(({ breadcrumb, match }, index) => (
      <div className="bc heading" key={match.url}>
        <Link to={match.url || ""}>{breadcrumb}</Link>&ensp;
        {index < breadcrumbs.length - 1 && ">"}
      </div>
    ))}
  </div>
);

export default withBreadcrumbs()(Breadcrumb);
