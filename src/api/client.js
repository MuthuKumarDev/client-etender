// Client
import firebase from 'firebase/app'
import "firebase/firestore";
import "firebase/auth";
import "firebase/storage";

class Client {
  constructor() {
    this._app = null;
  }

  initialize() {
    const firebaseConfig = {
      apiKey: "AIzaSyBGS1c8nUr1d2ecqTRFev4EQux34y7sqW0",
      authDomain: "e-tender-a2de9.firebaseapp.com",
      databaseURL: "https://e-tender-a2de9.firebaseio.com",
      projectId: "e-tender-a2de9",
      storageBucket: "e-tender-a2de9.appspot.com",
    };

    this._app = firebase.initializeApp(firebaseConfig);
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.NONE);
  }

  firestore() {
    return this._app.firestore();
  }

  auth() {
    return this._app.auth();
  }

  storage() {
    return this._app.storage();
  }
}

export default new Client();
