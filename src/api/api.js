import { URL } from "../config";
import cookie from "react-cookies";
//import { history } from '../helpers/index';
const fetch = require("node-fetch");
var GRAPHQL_DATA = "";

// const GRAPHQLDATA = ()=> {
// }
const Query = {
  USER: "user",
  CREATE_TENDER: "createTender",
  All_TENDERS: "tenders",
  TENDER_BY_ID: "tender",
  GET_VENDORS: "vendors",
  UPDATE_TENDERS_SHORTLIST: "updatetendershortlist",
  GET_CATEGORY_AND_SUBCATEGORY: "get_category_and_subcategory",
  CREATE_RESOURCE_CATEGORY: "create_resource_category",
  SEND_MAIl: "sendemail",
  All_TENDERS_FILTERS: "get_tender_filter_list",
  GET_RESOURCES_CATEGORY: "find_resource_and_category",
  GET_SECONDARYCATEGORY: "get_secondarycategory",
  UPDATE_TENDER: "updatetender",
  UPDATE_TENDER_STATUS_TENDOR_BY_ID: "update_tender_status",
  DELETE_RESOURCE_CATEGORY: "delete_resource_category",
  LOGIN: "login",
  GET_QUESTION_AND_ANSWER: "get_question_and_answer",
  DELETE_RESOURCE_CATEGORY: "delete_resource_category",
  CREATE_RESOURCE_SUBCATEGORY: "create_resource_subcategory",
  LOGIN: "login",
  SHOW_INTREST_TENDER: "create_vendor_showintrest",
  GENERATE_TENDER_ID: "genarate_tenderid",
  SEARCH_RESOURCE_CATEGORY: "search_resource_category",
  GET_PRODUCT_AND_SERVICE: "get_product_and_service",
  SEARCH_PRODUCT_AND_SERVICE: "search_product_and_services",
  GET_SERVICE_LOCATION: "get_service_location",
  CREATE_VENDOR: "create_vendor",
  UPDATE_VENDOR: "update_vendor",
  CREATE_VENDOR_CERTIFICATION: "create_vendor_certification",
  CREATE_VENDOR_ADDRESS: "create_vendor_address",
  CREATE_VENDOR_CONTACT: "create_vendor_contact",
  CREATE_VENDOR_PROJECTS: "create_vendor_projects",
  UPDATE_VENDOR_CERTIFICATE: "update_vendor_certificate",
  UPDATE_VENDOR_ADDRESS: "update_vendor_address",
  UPDATE_VENDOR_CONTACTS: "update_vendor_contacts",
  UPDATE_VENDOR_PROJECTS: "update_vendor_projects",
  GET_VENDOR_CERTIFICATION: "get_vendor_certification",
  GET_VENDOR_ADDRESS: "get_vendor_address",
  GET_VENDOR_CONTACTS: "get_vendor_contacts",
  GET_VENDOR_PROJECTS: "get_vendor_projects",

  DELETE_VENDOR_CERTIFICATION: "delete_vendor_certification",
  DELETE_VENDOR_CONTACTS: "delete_vendor_contacts",
  DELETE_VENDOR_ADDRESS: "delete_vendor_address",
  //DELETE_VENDOR_PROJECTS: "delete_vendor_projects"

  CREATE_QUESTION_AND_ANSWER: "create_question_and_answer",
  UPDATE_QUESTION_AND_ANSWER: "update_question_and_answer",
  CREATE_REPLY: "create_reply",
  DELETE_VENDOR_PROJECTS: "delete_vendor_projects",
  GET_TENDER_SHOW_INTEREST: "get_tender_vendor_intrests",
  UPDATE_REPLY: "update_reply",
  DELETE_QUESTION_REPLIES: "delete_question_replies"
};

const fetchData = async (queryName, variables) => {
  const payload = {
    queryName,
    variables
  };
  let token = localStorage.getItem("token") || cookie.load("token");
  let authorization = `Bearer ${token}`;

  if (token) {
    GRAPHQL_DATA = URL.GRAPHQL_DATA;
  } else {
    GRAPHQL_DATA = URL.GRAPHQL_LOGIN;
  }

  const ge_respone = await fetch(GRAPHQL_DATA, {
    method: "POST",
    headers: { "content-type": "application/json", authorization },
    body: JSON.stringify(payload)
  });
  console.log("payload.." + JSON.stringify(payload));
  if (ge_respone.status == 400) {
    // redirect to login screen
    //history.push('/adfs');
    //window.location.reload(false);
  }
  const response = await ge_respone.json();
  return response;
};

const fetchformData = async (formData) => {
  let token = localStorage.getItem("token") || cookie.load("token");
  let authorization = `Bearer ${token}`;

  if (token) {
    GRAPHQL_DATA = URL.GRAPHQL_DATA;
  } else {
    GRAPHQL_DATA = URL.GRAPHQL_LOGIN;
  }
  console.log(formData);
  const ge_respone = await fetch(GRAPHQL_DATA, {
    method: "POST",
    headers: { authorization },
    body: formData //JSON.stringify(formData),
  });
  console.log("payload.." + JSON.stringify(payload));
  if (ge_respone.status == 400) {
    // redirect to login screen
    //history.push('/adfs');
    //window.location.reload(false);
  }
  const response = await ge_respone.json();
  return response;
};
export default { Query, fetchData, fetchformData };
