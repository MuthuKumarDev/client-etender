/* eslint-disable class-methods-use-this */
// Base Api
import client from "./client";

export default class BaseApi {
  constructor() {
    this._client = client;
  }
}
