const SERVER_URL = process.env.REACT_APP_SERVER_HOST || window.location.origin;
const URL = {
  GRAPHQL_DATA: SERVER_URL + "/api/query/data",
  GRAPHQL_LOGIN: SERVER_URL + "/api/query/login"

  //API_DATA: SERVER_HOST + "/api/query/user-data",
};
export { URL, SERVER_URL };
