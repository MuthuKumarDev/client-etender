// Main Layout
import React, { Component } from "react";
import { Switch } from "react-router-dom";
import { Grid } from "../components/Grid";
import { Sidebar } from "../components/Sidebar";
import { SubRoutes } from "./index";
import { HeaderComponent } from "../components/Header";
//Main Component

// export default function MainLayout({ routes, history }) {
//   console.log("History", history)
//   return (
//     <div>
//       <Grid>
//         <Grid.Cell span={20}>
//           <Sidebar />
//         </Grid.Cell>
//         <Grid.Cell span={80}>
//           <HeaderComponent history={history} />
//           <Switch>
//             {routes.map((route, i) => (
//               <SubRoutes key={i} {...route} />
//             ))}
//           </Switch>
//         </Grid.Cell>
//       </Grid>
//     </div>
//   );
// }
export default class MainLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true
    };
  }
  onCollapseClick = (value) => {
    //console.log("oncollapse click..." + value);
    this.setState({ isOpen: value });
  };
  render() {
    console.log("History", this.props.history);
    return (
      <div>
        {this.state.isOpen ? (
          <Grid>
            <Grid.Cell span={20}>
              <Sidebar
                onCollapseClick={this.onCollapseClick}
                isOpen={this.state.isOpen}
              />
            </Grid.Cell>
            <Grid.Cell span={80}>
              <HeaderComponent history={this.props.history} />
              <Switch>
                {this.props.routes.map((route, i) => (
                  <SubRoutes key={i} {...route} />
                ))}
              </Switch>
            </Grid.Cell>
          </Grid>
        ) : (
          <Grid>
            <Grid.Cell span={5}>
              <Sidebar
                onCollapseClick={this.onCollapseClick}
                isOpen={this.state.isOpen}
              />
            </Grid.Cell>
            <Grid.Cell span={94}>
              <HeaderComponent history={this.props.history} />
              <Switch>
                {this.props.routes.map((route, i) => (
                  <SubRoutes key={i} {...route} />
                ))}
              </Switch>
            </Grid.Cell>
          </Grid>
        )}
      </div>
    );
  }
}
