// Sub Routes
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import history from "../history";
import cookie from "react-cookies";

//Main Component

export default function SubRoutes(route) {
  const authed = localStorage.hasOwnProperty("token") || cookie.load('token');
  return (
    <Route
      path={route.path}
      exact={route.exact}
      render={(props) =>{
          if(!route.authentication || authed){
            if(route.redirect){
              return <Redirect to={route.to} />; 
            }
            return <route.component {...props} routes={route.routes} />
          }else{
            return <Redirect to="/login" />
          }
        }
      }
    />
  );
}
