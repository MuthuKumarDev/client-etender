// Style - Helper
const breakPoints = {
	xs: 0,
	sm: 576,
	md: 768,
	lg: 992,
	xl: 1200,
};

export function addToClass(cls, variantMap, value, defaultValue) {
	if (value !== defaultValue && variantMap[value]) {
		cls[variantMap[value]] = true;
	}
}

export function mediaBreak(breakPoint, css) {
	if (breakPoint) {
		return `@media (min-width: ${breakPoints[breakPoint]}px) {
			${css};
		}`;
	}
	return '';
}