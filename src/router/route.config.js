// Route config
import React, { lazy } from "react";
import { MainLayout } from "../layout";
import {
  Dashboard,
  Login,
  SupplierLogin,
  TenderSummary,
  Tenders,
  CreateTender,
  InviteVendorRegister,
  VendorList,
  ResourceLibrary,
  InviteVendor,
  ForgotPassword,
  CreateBid,
  CreateBidUnitRate,
  BidList,
  CompareBids,
  FormExample
} from "../pages";

import { VendorRegisterForm } from '../pages/VendorRegistration';


// const BuyerLogin = import("../pages/Buyer_Login");
// const CreateTender = lazy(() => import("../pages/Create_Tender"));
// const InviteVendor = lazy(() => import("../pages/Invite_vendor"));
// const ForgotPassword = lazy(() => import("../pages/Forgot_Password"));
// const Dashboard = lazy(() => import("../pages/Dashboard"));
// const Tenders = lazy(() => import("../pages/Tenders"));
// const Vendors = lazy(() => import("../pages/Vendors"));
// const Tasks = lazy(() => import("../pages/Tasks"));
// const Analytics = lazy(() => import("../pages/Analytics"));
// const Auctions = lazy(() => import("../pages/Auctions"));
// const ResourceLibrary = lazy(() => import("../pages/ResourceLibrary"));

// const AllTenders = lazy(() => import("../pages/AllTenders"));
// const TenderSummary = lazy(() => import("../pages/Tender_Summary"));
// const VendorRegister = lazy(() => import("../pages/VendorRegistration"));
// const TenderList = lazy(() => import("../pages/Tender_list"));

export default [
  {
    path: "/login",
    component: Login,
    exact: true,
  },
  {
    path: "/supplierLogin",
    component: SupplierLogin,
    exact: true,
  },
  {
    name: "forgotpassword",
    path: "/forgotpassword",
    component: ForgotPassword,
    exact: true,
    authentication: false
  },
  {
    path: "/",
    component: MainLayout,
    exact: false,
    authentication: false,
    routes: [
      {
        exact: true,
        path:"/",
        redirect: true,
        to:'/tenders'
      },
      {
        path: "/Dashboard",
        component: Dashboard,
        exact: true,
        authentication: true
      },
      {
        path: "/tenders/view",
        component: TenderSummary,
        exact: true,
        authentication: true
      },
      {
        name: "Create Tender",
        path: "/tenders/create",
        component: CreateTender,
        exact: true,
        authentication: true
      },
      {
        path: "/tenders/:tenderId",
        component: TenderSummary,
        exact: true,
        authentication: true
      },
      {
        path: "/createbid/:tenderId",
        component: CreateBid,
        exact: true,
        authentication: true
      },
      {
        path: "/createbid_unitrate/:tenderId",
        component: CreateBidUnitRate,
        exact: true,
        authentication: true
      },
      {
        path: "/bidlist/:tenderId",
        component: BidList,
        exact: true,
        authentication: true
      },
      {
        path: "/comparebid",
        component:  CompareBids,
        exact: true,
        authentication: true
      },
      {
        path: "/invitevendor",
        component: InviteVendor,
        exact: true,
        authentication: true
      },
      {
        name: "Edit Tender",
        path: "/tenders/edit/:tenderId",
        component: CreateTender,
        exact: true,
        authentication: true
      },
      {
        name: "Tenders",
        path: "/tenders",
        component: Tenders,
        exact: true,
        authentication: true
      },
      {
        name: "Tenders",
        path: "/tenders/:status",
        component: Tenders,
        authentication: true
      },
      {
        name: "vendorregistor",
        path: "/vendorregistor",
        component: InviteVendorRegister,
        exact: true,
        authentication: true
      },
      {
        name: "vendors",
        path: "/vendors",
        component: VendorList,
        exact: true,
        authentication: false
      },
      {
        name: "resourcelibrary",
        path: "/resourcelibrary",
        component: ResourceLibrary,
        exact: true,
        authentication: false
      },
      {
        name: "vendorregistration",
        path: "/vendorregistration",
        component: VendorRegisterForm,
        exact: true,
        authentication: false
      },
      {
        name: "form",
        path: "/form",
        component: FormExample,
        exact: true,
        authentication: false
      }
    ]
  }
];
// export default [
//   {
//     name: "buyerlogin",
//     path: "/login",
//     component: BuyerLogin,
//     exact: true,
//     authentication: false
//   },
//   {
//     path: "/",
//     component: MainLayout,
//     routes: [
//       {
//         name: "Dashboard",
//         path: "/dashboard",
//         component: Dashboard,
//         exact: true,
//         authentication: true
//       },
//       {
//   name: "vendorregistor",
//   path: "/vendorregistor",
//   component: VendorRegister,
//   exact: true,
//   authentication: false
// },
//       {
//         name: "create-tender",
//         path: "/tenders/create/",
//         component: CreateTender,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Tender",
//         path: "/tenders/:tenderId",
//         component: TenderSummary,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "create-tender",
//         path: "/tenders/edit/:tenderId",
//         component: CreateTender,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "invite-vendor",
//         path: "/invite-vendor",
//         component: InviteVendor,
//         exact: false,
//         authentication: true
//       },
//       {
//         name: "forgotpassword",
//         path: "/forgot-password",
//         component: ForgotPassword,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Tenders",
//         path: "/tenders",
//         component: Tenders,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Tenders",
//         path: "/tenders/:status",
//         component: Tenders,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Vendors",
//         path: "/vendors",
//         component: Vendors,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Vendors",
//         path: "/vendors/:type",
//         component: Vendors,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Tasks",
//         path: "/tasks",
//         component: Tasks,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Analytics",
//         path: "/analytics",
//         component: Analytics,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Auctions",
//         path: "/auctions",
//         component: Auctions,
//         exact: true,
//         authentication: true
//       },
//       {
//         name: "Resource Librarry",
//         path: "/resourcelibrary",
//         component: ResourceLibrary,
//         exact: true,
//         authentication: true
//       }
//     ]
//   },
//   {
//     name: "default",
//     path: ["/", "/index.html"],
//     default: true
//   }
// ];
