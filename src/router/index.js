// Router Index
import React from "react";
import { Switch, BrowserRouter as Router } from "react-router-dom";
import { SubRoutes } from "../layout";
import history from "../history";
import { Provider } from "react-redux";
import store from "../store";

//components
import routes from "./route.config";

const Routes = () => (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        {routes.map((route, i) => (
          <SubRoutes key={i} {...route} />
        ))}
      </Switch>
    </Router>
  </Provider>
);

export default Routes;
